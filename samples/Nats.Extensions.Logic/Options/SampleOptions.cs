﻿namespace Nats.Extensions.Logic.Options
{
    public class SampleOptions
    {
        public string StanChannel { get; set; }

        public string RequestSubject { get; set; }
    }
}
