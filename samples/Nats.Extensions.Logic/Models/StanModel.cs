﻿using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Logic.Common;

namespace Nats.Extensions.Logic.Models
{
    [MessageType(SampleMessageTypes.StanType)]
    public class StanModel
    {
        public string Message { get; set; }
    }
}
