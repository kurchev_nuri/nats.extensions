﻿using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Logic.Common;

namespace Nats.Extensions.Logic.Models
{
    [MessageType(SampleMessageTypes.ResponseType)]
    public class ResponseModel
    {
        public string Message { get; set; }
    }
}
