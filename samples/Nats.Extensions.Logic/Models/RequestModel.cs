﻿using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Logic.Common;

namespace Nats.Extensions.Logic.Models
{
    [MessageType(SampleMessageTypes.RequestType)]
    public class RequestModel
    {
        public string Message { get; set; }
    }
}
