﻿namespace Nats.Extensions.Logic.Common
{
    public static class SampleMessageTypes
    {
        public const int StanType = 100;
        public const int RequestType = 101;
        public const int ResponseType = 102;
    }
}
