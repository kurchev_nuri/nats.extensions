﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Nats.Extensions.Logic.Models;
using Nats.Extensions.Logic.Options;
using Nats.Extensions.Stan;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.AspNet.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly IStanPublisher _publisher;
        private readonly IOptionsSnapshot<SampleOptions> _options;

        public HomeController(IStanPublisher publisher, IOptionsSnapshot<SampleOptions> options)
        {
            _options = options;
            _publisher = publisher;
        }

        [HttpGet]
        public async Task<string> GetAsync([FromQuery] string message, CancellationToken cancellationToken = default)
        {
            await _publisher.PublishAsync(_options.Value.StanChannel, new StanModel { Message = message }, cancellationToken);

            return $"A message: `{message}` has been sent to `STAN`.";
        }
    }
}
