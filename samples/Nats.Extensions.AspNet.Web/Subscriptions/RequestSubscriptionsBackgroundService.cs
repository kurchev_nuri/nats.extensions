﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Nats.Extensions.AspNet.Web.Handlers;
using Nats.Extensions.Broker.Builders.Requests;
using Nats.Extensions.Logic.Models;
using Nats.Extensions.Logic.Options;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.AspNet.Web.Subscriptions
{
    internal sealed class RequestSubscriptionsBackgroundService : BackgroundService
    {
        private readonly IOptions<SampleOptions> _options;
        private readonly IRequestSubscriptionBuilder _subscriptions;

        public RequestSubscriptionsBackgroundService(IOptions<SampleOptions> options, IRequestSubscriptionBuilder subscriptions)
        {
            _options = options;
            _subscriptions = subscriptions;
        }

        protected override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            return _subscriptions.SubscribeToSubject(_options.Value.RequestSubject)
                .WithMultipleHandlersAsync(handlers =>
                {
                    handlers.AddHandler<RequestModel, ResponseModel, SampleRequestHandler>();

                }, cancellationToken);
        }
    }
}
