﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.AspNet.Logger.Extensions;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Handlers.Base;
using Nats.Extensions.Logic.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.AspNet.Web.Handlers
{
    /// <summary>
    /// Пример обработки сообщения из канала.
    /// </summary>
    internal sealed class SampleRequestHandler : AbstractRequestHandler<RequestModel, ResponseModel>
    {
        private readonly ILogger<SampleRequestHandler> _logger;

        public SampleRequestHandler(ILogger<SampleRequestHandler> logger)
        {
            _logger = logger;
        }

        public override Task<ResponseModel> HandleAsync(HandlerContext<RequestModel> context, CancellationToken cancellationToken = default)
        {
            _logger.LogInformation("---------------------------Request-------------------------");

            _logger.LogInformation($" Request {{{Environment.NewLine} \t Saga {{{Environment.NewLine}" +
                                                    $"\t   sagaId: {context.Message.Saga.SagaId} {Environment.NewLine}" +
                                                    $"\t   eventId: {context.Message.Saga.EventId} {Environment.NewLine}" +
                                                    $"\t   event_prev: {context.Message.Saga.PreviousEventId} {Environment.NewLine} \t}}" +
                                                $"{Environment.NewLine} \t Content {{{Environment.NewLine}" +
                                                $" \t   message: {context.Message.Content.Message} {Environment.NewLine} \t}} " +
                                                $"{Environment.NewLine}}}"
            , context.Message);

            _logger.LogInformation("-------------------------------------------------------------");

            return Task.FromResult(new ResponseModel { Message = "Bye World" });
        }
    }
}
