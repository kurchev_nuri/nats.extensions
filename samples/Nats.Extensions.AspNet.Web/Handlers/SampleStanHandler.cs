﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nats.Extensions.AspNet.Logger.Extensions;
using Nats.Extensions.Broker;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Extensions;
using Nats.Extensions.Broker.Handlers.Base;
using Nats.Extensions.Logic.Models;
using Nats.Extensions.Logic.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.AspNet.Web.Handlers
{
    internal sealed class SampleStanHandler : AbstractSubjectHandler<StanModel>
    {
        private readonly IRequestService _requestService;
        private readonly ILogger<SampleStanHandler> _logger;
        private readonly IOptionsSnapshot<SampleOptions> _options;

        public SampleStanHandler(IRequestService requestService, ILogger<SampleStanHandler> logger, IOptionsSnapshot<SampleOptions> options)
        {
            _logger = logger;
            _options = options;
            _requestService = requestService;
        }

        public override async Task HandleAsync(HandlerContext<StanModel> context, CancellationToken cancellationToken = default)
        {
            _logger.LogInformation("---------------------------STAN-----------------------------");

            _logger.LogInformation($" Request {{{Environment.NewLine} \t Saga {{{Environment.NewLine}" +
                                                    $"\t   sagaId: {context.Message.Saga.SagaId} {Environment.NewLine}" +
                                                    $"\t   eventId: {context.Message.Saga.EventId} {Environment.NewLine} \t}}" +
                                                $"{Environment.NewLine} \t Content {{{Environment.NewLine}" +
                                                $" \t   message: {context.Message.Content.Message} {Environment.NewLine} \t}} " +
                                                $"{Environment.NewLine}}}"
            , context.Message);

            _logger.LogInformation("-------------------------------------------------------------");

            var result = await _requestService.BasedOn(context.Message)
                .RequestAsync<RequestModel, ResponseModel>(_options.Value.RequestSubject, new RequestModel { Message = "Hello World" }, cancellationToken: cancellationToken);

            result.EnsureSuccessResponse();

            _logger.LogInformation("---------------------------Response-------------------------");

            _logger.LogInformation($" Response {{{Environment.NewLine} \t Saga {{{Environment.NewLine}" +
                                                    $"\t   sagaId: {result.Saga.SagaId} {Environment.NewLine}" +
                                                    $"\t   eventId: {result.Saga.EventId} {Environment.NewLine}" +
                                                    $"\t   event_prev: {result.Saga.PreviousEventId} {Environment.NewLine} \t}}" +
                                                $"{Environment.NewLine} \t Content {{{Environment.NewLine}" +
                                                $" \t   message: {result.Content.Message} {Environment.NewLine} \t}} " +
                                                $"{Environment.NewLine}}}"
            , result);

            _logger.LogInformation("-------------------------------------------------------------");
        }
    }
}
