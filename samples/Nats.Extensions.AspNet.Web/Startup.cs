using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Nats.Extensions.AspNet.Web.Subscriptions;
using Nats.Extensions.Broker.Extensions;
using Nats.Extensions.Logic.Options;

namespace Nats.Extensions.AspNet.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment hostEnvironment)
        {
            Configuration = configuration;
            HostEnvironment = hostEnvironment;
        }

        protected IConfiguration Configuration { get; }

        protected IWebHostEnvironment HostEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddHandlersWithScopedLifetime(builder => builder.AddSystemTextSerializer());

            services.AddHostedService<SubscriptionsBackgroundService>();
            services.AddHostedService<RequestSubscriptionsBackgroundService>();

            services.Configure<SampleOptions>(Configuration.GetSection(nameof(SampleOptions)));
        }

        public void Configure(IApplicationBuilder app)
        {
            if (HostEnvironment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
