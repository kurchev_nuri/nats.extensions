﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Nats.Extensions.AspNet.Worker.Handlers;
using Nats.Extensions.Logic.Models;
using Nats.Extensions.Logic.Options;
using Nats.Extensions.Stan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.AspNet.Worker.Subscriptions
{
    internal sealed class SubscriptionsBackgroundService : BackgroundService
    {
        private readonly SampleOptions _options;

        private readonly IStanSubscriptionBuilder _subscriptions;

        public SubscriptionsBackgroundService(IOptions<SampleOptions> options, IStanSubscriptionBuilder subscriptions)
        {
            _options = options.Value;
            _subscriptions = subscriptions;
        }

        protected override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            return _subscriptions.SubscribeToSubject(_options.StanChannel)
                .WithMultipleHandlersAsync(handlers =>
                {
                    handlers.AddHandler<StanModel, SampleStanHandler>();

                }, cancellationToken);
        }
    }
}
