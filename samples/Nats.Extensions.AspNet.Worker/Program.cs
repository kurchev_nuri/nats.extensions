using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Nats.Extensions.AspNet.Logger.Extensions;
using NLog;
using NLog.Web;
using System;
using System.Threading.Tasks;

namespace Nats.Extensions.AspNet.Worker
{
    public class Program
    {
        private static async Task Main(string[] args)
        {
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                await CreateHostBuilder(args).Build().RunAsync();
            }
            catch (Exception exception)
            {
                logger.Error(exception, "Service failed to start due to exceptions.");
            }
            finally
            {
                LogManager.Shutdown();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
#if Windows
                .UseWindowsService()
#elif Linux
                .UseSystemd()
#endif
                .ConfigureNatsExtensions(builder =>
                {
                    builder.ShouldUsePipeAuthorization = false;
                    builder.ThrowExceptionIfDeserializationError = true;

                    builder.UseStartup<Startup>();
                })
                .ConfigureLogging((context, builder) =>
                {
                    if (context.HostingEnvironment.IsProduction())
                    {
                        builder.ClearProviders();
                        builder.AddNLogWeb("nlog.config");
                    }
                })
               .UseNatsExtensionsLogger();
    }
}
