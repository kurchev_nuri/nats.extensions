﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.AspNet.Logger.Extensions;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Handlers.Base;
using Nats.Extensions.Logic.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.AspNet.Worker.Handlers
{
    internal sealed class SampleStanHandler : AbstractSubjectHandler<StanModel>
    {
        private readonly ILogger<SampleStanHandler> _logger;

        public SampleStanHandler(ILogger<SampleStanHandler> logger)
        {
            _logger = logger;
        }

        public override Task HandleAsync(HandlerContext<StanModel> context, CancellationToken cancellationToken = default)
        {
            _logger.LogInformation("---------------------------STAN-----------------------------");

            _logger.LogInformation($" Request {{{Environment.NewLine} \t Saga {{{Environment.NewLine}" +
                                                    $"\t   sagaId: {context.Message.Saga.SagaId} {Environment.NewLine}" +
                                                    $"\t   eventId: {context.Message.Saga.EventId} {Environment.NewLine} \t}}" +
                                                $"{Environment.NewLine} \t Content {{{Environment.NewLine}" +
                                                $" \t   message: {context.Message.Content.Message} {Environment.NewLine} \t}} " +
                                                $"{Environment.NewLine}}}"
            , context.Message);

            _logger.LogInformation("-------------------------------------------------------------");

            return Task.CompletedTask;
        }
    }
}
