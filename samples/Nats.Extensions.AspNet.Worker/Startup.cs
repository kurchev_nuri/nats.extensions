﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Nats.Extensions.AspNet.Worker.Subscriptions;
using Nats.Extensions.Broker.Extensions;
using Nats.Extensions.Logic.Options;
using Nats.Extensions.Startup;

namespace Nats.Extensions.AspNet.Worker
{
    public class Startup : IStartup
    {
        public Startup(IConfiguration configuration, IHostEnvironment hostEnvironment)
        {
            Configuration = configuration;
            HostEnvironment = hostEnvironment;
        }

        protected IConfiguration Configuration { get; }

        protected IHostEnvironment HostEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHandlersWithSingletonLifetime();

            services.AddHostedService<SubscriptionsBackgroundService>();

            services.Configure<SampleOptions>(Configuration.GetSection(nameof(SampleOptions)));
        }
    }
}
