﻿using Nats.Extensions.Helper.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper
{
    /// <summary>
    /// Вспомогательная обертка над шиной (NSS).
    /// </summary>
    /// <typeparam name="TOptions"></typeparam>
    public interface IMessageQueueHelper<in TOptions>
         where TOptions : class
    {
        /// <summary>
        /// Статус подключения
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Публикация сообщения в шину (NSS).
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="message">Сообщение, которое будет опубликовано в шину (NSS).</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns>Возвращает Task</returns>
        Task PublishAsync(string subject, byte[] message, CancellationToken cancellationToken = default);

        /// <summary>
        /// Подписка на канал в шине (NSS).
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="handler">Обработчик, который будет обрабатывать сообщение из канала.</param>
        /// <param name="options">Настройки, который могут быть переданны во время подписки.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.</returns>
        IDisposable Subscribe(string subject, Func<Message, CancellationToken, Task> handler, TOptions options = default);

        /// <summary>
        ///  Подписка на канал в шине (NSS) c очередью.
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="queue">Наименование очереди.</param>
        /// <param name="handler">Обработчик, который будет обрабатывать сообщение из канала.</param>
        /// <param name="options">Настройки, который могут быть переданны во время подписки.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.</returns>
        IDisposable Subscribe(string subject, string queue, Func<Message, CancellationToken, Task> handler, TOptions options = default);
    }
}
