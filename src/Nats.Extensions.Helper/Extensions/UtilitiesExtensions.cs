﻿using Nats.Extensions.Common.Models;
using Nats.Extensions.Helper.Options.Connection;
using NATS.Client;
using System;

namespace Nats.Extensions.Helper.Extensions
{
    internal static class UtilitiesExtensions
    {
        private const string _authorizationViolation = "'Authorization Violation'";

        public static bool IsAuthorizationViolationException(this Exception exception)
        {
            return exception is NATSConnectionException && string.Equals(exception.Message, _authorizationViolation, StringComparison.OrdinalIgnoreCase);
        }

        public static void Bind(this ConnectionOptions options, PipeCredentialsModel credentials)
        {
            options.User = credentials.User;
            options.Password = credentials.Password;
        }
    }
}
