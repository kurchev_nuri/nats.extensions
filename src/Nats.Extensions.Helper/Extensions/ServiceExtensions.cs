﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Helper.Builders;
using Nats.Extensions.Helper.Options.Facades;
using Nats.Extensions.Helper.Options.Rules;
using Nats.Extensions.Helper.Resources;
using Scrutor;
using System;
using System.Linq;

namespace Nats.Extensions.Helper.Extensions
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Метод подключения и конфигурации зависимостей для NSS
        /// </summary>
        /// <param name="configure">Метод конфигурации.</param>
        public static IServiceCollection AddNatsExtensionsHelper(this IServiceCollection services, Action<HelperBuilder> configure)
        {
            ThrowIfConfigureIsNull(configure);

            configure(new HelperBuilder(services));

            return ConfigureServices(services);
        }

        #region Private Methods 

        private static IServiceCollection ConfigureServices(IServiceCollection services)
        {
            return services.Scan(u => u.FromAssemblies(typeof(ServiceExtensions).Assembly)
                .AddClasses(v => v.AssignableTo(typeof(IValidator<>)))
                    .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                    .As(type => type.GetInterfaces().Where(type => type.IsGenericType && type.Name.Contains(nameof(IValidator))))
                    .WithSingletonLifetime()
                .AddClasses(type => type.AssignableToAny(
                        typeof(IOptionRule<,>),
                        typeof(IOptionRules<,>))
                    )
                   .AsImplementedInterfaces()
                   .WithSingletonLifetime()
               );
        }

        private static void ThrowIfConfigureIsNull<TOptions>(Action<TOptions> configure)
        {
            if (configure == default)
            {
                throw new ArgumentNullException(nameof(configure), CommonResource.ArgumentNull);
            }
        }

        #endregion
    }
}
