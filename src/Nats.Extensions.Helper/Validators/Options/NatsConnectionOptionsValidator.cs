﻿using FluentValidation;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Resources;

namespace Nats.Extensions.Helper.Validators.Options
{
    internal sealed class NatsConnectionOptionsValidator : AbstractValidator<NatsConnectionOptions>
    {
        public NatsConnectionOptionsValidator()
        {
            Include(new ConnectionOptionsValidator());

            RuleFor(options => options.Name)
                .NotEmpty()
                .WithMessage(string.Format(ValidatorResource.NotEmpty, nameof(NatsConnectionOptions), nameof(NatsConnectionOptions.Name)));
        }
    }
}
