﻿using FluentValidation;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Resources;
using System;

namespace Nats.Extensions.Helper.Validators.Options
{
    internal sealed class ConnectionOptionsValidator : AbstractValidator<ConnectionOptions>
    {
        public ConnectionOptionsValidator()
        {
            RuleFor(connection => connection.ReconnectTimeout)
                .GreaterThan(TimeSpan.Zero)
                .WithMessage(
                    string.Format(ValidatorResource.GreaterThan, nameof(ConnectionOptions), nameof(ConnectionOptions.ReconnectTimeout), TimeSpan.Zero)
                );

            RuleFor(connection => connection.RefreshConnectionTimeout.Value)
                .GreaterThan(TimeSpan.Zero)
                .WithMessage(
                    string.Format(ValidatorResource.GreaterThan, nameof(ConnectionOptions), nameof(ConnectionOptions.RefreshConnectionTimeout), TimeSpan.Zero)
                )
                .When(connection => connection.RefreshConnectionTimeout.HasValue);

            When(connection => connection.Servers == default, () =>
            {
                RuleFor(connection => connection.Uri)
                    .NotNull()
                    .WithMessage(string.Format(ValidatorResource.NotNull, nameof(ConnectionOptions), nameof(ConnectionOptions.Uri)));
            })
           .Otherwise(() =>
           {
               RuleFor(connection => connection.Uri)
                  .Null()
                  .WithMessage(string.Format(ValidatorResource.EmptyNatsUri, nameof(ConnectionOptions), nameof(ConnectionOptions.Uri), nameof(ConnectionOptions.Servers)));

               RuleFor(connection => connection.Servers)
                   .NotNull()
                   .WithMessage(string.Format(ValidatorResource.NotNull, nameof(ConnectionOptions), nameof(ConnectionOptions.Servers)))
                   .Must(servers => servers.Count > 0)
                   .WithMessage(string.Format(ValidatorResource.AnyServer, nameof(ConnectionOptions), nameof(ConnectionOptions.Servers)));
           });
        }
    }
}
