﻿using FluentValidation;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Resources;
using System;

namespace Nats.Extensions.Helper.Validators.Options
{
    internal sealed class StanConnectionOptionsValidator : AbstractValidator<StanConnectionOptions>
    {
        public StanConnectionOptionsValidator()
        {
            Include(new ConnectionOptionsValidator());

            RuleFor(connection => connection.ClientId)
                .NotEmpty()
                .WithMessage(string.Format(ValidatorResource.NotEmpty, nameof(StanConnectionOptions), nameof(StanConnectionOptions.ClientId)));

            RuleFor(connection => connection.ClusterId)
                .NotEmpty()
                .WithMessage(string.Format(ValidatorResource.NotEmpty, nameof(StanConnectionOptions), nameof(StanConnectionOptions.ClusterId)));

            RuleFor(connection => connection.StanRefreshConnectionTimeout.Value)
                .GreaterThan(TimeSpan.Zero)
                .WithMessage(
                    string.Format(ValidatorResource.GreaterThan, nameof(StanConnectionOptions), nameof(StanConnectionOptions.StanRefreshConnectionTimeout), TimeSpan.Zero)
                )
                .When(connection => connection.StanRefreshConnectionTimeout.HasValue);
        }
    }
}
