﻿using FluentValidation;
using Nats.Extensions.Helper.Builders.Connection;
using Nats.Extensions.Helper.Resources;
using System;

namespace Nats.Extensions.Helper.Validators.Builders
{
    internal sealed class ConnectionBuilderValidator : AbstractValidator<ConnectionBuilder>
    {
        public ConnectionBuilderValidator()
        {
            RuleFor(connection => connection.ClientId)
                .NotEmpty()
                .WithMessage(string.Format(ValidatorResource.NotEmpty, nameof(ConnectionBuilder), nameof(ConnectionBuilder.ClientId)));

            RuleFor(connection => connection.RefreshConnectionTimeout.Value)
                .GreaterThan(TimeSpan.Zero)
                .WithMessage(
                    string.Format(ValidatorResource.GreaterThan, nameof(ConnectionBuilder), nameof(ConnectionBuilder.RefreshConnectionTimeout), TimeSpan.Zero)
                )
                .When(connection => connection.RefreshConnectionTimeout.HasValue);

            RuleFor(connection => connection.StanRefreshConnectionTimeout.Value)
                .GreaterThan(TimeSpan.Zero)
                .WithMessage(
                    string.Format(ValidatorResource.GreaterThan, nameof(ConnectionBuilder), nameof(ConnectionBuilder.StanRefreshConnectionTimeout), TimeSpan.Zero)
                )
                .When(connection => connection.StanRefreshConnectionTimeout.HasValue);

            When(connection => connection.Servers == default, () =>
            {
                RuleFor(connection => connection.Host)
                    .NotEmpty()
                    .WithMessage(string.Format(ValidatorResource.NotEmpty, nameof(ConnectionBuilder), nameof(ConnectionBuilder.Host)));

                RuleFor(connection => connection.Port)
                    .GreaterThan(0)
                    .WithMessage(string.Format(ValidatorResource.GreaterThan, nameof(ConnectionBuilder), nameof(ConnectionBuilder.Port), 0));
            })
           .Otherwise(() =>
           {
               RuleFor(connection => connection.Host)
                   .Empty()
                   .WithMessage(string.Format(ValidatorResource.Empty, nameof(ConnectionBuilder), nameof(ConnectionBuilder.Host), nameof(ConnectionBuilder.Servers)));

               RuleFor(connection => connection.Port)
                   .Equal(0)
                   .WithMessage(string.Format(ValidatorResource.Empty, nameof(ConnectionBuilder), nameof(ConnectionBuilder.Port), nameof(ConnectionBuilder.Servers)));

               RuleFor(connection => connection.Servers)
                   .NotNull()
                   .WithMessage(string.Format(ValidatorResource.NotNull, nameof(ConnectionBuilder), nameof(ConnectionBuilder.Servers)))
                   .Must(servers => servers.Count > 0)
                   .WithMessage(string.Format(ValidatorResource.AnyServer, nameof(ConnectionBuilder), nameof(ConnectionBuilder.Servers)));
           });
        }
    }
}
