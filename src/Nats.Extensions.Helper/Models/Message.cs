﻿using System.Buffers;
using System.Collections.Generic;

namespace Nats.Extensions.Helper.Models
{
    public sealed class Message
    {
        public Message(in ReadOnlySequence<byte> data, in Dictionary<string, string> attributes)
        {
            Data = data;
            Attributes = attributes;
        }

        public ReadOnlySequence<byte> Data { get; }

        public Dictionary<string, string> Attributes { get; }
    }
}
