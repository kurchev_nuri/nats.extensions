﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using Nats.Extensions.Common.Extensions;
using Nats.Extensions.Common.Models;
using Nats.Extensions.Common.Options;
using Nats.Extensions.Helper.Clients;
using Nats.Extensions.Helper.Clients.Nats;
using NATS.Client;
using System;
using System.Linq;

namespace Nats.Extensions.Helper.Builders.Connection
{
    public static class ConnectionBuilderExtensions
    {
        /// <summary>
        /// Добавляет сервера кластера к которому клиент может подключится.
        /// </summary>
        /// <param name="servers">Полной адрес с указанием порта. Пример: nats://localhost:4222</param>
        public static void AddServers(this ConnectionBuilder connection, params string[] servers)
        {
            connection.Servers = servers.ToHashSet();
        }

        /// <summary>
        /// Добавляет сервис авторизации по <see href="https://docs.microsoft.com/en-us/dotnet/api/system.io.pipes?view=netcore-3.1">System.IO.Pipes</see>
        /// </summary>
        /// <param name="configure">Метод инициализации.</param>
        public static void AddPipeAuthorization(this ConnectionBuilder connection, Action<PipeManagerOptions> configure)
        {
            connection.Services.AddClientPipeManager<PipeCredentialsModel>(configure);

            connection.Services.TryAddSingleton<IClient<IConnection>, NatsClientWithRemoteAuthorization>();
        }
    }
}
