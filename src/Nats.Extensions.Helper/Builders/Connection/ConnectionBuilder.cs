﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Helper.Common;
using System;
using System.Collections.Generic;

namespace Nats.Extensions.Helper.Builders.Connection
{
    public class ConnectionBuilder
    {
        public ConnectionBuilder(IServiceCollection services) => Services = services;

        internal IServiceCollection Services { get; }

        /// <summary>
        /// Порт на котором запущена служба NSS.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Host на котором запущена служба NSS.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Учетная запись пользователя `NATS`
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Пароль пользователя `NATS`
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Уникальный идентификатор подключения к NSS.
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Добавляет сервера кластера к которому клиент может подключится.
        /// </summary>
        internal HashSet<string> Servers { get; set; }

        /// <summary>
        /// Период переподключения при потери соединения с NSS.
        /// </summary>
        public TimeSpan ReconnectTimeout { get; set; } = CommonConsts.ReconnectTimeout;

        /// <summary>
        /// Период обновления соединений с NSS.
        /// Если значение задано, то обновления будут происходит по периоду.
        /// </summary>
        public TimeSpan? RefreshConnectionTimeout { get; set; }

        /// <summary>
        ///  Через заданный инетрвал будет происходить обновление подключения к `STAN`.
        ///  Если значение задано, то обновления будут происходит по периоду.
        /// </summary>
        public TimeSpan? StanRefreshConnectionTimeout { get; set; }
    }
}
