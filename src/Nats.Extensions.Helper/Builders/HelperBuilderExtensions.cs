﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using Nats.Extensions.Helper.Builders.Connection;
using Nats.Extensions.Helper.Clients;
using Nats.Extensions.Helper.Clients.Nats;
using Nats.Extensions.Helper.Clients.Stan;
using Nats.Extensions.Helper.Connection.States;
using Nats.Extensions.Helper.Connection.Watchers;
using Nats.Extensions.Helper.Factories;
using Nats.Extensions.Helper.Factories.Nats;
using Nats.Extensions.Helper.Implementations.Nats;
using Nats.Extensions.Helper.Implementations.Stan;
using Nats.Extensions.Helper.Options;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Resources;
using Nats.Extensions.Helper.Validators.Builders;
using NATS.Client;
using STAN.Client;
using System;
using System.Linq;

namespace Nats.Extensions.Helper.Builders
{
    public static class HelperBuilderExtensions
    {
        /// <summary>
        /// Конфигурация основных настроек подключения.
        /// </summary>
        /// <param name="configure">Метод конфигурации.</param>
        public static void ConfigureConnection(this HelperBuilder builder, Action<ConnectionBuilder> configure)
        {
            ThrowIfConfigureIsNull(configure);

            configure(builder.ConnectionBuilder);

            Validate(builder.ConnectionBuilder, new ConnectionBuilderValidator());
        }

        /// <summary>
        /// При вызове метода будет использоваться NATS соединение.
        /// </summary>
        /// <returns></returns>
        public static OptionsBuilder<NatsConnectionOptions> UseNats(this HelperBuilder builder)
        {
            ThrowIfConnectionBuilderIsNull(builder.ConnectionBuilder);

            CommonNatsServices(builder.Services);

            return builder.Services.AddOptions<NatsConnectionOptions>()
                .Configure(options =>
                {
                    options.Name = builder.ConnectionBuilder.ClientId;
                    MapConnection(options, builder.ConnectionBuilder);
                })
                .Validate<IValidator<NatsConnectionOptions>>(Validate);
        }

        /// <summary>
        /// При вызове метода будет использоваться NATS соединение.
        /// </summary>
        /// <param name="configure">Метод конфигурации.</param>
        public static OptionsBuilder<NatsConnectionOptions> UseNats(this HelperBuilder builder, Action<NatsConnectionOptions> configure)
        {
            ThrowIfArgumentsAreNull(configure, builder.ConnectionBuilder);

            CommonNatsServices(builder.Services);

            return builder.Services.AddOptions<NatsConnectionOptions>()
                .Configure(options =>
                {
                    options.Name = builder.ConnectionBuilder.ClientId;
                    MapConnection(options, builder.ConnectionBuilder);

                    configure(options);
                })
                .Validate<IValidator<NatsConnectionOptions>>(Validate);
        }

        /// <summary>
        /// При вызове метода будет использоваться NATS соединение.
        /// </summary>
        /// <param name="configure">Метод конфигурации.</param>
        public static OptionsBuilder<NatsConnectionOptions> UseNats(this HelperBuilder builder, Action<NatsConnectionOptions, IConnectionWatcher<NatsConnectionOptions>> configure)
        {
            ThrowIfArgumentsAreNull(configure, builder.ConnectionBuilder);

            CommonNatsServices(builder.Services);

            return builder.Services.AddOptions<NatsConnectionOptions>()
                .Configure<IConnectionWatcher<NatsConnectionOptions>>((options, watcher) =>
                {
                    options.Name = builder.ConnectionBuilder.ClientId;
                    MapConnection(options, builder.ConnectionBuilder);

                    configure(options, watcher);
                })
                .Validate<IValidator<NatsConnectionOptions>>(Validate);
        }

        /// <summary>
        /// При вызове метода будет использоваться STAN поверх существующего NATS соединения.
        /// </summary>
        /// <param name="configure">Метод конфигурации.</param>
        public static OptionsBuilder<StanConnectionOptions> UseStan(this HelperBuilder builder, Action<StanConnectionOptions> configure)
        {
            ThrowIfArgumentsAreNull(configure, builder.ConnectionBuilder);

            CommonNatsServices(builder.Services);
            CommonStanServices(builder.Services);

            builder.TryAddOptions<NatsConnectionOptions>(options =>
            {
                options.Name = builder.ConnectionBuilder.ClientId;
                MapConnection(options, builder.ConnectionBuilder);
            });

            return builder.Services.AddOptions<StanConnectionOptions>()
                .Configure(options =>
                {
                    MapConnection(options, builder.ConnectionBuilder);

                    options.ClientId = builder.ConnectionBuilder.ClientId;
                    options.StanRefreshConnectionTimeout = builder.ConnectionBuilder.StanRefreshConnectionTimeout;

                    configure(options);
                })
                .Validate<IValidator<StanConnectionOptions>>(Validate);
        }

        /// <summary>
        /// При вызове метода будет использоваться STAN поверх существующего NATS соединения.
        /// </summary>
        /// <param name="configure">Метод конфигурации.</param>
        public static OptionsBuilder<StanConnectionOptions> UseStan(this HelperBuilder builder, Action<StanConnectionOptions, IConnectionWatcher<StanConnectionOptions>> configure)
        {
            ThrowIfArgumentsAreNull(configure, builder.ConnectionBuilder);

            CommonStanServices(builder.Services);

            builder.TryAddOptions<NatsConnectionOptions>(options =>
            {
                options.Name = builder.ConnectionBuilder.ClientId;
                MapConnection(options, builder.ConnectionBuilder);
            });

            return builder.Services.AddOptions<StanConnectionOptions>()
                .Configure<IConnectionWatcher<StanConnectionOptions>>((options, watcher) =>
                {
                    MapConnection(options, builder.ConnectionBuilder);

                    options.ClientId = builder.ConnectionBuilder.ClientId;
                    options.StanRefreshConnectionTimeout = builder.ConnectionBuilder.StanRefreshConnectionTimeout;

                    configure(options, watcher);
                })
                .Validate<IValidator<StanConnectionOptions>>(Validate);
        }

        #region Private Methods

        private static void TryAddOptions<TOptions>(this HelperBuilder builder, Action<TOptions> configure)
            where TOptions : class
        {
            if (!builder.Services.Any(descriptor => descriptor.ServiceType == typeof(IConfigureOptions<TOptions>)))
            {
                builder.Services.AddOptions<TOptions>()
                    .Configure(configure)
                    .Validate<IValidator<TOptions>>(Validate);
            }
        }

        private static void CommonNatsServices(IServiceCollection services)
        {
            services.TryAddSingleton<ConnectionFactory>();

            services.TryAddSingleton<IConnectionState<NatsConnectionOptions>, ConnectionState<NatsConnectionOptions>>();
            services.TryAddSingleton<IConnectionWatcher<NatsConnectionOptions>, ConnectionWatcher<NatsConnectionOptions>>();

            services.TryAddSingleton<IClient<IConnection>, NatsClient>();
            services.TryAddSingleton<IConnectionFactory<NatsConnectionOptions, IConnection>, NatsConnectionFactory>();

            services.TryAddSingleton<IMessageQueueHelper<NatsExtensionsOptions>, NatsHelper>();
            services.TryAddSingleton<INatsRequestHelper>(provider => provider.GetService<IMessageQueueHelper<NatsExtensionsOptions>>() as NatsHelper);
        }

        private static void CommonStanServices(IServiceCollection services)
        {
            services.TryAddSingleton<StanConnectionFactory>();

            services.TryAddSingleton<IConnectionState<StanConnectionOptions>, ConnectionState<StanConnectionOptions>>();
            services.TryAddSingleton<IConnectionWatcher<StanConnectionOptions>, ConnectionWatcher<StanConnectionOptions>>();

            services.TryAddSingleton<IClient<IStanConnection>, StanClient>();
            services.TryAddSingleton<IConnectionFactory<StanConnectionOptions, IStanConnection>, Factories.Stan.StanConnectionFactory>();

            services.TryAddSingleton<IMessageQueueHelper<StanExtensionsOptions>, StanHelper>();
        }

        private static void MapConnection(ConnectionOptions options, ConnectionBuilder connection)
        {
            options.User = connection.User;
            options.Password = connection.Password;

            options.ReconnectTimeout = connection.ReconnectTimeout;
            options.RefreshConnectionTimeout = connection.RefreshConnectionTimeout;

            options.Servers = connection.Servers;

            if (connection.Servers == default)
            {
                options.Uri = new Uri($"nats://{connection.Host.Trim()}:{connection.Port}");
            }
        }

        private static bool Validate<TOptions>(TOptions options, IValidator<TOptions> validator)
        {
            validator.ValidateAndThrow(options);
            return true;
        }

        private static void ThrowIfConfigureIsNull<TOptions>(Action<TOptions> configure)
        {
            if (configure == default)
            {
                throw new ArgumentNullException(nameof(configure), CommonResource.ArgumentNull);
            }
        }

        private static void ThrowIfConnectionBuilderIsNull(ConnectionBuilder builder)
        {
            if (builder == default)
            {
                throw new InvalidOperationException(string.Format(BuilderResource.ShouldUse, nameof(UseStan), nameof(UseNats), nameof(ConfigureConnection)));
            }
        }

        private static void ThrowIfArgumentsAreNull<TOptions>(Action<TOptions> configure, ConnectionBuilder builder)
        {
            if (configure == default)
            {
                throw new ArgumentNullException(nameof(configure), CommonResource.ArgumentNull);
            }

            if (builder == default)
            {
                throw new InvalidOperationException(string.Format(BuilderResource.ShouldUse, nameof(UseStan), nameof(UseNats), nameof(ConfigureConnection)));
            }
        }

        private static void ThrowIfArgumentsAreNull<TOptions, TService>(Action<TOptions, TService> configure, ConnectionBuilder builder)
        {
            if (configure == default)
            {
                throw new ArgumentNullException(nameof(configure), CommonResource.ArgumentNull);
            }

            if (builder == default)
            {
                throw new InvalidOperationException(string.Format(BuilderResource.ShouldUse, nameof(UseStan), nameof(UseNats), nameof(ConfigureConnection)));
            }
        }

        #endregion
    }
}
