﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Helper.Builders.Connection;

namespace Nats.Extensions.Helper.Builders
{
    public class HelperBuilder
    {
        public HelperBuilder(IServiceCollection services)
        {
            Services = services;
            ConnectionBuilder = new ConnectionBuilder(services);
        }

        public IServiceCollection Services { get; }

        public ConnectionBuilder ConnectionBuilder { get; }
    }
}
