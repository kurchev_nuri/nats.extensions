﻿using Nats.Extensions.Helper.Common;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Nats.Extensions.Helper.Utilities
{
    internal static class AttributesHelper
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        public static void FlattenAttributes(in string subject, in byte[] attributes)
        {
            int position = 0;

            WriteAttribute(nameof(subject), subject, ref position, attributes);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        public static void FlattenAttributes(in string subject, in ulong sequence, in byte[] attributes)
        {
            int position = 0;

            WriteAttribute(nameof(subject), subject, ref position, attributes);
            WriteAttribute(nameof(sequence), sequence.ToString(), ref position, attributes);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        public static void RestoreAttributes(in ReadOnlySequence<byte> sequence, in Dictionary<string, string> attributes)
        {
            var reader = new SequenceReader<byte>(sequence);

            while (!reader.End)
            {
                if (!reader.TryReadTo(out ReadOnlySequence<byte> attribute, CommonConsts.AttributeDelimeter, advancePastDelimiter: true))
                {
                    attribute = sequence.Slice(reader.Position);
                    reader.Advance(attribute.Length);
                }

                var position = attribute.PositionOf(CommonConsts.AttributeSplitter);
                if (position != null)
                {
                    var key = ConvertToString(attribute.Slice(0, position.Value));
                    var value = ConvertToString(attribute.Slice(attribute.GetPosition(1, position.Value)));

                    attributes.Add(key, value);
                }
            }
        }

        #region Private Methods

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private static string ConvertToString(in ReadOnlySequence<byte> sequence)
        {
            return string.Create((int)sequence.Length, sequence, (elem, sequence) =>
            {
                var position = 0;
                var reader = new SequenceReader<byte>(sequence);

                while (!reader.End)
                {
                    if (reader.TryRead(out var value))
                    {
                        elem[position++] = Convert.ToChar(value);
                    }
                }
            });
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        static void WriteAttribute(string attributeName, string attributeValue, ref int position, byte[] attributes)
        {
            foreach (var item in attributeName)
            {
                attributes[position++] = Convert.ToByte(item);
            }

            attributes[position++] = CommonConsts.AttributeSplitter;

            foreach (var item in attributeValue)
            {
                attributes[position++] = Convert.ToByte(item);
            }

            attributes[position++] = CommonConsts.AttributeDelimeter;
        }

        #endregion
    }
}
