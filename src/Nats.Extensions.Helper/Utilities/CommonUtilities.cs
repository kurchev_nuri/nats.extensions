﻿using Nats.Extensions.Helper.Resources;
using NATS.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nats.Extensions.Helper.Utilities
{
    internal static class CommonUtilities
    {
        public static readonly char[] InvalidSubjectChars = { '\r', '\n', '\t', ' ' };

        public static string NormalizeSubject(string subject)
        {
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentNullException(nameof(subject));
            }

            if (Subscription.IsValidSubject(subject))
            {
                return subject;
            }

            var result = new StringBuilder(subject.Length);
            for (var i = 0; i < subject.Length; i++)
            {
                var j = i + 1;
                var c = subject[i];

                if (!InvalidSubjectChars.Contains(c))
                {
                    if (j >= subject.Length || c != '.' || subject[j] != '.')
                    {
                        result.Append(c);
                    }
                }
            }

            return result.ToString().Trim('.');
        }

        public static void ThrowIfArgumentsAreWrong<TArgument>(string subject, TArgument argument)
        {
            if (!Subscription.IsValidSubject(subject))
            {
                throw new ArgumentException(string.Format(CommonResource.InvalidSubject, string.Join(", ", InvalidSubjectChars)), nameof(subject));
            }

            if (EqualityComparer<TArgument>.Default.Equals(argument, default))
            {
                throw new ArgumentNullException(typeof(TArgument).AssemblyQualifiedName, CommonResource.ArgumentNull);
            }
        }

        public static void ThrowIfArgumentsAreWrong<TArgument>(string subject, string queue, TArgument argument)
        {
            if (!Subscription.IsValidSubject(subject))
            {
                throw new ArgumentException(string.Format(CommonResource.InvalidSubject, string.Join(", ", InvalidSubjectChars)), nameof(subject));
            }

            if (queue != default && !Subscription.IsValidQueueGroupName(queue))
            {
                throw new ArgumentException(string.Format(CommonResource.InvalidQueueGroup, string.Join(", ", InvalidSubjectChars)), nameof(queue));
            }

            if (EqualityComparer<TArgument>.Default.Equals(argument, default))
            {
                throw new ArgumentNullException(typeof(TArgument).AssemblyQualifiedName, CommonResource.ArgumentNull);
            }
        }
    }
}
