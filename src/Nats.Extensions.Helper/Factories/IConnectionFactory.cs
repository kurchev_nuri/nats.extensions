﻿using Nats.Extensions.Helper.Options.Connection;
using NATS.Client;
using System;

namespace Nats.Extensions.Helper.Factories
{
    internal interface IConnectionFactory<in TOptions, out TConnection> : IDisposable
        where TOptions : ConnectionOptions
    {
        void DestroyConnection(TOptions options);

        TConnection CreateConnection(TOptions options, Action<(ConnState? State, Exception Error)> onError);
    }
}
