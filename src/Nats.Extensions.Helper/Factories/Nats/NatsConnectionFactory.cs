﻿using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Options.Facades;
using NATS.Client;
using System;
using System.Collections.Concurrent;
using System.Threading;
using NatsOptions = NATS.Client.Options;

namespace Nats.Extensions.Helper.Factories.Nats
{
    internal sealed class NatsConnectionFactory : IConnectionFactory<NatsConnectionOptions, IConnection>
    {
        private readonly ConnectionFactory _factory;
        private readonly IOptionRules<NatsConnectionOptions, NatsOptions> _rules;

        private readonly ConcurrentDictionary<string, Lazy<IConnection>> _connections;

        public NatsConnectionFactory(ConnectionFactory factory, IOptionRules<NatsConnectionOptions, NatsOptions> rules)
        {
            _rules = rules;
            _factory = factory;

            _connections = new ConcurrentDictionary<string, Lazy<IConnection>>();
        }

        public IConnection CreateConnection(NatsConnectionOptions options, Action<(ConnState?, Exception)> onError)
        {
            var adapter = _connections.GetOrAdd(options.Name, connectionId =>
            {
                var natsOptions = _rules.ApplyRules(options);

                natsOptions.ClosedEventHandler = (sender, args) => onError(((sender as IConnection)?.State, args.Error));

                return new Lazy<IConnection>(() => _factory.CreateConnection(natsOptions), LazyThreadSafetyMode.ExecutionAndPublication);
            });

            return adapter.Value;
        }

        public void DestroyConnection(NatsConnectionOptions options)
        {
            if (_connections.TryRemove(options.Name, out var connection) && connection.IsValueCreated)
            {
                connection.Value.Dispose();
            }
        }

        public void Dispose()
        {
            foreach (var (_, connection) in _connections)
            {
                if (connection.IsValueCreated)
                {
                    connection.Value.Dispose();
                }
            }
        }
    }
}
