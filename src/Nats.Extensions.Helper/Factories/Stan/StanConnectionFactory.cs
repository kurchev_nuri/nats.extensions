﻿using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Options.Facades;
using NATS.Client;
using STAN.Client;
using System;
using System.Collections.Concurrent;
using System.Threading;
using ConnectionFactory = STAN.Client.StanConnectionFactory;

namespace Nats.Extensions.Helper.Factories.Stan
{
    internal sealed class StanConnectionFactory : IConnectionFactory<StanConnectionOptions, IStanConnection>
    {
        private readonly ConnectionFactory _factory;
        private readonly IOptionRules<StanConnectionOptions, StanOptions> _rules;

        private readonly ConcurrentDictionary<string, Lazy<IStanConnection>> _connections;

        public StanConnectionFactory(ConnectionFactory factory, IOptionRules<StanConnectionOptions, StanOptions> rules)
        {
            _rules = rules;
            _factory = factory;

            _connections = new ConcurrentDictionary<string, Lazy<IStanConnection>>();
        }

        public IStanConnection CreateConnection(StanConnectionOptions options, Action<(ConnState?, Exception)> onError)
        {
            var adapter = _connections.GetOrAdd(options.ClientId, connectionId =>
            {
                var stanOptions = _rules.ApplyRules(options);

                stanOptions.NatsConn = options.NatsConnection;
                stanOptions.ConnectionLostEventHandler = (sender, args) => onError?.Invoke(
                    (args.Connection?.NATSConnection?.State, args.ConnectionException)
                );

                return new Lazy<IStanConnection>(
                    () => _factory.CreateConnection(options.ClusterId, options.ClientId, stanOptions), LazyThreadSafetyMode.ExecutionAndPublication
                );
            });

            return adapter.Value;
        }

        public void DestroyConnection(StanConnectionOptions options)
        {
            if (_connections.TryRemove(options.ClientId, out var connection) && connection.IsValueCreated)
            {
                connection.Value.Dispose();
            }
        }

        public void Dispose()
        {
            foreach (var (_, connection) in _connections)
            {
                if (connection.IsValueCreated)
                {
                    connection.Value.Dispose();
                }
            }
        }
    }
}
