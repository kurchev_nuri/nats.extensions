﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nats.Extensions.Helper.Connection.States;
using Nats.Extensions.Helper.Resources;
using Nats.Extensions.Helper.Utilities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reactive.Disposables;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper.Connection.Watchers
{
    internal sealed class ConnectionWatcher<TOptions> : IConnectionWatcher<TOptions>
        where TOptions : class
    {
        private readonly IServiceProvider _provider;
        private readonly IConnectionState<TOptions> _connection;
        private readonly ILogger<ConnectionWatcher<TOptions>> _logger;

        public ConnectionWatcher(IServiceProvider provider, IConnectionState<TOptions> connection, ILogger<ConnectionWatcher<TOptions>> logger)
        {
            _logger = logger;
            _provider = provider;
            _connection = connection;
        }

        public IDisposable ExecuteWhenConnectionIsEstablished(Expression<Action> methodCall)
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionEstablishedError, $"{ExpressionInfoHelper.GetMemberName(methodCall)}");

            return _connection.State.Subscribe(connectionIsValid =>
            {
                if (connectionIsValid)
                {
                    try
                    {
                        method();
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                }
            });
        }

        public IDisposable ExecuteWhenFirstConnectionIsEstablished(Expression<Action> methodCall)
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var subscription = new SerialDisposable();

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionEstablishedError, $"{ExpressionInfoHelper.GetMemberName(methodCall)}");

            subscription.Disposable = _connection.State.Subscribe(connectionIsValid =>
            {
                if (connectionIsValid)
                {
                    try
                    {
                        method();
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                    finally
                    {
                        subscription.Dispose();
                    }
                }
            });

            return subscription;
        }

        public IDisposable ExecuteWhenConnectionIsEstablished<TService>(Expression<Action<TService>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionEstablishedError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            return _connection.State.Subscribe(connectionIsValid =>
            {
                if (connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        method(scope.ServiceProvider.GetRequiredService<TService>());
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                }
            });
        }

        public IDisposable ExecuteWhenFirstConnectionIsEstablished<TService>(Expression<Action<TService>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var subscription = new SerialDisposable();

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionEstablishedError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            subscription.Disposable = _connection.State.Subscribe(connectionIsValid =>
            {
                if (connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        method(scope.ServiceProvider.GetRequiredService<TService>());
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                    finally
                    {
                        subscription.Dispose();
                    }
                }
            });

            return subscription;
        }

        public IDisposable ExecuteWhenConnectionIsEstablished(Expression<Func<CancellationToken, Task>> methodCall)
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var method = methodCall.Compile();
            var cancellationTokenSource = new CancellationTokenSource();
            var errorMessage = string.Format(WatcherResource.ConnectionEstablishedError, $"{ExpressionInfoHelper.GetMemberName(methodCall)}");

            var subscription = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (connectionIsValid)
                {
                    try
                    {
                        await method(cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenFirstConnectionIsEstablished(Expression<Func<CancellationToken, Task>> methodCall)
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionEstablishedError, $"{ExpressionInfoHelper.GetMemberName(methodCall)}");

            subscription.Disposable = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (connectionIsValid)
                {
                    try
                    {
                        await method(cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                    finally
                    {
                        subscription.Dispose();
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenConnectionIsEstablished<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var method = methodCall.Compile();
            var cancellationTokenSource = new CancellationTokenSource();
            var errorMessage = string.Format(WatcherResource.ConnectionEstablishedError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            var subscription = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        await method(scope.ServiceProvider.GetRequiredService<TService>(), cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenFirstConnectionIsEstablished<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionEstablishedError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            subscription.Disposable = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        await method(scope.ServiceProvider.GetRequiredService<TService>(), cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                    finally
                    {
                        subscription.Dispose();
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenConnectionIsEstablished<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var method = methodCall.Compile();
            var cancellationTokenSource = new CancellationTokenSource();
            var errorMessage = string.Format(WatcherResource.ConnectionEstablishedError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            var subscription = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        await method(scope.ServiceProvider.GetRequiredService<TService>(), cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenFirstConnectionIsEstablished<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionEstablishedError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            subscription.Disposable = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        await method(scope.ServiceProvider.GetRequiredService<TService>(), cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                    finally
                    {
                        subscription.Dispose();
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenConnectionIsLost(Expression<Action> methodCall)
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionLostError, $"{ExpressionInfoHelper.GetMemberName(methodCall)}");

            return _connection.State.Subscribe(connectionIsValid =>
            {
                if (!connectionIsValid)
                {
                    try
                    {
                        method();
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                }
            });
        }

        public IDisposable ExecuteWhenFirstConnectionIsLost(Expression<Action> methodCall)
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var subscription = new SerialDisposable();

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionLostError, $"{ExpressionInfoHelper.GetMemberName(methodCall)}");

            subscription.Disposable = _connection.State.Subscribe(connectionIsValid =>
            {
                if (!connectionIsValid)
                {
                    try
                    {
                        method();
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                    finally
                    {
                        subscription.Dispose();
                    }
                }
            });

            return subscription;
        }

        public IDisposable ExecuteWhenConnectionIsLost<TService>(Expression<Action<TService>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionLostError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            return _connection.State.Subscribe(connectionIsValid =>
            {
                if (!connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        method(scope.ServiceProvider.GetRequiredService<TService>());
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                }
            });
        }

        public IDisposable ExecuteWhenFirstConnectionIsLost<TService>(Expression<Action<TService>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var subscription = new SerialDisposable();

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionLostError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            subscription.Disposable = _connection.State.Subscribe(connectionIsValid =>
            {
                if (!connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        method(scope.ServiceProvider.GetRequiredService<TService>());
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                    finally
                    {
                        subscription.Dispose();
                    }
                }
            });

            return subscription;
        }

        public IDisposable ExecuteWhenConnectionIsLost(Expression<Func<CancellationToken, Task>> methodCall)
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var method = methodCall.Compile();
            var cancellationTokenSource = new CancellationTokenSource();
            var errorMessage = string.Format(WatcherResource.ConnectionLostError, $"{ExpressionInfoHelper.GetMemberName(methodCall)}");

            var subscription = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (!connectionIsValid)
                {
                    try
                    {
                        await method(cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenFirstConnectionIsLost(Expression<Func<CancellationToken, Task>> methodCall)
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionLostError, $"{ExpressionInfoHelper.GetMemberName(methodCall)}");

            subscription.Disposable = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (!connectionIsValid)
                {
                    try
                    {
                        await method(cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                    finally
                    {
                        subscription.Dispose();
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenConnectionIsLost<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var method = methodCall.Compile();
            var cancellationTokenSource = new CancellationTokenSource();
            var errorMessage = string.Format(WatcherResource.ConnectionLostError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            var subscription = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (!connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        await method(scope.ServiceProvider.GetRequiredService<TService>(), cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenFirstConnectionIsLost<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionLostError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            subscription.Disposable = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (!connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        await method(scope.ServiceProvider.GetRequiredService<TService>(), cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                    finally
                    {
                        subscription.Dispose();
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenConnectionIsLost<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var method = methodCall.Compile();
            var cancellationTokenSource = new CancellationTokenSource();
            var errorMessage = string.Format(WatcherResource.ConnectionLostError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            var subscription = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (!connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        await method(scope.ServiceProvider.GetRequiredService<TService>(), cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        public IDisposable ExecuteWhenFirstConnectionIsLost<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall)
            where TService : class
        {
            ThrowIfArgumentIsNullOrEmpty(methodCall);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();

            var method = methodCall.Compile();
            var errorMessage = string.Format(WatcherResource.ConnectionLostError, $"{typeof(TService).FullName}.{ExpressionInfoHelper.GetMemberName(methodCall)}");

            subscription.Disposable = _connection.State.Subscribe(async connectionIsValid =>
            {
                if (!connectionIsValid)
                {
                    try
                    {
                        using var scope = _provider.CreateScope();

                        await method(scope.ServiceProvider.GetRequiredService<TService>(), cancellationTokenSource.Token);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, errorMessage);
                    }
                    finally
                    {
                        subscription.Dispose();
                    }
                }
            });

            return new CompositeDisposable { subscription, Disposable.Create(cancellationTokenSource, cancellationToken =>
            {
                cancellationToken.Cancel();
                cancellationToken.Dispose();
            })};
        }

        #region Private Methods

        private static void ThrowIfArgumentIsNullOrEmpty<TArgument>(TArgument methodCall)
        {
            if (EqualityComparer<TArgument>.Default.Equals(methodCall, default))
            {
                throw new ArgumentNullException(nameof(methodCall), CommonResource.ArgumentNull);
            }
        }

        #endregion
    }
}
