﻿using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper.Connection.Watchers
{
    /// <summary>
    /// Данный интерфейс позволяет следить за соединением с NSS.
    /// </summary>
    /// <typeparam name="TOptions">Тип соединения (NATS/STAN)</typeparam>
    public interface IConnectionWatcher<out TOptions>
        where TOptions : class
    {
        /// <summary>
        /// Подписка на событие о потери соединения с (NATS/STAN)
        /// </summary>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenConnectionIsLost(Expression<Action> methodCall);

        /// <summary>
        /// Подписка на первое событие о потери соединения с (NATS/STAN).
        /// </summary>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.</returns>
        IDisposable ExecuteWhenFirstConnectionIsLost(Expression<Action> methodCall);

        /// <summary>
        /// Подписка на событие о потери соединения с (NATS/STAN)
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenConnectionIsLost<TService>(Expression<Action<TService>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на первое событие о потери соединения с (NATS/STAN).
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.</returns>
        IDisposable ExecuteWhenFirstConnectionIsLost<TService>(Expression<Action<TService>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на событие о потери соединения с (NATS/STAN)
        /// </summary>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenConnectionIsLost(Expression<Func<CancellationToken, Task>> methodCall);

        /// <summary>
        /// Подписка на первое событие о потери соединения с (NATS/STAN).
        /// </summary>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.</returns>
        IDisposable ExecuteWhenFirstConnectionIsLost(Expression<Func<CancellationToken, Task>> methodCall);

        /// <summary>
        /// Подписка на событие о потери соединения с (NATS/STAN)
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenConnectionIsLost<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на первое событие о потери соединения с (NATS/STAN).
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.</returns>
        IDisposable ExecuteWhenFirstConnectionIsLost<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на событие о потери соединения с (NATS/STAN)
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <typeparam name="TResult">Тип, который возвращает метод.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenConnectionIsLost<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на первое событие о потери соединения с (NATS/STAN).
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <typeparam name="TResult">Тип, который возвращает метод.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.</returns>
        IDisposable ExecuteWhenFirstConnectionIsLost<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на событие об успешном соединении с (NATS/STAN)
        /// </summary>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenConnectionIsEstablished(Expression<Action> methodCall);

        /// <summary>
        /// Подписка на первое событие об успешном соединении с (NATS/STAN).
        /// </summary>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenFirstConnectionIsEstablished(Expression<Action> methodCall);

        /// <summary>
        /// Подписка на событие об успешном соединении с (NATS/STAN)
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenConnectionIsEstablished<TService>(Expression<Action<TService>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на первое событие об успешном соединении с (NATS/STAN).
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.</returns>
        IDisposable ExecuteWhenFirstConnectionIsEstablished<TService>(Expression<Action<TService>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на событие об успешном соединении с (NATS/STAN)
        /// </summary>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenConnectionIsEstablished(Expression<Func<CancellationToken, Task>> methodCall);

        /// <summary>
        /// Подписка на первое событие об успешном соединении с (NATS/STAN).
        /// </summary>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenFirstConnectionIsEstablished(Expression<Func<CancellationToken, Task>> methodCall);

        /// <summary>
        /// Подписка на событие об успешном соединении с (NATS/STAN)
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenConnectionIsEstablished<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на первое событие об успешном соединении с (NATS/STAN).
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.</returns>
        IDisposable ExecuteWhenFirstConnectionIsEstablished<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на событие об успешном соединении с (NATS/STAN)
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <typeparam name="TResult">Тип, который возвращает метод.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.</returns>
        IDisposable ExecuteWhenConnectionIsEstablished<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall) where TService : class;

        /// <summary>
        /// Подписка на первое событие об успешном соединении с (NATS/STAN).
        /// </summary>
        /// <typeparam name="TService">Тип объекта чей метод будет вызван при возникновении события.</typeparam>
        /// <typeparam name="TResult">Тип, который возвращает метод.</typeparam>
        /// <param name="methodCall">Метод, который будет вызван.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.</returns>
        IDisposable ExecuteWhenFirstConnectionIsEstablished<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall) where TService : class;
    }
}
