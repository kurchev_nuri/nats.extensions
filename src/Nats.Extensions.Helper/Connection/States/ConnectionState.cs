﻿using Nats.Extensions.Helper.Common;
using System;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper.Connection.States
{
    internal sealed class ConnectionState<TOptions> : IConnectionState<TOptions>
        where TOptions : class
    {
        private readonly Channel<bool> _channel;

        private const bool _connectionLost = false;
        private const bool _connectionEstablished = true;

        public ConnectionState()
        {
            _channel = Channel.CreateBounded<bool>(new BoundedChannelOptions(CommonConsts.MaxReplayCount)
            {
                SingleReader = true,
                FullMode = BoundedChannelFullMode.DropOldest
            });

            State = CreateState(_channel.Reader);
        }

        public IObservable<bool> State { get; }

        public void SetConnectionIsValid()
        {
            using var cancellationToken = new CancellationTokenSource(CommonConsts.ChannelWriteTimeout);

            while (!cancellationToken.IsCancellationRequested)
            {
                if (_channel.Writer.TryWrite(_connectionEstablished))
                {
                    cancellationToken.Cancel();
                }
            }
        }

        public void SetConnectionIsInvalid()
        {
            using var cancellationToken = new CancellationTokenSource(CommonConsts.ChannelWriteTimeout);

            while (!cancellationToken.IsCancellationRequested)
            {
                if (_channel.Writer.TryWrite(_connectionLost))
                {
                    cancellationToken.Cancel();
                }
            }
        }

        public async ValueTask SetConnectionIsValidAsync(CancellationToken cancellationToken = default)
        {
            using var cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);

            cancellationTokenSource.CancelAfter(CommonConsts.ChannelWriteTimeout);

            await _channel.Writer.WriteAsync(_connectionEstablished, cancellationTokenSource.Token);
        }

        public async ValueTask SetConnectionIsInvalidAsync(CancellationToken cancellationToken = default)
        {
            using var cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);

            cancellationTokenSource.CancelAfter(CommonConsts.ChannelWriteTimeout);

            await _channel.Writer.WriteAsync(_connectionLost, cancellationTokenSource.Token);
        }

        #region Private Methods

        private static IObservable<bool> CreateState(ChannelReader<bool> reader)
        {
            var observable = Observable.Create<bool>((observer, cancellationToken) =>
            {
                return Task.Run(async () =>
                {
                    while (await reader.WaitToReadAsync(cancellationToken))
                    {
                        while (reader.TryRead(out var connectionState))
                        {
                            if (connectionState == _connectionEstablished)
                            {
                                await Task.Delay(CommonConsts.ChannelReadTimeout, cancellationToken);
                            }

                            observer.OnNext(connectionState);
                        }
                    }

                }, cancellationToken);
            });

            return observable.Replay(CommonConsts.MaxReplayCount)
                .AutoConnect(CommonConsts.StartConnectingImmediately);
        }

        #endregion
    }
}
