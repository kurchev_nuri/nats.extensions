﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper.Connection.States
{
    internal interface IConnectionState<TOptions>
        where TOptions : class
    {
        IObservable<bool> State { get; }

        void SetConnectionIsValid();

        void SetConnectionIsInvalid();

        ValueTask SetConnectionIsValidAsync(CancellationToken cancellationToken = default);

        ValueTask SetConnectionIsInvalidAsync(CancellationToken cancellationToken = default);
    }
}
