﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.Helper.Models;
using Nats.Extensions.Helper.Resources;
using Nats.Extensions.Helper.Utilities;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO.Pipelines;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper.Buffers
{
    internal sealed class MessageBuffer
    {
        private const int _messageDelimetersAmount = 2;

        private const byte _messageDelimiter = (byte)'\u0003';
        private const byte _attributesDelimeter = (byte)'\u0002';

        private readonly byte[] _messageDelimiterSequence = new byte[] { _messageDelimiter };
        private readonly byte[] _attributesDelimiterSequence = new byte[] { _attributesDelimeter };

        private readonly Pipe _pipe;
        private readonly ILogger _logger;
        private readonly string _subject;
        private readonly int _attributesCapacity;

        public MessageBuffer(int attributesCapacity, string subject, ILogger logger)
        {
            _logger = logger;
            _subject = subject;
            _attributesCapacity = attributesCapacity;

            _pipe = new Pipe(new PipeOptions(
                resumeWriterThreshold: 65_536,
                useSynchronizationContext: false,
                writerScheduler: PipeScheduler.ThreadPool,
                readerScheduler: PipeScheduler.ThreadPool
            ));
        }

        public async ValueTask WriteAsync((byte[] Data, Memory<byte> Attributes) message, CancellationToken cancellationToken = default)
        {
            try
            {
                var length = message.Data.Length + message.Attributes.Length + _messageDelimetersAmount;
                var destination = _pipe.Writer.GetMemory(length);

                message.Attributes.CopyTo(destination);
                message.Data.CopyTo(destination[(message.Attributes.Length + 1)..]);

                _messageDelimiterSequence.CopyTo(destination.Slice(length - 1, 1));
                _attributesDelimiterSequence.CopyTo(destination.Slice(message.Attributes.Length, 1));

                _pipe.Writer.Advance(length);

                await _pipe.Writer.FlushAsync(cancellationToken);
            }
            catch (OperationCanceledException)
            {
                _logger.LogTrace(string.Format(BufferResource.WriteToBufferError, _subject));
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, string.Format(BufferResource.CannotWriteToBufferError, _subject));
            }
        }

        public async Task ReadAsync(Func<Message, CancellationToken, Task> handle, CancellationToken cancellationToken = default)
        {
            try
            {
                var messages = new List<ReadOnlySequence<byte>>();

                while (!cancellationToken.IsCancellationRequested)
                {
                    var result = await _pipe.Reader.ReadAsync(cancellationToken);

                    var (consumed, examined) = ParseMessages(messages, result.Buffer);

                    messages.ForEach(message =>
                    {
                        var position = message.PositionOf(_attributesDelimeter);

                        var attributes = new Dictionary<string, string>(_attributesCapacity, StringComparer.OrdinalIgnoreCase);

                        AttributesHelper.RestoreAttributes(message.Slice(0, position.Value), attributes);

                        var sequence = message.Slice(message.GetPosition(1, position.Value));

                        var data = ArrayPool<byte>.Shared.Rent((int)sequence.Length);

                        sequence.CopyTo(data);

                        var model = new Message(new ReadOnlySequence<byte>(data, 0, (int)sequence.Length), attributes);

                        Task.Run(() => handle(model, cancellationToken), cancellationToken)
                            .ContinueWith(completed => completed.Exception?.Handle(exception => LogError(model, exception)), cancellationToken)
                            .ContinueWith(completed => ArrayPool<byte>.Shared.Return(data), cancellationToken);
                    });

                    _pipe.Reader.AdvanceTo(consumed, examined);
                }
            }
            catch (OperationCanceledException)
            {
                _logger.LogTrace(string.Format(BufferResource.ReadFromBufferError, _subject));
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, string.Format(BufferResource.CannotReadFromBufferError, _subject));
            }
        }

        #region Private Methods

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private bool LogError(Message message, Exception exception)
        {
            if (exception is ObjectDisposedException disposed && disposed.ObjectName == typeof(IServiceProvider).Name)
            {
                _logger.LogTrace(
                    string.Format(BufferResource.ObjectDisposedError, message.Attributes["subject"], Environment.NewLine, Encoding.UTF8.GetString(message.Data.FirstSpan))
                );

                return true;
            }

            if (exception is OperationCanceledException)
            {
                _logger.LogTrace(
                    string.Format(BufferResource.OperationCanceledError, message.Attributes["subject"], Environment.NewLine, Encoding.UTF8.GetString(message.Data.FirstSpan))
                );

                return true;
            }

            _logger.LogError(exception,
                string.Format(BufferResource.HandlingError, message.Attributes["subject"], Environment.NewLine, Encoding.UTF8.GetString(message.Data.FirstSpan))
            );

            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private static (SequencePosition Consumed, SequencePosition Examined) ParseMessages(in List<ReadOnlySequence<byte>> messages, in ReadOnlySequence<byte> readBuffer)
        {
            messages.Clear();
            var buffer = readBuffer;

            while (true)
            {
                var position = buffer.PositionOf(_messageDelimiter);
                if (position == null)
                {
                    break;
                }

                messages.Add(buffer.Slice(0, position.Value));
                buffer = buffer.Slice(buffer.GetPosition(1, position.Value));
            }

            return (buffer.Start, buffer.End);
        }

        #endregion
    }
}
