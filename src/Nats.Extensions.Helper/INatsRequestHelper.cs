﻿using Nats.Extensions.Helper.Models;
using Nats.Extensions.Helper.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper
{
    public interface INatsRequestHelper
    {
        /// <summary>
        /// Подписка на канал в шине (NSS) с возможностью ответа на запрос.
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="handler">Обработчик, который будет обрабатывать сообщение из канала.</param>
        /// <param name="failure">Метод обратного вызова для обработки ошибок.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.</returns>
        IDisposable Subscribe(string subject, Func<Message, CancellationToken, Task<byte[]>> handler, Func<Exception, byte[]> failure);

        /// <summary>
        /// Подписка на канал в шине (NSS) с возможностью ответа на запрос.
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="handler">Обработчик, который будет обрабатывать сообщение из канала.</param>
        /// <param name="failure">Метод обратного вызова для обработки ошибок.</param>
        /// <param name="queue">Наименование очереди. Опционально.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.</returns>
        IDisposable Subscribe(string subject, string queue, Func<Message, CancellationToken, Task<byte[]>> handler, Func<Exception, byte[]> failure);

        /// <summary>
        ///  Публикация сообщения в шину (NSS) с ответом от обработчика.
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="message">Сообщение, которое будет опубликовано в шину (NSS).</param>
        /// <param name="options">Настройки, который могут быть переданны во время публикации сообщения.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns>Возвращает сообщение, которое было получено от обработчика</returns>
        Task<Message> RequestAsync(string subject, byte[] message, NatsRequestOptions options = default, CancellationToken cancellationToken = default);

    }
}
