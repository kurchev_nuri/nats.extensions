﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.Helper.Buffers;
using Nats.Extensions.Helper.Clients;
using Nats.Extensions.Helper.Common;
using Nats.Extensions.Helper.Models;
using Nats.Extensions.Helper.Options;
using Nats.Extensions.Helper.Resources;
using Nats.Extensions.Helper.Utilities;
using NATS.Client;
using Polly;
using Polly.Retry;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper.Implementations.Nats
{
    internal sealed class NatsHelper : IMessageQueueHelper<NatsExtensionsOptions>, INatsRequestHelper
    {
        private IConnection _connection;

        private readonly ILogger<NatsHelper> _logger;
        private readonly IClient<IConnection> _client;

        public NatsHelper(ILogger<NatsHelper> logger, IClient<IConnection> client)
        {
            _logger = logger;
            _client = client;

            _client.Connection.Subscribe(connection =>
            {
                if (ConnectionIsValid(connection))
                {
                    _connection = connection;
                    _logger.LogInformation(string.Format(HelperResource.NatsConnectionEstablished, connection.ConnectedUrl));
                }
            });
        }

        public bool IsConnected => ConnectionIsValid(_connection);

        public Task PublishAsync(string subject, byte[] message, CancellationToken cancellationToken = default)
        {
            CommonUtilities.ThrowIfArgumentsAreWrong(subject, message);

            if (!IsConnected)
            {
                return Task.FromException(new InvalidOperationException(HelperResource.NatsConnectionLost));
            }

            var completionSource = new TaskCompletionSource<object>(TaskCreationOptions.RunContinuationsAsynchronously);
            Task.Run(() =>
            {
                try
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        completionSource.SetCanceled();
                        return;
                    }

                    _connection.Publish(subject, message);

                    completionSource.SetResult(default);
                }
                catch (Exception exception)
                {
                    completionSource.SetException(exception);
                }

            }, CancellationToken.None);

            return completionSource.Task;
        }

        public async Task<Message> RequestAsync(string subject, byte[] message, NatsRequestOptions options = null, CancellationToken cancellationToken = default)
        {
            if (!IsConnected)
            {
                throw new InvalidOperationException(HelperResource.NatsConnectionLost);
            }

            var timeout = options?.RequestTimeout.TotalMilliseconds ?? CommonConsts.RequestTimeout.TotalMilliseconds;

            var result = await _connection.RequestAsync(subject, message, Convert.ToInt32(timeout), cancellationToken);

            return CreateMessage(result);
        }

        public IDisposable Subscribe(string subject, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options = default)
        {
            CommonUtilities.ThrowIfArgumentsAreWrong(subject, handler);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();

            var policy = CreateRetryPolicy(subject);
            var natsHandler = CreateHandler(subject, handler, cancellationTokenSource.Token);

            var connectionSubscription = _client.Connection.Subscribe(connection =>
            {
                subscription.Disposable = policy.Execute(cancellationToken =>
                {
                    if (ConnectionIsValid(connection) && !cancellationToken.IsCancellationRequested)
                    {
                        return connection.SubscribeAsync(subject, natsHandler);
                    }

                    return Disposable.Empty;

                }, cancellationTokenSource.Token);
            });

            return new CompositeDisposable { connectionSubscription, new CancellationDisposable(cancellationTokenSource), subscription };
        }

        public IDisposable Subscribe(string subject, string queue, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options = default)
        {
            CommonUtilities.ThrowIfArgumentsAreWrong(subject, queue, handler);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();
            
            var policy = CreateRetryPolicy(subject);
            var natsHandler = CreateHandler(subject, handler, cancellationTokenSource.Token);

            var connectionSubscription = _client.Connection.Subscribe(connection =>
            {
                subscription.Disposable = policy.Execute(cancellationToken =>
                {
                    if (ConnectionIsValid(connection) && !cancellationToken.IsCancellationRequested)
                    {
                        return connection.SubscribeAsync(subject, queue, natsHandler);
                    }

                    return Disposable.Empty;

                }, cancellationTokenSource.Token);
            });

            return new CompositeDisposable { connectionSubscription, new CancellationDisposable(cancellationTokenSource), subscription };
        }

        public IDisposable Subscribe(string subject, Func<Message, CancellationToken, Task<byte[]>> handler, Func<Exception, byte[]> failure)
        {
            CommonUtilities.ThrowIfArgumentsAreWrong(subject, handler);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();
            
            var policy = CreateRetryPolicy(subject);
            var natsHandler = CreateRequestHandler(handler, failure, cancellationTokenSource.Token);

            var connectionSubscription = _client.Connection.Subscribe(connection =>
            {
                subscription.Disposable = policy.Execute(cancellationToken =>
                {
                    if (ConnectionIsValid(connection) && !cancellationToken.IsCancellationRequested)
                    {
                        return connection.SubscribeAsync(subject, natsHandler);
                    }

                    return Disposable.Empty;

                }, cancellationTokenSource.Token);
            });

            return new CompositeDisposable { connectionSubscription, new CancellationDisposable(cancellationTokenSource), subscription };
        }

        public IDisposable Subscribe(string subject, string queue, Func<Message, CancellationToken, Task<byte[]>> handler, Func<Exception, byte[]> failure)
        {
            CommonUtilities.ThrowIfArgumentsAreWrong(subject, queue, handler);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();
            
            var policy = CreateRetryPolicy(subject);
            var natsHandler = CreateRequestHandler(handler, failure, cancellationTokenSource.Token);

            var connectionSubscription = _client.Connection.Subscribe(connection =>
            {
                subscription.Disposable = policy.Execute(cancellationToken =>
                {
                    if (ConnectionIsValid(connection) && !cancellationToken.IsCancellationRequested)
                    {
                        return connection.SubscribeAsync(subject, queue, natsHandler);
                    }

                    return Disposable.Empty;

                }, cancellationTokenSource.Token);
            });

            return new CompositeDisposable { connectionSubscription, new CancellationDisposable(cancellationTokenSource), subscription };
        }

        #region Private Methods

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private static bool ConnectionIsValid(IConnection connection) => connection != default && !connection.IsClosed();

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private bool LogError(Msg message, Exception exception)
        {
            _logger.LogError(exception, string.Format(HelperResource.HandlerError, message.Subject, Encoding.UTF8.GetString(message.Data)));
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private static Message CreateMessage(Msg message)
        {
            return new Message(new ReadOnlySequence<byte>(message.Data), new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                [nameof(message.Subject)] = message.Subject,
            });
        }

        private RetryPolicy CreateRetryPolicy(string subject)
        {
            return Policy.Handle<Exception>()
                .WaitAndRetryForever(number => CommonConsts.SubscriptionRetryTimeout,
                    (exception, retryTimeout) => _logger.LogError(exception, string.Format(HelperResource.SubscriptionError, subject))
                );
        }

        private void Respond(MsgHandlerEventArgs arguments, Task<byte[]> task, Func<Exception, byte[]> failure)
        {
            if (!string.IsNullOrWhiteSpace(arguments.Message.Reply))
            {
                if (task.IsFaulted)
                {
                    LogError(arguments.Message, task.Exception);
                    arguments.Message.Respond(failure(task.Exception));
                }
                else
                {
                    arguments.Message.Respond(task.Result);
                }
            }
        }

        private EventHandler<MsgHandlerEventArgs> CreateRequestHandler(Func<Message, CancellationToken, Task<byte[]>> action, Func<Exception, byte[]> failure, CancellationToken cancellationToken)
        {
            return new EventHandler<MsgHandlerEventArgs>((sender, arguments) =>
            {
                Task.Run(() => action(CreateMessage(arguments.Message), cancellationToken), cancellationToken)
                    .ContinueWith(completed => Respond(arguments, completed, failure), cancellationToken)
                    .ContinueWith(completed => completed.Exception?.Handle(exception => LogError(arguments.Message, exception)), cancellationToken);
            });
        }

        private EventHandler<MsgHandlerEventArgs> CreateHandler(string subject, Func<Message, CancellationToken, Task> handle, CancellationToken cancellationToken)
        {
            var buffer = new MessageBuffer(CommonConsts.AttributesCapacity, subject, _logger);
            var arrayPool = ArrayPool<byte>.Shared.Rent(CommonConsts.AttributeDefaultPoolSize);

            Task.Run(() => buffer.ReadAsync(handle, cancellationToken), cancellationToken);

            cancellationToken.Register(arrayPool => ArrayPool<byte>.Shared.Return(arrayPool as byte[]), arrayPool);

            return new EventHandler<MsgHandlerEventArgs>((sender, arguments) =>
            {
                var length = nameof(arguments.Message.Subject).Length + arguments.Message.Subject.Length + CommonConsts.AttributeDelimetersAmount;
                if (length > arrayPool.Length)
                {
                    ArrayPool<byte>.Shared.Return(arrayPool);
                    arrayPool = ArrayPool<byte>.Shared.Rent(length);
                }

                AttributesHelper.FlattenAttributes(arguments.Message.Subject, arrayPool);

                var valueTask = buffer.WriteAsync((arguments.Message.Data, new ArraySegment<byte>(arrayPool, 0, length)), cancellationToken);
                if (!valueTask.IsCompleted)
                {
                    valueTask.AsTask()
                        .ConfigureAwait(false)
                        .GetAwaiter()
                        .GetResult();
                }
            });
        }

        #endregion
    }
}
