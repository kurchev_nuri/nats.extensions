﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.Helper.Buffers;
using Nats.Extensions.Helper.Clients;
using Nats.Extensions.Helper.Common;
using Nats.Extensions.Helper.Models;
using Nats.Extensions.Helper.Options;
using Nats.Extensions.Helper.Options.Facades;
using Nats.Extensions.Helper.Resources;
using Nats.Extensions.Helper.Utilities;
using Polly;
using Polly.Retry;
using STAN.Client;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper.Implementations.Stan
{
    internal sealed class StanHelper : IMessageQueueHelper<StanExtensionsOptions>
    {
        private IStanConnection _connection;

        private readonly ILogger<StanHelper> _logger;
        private readonly IClient<IStanConnection> _client;
        private readonly IOptionRules<StanExtensionsOptions, StanSubscriptionOptions> _rules;

        public StanHelper(ILogger<StanHelper> logger,
                          IClient<IStanConnection> client,
                          IOptionRules<StanExtensionsOptions, StanSubscriptionOptions> rules)
        {
            _rules = rules;
            _client = client;
            _logger = logger;

            _client.Connection.Subscribe(connection =>
            {
                if (ConnectionIsValid(connection))
                {
                    _connection = connection;
                    _logger.LogInformation(string.Format(HelperResource.NatsConnectionEstablished, connection.NATSConnection.ConnectedUrl));
                }
            });
        }

        public bool IsConnected => ConnectionIsValid(_connection);

        public Task PublishAsync(string subject, byte[] message, CancellationToken cancellationToken = default)
        {
            CommonUtilities.ThrowIfArgumentsAreWrong(subject, message);

            if (!IsConnected)
            {
                return Task.FromException(new InvalidOperationException(HelperResource.StanConnectionLost));
            }

            if (cancellationToken.IsCancellationRequested)
            {
                return Task.FromCanceled(cancellationToken);
            }

            return _connection.PublishAsync(subject, message);
        }

        public IDisposable Subscribe(string subject, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options = default)
        {
            CommonUtilities.ThrowIfArgumentsAreWrong(subject, handler);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();

            var policy = CreateRetryPolicy(subject);
            var stanHandler = CreateHandler(subject, handler, options, cancellationTokenSource.Token);

            var connectionSubscription = _client.Connection.Subscribe(connection =>
            {
                subscription.Disposable = policy.Execute(cancellationToken =>
                {
                    if (ConnectionIsValid(connection) && !cancellationToken.IsCancellationRequested)
                    {
                        return connection.Subscribe(subject, _rules.ApplyRules(options), stanHandler);
                    }

                    return Disposable.Empty;

                }, cancellationTokenSource.Token);
            });

            return new CompositeDisposable { connectionSubscription, new CancellationDisposable(cancellationTokenSource), subscription };
        }

        public IDisposable Subscribe(string subject, string queue, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options = default)
        {
            CommonUtilities.ThrowIfArgumentsAreWrong(subject, queue, handler);

            var subscription = new SerialDisposable();
            var cancellationTokenSource = new CancellationTokenSource();

            var policy = CreateRetryPolicy(subject);
            var stanHandler = CreateHandler(subject, handler, options, cancellationTokenSource.Token);

            var connectionSubscription = _client.Connection.Subscribe(connection =>
            {
                subscription.Disposable = policy.Execute(cancellationToken =>
                {
                    if (ConnectionIsValid(connection) && !cancellationToken.IsCancellationRequested)
                    {
                        return connection.Subscribe(subject, queue, _rules.ApplyRules(options), stanHandler);
                    }

                    return Disposable.Empty;

                }, cancellationTokenSource.Token);
            });

            return new CompositeDisposable { connectionSubscription, new CancellationDisposable(cancellationTokenSource), subscription };
        }

        #region Private Methods 

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private static bool ConnectionIsValid(IStanConnection connection)
        {
            return connection?.NATSConnection != default && !connection.NATSConnection.IsClosed();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private bool LogError(StanMsg message, Exception exception)
        {
            _logger.LogError(exception, string.Format(HelperResource.HandlerError, message.Subject, Encoding.UTF8.GetString(message.Data)));
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private static void AckMessage(StanMsgHandlerArgs arguments, ManualResetEvent manualReset)
        {
            manualReset.Set();
            arguments.Message.Ack();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private static Message CreateMessage(StanMsg message)
        {
            return new Message(new ReadOnlySequence<byte>(message.Data), new Dictionary<string, string>(CommonConsts.AttributesCapacity, StringComparer.OrdinalIgnoreCase)
            {
                [nameof(message.Subject)] = message.Subject,
                [nameof(message.Sequence)] = message.Sequence.ToString()
            });
        }

        private RetryPolicy CreateRetryPolicy(string subject)
        {
            return Policy.Handle<Exception>()
                .WaitAndRetryForever(number => CommonConsts.SubscriptionRetryTimeout,
                    (exception, retryTimeout) => _logger.LogError(exception, string.Format(HelperResource.SubscriptionError, subject))
                );
        }

        private EventHandler<StanMsgHandlerArgs> CreateHandler(string subject, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options, CancellationToken cancellationToken)
        {
            //TODO: Переработать. Перейти на ManualAcks и избавится от CreateSerialHandler. Прокинуть ManualAcks в HandlerContext
            if (options?.ManualAcks == true)
            {
                return CreateConsequentHandler(handler, cancellationToken);
            }

            if (options?.SerialAcks == true)
            {
                return CreateSerialHandler(handler, cancellationToken);
            }

            return CreateInconsequentHandler(subject, handler, cancellationToken);
        }

        /// <summary>
        /// Сообщения будут браться из очереди последовательно, то есть пока не произойдет ACK на предыдущее сообщение следующее не возьмется.
        /// </summary>
        /// <param name="action">Обработчик сообщения.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns>Возвращается NSS-обработчик</returns>
        private EventHandler<StanMsgHandlerArgs> CreateSerialHandler(Func<Message, CancellationToken, Task> action, CancellationToken cancellationToken)
        {
            return new EventHandler<StanMsgHandlerArgs>((sender, arguments) =>
            {
                var manualReset = new ManualResetEvent(false);

                Task.Run(() => action(CreateMessage(arguments.Message), cancellationToken), cancellationToken)
                    .ContinueWith(completed => completed.Exception?.Handle(exception => LogError(arguments.Message, exception)), cancellationToken)
                    .ContinueWith(completed => AckMessage(arguments, manualReset), cancellationToken)
                    .ContinueWith(completed => completed.Exception?.Handle(exception => LogError(arguments.Message, exception)), cancellationToken);

                manualReset.WaitOne();
                manualReset.Dispose();
            });
        }

        /// <summary>
        /// Сообщения будут браться из очереди по приходу из канала, ACK произойдет после того как `callback` на подписку отработает.
        /// </summary>
        /// <param name="action">Обработчик сообщения.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns>Возвращается NSS-обработчик</returns>
        private EventHandler<StanMsgHandlerArgs> CreateConsequentHandler(Func<Message, CancellationToken, Task> action, CancellationToken cancellationToken)
        {
            return new EventHandler<StanMsgHandlerArgs>((sender, arguments) =>
            {
                Task.Run(() => action(CreateMessage(arguments.Message), cancellationToken), cancellationToken)
                    .ContinueWith(completed => completed.Exception?.Handle(exception => LogError(arguments.Message, exception)), cancellationToken)
                    .ContinueWith(completed => arguments.Message.Ack(), cancellationToken)
                    .ContinueWith(completed => completed.Exception?.Handle(exception => LogError(arguments.Message, exception)), cancellationToken);
            });
        }

        /// <summary>
        /// Сообщения будут браться из очереди по приходу из канала и клясться в буфер, ACK произойдет как только сообщение попадет в буфер.
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="action">Обработчик сообщения.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns>Возвращается NSS-обработчик</returns>
        private EventHandler<StanMsgHandlerArgs> CreateInconsequentHandler(string subject, Func<Message, CancellationToken, Task> action, CancellationToken cancellationToken)
        {
            var length = subject.Length +
                         nameof(subject).Length +
                         nameof(StanMsg.Sequence).Length +
                         ulong.MaxValue.ToString().Length +
                         2 * CommonConsts.AttributeDelimetersAmount;

            var arrayPool = ArrayPool<byte>.Shared.Rent(length);
            var buffer = new MessageBuffer(CommonConsts.AttributesCapacity, subject, _logger);

            Task.Run(() => buffer.ReadAsync(action, cancellationToken), cancellationToken);

            cancellationToken.Register(arrayPool => ArrayPool<byte>.Shared.Return(arrayPool as byte[]), arrayPool);

            return new EventHandler<StanMsgHandlerArgs>((sender, arguments) =>
            {
                AttributesHelper.FlattenAttributes(arguments.Message.Subject, arguments.Message.Sequence, arrayPool);

                var valueTask = buffer.WriteAsync((arguments.Message.Data, new ArraySegment<byte>(arrayPool, 0, length)), cancellationToken);
                if (!valueTask.IsCompleted)
                {
                    valueTask.AsTask()
                        .ConfigureAwait(false)
                        .GetAwaiter()
                        .GetResult();
                }
            });
        }

        #endregion
    }
}
