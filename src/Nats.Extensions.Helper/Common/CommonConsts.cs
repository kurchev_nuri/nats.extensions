﻿using System;

namespace Nats.Extensions.Helper.Common
{
    internal static class CommonConsts
    {
        public const int MaxReplayCount = 1;
        public const long ContinueRetries = 1;
        public const int StartConnectingImmediately = -1;

        public static readonly TimeSpan RequestTimeout = TimeSpan.FromSeconds(5);
        public static readonly TimeSpan ReconnectTimeout = TimeSpan.FromSeconds(5);

        public static readonly TimeSpan ChannelReadTimeout = TimeSpan.FromSeconds(1);
        public static readonly TimeSpan ChannelWriteTimeout = TimeSpan.FromSeconds(3);

        public static readonly TimeSpan SubscriptionRetryTimeout = TimeSpan.FromSeconds(5);

        public const int AttributesCapacity = 1;

        public const int AttributeDelimetersAmount = 2;
        public const int AttributeDefaultPoolSize = 4096;

        public const byte AttributeSplitter = (byte)':';
        public const byte AttributeDelimeter = (byte)'\r';
    }
}
