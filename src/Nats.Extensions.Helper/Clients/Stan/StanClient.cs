﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nats.Extensions.Helper.Common;
using Nats.Extensions.Helper.Connection.States;
using Nats.Extensions.Helper.Exceptions;
using Nats.Extensions.Helper.Factories;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Resources;
using NATS.Client;
using STAN.Client;
using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace Nats.Extensions.Helper.Clients.Stan
{
    internal sealed class StanClient : IClient<IStanConnection>, IDisposable
    {
        private readonly ILogger<StanClient> _logger;
        private readonly IClient<IConnection> _client;
        private readonly IConnectionState<StanConnectionOptions> _connectionState;
        private readonly IConnectionFactory<StanConnectionOptions, IStanConnection> _factory;

        public StanClient(ILogger<StanClient> logger,
                          IClient<IConnection> client,
                          IOptions<StanConnectionOptions> options,
                          IConnectionState<StanConnectionOptions> connectionState,
                          IConnectionFactory<StanConnectionOptions, IStanConnection> factory)
        {
            _client = client;
            _logger = logger;
            _factory = factory;
            _connectionState = connectionState;

            Connection = CreateConnection(options.Value);
        }

        public void Dispose() => _factory?.Dispose();

        public IObservable<IStanConnection> Connection { get; }

        #region Private Methods

        private IStanConnection RefreshConnection(IStanConnection connection, long refreshesAmount)
        {
            if (refreshesAmount > 0)
            {
                _logger.LogInformation(ClientResource.AutoRefreshStanConnection);

                _connectionState.SetConnectionIsInvalid();

                throw new StanConnectionLostException(ClientResource.AutoRefreshStanConnection);
            }

            return connection;
        }

        private IObservable<IStanConnection> CreateConnection(StanConnectionOptions options)
        {
            var observable = Observable.Create<IStanConnection>(observer =>
            {
                return _client.Connection.Subscribe(connection =>
                {
                    options.NatsConnection = connection;

                    try
                    {
                        _factory.DestroyConnection(options);

                        observer.OnNext(_factory.CreateConnection(options, connection =>
                        {
                            _logger.LogError(connection.Error, ClientResource.StanConnectionLost);

                            _connectionState.SetConnectionIsInvalid();

                            if (connection.Error is StanException)
                            {
                                observer.OnError(connection.Error);
                            }
                        }));

                        _connectionState.SetConnectionIsValid();
                    }
                    catch (Exception exception) when (exception is not StanConnectionLostException)
                    {
                        _logger.LogError(exception, ClientResource.StanReconnect);

                        observer.OnError(exception);
                    }
                });
            });

            if (options.StanRefreshConnectionTimeout.HasValue)
            {
                observable = observable.SelectMany(
                    connection => Observable.Timer(TimeSpan.Zero, options.StanRefreshConnectionTimeout.Value, TaskPoolScheduler.Default), RefreshConnection
                );
            }

            return observable.RetryWhen(
                    exceptions => exceptions.SelectMany(Observable.Timer(options.ReconnectTimeout, TaskPoolScheduler.Default))
                )
                .Replay(CommonConsts.MaxReplayCount)
                .AutoConnect(CommonConsts.StartConnectingImmediately);
        }

        #endregion
    }
}
