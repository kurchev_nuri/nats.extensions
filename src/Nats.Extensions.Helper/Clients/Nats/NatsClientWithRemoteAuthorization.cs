﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nats.Extensions.Common.PipeHelper.Providers;
using Nats.Extensions.Helper.Common;
using Nats.Extensions.Helper.Connection.States;
using Nats.Extensions.Helper.Extensions;
using Nats.Extensions.Helper.Factories;
using Nats.Extensions.Helper.Options.Connection;
using NATS.Client;
using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper.Clients.Nats
{
    internal sealed class NatsClientWithRemoteAuthorization : NatsClient
    {
        private readonly ICredentialsProvider _credentials;

        public NatsClientWithRemoteAuthorization(ILogger<NatsClient> logger,
                                                 ICredentialsProvider credentials,
                                                 IOptions<NatsConnectionOptions> options,
                                                 IConnectionState<NatsConnectionOptions> connectionState,
                                                 IConnectionFactory<NatsConnectionOptions, IConnection> factory)
            : base(logger, options, connectionState, factory)
        {
            _credentials = credentials;
        }

        protected override IObservable<IConnection> CreateConnection(IObservable<IConnection> observable)
        {
            return observable.RetryWhen(exceptions => exceptions.SelectMany(exception =>
            {
                LogRetryException(exception);

                if (string.IsNullOrWhiteSpace(Options.User) || string.IsNullOrWhiteSpace(Options.Password) || exception.IsAuthorizationViolationException())
                {
                    return Observable.FromAsync(async cancellationToken =>
                    {
                        var credentials = await _credentials.RequestAsync(cancellationToken);
                        if (credentials.IsSuccess)
                        {
                            Options.Bind(credentials.Value);
                        }
                        else
                        {
                            await Task.Delay(Options.ReconnectTimeout, cancellationToken);
                        }

                        return CommonConsts.ContinueRetries;
                    });
                }

                return Observable.Timer(Options.ReconnectTimeout, TaskPoolScheduler.Default);
            }))
            .Replay(CommonConsts.MaxReplayCount)
            .AutoConnect(CommonConsts.StartConnectingImmediately);
        }
    }
}
