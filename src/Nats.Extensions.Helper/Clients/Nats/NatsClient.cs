﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nats.Extensions.Helper.Common;
using Nats.Extensions.Helper.Connection.States;
using Nats.Extensions.Helper.Exceptions;
using Nats.Extensions.Helper.Factories;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Resources;
using NATS.Client;
using System;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;

namespace Nats.Extensions.Helper.Clients.Nats
{
    internal class NatsClient : IClient<IConnection>, IDisposable
    {
        private readonly ILogger<NatsClient> _logger;
        private readonly Lazy<IObservable<IConnection>> _connection;
        private readonly IConnectionState<NatsConnectionOptions> _connectionState;
        private readonly IConnectionFactory<NatsConnectionOptions, IConnection> _factory;

        public NatsClient(ILogger<NatsClient> logger,
                          IOptions<NatsConnectionOptions> options,
                          IConnectionState<NatsConnectionOptions> connectionState,
                          IConnectionFactory<NatsConnectionOptions, IConnection> factory)
        {
            _logger = logger;
            _factory = factory;
            _connectionState = connectionState;

            Options = options.Value;
            _connection = new Lazy<IObservable<IConnection>>(CreateConnection, LazyThreadSafetyMode.ExecutionAndPublication);
        }

        public void Dispose() => _factory?.Dispose();

        protected NatsConnectionOptions Options { get; }

        public IObservable<IConnection> Connection => _connection.Value;

        #region Virtual Methods

        protected virtual void LogRetryException(Exception exception)
        {
            if (string.IsNullOrWhiteSpace(Options.User) || string.IsNullOrWhiteSpace(Options.Password))
            {
                _logger.LogError(ClientResource.EmptyCredentials);
            }
            else if (exception is not NatsConnectionLostException)
            {
                _logger.LogError(exception, ClientResource.NatsReconnect);
            }
        }

        protected virtual IObservable<IConnection> CreateConnection(IObservable<IConnection> observable)
        {
            return observable.RetryWhen(exceptions => exceptions.SelectMany(exception =>
            {
                LogRetryException(exception);

                return Observable.Timer(Options.ReconnectTimeout, TaskPoolScheduler.Default);
            }))
            .Replay(CommonConsts.MaxReplayCount)
            .AutoConnect(CommonConsts.StartConnectingImmediately);
        }

        #endregion

        #region Private Methods 

        private IConnection RefreshConnection(IConnection connection, long refreshesAmount)
        {
            if (refreshesAmount > 0)
            {
                _logger.LogInformation(ClientResource.AutoRefreshNatsConnection);

                _connectionState.SetConnectionIsInvalid();

                throw new NatsConnectionLostException(ClientResource.AutoRefreshNatsConnection);
            }

            return connection;
        }

        private IObservable<IConnection> CreateConnection()
        {
            var observable = Observable.Create<IConnection>(async (observer, cancellationToken) =>
            {
                try
                {
                    observer.OnNext(_factory.CreateConnection(Options, connection =>
                    {
                        if (connection.Error != default)
                        {
                            _connectionState.SetConnectionIsInvalid();

                            _logger.LogError(connection.Error, ClientResource.NatsConnectionLost);

                            observer.OnError(connection.Error);
                        }

                        _logger.LogInformation(string.Format(ClientResource.ConnectionState, connection.State));
                    }));

                    await _connectionState.SetConnectionIsValidAsync(cancellationToken);
                }
                catch (Exception exception)
                {
                    observer.OnError(exception);
                }

                return Disposable.Create(_factory, factory => factory.DestroyConnection(Options));
            });

            if (Options.RefreshConnectionTimeout.HasValue)
            {
                observable = observable.SelectMany(
                    connection => Observable.Timer(TimeSpan.Zero, Options.RefreshConnectionTimeout.Value, TaskPoolScheduler.Default), RefreshConnection
                );
            }

            return CreateConnection(observable);
        }

        #endregion
    }
}
