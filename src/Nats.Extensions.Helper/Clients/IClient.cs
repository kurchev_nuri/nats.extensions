﻿using System;

namespace Nats.Extensions.Helper.Clients
{
    internal interface IClient<TConnection>
        where TConnection : class
    {
        IObservable<TConnection> Connection { get; }
    }
}
