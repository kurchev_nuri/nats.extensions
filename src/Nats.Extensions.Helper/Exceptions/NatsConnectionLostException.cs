﻿using System;
using System.Runtime.Serialization;

namespace Nats.Extensions.Helper.Exceptions
{
    internal sealed class NatsConnectionLostException : Exception
    {
        public NatsConnectionLostException()
        {
        }

        public NatsConnectionLostException(string message) : base(message)
        {
        }

        public NatsConnectionLostException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public NatsConnectionLostException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
