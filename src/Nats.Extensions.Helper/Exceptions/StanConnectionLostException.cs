﻿using System;
using System.Runtime.Serialization;

namespace Nats.Extensions.Helper.Exceptions
{
    internal sealed class StanConnectionLostException : Exception
    {
        public StanConnectionLostException()
        {
        }

        public StanConnectionLostException(string message) : base(message)
        {
        }

        public StanConnectionLostException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public StanConnectionLostException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
