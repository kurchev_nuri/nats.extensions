﻿using Nats.Extensions.Helper.Resources;
using System;

namespace Nats.Extensions.Helper.Options.Rules
{
    internal abstract class BaseOptionRule<TSourceOptions, TDestinationOptions> : IOptionRule<TSourceOptions, TDestinationOptions>
        where TSourceOptions : class
        where TDestinationOptions : class
    {
        TDestinationOptions IOptionRule<TSourceOptions, TDestinationOptions>.ApplyRule(TSourceOptions source, TDestinationOptions destination)
        {
            if (source == default || destination == default)
            {
                throw new ArgumentNullException(string.Join(", ", new[] { nameof(source), nameof(destination) }), CommonResource.ArgumentNull);
            }

            return ApplyRule(source, destination);
        }

        protected abstract TDestinationOptions ApplyRule(TSourceOptions source, TDestinationOptions destination);
    }
}
