﻿using STAN.Client;

namespace Nats.Extensions.Helper.Options.Rules.Stan.Subscription
{
    internal sealed class AddStartAtSequenceRule : BaseOptionRule<StanExtensionsOptions, StanSubscriptionOptions>
    {
        protected override StanSubscriptionOptions ApplyRule(StanExtensionsOptions options, StanSubscriptionOptions stanOptions)
        {
            if (options.StartAtSequence.HasValue)
            {
                stanOptions.StartAt(options.StartAtSequence.Value);
            }

            return stanOptions;
        }
    }
}
