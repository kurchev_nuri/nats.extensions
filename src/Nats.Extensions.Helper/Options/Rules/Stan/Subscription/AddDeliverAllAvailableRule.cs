﻿using STAN.Client;

namespace Nats.Extensions.Helper.Options.Rules.Stan.Subscription
{
    internal sealed class AddDeliverAllAvailableRule : BaseOptionRule<StanExtensionsOptions, StanSubscriptionOptions>
    {
        protected override StanSubscriptionOptions ApplyRule(StanExtensionsOptions options, StanSubscriptionOptions stanOptions)
        {
            if (options.ShouldDeliverAllAvailable)
            {
                stanOptions.DeliverAllAvailable();
            }

            return stanOptions;
        }
    }
}
