﻿using STAN.Client;
using System;

namespace Nats.Extensions.Helper.Options.Rules.Stan.Subscription
{
    internal sealed class AddAckWaitRule : BaseOptionRule<StanExtensionsOptions, StanSubscriptionOptions>
    {
        protected override StanSubscriptionOptions ApplyRule(StanExtensionsOptions options, StanSubscriptionOptions stanOptions)
        {
            if (options.AckWait.HasValue)
            {
                stanOptions.AckWait = Convert.ToInt32(options.AckWait.Value.TotalMilliseconds);
            }

            return stanOptions;
        }
    }
}
