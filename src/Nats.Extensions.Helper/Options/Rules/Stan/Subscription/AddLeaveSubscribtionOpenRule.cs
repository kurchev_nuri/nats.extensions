﻿using STAN.Client;

namespace Nats.Extensions.Helper.Options.Rules.Stan.Subscription
{
    internal sealed class AddLeaveSubscribtionOpenRule : BaseOptionRule<StanExtensionsOptions, StanSubscriptionOptions>
    {
        protected override StanSubscriptionOptions ApplyRule(StanExtensionsOptions options, StanSubscriptionOptions stanOptions)
        {
            stanOptions.LeaveOpen = options.LeaveOpen;

            return stanOptions;
        }
    }
}
