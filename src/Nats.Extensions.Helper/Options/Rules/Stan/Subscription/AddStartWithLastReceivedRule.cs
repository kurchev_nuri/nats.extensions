﻿using STAN.Client;

namespace Nats.Extensions.Helper.Options.Rules.Stan.Subscription
{
    internal sealed class AddStartWithLastReceivedRule : BaseOptionRule<StanExtensionsOptions, StanSubscriptionOptions>
    {
        protected override StanSubscriptionOptions ApplyRule(StanExtensionsOptions options, StanSubscriptionOptions stanOptions)
        {
            if (options.ShouldStartWithLastReceived)
            {
                stanOptions.StartWithLastReceived();
            }

            return stanOptions;
        }
    }
}
