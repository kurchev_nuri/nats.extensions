﻿using STAN.Client;

namespace Nats.Extensions.Helper.Options.Rules.Stan.Subscription
{
    internal sealed class AddManualAcksRule : BaseOptionRule<StanExtensionsOptions, StanSubscriptionOptions>
    {
        protected override StanSubscriptionOptions ApplyRule(StanExtensionsOptions options, StanSubscriptionOptions stanOptions)
        {
            stanOptions.ManualAcks = options.ManualAcks;

            return stanOptions;
        }
    }
}
