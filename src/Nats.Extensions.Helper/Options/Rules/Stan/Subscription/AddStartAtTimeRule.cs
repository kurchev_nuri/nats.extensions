﻿using STAN.Client;

namespace Nats.Extensions.Helper.Options.Rules.Stan.Subscription
{
    internal sealed class AddStartAtTimeRule : BaseOptionRule<StanExtensionsOptions, StanSubscriptionOptions>
    {
        protected override StanSubscriptionOptions ApplyRule(StanExtensionsOptions options, StanSubscriptionOptions stanOptions)
        {
            if (options.StartAtTime.HasValue)
            {
                stanOptions.StartAt(options.StartAtTime.Value);
            }

            return stanOptions;
        }
    }
}
