﻿using STAN.Client;

namespace Nats.Extensions.Helper.Options.Rules.Stan.Subscription
{
    internal sealed class AddDurableNameRule : BaseOptionRule<StanExtensionsOptions, StanSubscriptionOptions>
    {
        protected override StanSubscriptionOptions ApplyRule(StanExtensionsOptions option, StanSubscriptionOptions stanOptions)
        {
            if (!string.IsNullOrEmpty(option.DurableName))
            {
                stanOptions.DurableName = option.DurableName;
            }

            return stanOptions;
        }
    }
}
