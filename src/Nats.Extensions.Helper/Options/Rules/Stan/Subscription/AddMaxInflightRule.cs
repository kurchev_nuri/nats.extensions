﻿using STAN.Client;

namespace Nats.Extensions.Helper.Options.Rules.Stan.Subscription
{
    internal sealed class AddMaxInflightRule : BaseOptionRule<StanExtensionsOptions, StanSubscriptionOptions>
    {
        protected override StanSubscriptionOptions ApplyRule(StanExtensionsOptions options, StanSubscriptionOptions stanOptions)
        {
            if (options.MaxInflight.HasValue)
            {
                stanOptions.MaxInflight = options.MaxInflight.Value;
            }

            return stanOptions;
        }
    }
}
