﻿using STAN.Client;

namespace Nats.Extensions.Helper.Options.Rules.Stan.Subscription
{
    internal sealed class AddStartAtDurationRule : BaseOptionRule<StanExtensionsOptions, StanSubscriptionOptions>
    {
        protected override StanSubscriptionOptions ApplyRule(StanExtensionsOptions options, StanSubscriptionOptions stanOptions)
        {
            if (options.StartAtDuration.HasValue)
            {
                stanOptions.StartAt(options.StartAtDuration.Value);
            }

            return stanOptions;
        }
    }
}
