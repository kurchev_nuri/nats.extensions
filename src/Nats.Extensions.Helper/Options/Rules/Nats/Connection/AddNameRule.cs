﻿using Nats.Extensions.Helper.Options.Connection;
using NatsOptions = NATS.Client.Options;

namespace Nats.Extensions.Helper.Options.Rules.Nats.Connection
{
    internal sealed class AddNameRule : BaseOptionRule<NatsConnectionOptions, NatsOptions>
    {
        protected override NatsOptions ApplyRule(NatsConnectionOptions options, NatsOptions natsOptions)
        {
            natsOptions.Name = options.Name;

            return natsOptions;
        }
    }
}
