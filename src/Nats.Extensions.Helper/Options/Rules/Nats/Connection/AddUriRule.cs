﻿using Nats.Extensions.Helper.Options.Connection;
using System.Linq;
using NatsOptions = NATS.Client.Options;

namespace Nats.Extensions.Helper.Options.Rules.Nats.Connection
{
    internal sealed class AddUriRule : BaseOptionRule<NatsConnectionOptions, NatsOptions>
    {
        protected override NatsOptions ApplyRule(NatsConnectionOptions options, NatsOptions natsOptions)
        {
            if (options.Uri != default)
            {
                natsOptions.Url = options.Uri.ToString();
            }

            if (options.Servers != default)
            {
                natsOptions.Servers = options.Servers.ToArray();
            }

            natsOptions.AllowReconnect = false;

            return natsOptions;
        }
    }
}
