﻿using Nats.Extensions.Helper.Options.Connection;
using NatsOptions = NATS.Client.Options;

namespace Nats.Extensions.Helper.Options.Rules.Nats.Connection
{
    internal sealed class AddCredentialsRule : BaseOptionRule<NatsConnectionOptions, NatsOptions>
    {
        protected override NatsOptions ApplyRule(NatsConnectionOptions options, NatsOptions natsOptions)
        {
            if (!string.IsNullOrWhiteSpace(options.User) && !string.IsNullOrWhiteSpace(options.Password))
            {
                natsOptions.User = options.User;
                natsOptions.Password = options.Password;
            }

            return natsOptions;
        }
    }
}
