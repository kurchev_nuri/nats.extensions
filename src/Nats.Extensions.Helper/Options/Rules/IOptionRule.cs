﻿namespace Nats.Extensions.Helper.Options.Rules
{
    internal interface IOptionRule<in TSourceOptions, TDestinationOptions>
        where TSourceOptions : class
        where TDestinationOptions : class
    {
        TDestinationOptions ApplyRule(TSourceOptions source, TDestinationOptions destination);
    }
}
