﻿using Nats.Extensions.Helper.Common;
using System;

namespace Nats.Extensions.Helper.Options
{
    public sealed class NatsRequestOptions
    {
        /// <summary>
        /// Интервал, который отведен на запрос. По умолчанию равен 5 секундам.
        /// </summary>
        public TimeSpan RequestTimeout { get; set; } = CommonConsts.RequestTimeout;
    }
}
