﻿namespace Nats.Extensions.Helper.Options.Connection
{
    /// <summary>
    /// Настройки подключения Nats
    /// </summary>
    public sealed class NatsConnectionOptions : ConnectionOptions
    {
        /// <summary>
        /// Уникальный Client-Id сервиса
        /// </summary>
        public string Name { get; internal set; }
    }
}
