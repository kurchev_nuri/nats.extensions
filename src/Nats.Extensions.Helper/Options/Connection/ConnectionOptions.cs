﻿using System;
using System.Collections.Generic;

namespace Nats.Extensions.Helper.Options.Connection
{
    public class ConnectionOptions
    {
        /// <summary>
        /// URI для подключения
        /// </summary>
        public Uri Uri { get; internal set; }

        /// <summary>
        /// Учетная запись пользователя `NATS`
        /// </summary>
        public string User { get; internal set; }

        /// <summary>
        /// Пароль пользователя `NATS`
        /// </summary>
        public string Password { get; internal set; }

        /// <summary>
        /// Через заданный инетрвал будет происходить переподключение
        /// </summary>
        public TimeSpan ReconnectTimeout { get; internal set; }

        /// <summary>
        /// Через заданный инетрвал будет происходить обновления подключений
        /// </summary>
        public TimeSpan? RefreshConnectionTimeout { get; internal set; }

        /// <summary>
        /// Список серверов кластера
        /// </summary>
        public HashSet<string> Servers { get; internal set; }
    }
}
