﻿using NATS.Client;
using System;

namespace Nats.Extensions.Helper.Options.Connection
{
    public sealed class StanConnectionOptions : ConnectionOptions
    {
        /// <summary>
        /// Название кластера, это константа
        /// </summary>
        public string ClusterId { get; set; }

        /// <summary>
        /// Уникальный Id сервиса
        /// </summary>
        public string ClientId { get; internal set; }

        /// <summary>
        /// 
        /// </summary>
        public IConnection NatsConnection { get; internal set; }

        /// <summary>
        ///  Через заданный инетрвал будет происходить обновление подключения
        /// </summary>
        public TimeSpan? StanRefreshConnectionTimeout { get; internal set; }
    }
}
