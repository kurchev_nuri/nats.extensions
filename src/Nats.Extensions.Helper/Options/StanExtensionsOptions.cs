﻿using System;

namespace Nats.Extensions.Helper.Options
{
    public sealed class StanExtensionsOptions
    {
        /// <summary>
        /// Именованная подписка.
        /// </summary>
        public string DurableName { get; set; }

        /// <summary>
        /// Управляет количеством сообщений, которые кластер будет получать без ACK.
        /// </summary>
        public int? MaxInflight { get; set; }

        /// <summary>
        /// Если значение равно True, то ACK произойдет после того как `callback` на подписку отработает.
        /// </summary>
        public bool ManualAcks { get; set; }

        /// <summary>
        ///  Если значение равно True, то сообщения будут браться из очереди последовательно, то есть пока не произойдет ACK на предыдущее сообщение следующее не возьмется.
        /// </summary>
        public bool SerialAcks { get; set; }

        /// <summary>
        /// Кол-во времени, которое кластер будет ждать ACK от клиент, для данного сообщения.
        /// </summary>
        public TimeSpan? AckWait { get; set; }

        /// <summary>
        /// Если значение равно True, то вызывает `Close()` во время утилизации подписки или `Unsubscribe()`.
        /// Использовать когда есть необходимость продолжить обрабатывать сообщения по `DurableName`.
        /// Значение по умолчанию равно True.
        /// </summary>
        public bool LeaveOpen { get; set; } = true;

        /// <summary>
        /// Если значение равно True, то подписка получит последнее сообщение в канале.
        /// </summary>
        public bool ShouldStartWithLastReceived { get; set; }

        /// <summary>
        /// Если значение задано, то подписка получит все сообщения с заданного времени.
        /// </summary>
        public DateTime? StartAtTime { get; set; }

        /// <summary>
        /// Если значение задано, то подписка получит все сообщения с заданной дельтой.
        /// </summary>
        public TimeSpan? StartAtDuration { get; set; }

        /// <summary>
        /// Если значение задано, то подписка получит все сообщения с заданого sequence-id.
        /// </summary>
        public ulong? StartAtSequence { get; set; }

        /// <summary>
        /// Получить все доступные сообщения.
        /// </summary>
        public bool ShouldDeliverAllAvailable { get; set; }
    }
}
