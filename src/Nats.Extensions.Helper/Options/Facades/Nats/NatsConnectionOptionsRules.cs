﻿using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Options.Rules;
using NATS.Client;
using System.Collections.Generic;
using System.Linq;
using NatsOptions = NATS.Client.Options;

namespace Nats.Extensions.Helper.Options.Facades.Nats
{
    internal sealed class NatsConnectionOptionsRules : IOptionRules<NatsConnectionOptions, NatsOptions>
    {
        private readonly IEnumerable<IOptionRule<NatsConnectionOptions, NatsOptions>> _rules;

        public NatsConnectionOptionsRules(IEnumerable<IOptionRule<NatsConnectionOptions, NatsOptions>> rules) => _rules = rules;

        public NatsOptions ApplyRules(NatsConnectionOptions source)
        {
            var options = ConnectionFactory.GetDefaultOptions();

            return source == default ? options : _rules.Aggregate(options, (destination, rule) => rule.ApplyRule(source, destination));
        }
    }
}
