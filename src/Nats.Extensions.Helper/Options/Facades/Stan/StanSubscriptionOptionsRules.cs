﻿using Nats.Extensions.Helper.Options.Rules;
using STAN.Client;
using System.Collections.Generic;
using System.Linq;

namespace Nats.Extensions.Helper.Options.Facades.Stan
{
    internal sealed class StanSubscriptionOptionsRules : IOptionRules<StanExtensionsOptions, StanSubscriptionOptions>
    {
        private readonly IEnumerable<IOptionRule<StanExtensionsOptions, StanSubscriptionOptions>> _rules;

        public StanSubscriptionOptionsRules(IEnumerable<IOptionRule<StanExtensionsOptions, StanSubscriptionOptions>> rules) => _rules = rules;

        public StanSubscriptionOptions ApplyRules(StanExtensionsOptions source)
        {
            var options = StanSubscriptionOptions.GetDefaultOptions();

            return source == default ? options : _rules.Aggregate(options, (destination, rule) => rule.ApplyRule(source, destination));
        }
    }
}
