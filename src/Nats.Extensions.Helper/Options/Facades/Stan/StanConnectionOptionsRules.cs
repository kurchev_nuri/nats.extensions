﻿using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Options.Rules;
using STAN.Client;
using System.Collections.Generic;
using System.Linq;

namespace Nats.Extensions.Helper.Options.Facades.Stan
{
    internal sealed class StanConnectionOptionsRules : IOptionRules<StanConnectionOptions, StanOptions>
    {
        private readonly IEnumerable<IOptionRule<StanConnectionOptions, StanOptions>> _rules;

        public StanConnectionOptionsRules(IEnumerable<IOptionRule<StanConnectionOptions, StanOptions>> rules) => _rules = rules;

        public StanOptions ApplyRules(StanConnectionOptions source)
        {
            var options = StanOptions.GetDefaultOptions();

            return source == default ? options : _rules.Aggregate(options, (destination, rule) => rule.ApplyRule(source, destination));
        }
    }
}
