﻿namespace Nats.Extensions.Helper.Options.Facades
{
    public interface IOptionRules<in TSourceOptions, out TDestinationOptions>
        where TSourceOptions : class
        where TDestinationOptions : class
    {
        TDestinationOptions ApplyRules(TSourceOptions source);
    }
}
