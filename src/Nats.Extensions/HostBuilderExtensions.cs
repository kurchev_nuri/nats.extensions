﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Nats.Extensions.Broker.Extensions;
using Nats.Extensions.Builders;
using Nats.Extensions.Common;
using Nats.Extensions.Common.Utilities;
using Nats.Extensions.Helper.Builders;
using Nats.Extensions.Helper.Builders.Connection;
using Nats.Extensions.Nats;
using Nats.Extensions.Settings;
using Nats.Extensions.Stan;
using Nats.Extensions.Startup;
using System;
using System.Linq;

namespace Nats.Extensions
{
    public static class HostBuilderExtensions
    {
        /// <summary>
        ///  Конфигурация общих настроек для сервиса встраиваемого в Core.
        /// </summary>
        /// <param name="configure">Метод конфигурации дополнительных зависимостей сервиса.</param>
        /// <returns></returns>
        public static IHostBuilder ConfigureNatsExtensions(this IHostBuilder builder, Action<InternalServicesBuilder> configure = default)
        {
            return builder.ConfigureServices((context, services) =>
            {
                var (configuration, environment) = (context.Configuration, context.HostingEnvironment);

                var natsSettings = configuration.GetSection(nameof(NatsSettings)).Get<NatsSettings>();
                var commonSettings = configuration.GetSection(InternalConsts.CommonSettings).Get<CommonSettings>();

                var servicesBuilder = new InternalServicesBuilder();

                configure?.Invoke(servicesBuilder);

                services.AddNatsExtensionsBroker(builder =>
                {
                    builder.ThrowExceptionIfDeserializationError = servicesBuilder.ThrowExceptionIfDeserializationError;

                    builder.ConfigureConnection(connection =>
                    {
                        connection.Port = natsSettings.Port;
                        connection.Host = natsSettings.Host;

                        connection.ReconnectTimeout = natsSettings.ReconnectTimeout ?? InternalConsts.ReconnectTimeout;
                        connection.ClientId = SystemUtilities.ConcatServiceNameWithHostName(commonSettings.ServiceName);

                        if (natsSettings.RefreshConnectionTimeout.HasValue)
                        {
                            connection.RefreshConnectionTimeout = natsSettings.RefreshConnectionTimeout.Value;
                        }

                        if (natsSettings.StanRefreshConnectionTimeout.HasValue)
                        {
                            connection.StanRefreshConnectionTimeout = natsSettings.StanRefreshConnectionTimeout.Value;
                        }

                        if (servicesBuilder.ShouldUsePipeAuthorization)
                        {
                            connection.AddPipeAuthorization(options =>
                            {
                                options.PipeName = natsSettings.PipeName;
                            });

                            if (environment.IsDevelopment() && NotEmptySettings(natsSettings))
                            {
                                connection.User = natsSettings.User;
                                connection.Password = natsSettings.Password;
                            }
                        }
                        else
                        {
                            connection.User = natsSettings.User;
                            connection.Password = natsSettings.Password;
                        }
                    });

                    var natsBuilder = builder.UseNats((options, watcher) =>
                    {
                        servicesBuilder.NatsWatcher?.Invoke(watcher);
                    });

                    var stanBuilder = builder.UseStan((options, watcher) =>
                    {
                        options.ClusterId = natsSettings.ClusterId;
                        servicesBuilder.StanWatcher?.Invoke(watcher);
                    });
                });

                servicesBuilder.ConfigureStartup?.Invoke(context, services);

                services.AddSingleton<INatsPublisher, NatsPublisher>();
                services.AddSingleton<INatsConnectionWatcher, NatsConnectionWatcher>();
                services.AddSingleton<INatsSubscriptionBuilder, NatsSubscriptionBuilder>();

                services.AddSingleton<IStanPublisher, StanPublisher>();
                services.AddSingleton<IStanConnectionWatcher, StanConnectionWatcher>();
                services.AddSingleton<IStanSubscriptionBuilder, StanSubscriptionBuilder>();

                services.Configure<CommonSettings>(configuration.GetSection(InternalConsts.CommonSettings));
            });
        }

        public static void ConfigureStartup<TStartup>(this HostBuilderContext context, IServiceCollection services)
            where TStartup : IStartup
        {
            services.Configure<HostOptions>(option => option.ShutdownTimeout = InternalConsts.ShutdownTimeout);

            var contextProps = context.GetType().GetProperties();
            var constructor = typeof(TStartup).GetConstructors()
                .OrderByDescending(constructor => constructor.GetParameters().Length)
                .First();

            var parametrs = constructor.GetParameters()
                .SelectMany(param => contextProps.Where(prop => prop.PropertyType == param.ParameterType)
                    .Select(prop => prop.GetValue(context)))
                .ToArray();

            if (constructor.GetParameters().Length != parametrs.Length)
            {
                throw new ArgumentException(
                    $"Доступные параметры конструктора `{typeof(TStartup).Name}`: `[{string.Join(", ", contextProps.Select(property => property.Name))}]`."
                );
            }

            (Activator.CreateInstance(typeof(TStartup), parametrs) as IStartup).ConfigureServices(services);
        }

        #region Private Methods


        private static bool NotEmptySettings(NatsSettings settings)
        {
            return !string.IsNullOrWhiteSpace(settings.User) && !string.IsNullOrWhiteSpace(settings.Password);
        }

        #endregion
    }
}
