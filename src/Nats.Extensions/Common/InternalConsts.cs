﻿using System;

namespace Nats.Extensions.Common
{
    internal static class InternalConsts
    {
        public const string CommonSettings = "Settings";

        public static readonly TimeSpan ShutdownTimeout = TimeSpan.FromSeconds(20);
        public static readonly TimeSpan ReconnectTimeout = TimeSpan.FromSeconds(5);
    }
}
