﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Nats.Extensions.Helper.Connection.Watchers;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Startup;
using System;

namespace Nats.Extensions.Builders
{
    public sealed class InternalServicesBuilder
    {
        internal Action<IConnectionWatcher<NatsConnectionOptions>> NatsWatcher { get; private set; }

        internal Action<IConnectionWatcher<StanConnectionOptions>> StanWatcher { get; private set; }

        internal Action<HostBuilderContext, IServiceCollection> ConfigureStartup { get; private set; }

        /// <summary>
        /// Если поле установлено в `True` то будет использоваться авторизация через сторонний сервис по средствам `System.IO.Pipes`, иначе необходимо явно указать учетные данные в `embeddedsettings.json`.
        /// Значение по умолчанию `True`.
        /// </summary>
        public bool ShouldUsePipeAuthorization { get; set; } = true;

        /// <summary>
        /// Если поле установлено в `True` то будет пробрасываться `Exception` при возникновении ошибок десериализации, иначе сообщение будет принято и передано обработчику `HandleAsync`.
        /// Значение по умолчанию `True`.
        /// </summary>
        public bool ThrowExceptionIfDeserializationError { get; set; } = true;

        /// <summary>
        /// Метод добавления обработчиков на события (потеря/появление) соединения с NATS
        /// </summary>
        /// <param name="configure">Интерфейс, который позволяет добавить обработчики на события (потеря/появление) соединения с NATS</param>
        public void ConfigureNatsWatcher(Action<IConnectionWatcher<NatsConnectionOptions>> configure)
        {
            NatsWatcher = watcher => configure(watcher);
        }

        /// <summary>
        /// Метод добавления обработчиков на события (потеря/появление) соединения со STAN
        /// </summary>
        /// <param name="configure">Интерфейс, который позволяет добавить обработчики на события (потеря/появление) соединения со STAN</param>
        public void ConfigureStanWatcher(Action<IConnectionWatcher<StanConnectionOptions>> configure)
        {
            StanWatcher = watcher => configure(watcher);
        }

        /// <summary>
        /// Метод настройки внутренних зависимостей сервиса.
        /// </summary>
        /// <typeparam name="TStartup">Тип класса с зависимостями.</typeparam>
        public void UseStartup<TStartup>()
            where TStartup : IStartup
        {
            ConfigureStartup = (context, services) => context.ConfigureStartup<TStartup>(services);
        }
    }
}
