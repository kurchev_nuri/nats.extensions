﻿using System;

namespace Nats.Extensions.Settings
{
    public class NatsSettings
    {
        public int Port { get; set; }

        public string Host { get; set; }

        public string User { get; set; }

        public string Password { get; set; }

        public string ClusterId { get; set; }

        public string PipeName { get; set; }

        public TimeSpan? ReconnectTimeout { get; set; }

        public TimeSpan? RefreshConnectionTimeout { get; set; }

        public TimeSpan? StanRefreshConnectionTimeout { get; set; }
    }
}
