﻿namespace Nats.Extensions.Settings
{
    public class CommonSettings
    {
        public string ServiceName { get; set; }

        public string LogsSubject { get; set; }
    }
}
