﻿using Nats.Extensions.Broker;
using Nats.Extensions.Helper.Options;

namespace Nats.Extensions.Nats
{
    public interface INatsPublisher : IPublisher<NatsExtensionsOptions>
    {
    }
}
