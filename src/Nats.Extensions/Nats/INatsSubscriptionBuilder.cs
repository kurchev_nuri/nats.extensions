﻿using Nats.Extensions.Broker.Builders.Subscriptions;
using Nats.Extensions.Helper.Options;

namespace Nats.Extensions.Nats
{
    public interface INatsSubscriptionBuilder : ISubscriptionBuilder<NatsExtensionsOptions>
    {
    }
}
