﻿using Nats.Extensions.Broker;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Helper.Options;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Nats
{
    internal sealed class NatsPublisher : INatsPublisher
    {
        private readonly IPublisher<NatsExtensionsOptions> _publisher;

        public NatsPublisher(IPublisher<NatsExtensionsOptions> publisher)
        {
            _publisher = publisher;
        }

        public Task PublishAsync<TContent>(string subject, BaseMessage<TContent> message, CancellationToken cancellationToken = default)
        {
            return _publisher.PublishAsync(subject, message, cancellationToken);
        }

        public Task PublishAsync<TContent>(string subject, TContent content, CancellationToken cancellationToken = default)
        {
            return _publisher.PublishAsync(subject, content, cancellationToken);
        }
    }
}
