﻿using Nats.Extensions.Helper.Connection.Watchers;
using Nats.Extensions.Helper.Options.Connection;

namespace Nats.Extensions.Nats
{
    public interface INatsConnectionWatcher : IConnectionWatcher<NatsConnectionOptions>
    {
    }
}
