﻿using Nats.Extensions.Broker.Builders.SubscriptionHandlers;
using Nats.Extensions.Broker.Builders.Subscriptions;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Helper.Options;

namespace Nats.Extensions.Nats
{
    internal sealed class NatsSubscriptionBuilder : INatsSubscriptionBuilder
    {
        private readonly ISubscriptionBuilder<NatsExtensionsOptions> _subscription;

        public NatsSubscriptionBuilder(ISubscriptionBuilder<NatsExtensionsOptions> subscription)
        {
            _subscription = subscription;
        }

        public AddMultipleHandlersBuilder<NatsExtensionsOptions> SubscribeToSubject(string subject, string queue = default, NatsExtensionsOptions options = default)
        {
            return _subscription.SubscribeToSubject(subject, queue, options);
        }

        public RemoveMultipleHandlersBuilder<NatsExtensionsOptions> UnsubscribeFromSubject(string subject, string queue = default, NatsExtensionsOptions options = default)
        {
            return _subscription.UnsubscribeFromSubject(subject, queue, options);
        }

        public void SubscribeToSubject<TContent, THandler>(string subject, string queue = default, NatsExtensionsOptions options = default)
            where TContent : class
            where THandler : class, ISubjectHandler<TContent>
        {
            _subscription.SubscribeToSubject<TContent, THandler>(subject, queue, options);
        }

        public void UnsubscribeFromSubject<TContent, THandler>(string subject)
            where TContent : class
            where THandler : class, ISubjectHandler<TContent>
        {
            _subscription.UnsubscribeFromSubject<TContent, THandler>(subject);
        }
    }
}
