﻿using Microsoft.Extensions.DependencyInjection;

namespace Nats.Extensions.Startup
{
    public interface IStartup
    {
        void ConfigureServices(IServiceCollection services);
    }
}
