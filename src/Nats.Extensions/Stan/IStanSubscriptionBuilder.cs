﻿using Nats.Extensions.Broker.Builders.Subscriptions;
using Nats.Extensions.Helper.Options;

namespace Nats.Extensions.Stan
{
    public interface IStanSubscriptionBuilder : ISubscriptionBuilder<StanExtensionsOptions>
    {
    }
}
