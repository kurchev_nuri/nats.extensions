﻿using Nats.Extensions.Broker;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Helper.Options;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Stan
{
    internal sealed class StanPublisher : IStanPublisher
    {
        private readonly IPublisher<StanExtensionsOptions> _publisher;

        public StanPublisher(IPublisher<StanExtensionsOptions> publisher)
        {
            _publisher = publisher;
        }

        public Task PublishAsync<TContent>(string subject, BaseMessage<TContent> message, CancellationToken cancellationToken = default)
        {
            return _publisher.PublishAsync(subject, message, cancellationToken);
        }

        public Task PublishAsync<TContent>(string subject, TContent content, CancellationToken cancellationToken = default)
        {
            return _publisher.PublishAsync(subject, content, cancellationToken);
        }
    }
}
