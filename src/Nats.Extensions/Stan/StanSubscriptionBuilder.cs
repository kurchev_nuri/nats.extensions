﻿using Nats.Extensions.Broker.Builders.SubscriptionHandlers;
using Nats.Extensions.Broker.Builders.Subscriptions;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Helper.Options;

namespace Nats.Extensions.Stan
{
    internal sealed class StanSubscriptionBuilder : IStanSubscriptionBuilder
    {
        private readonly ISubscriptionBuilder<StanExtensionsOptions> _subscription;

        public StanSubscriptionBuilder(ISubscriptionBuilder<StanExtensionsOptions> subscription)
        {
            _subscription = subscription;
        }

        public AddMultipleHandlersBuilder<StanExtensionsOptions> SubscribeToSubject(string subject, string queue = default, StanExtensionsOptions options = default)
        {
            return _subscription.SubscribeToSubject(subject, queue, options);
        }

        public RemoveMultipleHandlersBuilder<StanExtensionsOptions> UnsubscribeFromSubject(string subject, string queue = default, StanExtensionsOptions options = default)
        {
            return _subscription.UnsubscribeFromSubject(subject, queue, options);
        }

        public void SubscribeToSubject<TContent, THandler>(string subject, string queue = default, StanExtensionsOptions options = default)
            where TContent : class
            where THandler : class, ISubjectHandler<TContent>
        {
            _subscription.SubscribeToSubject<TContent, THandler>(subject, queue, options);
        }

        public void UnsubscribeFromSubject<TContent, THandler>(string subject)
           where TContent : class
           where THandler : class, ISubjectHandler<TContent>
        {
            _subscription.UnsubscribeFromSubject<TContent, THandler>(subject);
        }
    }
}
