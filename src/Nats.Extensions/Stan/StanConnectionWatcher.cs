﻿using Nats.Extensions.Helper.Connection.Watchers;
using Nats.Extensions.Helper.Options.Connection;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Stan
{
    internal sealed class StanConnectionWatcher : IStanConnectionWatcher
    {
        private readonly IConnectionWatcher<StanConnectionOptions> _watcher;

        public StanConnectionWatcher(IConnectionWatcher<StanConnectionOptions> watcher)
        {
            _watcher = watcher;
        }

        public IDisposable ExecuteWhenConnectionIsEstablished(Expression<Action> methodCall)
        {
            return _watcher.ExecuteWhenConnectionIsEstablished(methodCall);
        }

        public IDisposable ExecuteWhenConnectionIsEstablished<TService>(Expression<Action<TService>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenConnectionIsEstablished(methodCall);
        }

        public IDisposable ExecuteWhenConnectionIsEstablished(Expression<Func<CancellationToken, Task>> methodCall)
        {
            return _watcher.ExecuteWhenConnectionIsEstablished(methodCall);
        }

        public IDisposable ExecuteWhenConnectionIsEstablished<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenConnectionIsEstablished(methodCall);
        }

        public IDisposable ExecuteWhenConnectionIsEstablished<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenConnectionIsEstablished(methodCall);
        }

        public IDisposable ExecuteWhenConnectionIsLost(Expression<Action> methodCall)
        {
            return _watcher.ExecuteWhenConnectionIsLost(methodCall);
        }

        public IDisposable ExecuteWhenConnectionIsLost<TService>(Expression<Action<TService>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenConnectionIsLost(methodCall);
        }

        public IDisposable ExecuteWhenConnectionIsLost(Expression<Func<CancellationToken, Task>> methodCall)
        {
            return _watcher.ExecuteWhenConnectionIsLost(methodCall);
        }

        public IDisposable ExecuteWhenConnectionIsLost<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenConnectionIsLost(methodCall);
        }

        public IDisposable ExecuteWhenConnectionIsLost<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenConnectionIsLost(methodCall);
        }

        public IDisposable ExecuteWhenFirstConnectionIsEstablished(Expression<Action> methodCall)
        {
            return _watcher.ExecuteWhenFirstConnectionIsEstablished(methodCall);
        }

        public IDisposable ExecuteWhenFirstConnectionIsEstablished<TService>(Expression<Action<TService>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenFirstConnectionIsEstablished(methodCall);
        }

        public IDisposable ExecuteWhenFirstConnectionIsEstablished(Expression<Func<CancellationToken, Task>> methodCall)
        {
            return _watcher.ExecuteWhenFirstConnectionIsEstablished(methodCall);
        }

        public IDisposable ExecuteWhenFirstConnectionIsEstablished<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenFirstConnectionIsEstablished(methodCall);
        }

        public IDisposable ExecuteWhenFirstConnectionIsEstablished<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenFirstConnectionIsEstablished(methodCall);
        }

        public IDisposable ExecuteWhenFirstConnectionIsLost(Expression<Action> methodCall)
        {
            return _watcher.ExecuteWhenFirstConnectionIsLost(methodCall);
        }

        public IDisposable ExecuteWhenFirstConnectionIsLost<TService>(Expression<Action<TService>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenFirstConnectionIsLost(methodCall);
        }

        public IDisposable ExecuteWhenFirstConnectionIsLost(Expression<Func<CancellationToken, Task>> methodCall)
        {
            return _watcher.ExecuteWhenFirstConnectionIsLost(methodCall);
        }

        public IDisposable ExecuteWhenFirstConnectionIsLost<TService>(Expression<Func<TService, CancellationToken, Task>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenFirstConnectionIsLost(methodCall);
        }

        public IDisposable ExecuteWhenFirstConnectionIsLost<TService, TResult>(Expression<Func<TService, CancellationToken, Task<TResult>>> methodCall)
            where TService : class
        {
            return _watcher.ExecuteWhenFirstConnectionIsLost(methodCall);
        }
    }
}
