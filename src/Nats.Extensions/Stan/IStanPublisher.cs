﻿using Nats.Extensions.Broker;
using Nats.Extensions.Helper.Options;

namespace Nats.Extensions.Stan
{
    public interface IStanPublisher : IPublisher<StanExtensionsOptions>
    {
    }
}
