﻿using Nats.Extensions.Helper.Connection.Watchers;
using Nats.Extensions.Helper.Options.Connection;

namespace Nats.Extensions.Stan
{
    public interface IStanConnectionWatcher : IConnectionWatcher<StanConnectionOptions>
    {
    }
}
