﻿using Nats.Extensions.Broker.Filters;
using System;

namespace Nats.Extensions.Broker.Features
{
    internal interface IFilterFeatures
    {
        void ReleaseFilter<TContent>(IHandlerFilter<TContent> filter)
            where TContent : class;

        IHandlerFilter<TContent> CreateFilter<TContent>(IServiceProvider provider)
            where TContent : class;
    }
}
