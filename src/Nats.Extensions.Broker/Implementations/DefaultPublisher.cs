﻿using Nats.Extensions.Broker.Factories;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Common.Serializers;
using Nats.Extensions.Helper;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Implementations
{
    internal sealed class DefaultPublisher<TOptions> : IPublisher<TOptions>
         where TOptions : class
    {
        private readonly ISerializer _serializer;
        private readonly IContentFactory _factory;
        private readonly IMessageQueueHelper<TOptions> _helper;

        public DefaultPublisher(ISerializer serializer, IMessageQueueHelper<TOptions> helper, IContentFactory factory)
        {
            _helper = helper;
            _factory = factory;
            _serializer = serializer;
        }

        public Task PublishAsync<TContent>(string subject, BaseMessage<TContent> message, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentNullException(nameof(subject), $"Аргумент {nameof(subject)} не должен быть пустым.");
            }

            return _helper.PublishAsync(subject, _serializer.SerializeToUtf8Bytes(_factory.SetAuxiliaryInfo(message)), cancellationToken);
        }

        public Task PublishAsync<TContent>(string subject, TContent content, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentNullException(nameof(subject), $"Аргумент {nameof(subject)} не должен быть пустым.");
            }

            return _helper.PublishAsync(subject, _serializer.SerializeToUtf8Bytes(_factory.CreateMessage(content)), cancellationToken);
        }
    }
}
