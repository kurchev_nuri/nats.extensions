﻿using Nats.Extensions.Broker.Subscriptions;
using Nats.Extensions.Helper;
using System;
using System.Collections.Generic;

namespace Nats.Extensions.Broker.Implementations
{
    internal sealed class DefaultSubscriber<TOptions> : ISubscriber<TOptions>
        where TOptions : class
    {
        private readonly IMessageQueueHelper<TOptions> _helper;

        public DefaultSubscriber(IMessageQueueHelper<TOptions> helper)
        {
            _helper = helper;
        }

        public IDisposable Subscribe(string subject, ISubscription subscription, TOptions options = default)
        {
            ThrowIfArgumentsAreNullOrEmpty(subject, subscription);

            return _helper.Subscribe(subject, subscription.HandleAsync, options);
        }

        public IDisposable Subscribe(string subject, string queue, ISubscription subscription, TOptions options = default)
        {
            ThrowIfArgumentsAreNullOrEmpty(subject, queue, subscription);

            return _helper.Subscribe(subject, queue, subscription.HandleAsync, options);
        }

        #region Private Methods

        private static void ThrowIfArgumentsAreNullOrEmpty<TArgument>(string subject, TArgument handler)
        {
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentNullException(nameof(subject), "Входной параметр не может быть пустым или null.");
            }

            if (EqualityComparer<TArgument>.Default.Equals(handler, default))
            {
                throw new ArgumentNullException(nameof(handler), $"Аргумент {nameof(handler)} не должен быть пустым.");
            }
        }

        private static void ThrowIfArgumentsAreNullOrEmpty<TArgument>(string subject, string queue, TArgument handler)
        {
            if (string.IsNullOrWhiteSpace(queue))
            {
                throw new ArgumentNullException(nameof(queue), "Входной параметр не может быть пустым или null.");
            }

            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentNullException(nameof(subject), "Входной параметр не может быть пустым или null.");
            }

            if (EqualityComparer<TArgument>.Default.Equals(handler, default))
            {
                throw new ArgumentNullException(nameof(handler), "Входной параметр не может быть пустым или null.");
            }
        }

        #endregion
    }
}
