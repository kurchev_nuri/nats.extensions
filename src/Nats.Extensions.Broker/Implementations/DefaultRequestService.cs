﻿using Nats.Extensions.Broker.Exceptions;
using Nats.Extensions.Broker.Factories;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Common.Serializers;
using Nats.Extensions.Helper;
using Nats.Extensions.Helper.Models;
using Nats.Extensions.Helper.Options;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Implementations
{
    internal sealed class DefaultRequestService : IRequestService
    {
        private readonly ISerializer _serializer;
        private readonly IContentFactory _factory;
        private readonly INatsRequestHelper _helper;

        public DefaultRequestService(ISerializer serializer, INatsRequestHelper helper, IContentFactory factory)
        {
            _helper = helper;
            _factory = factory;
            _serializer = serializer;
        }

        public async Task<BaseResponse<TResponse>> RequestAsync<TRequest, TResponse>(string subject, TRequest content, NatsRequestOptions options = default, CancellationToken cancellationToken = default)
            where TResponse : class
        {
            ThrowIfSubjectIsNullOrEmpty(subject);

            var request = _serializer.SerializeToUtf8Bytes(_factory.CreateMessage(content));
            var response = await _helper.RequestAsync(subject, request, options, cancellationToken);

            return HandleResponse<TResponse>(response);
        }

        public async Task<BaseResponse<TResponse>> RequestAsync<TRequest, TResponse>(string subject, BaseMessage<TRequest> message, NatsRequestOptions options = default, CancellationToken cancellationToken = default)
            where TResponse : class
        {
            ThrowIfSubjectIsNullOrEmpty(subject);

            var request = _serializer.SerializeToUtf8Bytes(_factory.SetAuxiliaryInfo(message));
            var response = await _helper.RequestAsync(subject, request, options, cancellationToken);

            return HandleResponse<TResponse>(response);
        }

        #region Private Methods

        private static void ThrowIfSubjectIsNullOrEmpty(string subject)
        {
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentNullException(nameof(subject), "Входной параметр не может быть пустым или null");
            }
        }

        private BaseResponse<TResponse> HandleResponse<TResponse>(Message message)
            where TResponse : class
        {
            try
            {
                return _serializer.Deserialize<BaseResponse<TResponse>>(message.Data.FirstSpan);
            }
            catch (Exception exception)
            {
                throw new DeserializationErrorException(exception.Message, new { Message = Encoding.UTF8.GetString(message.Data.FirstSpan) });
            }
        }

        #endregion
    }
}
