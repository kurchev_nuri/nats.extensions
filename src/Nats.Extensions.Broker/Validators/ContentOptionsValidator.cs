﻿using FluentValidation;
using Nats.Extensions.Broker.Options;

namespace Nats.Extensions.Broker.Validators
{
    internal sealed class ContentOptionsValidator : AbstractValidator<ContentOptions>
    {
        public ContentOptionsValidator()
        {
            RuleFor(u => u.HostName)
                .NotEmpty()
                .WithMessage($"Данное поле {nameof(ContentOptions.HostName)} не должно быть пустым.");

            RuleFor(u => u.ClientId)
                .NotEmpty()
                .WithMessage($"Данное поле {nameof(ContentOptions.ClientId)} не должно быть пустым.");
        }
    }
}
