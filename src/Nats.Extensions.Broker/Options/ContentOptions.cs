﻿using Nats.Extensions.Broker.Common;

namespace Nats.Extensions.Broker.Options
{
    public sealed class ContentOptions
    {
        public string ClientId { get; set; }

        public string HostName { get; set; }

        public bool ThrowExceptionIfDeserializationError { get; set; } = CommonConsts.Always;

    }
}
