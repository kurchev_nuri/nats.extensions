﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Models
{
    public class InfoModel
    {
        [JsonProperty("host_name")]
        [JsonPropertyName("host_name")]
        public string HostName { get; set; }

        [JsonPropertyName("creator")]
        [JsonProperty("creator", Required = Required.Always)]
        public string ClientId { get; set; }

        [JsonPropertyName("creation_dt")]
        [JsonProperty("creation_dt", Required = Required.Always)]
        public DateTimeOffset CreationDateTime { get; set; }
    }
}
