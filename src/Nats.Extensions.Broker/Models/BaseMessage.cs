﻿using Nats.Extensions.Broker.Serializers.Converters.Newtonsoft;
using Newtonsoft.Json;
using System.Text.Json.Serialization;
using NewtonsoftJsonConverter = Newtonsoft.Json.JsonConverterAttribute;

namespace Nats.Extensions.Broker.Models
{
    public class BaseMessage
    {
        [JsonPropertyName("type")]
        [JsonProperty("type", Required = Required.Always)]
        public int MessageType { get; set; }

        [JsonPropertyName("saga")]
        [NewtonsoftJsonConverter(typeof(SagaConverter)), JsonProperty(Required = Required.Always)]
        public SagaModel Saga { get; set; }

        [JsonPropertyName("info")]
        [JsonProperty("info", Required = Required.Always)]
        public InfoModel Info { get; set; }

        [JsonPropertyName("recipient")]
        [JsonProperty("recipient", Required = Required.Default)]
        public RecipientModel Recipient { get; set; }
    }

    public class BaseMessage<T> : BaseMessage
    {
        [JsonPropertyName("content")]
        [JsonProperty("content", Order = int.MaxValue)]
        public T Content { get; set; }
    }
}
