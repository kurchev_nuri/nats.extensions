﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Models
{
    public class BaseResponse<TResponse> : BaseMessage<TResponse>
    {
        public BaseResponse()
        {
        }

        internal BaseResponse(Exception exception)
        {
            var errorType = exception.GetType().Name;

            Error = new ErrorModel
            {
                Data = exception?.Data?.Count != 0 ? exception.Data : default,
                Type = errorType.Remove(errorType.IndexOf(nameof(Exception))),
                Message = exception?.Message ?? exception?.InnerException?.Message
            };
        }

        [JsonPropertyName("error")]
        [JsonProperty("error", Order = int.MaxValue)]
        public ErrorModel Error { get; set; }
    }
}
