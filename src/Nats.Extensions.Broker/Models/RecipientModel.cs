﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Models
{
    public class RecipientModel
    {
        [JsonProperty("host_name")]
        [JsonPropertyName("host_name")]
        public string HostName { get; set; }
    }
}
