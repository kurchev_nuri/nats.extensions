﻿using Nats.Extensions.Broker.Serializers.Converters.Newtonsoft;
using Nats.Extensions.Broker.Serializers.Converters.SystemText;
using Newtonsoft.Json;
using System.Text.Json.Serialization;
using NewtonsoftJsonConverter = Newtonsoft.Json.JsonConverterAttribute;
using SystemTextJsonConverter = System.Text.Json.Serialization.JsonConverterAttribute;

namespace Nats.Extensions.Broker.Models
{
    public class ErrorModel
    {
        [JsonProperty("data")]
        [JsonPropertyName("data")]
        public object Data { get; set; }

        [JsonProperty("message")]
        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonProperty("type")]
        [JsonPropertyName("type")]
        [NewtonsoftJsonConverter(typeof(RequestErrorTypeNewtonsoftConverter))]
        [SystemTextJsonConverter(typeof(RequestErrorTypeSystemTextConverter))]
        public string Type { get; set; }
    }
}
