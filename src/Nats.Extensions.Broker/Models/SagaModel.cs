﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Models
{
    public class SagaModel
    {
        [JsonPropertyName("id")]
        [JsonProperty("id", Required = Required.Always)]
        public Guid SagaId { get; set; }

        [JsonPropertyName("event_id")]
        [JsonProperty("event_id", Required = Required.Always)]
        public Guid EventId { get; set; }

        [JsonProperty("event_prev")]
        [JsonPropertyName("event_prev")]
        public Guid? PreviousEventId { get; set; }
    }
}
