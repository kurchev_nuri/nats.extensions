﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Helper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Subscriptions
{
    internal sealed class ScopeSubscriptionWithMultipleHandlers : ISubscription
    {
        private readonly IServiceProvider _provider;
        private readonly Dictionary<int, List<Type>> _handlers;

        public ScopeSubscriptionWithMultipleHandlers(IServiceProvider provider, List<(int MessageType, Type HandlerType)> handlers)
        {
            _provider = provider;
            _handlers = handlers.GroupBy(k => k.MessageType, v => v.HandlerType)
                .ToDictionary(u => u.Key, v => v.ToList());
        }

        public async Task HandleAsync(Message message, CancellationToken cancellationToken = default)
        {
            using var scope = _provider.CreateScope();

            var context = ActivatorUtilities.CreateInstance<HandlerContext>(scope.ServiceProvider, message);

            if (_handlers.TryGetValue(context.MessageType, out var types))
            {
                var handlers = types.Select(type => scope.ServiceProvider.GetService(type))
                    .OfType<ISubjectHandler>()
                    .Select(handler => handler.HandleAsync(context, cancellationToken));

                await Task.WhenAll(handlers);
            }
        }
    }
}
