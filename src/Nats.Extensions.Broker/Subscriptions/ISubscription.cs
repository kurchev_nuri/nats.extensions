﻿using Nats.Extensions.Helper.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Subscriptions
{
    public interface ISubscription
    {
        Task HandleAsync(Message message, CancellationToken cancellationToken = default);
    }
}
