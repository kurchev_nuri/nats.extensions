﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Broker.Handlers.Base;
using Nats.Extensions.Helper.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Subscriptions
{
    internal sealed class SingletonSubscription<TContent> : ISubscription
        where TContent : class
    {
        private readonly IServiceProvider _provider;
        private readonly ISubjectHandler<TContent> _handler;

        public SingletonSubscription(IServiceProvider provider, ISubjectHandler<TContent> handler)
        {
            _handler = handler;
            _provider = provider;
        }

        public Task HandleAsync(Message message, CancellationToken cancellationToken = default)
        {
            var context = ActivatorUtilities.CreateInstance<HandlerContext<TContent>>(_provider, message);

            if (_handler is AbstractSubjectHandler<TContent> subjectHandler)
            {
                return (subjectHandler as ISubjectHandler).HandleAsync(context, cancellationToken);
            }
            else
            {
                return _handler.HandleAsync(context, cancellationToken);
            }
        }
    }
}
