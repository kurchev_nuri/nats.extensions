﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Helper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Subscriptions
{
    internal sealed class SingletonSubscriptionWithMultipleHandlers : ISubscription
    {
        private readonly IServiceProvider _provider;
        private readonly Dictionary<int, List<ISubjectHandler>> _handlers;

        public SingletonSubscriptionWithMultipleHandlers(IServiceProvider provider, List<(int MessageType, Type HandlerType)> handlers)
        {
            _provider = provider;
            _handlers = handlers.GroupBy(k => k.MessageType, v => v.HandlerType)
                .ToDictionary(
                    group => group.Key,
                    group => group.Select(type => provider.GetService(type)).OfType<ISubjectHandler>().ToList()
                );
        }

        public Task HandleAsync(Message message, CancellationToken cancellationToken = default)
        {
            var context = ActivatorUtilities.CreateInstance<HandlerContext>(_provider, message);

            if (_handlers.TryGetValue(context.MessageType, out var handlers))
            {
                return Task.WhenAll(handlers.Select(handler => handler.HandleAsync(context, cancellationToken)));
            }

            return Task.CompletedTask;
        }
    }
}
