﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Broker.Handlers.Base;
using Nats.Extensions.Helper.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Subscriptions
{
    internal sealed class ScopeSubscription<TContent> : ISubscription
        where TContent : class
    {
        private readonly IServiceProvider _provider;

        public ScopeSubscription(IServiceProvider provider) => _provider = provider;

        public async Task HandleAsync(Message message, CancellationToken cancellationToken = default)
        {
            using var scope = _provider.CreateScope();

            var handler = scope.ServiceProvider.GetRequiredService<ISubjectHandler<TContent>>();
            var context = ActivatorUtilities.CreateInstance<HandlerContext<TContent>>(scope.ServiceProvider, message);

            if (handler is AbstractSubjectHandler<TContent> subjectHandler)
            {
                await (subjectHandler as ISubjectHandler).HandleAsync(context, cancellationToken);
            }
            else
            {
                await handler.HandleAsync(context, cancellationToken);
            }
        }
    }
}
