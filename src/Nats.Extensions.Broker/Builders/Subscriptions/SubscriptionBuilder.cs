﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.Broker.Activators.Subscriptions;
using Nats.Extensions.Broker.Builders.SubscriptionHandlers;
using Nats.Extensions.Broker.Builders.SubscriptionHandlers.Base;
using Nats.Extensions.Broker.Extensions;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Broker.Utilities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Channels;

namespace Nats.Extensions.Broker.Builders.Subscriptions
{
    internal sealed class SubscriptionBuilder<TOptions> : IDisposable, ISubscriptionBuilder<TOptions>
        where TOptions : class
    {
        private readonly ISubscriptionActivator<TOptions> _activator;
        private readonly ILogger<SubscriptionBuilder<TOptions>> _logger;

        private readonly IDisposable _subscription;
        private readonly Channel<(string, string, TOptions, IHandlerItems)> _channel;

        private readonly ConcurrentDictionary<string, IDisposable> _subscriptions;
        private readonly ConcurrentDictionary<string, (IDisposable Subscription, TOptions Options, List<(int, Type)>)> _multipleSubscriptions;

        public SubscriptionBuilder(ISubscriptionActivator<TOptions> activator, ILogger<SubscriptionBuilder<TOptions>> logger)
        {
            _logger = logger;
            _activator = activator;

            _subscriptions = new ConcurrentDictionary<string, IDisposable>();
            _multipleSubscriptions = new ConcurrentDictionary<string, (IDisposable, TOptions, List<(int, Type)>)>();

            _channel = Channel.CreateUnbounded<(string, string, TOptions, IHandlerItems)>(new UnboundedChannelOptions { SingleReader = true });

            _subscription = _channel.Reader
                .ToObservable()
                .Subscribe(UpdateHandlers);
        }

        public void Dispose()
        {
            _subscription.Dispose();

            foreach (var (_, value) in _multipleSubscriptions.ToArray())
            {
                value.Subscription.Dispose();
            }

            foreach (var (_, subscription) in _subscriptions.ToArray())
            {
                subscription.Dispose();
            }
        }

        public AddMultipleHandlersBuilder<TOptions> SubscribeToSubject(string subject, string queue = default, TOptions options = default)
        {
            ThrowIfSubjectIsNullOrEmpty(subject);

            return new AddMultipleHandlersBuilder<TOptions>(subject, queue, options, _channel.Writer);
        }

        public RemoveMultipleHandlersBuilder<TOptions> UnsubscribeFromSubject(string subject, string queue = default, TOptions options = default)
        {
            ThrowIfSubjectIsNullOrEmpty(subject);

            if (options == default && _multipleSubscriptions.TryGetValue(subject, out var value))
            {
                options = value.Options;
            }

            return new RemoveMultipleHandlersBuilder<TOptions>(subject, queue, options, _channel.Writer);
        }

        public void SubscribeToSubject<TContent, THandler>(string subject, string queue = default, TOptions options = default)
            where TContent : class
            where THandler : class, ISubjectHandler<TContent>
        {
            ThrowIfSubjectIsNullOrEmpty(subject);

            var subscriptionKey = SubscriptionUtilities.MakeKey<THandler>(subject);

            if (_subscriptions.TryRemove(subscriptionKey, out var subscription))
            {
                subscription.Dispose();
            }

            _subscriptions.TryAdd(subscriptionKey, _activator.CreateSubscription<TContent>(subject, queue, options));
        }

        public void UnsubscribeFromSubject<TContent, THandler>(string subject)
            where TContent : class
            where THandler : class, ISubjectHandler<TContent>
        {
            if (_subscriptions.TryRemove(SubscriptionUtilities.MakeKey<THandler>(subject), out var subscription))
            {
                subscription.Dispose();
            }
        }

        #region Private Methods

        private static void ThrowIfSubjectIsNullOrEmpty(string subject)
        {
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentNullException(nameof(subject), "Входной параметр не может быть пустым или null");
            }
        }

        private void UpdateHandlers((string, string, TOptions, IHandlerItems) arguments)
        {
            var (subject, queue, options, handlerItems) = arguments;

            if (_multipleSubscriptions.TryRemove(subject, out var value))
            {
                var (subscription, _, currentHandlers) = value;

                subscription.Dispose();

                CreateSubscription(currentHandlers);
            }
            else
            {
                CreateSubscription(new List<(int, Type)>());
            }

            void CreateSubscription(List<(int, Type)> currentHandlers)
            {
                var newHandlers = handlerItems.Merge(currentHandlers);
                if (newHandlers.Any())
                {
                    try
                    {
                        _multipleSubscriptions.TryAdd(subject, (_activator.CreateSubscription(subject, newHandlers, queue, options), options, newHandlers));
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, $"Во время подписки на канал `{subject}` произошла ошибка.");
                    }
                }
            }
        }

        #endregion
    }
}
