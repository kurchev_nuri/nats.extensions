﻿using Nats.Extensions.Broker.Builders.SubscriptionHandlers;
using Nats.Extensions.Broker.Handlers;

namespace Nats.Extensions.Broker.Builders.Subscriptions
{
    public interface ISubscriptionBuilder<TOptions>
        where TOptions : class
    {
        /// <summary>
        /// Отписка от канала с одним строго типизированным обработчиком.
        /// </summary>
        /// <typeparam name="TContent">Тип сообщения, который ожидается в канале.</typeparam>
        /// <typeparam name="THandler">Тип обработчкиа, который будет обрабатывать сообщение.</typeparam>
        /// <param name="subject">Наименование канала.</param>
        void UnsubscribeFromSubject<TContent, THandler>(string subject)
            where TContent : class
            where THandler : class, ISubjectHandler<TContent>;

        /// <summary>
        /// Подписка на канал с одним строго типизированным обработчиком.
        /// </summary>
        /// <typeparam name="TContent">Тип сообщения, который ожидается в канале.</typeparam>
        /// <typeparam name="THandler">Тип обработчкиа, который будет обрабатывать сообщение.</typeparam>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="queue">Наименование очереди. Опционально.</param>
        /// <param name="options">Настройки, который могут быть переданны во время подписки.</param>
        void SubscribeToSubject<TContent, THandler>(string subject, string queue = default, TOptions options = default)
            where TContent : class
            where THandler : class, ISubjectHandler<TContent>;

        /// <summary>
        /// Подписка на канал с несколькими обработчиками отличные по типу сообщения.
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="queue">Наименование очереди. Опционально.</param>
        /// <param name="options">Настройки, который могут быть переданны во время подписки.</param>
        /// <returns></returns>
        AddMultipleHandlersBuilder<TOptions> SubscribeToSubject(string subject, string queue = default, TOptions options = default);

        /// <summary>
        /// Удаляет указанные обработчики из канала. Если все обработчики были удалены, то происходит отписка от канала.
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="queue">Наименование очереди. Опционально.</param>
        /// <param name="options">Настройки, который могут быть переданны во время подписки.</param>
        /// <returns></returns>
        RemoveMultipleHandlersBuilder<TOptions> UnsubscribeFromSubject(string subject, string queue = default, TOptions options = default);
    }
}
