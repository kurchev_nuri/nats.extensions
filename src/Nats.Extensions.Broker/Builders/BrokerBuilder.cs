﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Common;
using Nats.Extensions.Helper.Builders;

namespace Nats.Extensions.Broker.Builders
{
    public sealed class BrokerBuilder : HelperBuilder
    {
        public BrokerBuilder(IServiceCollection services)
            : base(services)
        {
        }

        public bool ThrowExceptionIfDeserializationError { get; set; } = CommonConsts.Always;
    }
}
