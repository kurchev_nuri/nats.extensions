﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.Broker.Activators.Requests;
using Nats.Extensions.Broker.Builders.RequestHandlers;
using Nats.Extensions.Broker.Builders.RequestHandlers.Base;
using Nats.Extensions.Broker.Extensions;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Broker.Utilities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Channels;

namespace Nats.Extensions.Broker.Builders.Requests
{
    internal sealed class RequestSubscriptionBuilder : IDisposable, IRequestSubscriptionBuilder
    {
        private readonly IRequestSubscriptionActivator _activator;
        private readonly ILogger<RequestSubscriptionBuilder> _logger;

        private readonly IDisposable _subscription;
        private readonly Channel<(string, string, IRequestHandlerItems)> _channel;

        private readonly ConcurrentDictionary<string, IDisposable> _subscriptions;
        private readonly ConcurrentDictionary<string, (IDisposable Subscription, List<(int, Type)>)> _multipleSubscriptions;

        public RequestSubscriptionBuilder(IRequestSubscriptionActivator activator, ILogger<RequestSubscriptionBuilder> logger)
        {
            _logger = logger;
            _activator = activator;

            _subscriptions = new ConcurrentDictionary<string, IDisposable>();
            _multipleSubscriptions = new ConcurrentDictionary<string, (IDisposable, List<(int, Type)>)>();

            _channel = Channel.CreateUnbounded<(string, string, IRequestHandlerItems)>(new UnboundedChannelOptions { SingleReader = true });

            _subscription = _channel.Reader
                .ToObservable()
                .Subscribe(UpdateHandlers);
        }

        public void Dispose()
        {
            _subscription.Dispose();

            foreach (var (_, value) in _multipleSubscriptions.ToArray())
            {
                value.Subscription.Dispose();
            }

            foreach (var (_, subscription) in _subscriptions.ToArray())
            {
                subscription.Dispose();
            }
        }

        public AddMultipleRequestHandlersBuilder SubscribeToSubject(string subject, string queue = default)
        {
            ThrowIfSubjectIsNullOrEmpty(subject);

            return new AddMultipleRequestHandlersBuilder(subject, queue, _channel.Writer);
        }

        public RemoveMultipleRequestHandlersBuilder UnsubscribeFromSubject(string subject, string queue = default)
        {
            ThrowIfSubjectIsNullOrEmpty(subject);

            return new RemoveMultipleRequestHandlersBuilder(subject, queue, _channel.Writer);
        }

        public void SubscribeToSubject<TRequest, TResponse, THandler>(string subject, string queue = default)
            where TRequest : class
            where TResponse : class
            where THandler : class, IRequestHandler<TRequest, TResponse>
        {
            ThrowIfSubjectIsNullOrEmpty(subject);

            var subscriptionKey = SubscriptionUtilities.MakeKey<THandler>(subject);

            if (_subscriptions.TryRemove(subscriptionKey, out var subscription))
            {
                subscription.Dispose();
            }

            _subscriptions.TryAdd(subscriptionKey, _activator.CreateSubscription<TRequest, TResponse>(subject, queue));
        }

        public void UnsubscribeFromSubject<TRequest, TResponse, THandler>(string subject)
            where TRequest : class
            where TResponse : class
            where THandler : class, IRequestHandler<TRequest, TResponse>
        {
            if (_subscriptions.TryRemove(SubscriptionUtilities.MakeKey<THandler>(subject), out var subscription))
            {
                subscription.Dispose();
            }
        }

        #region Private Methods

        private static void ThrowIfSubjectIsNullOrEmpty(string subject)
        {
            if (string.IsNullOrWhiteSpace(subject))
            {
                throw new ArgumentNullException(nameof(subject), "Входной параметр не может быть пустым или null");
            }
        }

        private void UpdateHandlers((string, string, IRequestHandlerItems) arguments)
        {
            var (subject, queue, handlerItems) = arguments;

            if (_multipleSubscriptions.TryRemove(subject, out var value))
            {
                var (subscription, currentHandlers) = value;

                subscription.Dispose();

                CreateSubscription(currentHandlers);
            }
            else
            {
                CreateSubscription(new List<(int, Type)>());
            }

            void CreateSubscription(List<(int, Type)> currentHandlers)
            {
                var newHandlers = handlerItems.Merge(currentHandlers);
                if (newHandlers.Any())
                {
                    try
                    {
                        _multipleSubscriptions.TryAdd(subject, (_activator.CreateSubscription(subject, newHandlers, queue), newHandlers));
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, $"Во время подписки на канал `{subject}` произошла ошибка.");
                    }
                }
            }
        }

        #endregion
    }
}
