﻿using Nats.Extensions.Broker.Builders.RequestHandlers;
using Nats.Extensions.Broker.Handlers;

namespace Nats.Extensions.Broker.Builders.Requests
{
    public interface IRequestSubscriptionBuilder
    {
        /// <summary>
        /// Отписка от канала с одним строго типизированным обработчиком с возможностью  ответа на запрос.
        /// </summary>
        /// <typeparam name="TRequest">Тип принимаего сообщения.</typeparam>
        /// <typeparam name="TResponse">Тип отправляемого сообщения.</typeparam>
        /// <typeparam name="THandler">Тип обработчика, который будет обрабатывать сообщение.</typeparam>
        /// <param name="subject">Наименование канала.</param>
        void UnsubscribeFromSubject<TRequest, TResponse, THandler>(string subject)
            where TRequest : class
            where TResponse : class
            where THandler : class, IRequestHandler<TRequest, TResponse>;

        /// <summary>
        /// Подписка на канал в шине (NSS) с возможностью ответа на запрос.
        /// </summary>
        /// <typeparam name="TRequest">Тип принимаего сообщения.</typeparam>
        /// <typeparam name="TResponse">Тип отправляемого сообщения.</typeparam>
        /// <typeparam name="THandler">Тип обработчика, который будет обрабатывать сообщение.</typeparam>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="queue">Наименование очереди. Опционально.</param>
        void SubscribeToSubject<TRequest, TResponse, THandler>(string subject, string queue = default)
            where TRequest : class
            where TResponse : class
            where THandler : class, IRequestHandler<TRequest, TResponse>;

        /// <summary>
        /// Подписка на канал с несколькими обработчиками отличные по типу сообщения.
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="queue">Наименование очереди. Опционально.</param>
        /// <returns></returns>
        AddMultipleRequestHandlersBuilder SubscribeToSubject(string subject, string queue = default);

        /// <summary>
        /// Удаляет указанные обработчики из канала. Если все обработчики были удалены, то происходит отписка от канала.
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="queue">Наименование очереди. Опционально.</param>
        /// <returns></returns>
        RemoveMultipleRequestHandlersBuilder UnsubscribeFromSubject(string subject, string queue = default);
    }
}
