﻿using Nats.Extensions.Broker.Handlers.Base;

namespace Nats.Extensions.Broker.Builders.SubscriptionHandlers.Base
{
    public interface IRemoveHandler
    {
        void RemoveHandler<TContent, THandler>()
            where TContent : class
            where THandler : AbstractSubjectHandler<TContent>;
    }
}
