﻿using System;
using System.Collections.Generic;

namespace Nats.Extensions.Broker.Builders.SubscriptionHandlers.Base
{
    public interface IHandlerItems
    {
        List<(int, Type)> Merge(List<(int, Type)> currentHandlers);
    }
}
