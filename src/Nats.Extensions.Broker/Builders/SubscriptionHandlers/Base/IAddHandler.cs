﻿using Nats.Extensions.Broker.Handlers.Base;

namespace Nats.Extensions.Broker.Builders.SubscriptionHandlers.Base
{
    public interface IAddHandler
    {
        void AddHandler<TContent, THandler>()
            where TContent : class
            where THandler : AbstractSubjectHandler<TContent>;
    }
}
