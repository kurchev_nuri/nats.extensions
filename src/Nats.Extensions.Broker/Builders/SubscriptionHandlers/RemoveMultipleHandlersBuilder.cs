﻿using Nats.Extensions.Broker.Builders.SubscriptionHandlers.Base;
using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Builders.SubscriptionHandlers
{
    public sealed class RemoveMultipleHandlersBuilder<TOptions>
        where TOptions : class
    {
        private readonly string _queue;
        private readonly string _subject;
        private readonly TOptions _options;
        private readonly ChannelWriter<(string, string, TOptions, IHandlerItems)> _writer;

        public RemoveMultipleHandlersBuilder(string subject, string queue, TOptions options, ChannelWriter<(string, string, TOptions, IHandlerItems)> writer)
        {
            _queue = queue;
            _writer = writer;
            _subject = subject;
            _options = options;
        }

        public async Task WithMultipleHandlersAsync(Action<IRemoveHandler> configure, CancellationToken cancellationToken = default)
        {
            var items = new RemoveHandlerItems();

            configure?.Invoke(items);

            await _writer.WriteAsync((_subject, _queue, _options, items), cancellationToken);
        }
    }
}
