﻿using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Broker.Builders.SubscriptionHandlers.Base;
using Nats.Extensions.Broker.Handlers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Nats.Extensions.Broker.Builders.SubscriptionHandlers
{
    internal sealed class AddHandlerItems : IHandlerItems, IAddHandler
    {
        private readonly List<(int, Type)> _handlers = new List<(int, Type)>();

        public List<(int, Type)> Merge(List<(int, Type)> currentHandlers)
        {
            return currentHandlers.Union(_handlers).ToList();
        }

        public void AddHandler<TContent, THandler>()
            where TContent : class
            where THandler : AbstractSubjectHandler<TContent>
        {
            var messageType = typeof(TContent).GetCustomAttribute<MessageTypeAttribute>();
            if (messageType == default)
            {
                throw new ArgumentException($"Для типа контента `{typeof(TContent).Name}` должен быть определен атрибут `{nameof(MessageTypeAttribute)}` с заданным типом сообщения.");
            }

            _handlers.Add((messageType.Value, typeof(THandler)));
        }
    }
}
