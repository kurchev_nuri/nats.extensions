﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Serializers.Helpers;
using Nats.Extensions.Common.Extensions;
using Newtonsoft.Json;
using System;
using System.Text.Json;

namespace Nats.Extensions.Broker.Builders.Serializers
{
    public sealed class SerializersBuilder
    {
        private readonly IServiceCollection _services;

        public SerializersBuilder(IServiceCollection services) => _services = services;

        public void AddNewtonsoftSerializer(Action<JsonSerializerSettings> configure = default)
        {
            _services.AddNewtonsoftSerializer(configure);
        }

        public void AddSystemTextSerializer(Action<JsonSerializerOptions> configure = default)
        {
            _services.AddSystemTextSerializer(options =>
            {
                configure?.Invoke(options);
                options.SetConverters();
            });
        }
    }
}
