﻿using Nats.Extensions.Broker.Builders.RequestHandlers.Base;
using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Builders.RequestHandlers
{
    public sealed class RemoveMultipleRequestHandlersBuilder
    {
        private readonly string _queue;
        private readonly string _subject;
        private readonly ChannelWriter<(string, string, IRequestHandlerItems)> _writer;

        public RemoveMultipleRequestHandlersBuilder(string subject, string queue, ChannelWriter<(string, string, IRequestHandlerItems)> writer)
        {
            _queue = queue;
            _writer = writer;
            _subject = subject;
        }

        public async Task WithMultipleHandlersAsync(Action<IRemoveRequestHandler> configure, CancellationToken cancellationToken = default)
        {
            var items = new RemoveRequestHandler();

            configure?.Invoke(items);

            await _writer.WriteAsync((_subject, _queue, items), cancellationToken);
        }
    }
}
