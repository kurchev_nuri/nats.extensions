﻿using Nats.Extensions.Broker.Builders.RequestHandlers.Base;
using System;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Builders.RequestHandlers
{
    public sealed class AddMultipleRequestHandlersBuilder
    {
        private readonly string _queue;
        private readonly string _subject;
        private readonly ChannelWriter<(string, string, IRequestHandlerItems)> _writer;

        public AddMultipleRequestHandlersBuilder(string subject, string queue, ChannelWriter<(string, string, IRequestHandlerItems)> writer)
        {
            _queue = queue;
            _writer = writer;
            _subject = subject;
        }

        public async Task WithMultipleHandlersAsync(Action<IAddRequestHandler> configure, CancellationToken cancellationToken = default)
        {
            var items = new AddRequestHandler();

            configure?.Invoke(items);

            await _writer.WriteAsync((_subject, _queue, items), cancellationToken);
        }
    }
}
