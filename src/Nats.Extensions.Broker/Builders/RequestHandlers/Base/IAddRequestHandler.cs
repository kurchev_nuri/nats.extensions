﻿using Nats.Extensions.Broker.Handlers.Base;

namespace Nats.Extensions.Broker.Builders.RequestHandlers.Base
{
    public interface IAddRequestHandler
    {
        void AddHandler<TRequest, TResponse, THandler>()
            where TRequest : class
            where TResponse : class
            where THandler : AbstractRequestHandler<TRequest, TResponse>;
    }
}
