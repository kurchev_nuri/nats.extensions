﻿using System;
using System.Collections.Generic;

namespace Nats.Extensions.Broker.Builders.RequestHandlers.Base
{
    public interface IRequestHandlerItems
    {
        List<(int, Type)> Merge(List<(int, Type)> currentHandlers);
    }
}
