﻿using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Broker.Builders.RequestHandlers.Base;
using Nats.Extensions.Broker.Handlers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Nats.Extensions.Broker.Builders.RequestHandlers
{
    internal sealed class RemoveRequestHandler : IRequestHandlerItems, IRemoveRequestHandler
    {
        private readonly List<(int, Type)> _handlers = new();

        public List<(int, Type)> Merge(List<(int, Type)> currentHandlers)
        {
            return currentHandlers.Except(_handlers).ToList();
        }

        public void RemoveHandler<TRequest, TResponse, THandler>()
            where TRequest : class
            where TResponse : class
            where THandler : AbstractRequestHandler<TRequest, TResponse>
        {
            var messageType = typeof(TRequest).GetCustomAttribute<MessageTypeAttribute>();
            if (messageType == default)
            {
                throw new ArgumentException($"Для типа контента `{typeof(TRequest).Name}` должен быть определен атрибут `{nameof(MessageTypeAttribute)}` с заданным типом сообщения.");
            }

            _handlers.Add((messageType.Value, typeof(THandler)));
        }
    }
}
