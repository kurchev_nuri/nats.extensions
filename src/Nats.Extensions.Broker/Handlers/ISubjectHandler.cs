﻿using Nats.Extensions.Broker.Contexts;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Handlers
{
    internal interface ISubjectHandler
    {
        Task HandleAsync(HandlerContext context, CancellationToken cancellationToken = default);
    }

    public interface ISubjectHandler<TContent>
        where TContent : class
    {
        Task HandleAsync(HandlerContext<TContent> context, CancellationToken cancellationToken = default);
    }
}
