﻿using Nats.Extensions.Broker.Contexts;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Handlers
{
    internal interface IRequestHandler
    {
        Task<byte[]> HandleAsync(HandlerContext context, CancellationToken cancellationToken = default);
    }

    public interface IRequestHandler<TRequest, TResponse>
        where TRequest : class
        where TResponse : class
    {
        Task<TResponse> HandleAsync(HandlerContext<TRequest> context, CancellationToken cancellationToken = default);
    }
}
