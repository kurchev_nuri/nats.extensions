﻿using Nats.Extensions.Broker.Common;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Exceptions;
using Nats.Extensions.Broker.Features;
using Nats.Extensions.Broker.Filters;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Broker.Options;
using Nats.Extensions.Broker.Utilities;
using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Handlers.Base
{
    /// <summary>
    ///  Базовый класс для обработчиков (Handler) сообщений с возможностью ответа на запрос.
    /// </summary>
    /// <typeparam name="TRequest">Тип обрабатываемого сообщения.</typeparam>
    /// <typeparam name="TResponse">Тип отправляемого в ответ сообщения.</typeparam>
    public abstract class AbstractRequestHandler<TRequest, TResponse> : IRequestHandler<TRequest, TResponse>, IRequestHandler
        where TRequest : class
        where TResponse : class
    {
        private readonly IFilterFeatures _features;

        protected AbstractRequestHandler()
        {
            _features = GetType()
                .GetMethod(nameof(HandleAsync), BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                ?.GetCustomAttribute<TypeHandlerFilterAttribute>();
        }

        async Task<byte[]> IRequestHandler.HandleAsync(HandlerContext handlerContext, CancellationToken cancellationToken)
        {
            var context = handlerContext is HandlerContext<TRequest, TResponse> ctx ? ctx : new HandlerContext<TRequest, TResponse>(handlerContext);
            try
            {
                if (!context.IsValid && context.ThrowExceptionIfDeserializationError)
                {
                    throw new DeserializationErrorException(
                        string.Join(Environment.NewLine, context.Errors.Select(exception => exception.Message ?? exception.InnerException?.Message))
                    );
                }

                if (!RecipientValidation(context.Options, context?.Message?.Recipient))
                {
                    throw BaseMessageUtilities.WrongRecipientError(context.Options);
                }

                if (_features != default)
                {
                    (context.Response.Content, context.Response.Error) = await HandleWithFilterAsync(context, cancellationToken);
                }
                else
                {
                    context.Response.Content = await HandleAsync(context, cancellationToken);
                }
            }
            catch (Exception exception)
            {
                context.Response.Error = BaseMessageUtilities.HandleException(exception);
            }

            return context.Serializer.SerializeToUtf8Bytes(context.Response);
        }

        /// <summary>
        /// Обрабатывает входящее сообщение.
        /// </summary>
        /// <param name="context">Сообщение.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns>TResponse отправляется в качестве ответа на Replay для NATS.</returns>
        public abstract Task<TResponse> HandleAsync(HandlerContext<TRequest> context, CancellationToken cancellationToken = default);

        /// <summary>
        /// При переопределении позволяет задать дополнительные условия для запуска метода `HandleAsync`.
        /// По умолчанию производится проверка поле `Recipient`.
        /// </summary>
        /// <returns>Возвращает значение по которому будет принято решение о запуске метода `HandleAsync`.</returns>
        protected virtual bool ShouldValidateRecipient() => CommonConsts.Always;

        /// <summary>
        /// Производит консолидацию принятого получателя с настройками машины по умолчанию согласно документу.</see>
        /// </summary>
        /// <param name="recipient">Получатель сообщения.</param>
        /// <param name="options">Настройками машины по умолчанию.</param>
        /// <returns>Возвращает значение по которому будет принято решение об обработке принятого сообщения.</returns>
        protected virtual bool RecipientValidation(ContentOptions options, RecipientModel recipient)
        {
            if (ShouldValidateRecipient())
            {
                return BaseMessageUtilities.RecipientValidation(options, recipient);
            }

            return CommonConsts.CorrectRecipient;
        }

        #region Private Methods

        private async Task<(TResponse, ErrorModel)> HandleWithFilterAsync(HandlerContext<TRequest> context, CancellationToken cancellationToken = default)
        {
            var filter = _features.CreateFilter<TRequest>(context.Provider);
            try
            {
                await filter.OnExecuting(context, cancellationToken);

                var result = await HandleAsync(context, cancellationToken);

                await filter.OnExecuted(context, cancellationToken);

                return (result, default);
            }
            catch (Exception exception)
            {
                await filter.OnException(new ExceptionContext<TRequest>(exception, context), cancellationToken);

                return (default, BaseMessageUtilities.HandleException(exception));
            }
            finally
            {
                _features.ReleaseFilter(filter);
            }
        }

        #endregion
    }
}
