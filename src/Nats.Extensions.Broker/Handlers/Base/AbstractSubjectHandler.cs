﻿using Nats.Extensions.Broker.Common;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Features;
using Nats.Extensions.Broker.Filters;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Broker.Options;
using Nats.Extensions.Broker.Utilities;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Handlers.Base
{
    /// <summary>
    /// Базовый класс для обработчиков (Handler) сообщений.
    /// </summary>
    /// <typeparam name="TContent">Тип обрабатываемого сообщения.</typeparam>
    public abstract class AbstractSubjectHandler<TContent> : ISubjectHandler<TContent>, ISubjectHandler
        where TContent : class
    {
        private readonly IFilterFeatures _features;

        protected AbstractSubjectHandler()
        {
            _features = GetType()
                .GetMethod(nameof(HandleAsync), BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                ?.GetCustomAttribute<TypeHandlerFilterAttribute>();
        }

        Task ISubjectHandler.HandleAsync(HandlerContext handlerContext, CancellationToken cancellationToken)
        {
            var context = handlerContext is HandlerContext<TContent> ctx ? ctx : new HandlerContext<TContent>(handlerContext);

            if (!context.IsValid && context.ThrowExceptionIfDeserializationError)
            {
                throw new AggregateException($"Обработчик сообщения `{nameof(HandleAsync)}` завершился с ошибками", context.Errors);
            }

            if (!RecipientValidation(context.Options, context?.Message?.Recipient) || !ShouldInvoke(context))
            {
                return Task.CompletedTask;
            }

            if (_features != default)
            {
                return HandleWithFilterAsync(context, cancellationToken);
            }
            else
            {
                return HandleAsync(context, cancellationToken);
            }
        }

        /// <summary>
        /// Обрабатывает входящее сообщение.
        /// </summary>
        /// <param name="context">Контекст принятого сообщения.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns></returns>
        public abstract Task HandleAsync(HandlerContext<TContent> context, CancellationToken cancellationToken = default);

        /// <summary>
        /// При переопределении позволяет задать дополнительные условия для запуска метода `HandleAsync`.
        /// По умолчанию запуск происходит по типу сообщения `BaseMessage.MessageType`.
        /// </summary>
        /// <param name="context">Контекст принятого сообщения.</param>
        /// <returns>Возвращает значение по которому будет принято решение о запуске метода `HandleAsync`.</returns>
        protected virtual bool ShouldInvoke(HandlerContext<TContent> context) => CommonConsts.Always;

        /// <summary>
        /// При переопределении позволяет задать дополнительные условия для запуска метода `HandleAsync`.
        /// По умолчанию производится проверка поле `Recipient`.
        /// </summary>
        /// <returns>Возвращает значение по которому будет принято решение о запуске метода `HandleAsync`.</returns>
        protected virtual bool ShouldValidateRecipient() => CommonConsts.Always;

        /// <summary>
        /// Производит проверку принятого получателя с настройками машины по умолчанию согласно документу.</see>
        /// </summary>
        /// <param name="recipient">Получатель сообщения.</param>
        /// <param name="options">Настройками машины по умолчанию.</param>
        /// <returns>Возвращает значение по которому будет принято решение об обработке принятого сообщения.</returns>
        protected virtual bool RecipientValidation(ContentOptions options, RecipientModel recipient)
        {
            if (ShouldValidateRecipient())
            {
                return BaseMessageUtilities.RecipientValidation(options, recipient);
            }

            return CommonConsts.CorrectRecipient;
        }

        #region Private Methods

        private async Task HandleWithFilterAsync(HandlerContext<TContent> context, CancellationToken cancellationToken = default)
        {
            var filter = _features.CreateFilter<TContent>(context.Provider);
            try
            {
                await filter.OnExecuting(context, cancellationToken);

                await HandleAsync(context, cancellationToken);

                await filter.OnExecuted(context, cancellationToken);
            }
            catch (Exception exception)
            {
                await filter.OnException(new ExceptionContext<TContent>(exception, context), cancellationToken);
            }
            finally
            {
                _features.ReleaseFilter(filter);
            }
        }

        #endregion
    }
}
