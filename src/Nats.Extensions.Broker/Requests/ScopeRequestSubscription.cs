﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Broker.Handlers.Base;
using Nats.Extensions.Broker.Utilities;
using Nats.Extensions.Helper.Models;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Russian.Post.MessageBroker.Requests
{
    internal sealed class ScopeRequestSubscription<TRequest, TResponse> : IRequestSubscription
        where TRequest : class
        where TResponse : class
    {
        private readonly int? _messageType;
        private readonly IServiceProvider _provider;
        private readonly IEnumerable<int> _allowedMessageTypes;

        public ScopeRequestSubscription(IServiceProvider provider)
        {
            _provider = provider;
            _messageType = typeof(TRequest).GetCustomAttribute<MessageTypeAttribute>()?.Value;

            if (_messageType.HasValue)
            {
                _allowedMessageTypes = new[] { _messageType.Value };
            }
        }

        public async Task<byte[]> HandleAsync(Message message, CancellationToken cancellationToken = default)
        {
            using var scope = _provider.CreateScope();

            var plainContext = ActivatorUtilities.CreateInstance<HandlerContext>(scope.ServiceProvider, message);

            if (_messageType.HasValue && plainContext.MessageType != _messageType)
            {
                return ActivatorUtilities.CreateInstance<DefaultResponseHelper<TResponse>>(_provider)
                    .CreateMessageTypeErrorResponse(plainContext.MessageType, _allowedMessageTypes);
            }

            var handler = scope.ServiceProvider.GetRequiredService<IRequestHandler<TRequest, TResponse>>();
            if (handler is AbstractRequestHandler<TRequest, TResponse> requestHandler)
            {
                return await (requestHandler as IRequestHandler).HandleAsync(plainContext, cancellationToken);
            }
            else
            {
                var context = new HandlerContext<TRequest, TResponse>(plainContext);
                try
                {
                    context.Response.Content = await handler.HandleAsync(context, cancellationToken);
                }
                catch (Exception exception)
                {
                    context.Response.Error = BaseMessageUtilities.HandleException(exception);
                }

                return context.Serializer.SerializeToUtf8Bytes(context.Response);
            }
        }
    }
}
