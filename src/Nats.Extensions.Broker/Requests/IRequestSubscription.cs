﻿using Nats.Extensions.Helper.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Russian.Post.MessageBroker.Requests
{
    public interface IRequestSubscription
    {
        Task<byte[]> HandleAsync(Message message, CancellationToken cancellationToken = default);
    }
}
