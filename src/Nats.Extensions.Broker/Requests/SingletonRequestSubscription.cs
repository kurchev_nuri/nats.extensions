﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Broker.Handlers.Base;
using Nats.Extensions.Broker.Utilities;
using Nats.Extensions.Helper.Models;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Russian.Post.MessageBroker.Requests
{
    internal sealed class SingletonRequestSubscription<TRequest, TResponse> : IRequestSubscription
        where TRequest : class
        where TResponse : class
    {
        private readonly IServiceProvider _provider;
        private readonly IRequestHandler<TRequest, TResponse> _handler;

        private readonly int? _messageType;
        private readonly IEnumerable<int> _allowedMessageTypes;

        public SingletonRequestSubscription(IServiceProvider provider, IRequestHandler<TRequest, TResponse> handler)
        {
            _handler = handler;
            _provider = provider;
            _messageType = typeof(TRequest).GetCustomAttribute<MessageTypeAttribute>()?.Value;

            if (_messageType.HasValue)
            {
                _allowedMessageTypes = new[] { _messageType.Value };
            }
        }

        public async Task<byte[]> HandleAsync(Message message, CancellationToken cancellationToken)
        {
            var plainContext = ActivatorUtilities.CreateInstance<HandlerContext>(_provider, message);

            if (_messageType.HasValue && plainContext.MessageType != _messageType)
            {
                return ActivatorUtilities.CreateInstance<DefaultResponseHelper<TResponse>>(_provider)
                    .CreateMessageTypeErrorResponse(plainContext.MessageType, _allowedMessageTypes);
            }

            if (_handler is AbstractRequestHandler<TRequest, TResponse> requestHandler)
            {
                return await (requestHandler as IRequestHandler).HandleAsync(plainContext, cancellationToken);
            }
            else
            {
                var context = new HandlerContext<TRequest, TResponse>(plainContext);
                try
                {
                    context.Response.Content = await _handler.HandleAsync(context, cancellationToken);
                }
                catch (Exception exception)
                {
                    context.Response.Error = BaseMessageUtilities.HandleException(exception);
                }

                return context.Serializer.SerializeToUtf8Bytes(context.Response);
            }
        }
    }
}
