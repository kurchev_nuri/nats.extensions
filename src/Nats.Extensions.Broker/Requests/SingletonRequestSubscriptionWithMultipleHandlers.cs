﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Broker.Utilities;
using Nats.Extensions.Helper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Russian.Post.MessageBroker.Requests
{
    internal sealed class SingletonRequestSubscriptionWithMultipleHandlers : IRequestSubscription
    {
        private readonly IServiceProvider _provider;
        private readonly Dictionary<int, List<IRequestHandler>> _handlers;

        public SingletonRequestSubscriptionWithMultipleHandlers(IServiceProvider provider, List<(int MessageType, Type HandlerType)> handlers)
        {
            _provider = provider;

            _handlers = handlers.GroupBy(k => k.MessageType, v => v.HandlerType)
                .ToDictionary(
                    group => group.Key,
                    group => group.Select(type => _provider.GetRequiredService(type)).OfType<IRequestHandler>().ToList()
                );
        }

        public async Task<byte[]> HandleAsync(Message model, CancellationToken cancellationToken = default)
        {
            var context = ActivatorUtilities.CreateInstance<HandlerContext>(_provider, model);

            if (_handlers.TryGetValue(context.MessageType, out var handlers))
            {
                var firstCompletedTask = await Task.WhenAny(handlers.Select(handler => handler.HandleAsync(context, cancellationToken)));

                return await firstCompletedTask;
            }

            return ActivatorUtilities.CreateInstance<DefaultResponseHelper<object>>(_provider)
                .CreateMessageTypeErrorResponse(context.MessageType, _handlers.Keys);
        }
    }
}
