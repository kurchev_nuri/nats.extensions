﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Contexts;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Broker.Utilities;
using Nats.Extensions.Helper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Russian.Post.MessageBroker.Requests
{
    internal sealed class ScopeRequestSubscriptionWithMultipleHandlers : IRequestSubscription
    {
        private readonly IServiceProvider _provider;
        private readonly Dictionary<int, List<Type>> _handlers;

        public ScopeRequestSubscriptionWithMultipleHandlers(IServiceProvider provider, List<(int MessageType, Type HandlerType)> handlers)
        {
            _provider = provider;

            _handlers = handlers.GroupBy(k => k.MessageType, v => v.HandlerType)
                .ToDictionary(
                    group => group.Key,
                    group => group.ToList()
                );
        }

        public async Task<byte[]> HandleAsync(Message message, CancellationToken cancellationToken = default)
        {
            using var scope = _provider.CreateScope();

            var context = ActivatorUtilities.CreateInstance<HandlerContext>(scope.ServiceProvider, message);

            if (_handlers.TryGetValue(context.MessageType, out var handlers))
            {
                var tasks = handlers.Select(type => scope.ServiceProvider.GetRequiredService(type))
                    .OfType<IRequestHandler>()
                    .Select(handler => handler.HandleAsync(context, cancellationToken));

                var firstCompletedTask = await Task.WhenAny(tasks);

                return await firstCompletedTask;
            }

            return ActivatorUtilities.CreateInstance<DefaultResponseHelper<object>>(_provider)
                .CreateMessageTypeErrorResponse(context.MessageType, _handlers.Keys);
        }
    }
}
