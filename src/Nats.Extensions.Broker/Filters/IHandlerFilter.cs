﻿using Nats.Extensions.Broker.Contexts;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Filters
{
    /// <summary>
    /// Фильтр, который может быть применен для метода `HandleAsync`.
    /// </summary>
    /// <typeparam name="TContent">Тип обрабатываемого сообщения.</typeparam>
    public interface IHandlerFilter<TContent>
        where TContent : class
    {
        /// <summary>
        /// Метод, который будет вызван при успешном завершении метода `HandleAsync`.
        /// </summary>
        /// <param name="context">Контекст обработки.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns></returns>
        Task OnExecuted(HandlerContext<TContent> context, CancellationToken cancellationToken = default);

        /// <summary>
        /// Метод, который будет вызван перед вызовом метода `HandleAsync`.
        /// </summary>
        /// <param name="context">Контекст обработки.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns></returns>
        Task OnExecuting(HandlerContext<TContent> context, CancellationToken cancellationToken = default);

        /// <summary>
        /// Метод, который будет вызван в случае возникновения `Exception` в методе `HandleAsync`.
        /// </summary>
        /// <param name="context">Контекст обработки.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns></returns>
        Task OnException(ExceptionContext<TContent> context, CancellationToken cancellationToken = default);
    }
}
