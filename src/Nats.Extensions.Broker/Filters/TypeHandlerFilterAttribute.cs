﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Features;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nats.Extensions.Broker.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class TypeHandlerFilterAttribute : Attribute, IFilterFeatures
    {
        public TypeHandlerFilterAttribute(Type type)
        {
            ImplementationType = type ?? throw new ArgumentNullException(nameof(type));
        }

        public TypeHandlerFilterAttribute(Type type, params object[] arguments)
            : this(type)
        {
            Arguments = arguments;
        }

        public Type ImplementationType { get; }

        public IEnumerable<object> Arguments { get; }

        public IHandlerFilter<TContent> CreateFilter<TContent>(IServiceProvider provider)
            where TContent : class
        {
            if (Arguments == default || !Arguments.Any())
            {
                return ActivatorUtilities.CreateInstance(provider, ImplementationType) as IHandlerFilter<TContent>;
            }

            return ActivatorUtilities.CreateInstance(provider, ImplementationType, Arguments.ToArray()) as IHandlerFilter<TContent>;
        }

        public void ReleaseFilter<TContent>(IHandlerFilter<TContent> filter)
            where TContent : class
        {
            (filter as IDisposable)?.Dispose();
        }
    }
}
