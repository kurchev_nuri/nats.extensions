﻿namespace Nats.Extensions.Broker.Common
{
    /// <summary>
    /// Основные типы сообщений используемые платформой
    /// </summary>
    public static class CommonMessageTypes
    {
        public const int UnknownType = 0;
        public const int Logs = 1;
        public const int ForOptions = 2;
        public const int HandlerState = 3;
        public const int RequestSettings = 4;
        public const int OnOptionsChanged = 5;
    }
}
