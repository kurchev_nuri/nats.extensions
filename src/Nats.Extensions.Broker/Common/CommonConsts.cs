﻿namespace Nats.Extensions.Broker.Common
{
    internal static class CommonConsts
    {
        public const string DefaultRequestErrorType = "UnknownError";

        public const bool Never = false;
        public const bool Always = true;
        public const bool CorrectRecipient = true;

        public const string ProjectType = "project";
    }
}
