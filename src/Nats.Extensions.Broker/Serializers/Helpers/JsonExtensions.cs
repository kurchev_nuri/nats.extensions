﻿using Nats.Extensions.Broker.Serializers.Converters.SystemText;
using System.Collections.Generic;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Serializers.Helpers
{
    internal static class JsonExtensions
    {
        public static string PropertyName(PropertyInfo info, JsonNamingPolicy policy)
        {
            return info.GetCustomAttribute<JsonPropertyNameAttribute>(false)?.Name ?? policy?.ConvertName(info.Name) ?? info.Name;
        }

        public static JsonSerializerOptions SetConverters(this JsonSerializerOptions options)
        {
            options.Converters.Add(new InfoModelConverterFactory());
            options.Converters.Add(new SagaModelConverterFactory());
            options.Converters.Add(new ExceptionConverterFactory());
            options.Converters.Add(new BaseMessageConverterFactory());
            options.Converters.Add(new BaseResponseConverterFactory());

            return options;
        }

        public static JsonSerializerOptions CloneOptions(this JsonSerializerOptions options)
        {
            return new JsonSerializerOptions
            {
                Encoder = options.Encoder,
                MaxDepth = options.MaxDepth,
                WriteIndented = options.WriteIndented,
                IgnoreNullValues = options.IgnoreNullValues,
                DefaultBufferSize = options.DefaultBufferSize,
                AllowTrailingCommas = options.AllowTrailingCommas,
                DictionaryKeyPolicy = options.DictionaryKeyPolicy,
                ReadCommentHandling = options.ReadCommentHandling,
                PropertyNamingPolicy = options.PropertyNamingPolicy,
                IgnoreReadOnlyProperties = options.IgnoreReadOnlyProperties,
                PropertyNameCaseInsensitive = options.PropertyNameCaseInsensitive,
            };
        }

        public static JsonSerializerOptions ExtendConverters(this JsonSerializerOptions options, JsonConverter excludedConverter, IList<JsonConverter> converters)
        {
            foreach (var converter in converters)
            {
                if (converter != excludedConverter)
                {
                    options.Converters.Add(converter);
                }
            }

            return options;
        }
    }
}
