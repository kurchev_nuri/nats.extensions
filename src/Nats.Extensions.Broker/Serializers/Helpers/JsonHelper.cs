﻿using System.Linq;
using System.Text;

namespace Nats.Extensions.Broker.Serializers.Helpers
{
    public static class JsonHelper
    {
        public static string FromSnakeCaseToPascalCase(string value)
        {
            var propertyName = value.Split('_')
                .Aggregate(new StringBuilder(value.Length), (result, name) =>
                {
                    result.Append(string.Concat(char.ToUpperInvariant(name[0]), name.Substring(1)));
                    return result;
                });

            return propertyName.ToString();
        }
    }
}
