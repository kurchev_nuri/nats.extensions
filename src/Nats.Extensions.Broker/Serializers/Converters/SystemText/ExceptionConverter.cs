﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Serializers.Converters.SystemText
{
    internal sealed class ExceptionConverter : JsonConverter<Exception>
    {
        public override Exception Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) => throw new NotImplementedException();

        public override void Write(Utf8JsonWriter writer, Exception value, JsonSerializerOptions options)
        {
            writer.WriteStartObject();

            WriteString(nameof(value.Source), value.Source);
            WriteString(nameof(value.Message), value.Message);
            WriteString(nameof(value.HelpLink), value.HelpLink);
            WriteString(nameof(value.StackTrace), value.StackTrace);

            writer.WriteNumber(nameof(value.HResult), value.HResult);

            if (value.Data != default)
            {
                writer.WritePropertyName(nameof(value.Data));
                writer.WriteStartObject();

                foreach (var key in value.Data.Keys)
                {
                    writer.WritePropertyName(key.ToString());
                    JsonSerializer.Serialize(writer, value.Data[key], options);
                }

                writer.WriteEndObject();
            }

            if (value.InnerException != default)
            {
                writer.WritePropertyName(nameof(value.InnerException));
                JsonSerializer.Serialize(writer, value.InnerException, options);
            }

            writer.WriteEndObject();

            void WriteString(string propertyName, string propertyValue)
            {
                if (!string.IsNullOrWhiteSpace(propertyValue))
                {
                    writer.WriteString(propertyName, propertyValue);
                }
            }
        }
    }
}
