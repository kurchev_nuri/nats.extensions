﻿using Nats.Extensions.Broker.Models;
using Nats.Extensions.Broker.Serializers.Helpers;
using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Serializers.Converters.SystemText
{
    internal sealed class SagaModelConverterFactory : JsonConverterFactory
    {
        public override bool CanConvert(Type typeToConvert) => typeToConvert == typeof(SagaModel);

        public override JsonConverter CreateConverter(Type typeToConvert, JsonSerializerOptions options)
        {
            var converterOptions = options.CloneOptions().ExtendConverters(this, options.Converters);

            return (JsonConverter)Activator.CreateInstance(typeof(SagaModelConverter), converterOptions)!;
        }
    }
}
