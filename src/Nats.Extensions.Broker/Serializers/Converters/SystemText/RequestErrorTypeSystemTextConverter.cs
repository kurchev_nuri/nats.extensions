﻿using Nats.Extensions.Broker.Common;
using Nats.Extensions.Broker.Serializers.Helpers;
using Nats.Extensions.Common.Serializers.Helper;
using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Serializers.Converters.SystemText
{
    internal sealed class RequestErrorTypeSystemTextConverter : JsonConverter<string>
    {
        private readonly JsonSnakeCaseNamingPolicy _policy = new();

        public override string Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var value = reader.GetString();
            if (string.IsNullOrWhiteSpace(value))
            {
                return CommonConsts.DefaultRequestErrorType;
            }

            return JsonHelper.FromSnakeCaseToPascalCase(value);
        }

        public override void Write(Utf8JsonWriter writer, string value, JsonSerializerOptions options)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                writer.WriteStringValue(_policy.ConvertName(CommonConsts.DefaultRequestErrorType));
            }
            else
            {
                writer.WriteStringValue(_policy.ConvertName(value));
            }
        }
    }
}
