﻿using Nats.Extensions.Broker.Models;
using Nats.Extensions.Broker.Serializers.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Serializers.Converters.SystemText
{
    internal sealed class BaseResponseConverter<TContent> : JsonConverter<BaseResponse<TContent>>
    {
        private readonly JsonSerializerOptions _options;

        private readonly IDictionary<string, string> _propertiesNames;
        private readonly IDictionary<string, PropertyInfo> _propertiesInfos;

        private readonly BindingFlags _flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly;

        public BaseResponseConverter(JsonSerializerOptions options)
        {
            _options = options;

            _propertiesNames = typeof(BaseResponse<TContent>)
                .GetProperties()
                .ToDictionary(property => property.Name, property => JsonExtensions.PropertyName(property, options.PropertyNamingPolicy));

            _propertiesInfos = typeof(BaseMessage).GetProperties(_flags)
                .Concat(typeof(BaseMessage<TContent>).GetProperties(_flags))
                .Concat(typeof(BaseResponse<TContent>).GetProperties(_flags))
                .ToDictionary(info => JsonExtensions.PropertyName(info, options.PropertyNamingPolicy));
        }

        public override BaseResponse<TContent> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var result = JsonSerializer.Deserialize<BaseResponse<TContent>>(ref reader, _options);

            ThrowIfValueIsInvalid(result);

            return result;
        }

        public override void Write(Utf8JsonWriter writer, BaseResponse<TContent> value, JsonSerializerOptions options)
        {
            ThrowIfValueIsInvalid(value);

            writer.WriteStartObject();

            foreach (var (propertyName, propertyInfo) in _propertiesInfos)
            {
                var propertyValue = propertyInfo.GetValue(value);

                if (_options.IgnoreNullValues)
                {
                    if (propertyValue != default)
                    {
                        writer.WritePropertyName(propertyName);
                        JsonSerializer.Serialize(writer, propertyValue, _options);
                    }
                }
                else
                {
                    writer.WritePropertyName(propertyName);
                    JsonSerializer.Serialize(writer, propertyValue, _options);
                }
            }

            writer.WriteEndObject();
        }

        #region Private Methods

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private void ThrowIfValueIsInvalid(BaseResponse<TContent> message)
        {
            if (message == default)
            {
                throw new JsonException($"`{nameof(message)}` не может быть равным null.");
            }

            if (message.Saga == default)
            {
                throw new JsonException($"`{nameof(message)}.{_propertiesNames[nameof(message.Saga)]}` не может быть равным null.");
            }

            if (message.Info == default)
            {
                throw new JsonException($"`{nameof(message)}.{_propertiesNames[nameof(message.Info)]}` не может быть равным null.");
            }
        }

        #endregion
    }
}
