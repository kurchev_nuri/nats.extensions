﻿using Nats.Extensions.Broker.Models;
using Nats.Extensions.Broker.Serializers.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Serializers.Converters.SystemText
{
    internal sealed class SagaModelConverter : JsonConverter<SagaModel>
    {
        private readonly JsonSerializerOptions _options;
        private readonly IDictionary<string, string> _properties;

        public SagaModelConverter(JsonSerializerOptions options)
        {
            _options = options;
            _properties = typeof(SagaModel)
                .GetProperties()
                .ToDictionary(property => property.Name, property => JsonExtensions.PropertyName(property, options.PropertyNamingPolicy));
        }

        public override SagaModel Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var result = JsonSerializer.Deserialize<SagaModel>(ref reader, _options);

            ThrowIfValueIsInvalid(result);

            return result;
        }

        public override void Write(Utf8JsonWriter writer, SagaModel value, JsonSerializerOptions options)
        {
            ThrowIfValueIsInvalid(value);

            JsonSerializer.Serialize(writer, value, _options);
        }

        #region Private Methods

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private void ThrowIfValueIsInvalid(SagaModel saga)
        {
            if (saga == default)
            {
                throw new JsonException($"`{nameof(saga)}` не может быть пустым или null.");
            }

            if (saga.SagaId == Guid.Empty)
            {
                throw new JsonException($"`{nameof(saga)}.{_properties[nameof(saga.SagaId)]}` не может быть {Guid.Empty} или null.");
            }

            if (saga.EventId == Guid.Empty)
            {
                throw new JsonException($"`{nameof(saga)}.{_properties[nameof(saga.EventId)]}` не может быть {Guid.Empty} или null.");
            }
        }

        #endregion
    }
}
