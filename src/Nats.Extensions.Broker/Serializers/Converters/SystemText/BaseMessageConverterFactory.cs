﻿using Nats.Extensions.Broker.Models;
using Nats.Extensions.Broker.Serializers.Helpers;
using System;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Serializers.Converters.SystemText
{
    internal sealed class BaseMessageConverterFactory : JsonConverterFactory
    {
        public override bool CanConvert(Type typeToConvert)
        {
            if (!typeToConvert.IsGenericType)
            {
                return false;
            }

            if (typeToConvert.GenericTypeArguments.Length > 1)
            {
                return false;
            }

            return typeToConvert == typeof(BaseMessage<>).MakeGenericType(typeToConvert.GenericTypeArguments.First());
        }

        public override JsonConverter CreateConverter(Type typeToConvert, JsonSerializerOptions options)
        {
            var converterType = typeof(BaseMessageConverter<>)
                .MakeGenericType(typeToConvert.GenericTypeArguments.First());

            var converterOptions = options.CloneOptions().ExtendConverters(this, options.Converters);

            return (JsonConverter)Activator.CreateInstance(converterType, converterOptions);
        }
    }
}
