﻿using Nats.Extensions.Broker.Models;
using Nats.Extensions.Broker.Serializers.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Nats.Extensions.Broker.Serializers.Converters.SystemText
{
    internal sealed class InfoModelConverter : JsonConverter<InfoModel>
    {
        private readonly JsonSerializerOptions _options;
        private readonly IDictionary<string, string> _properties;

        public InfoModelConverter(JsonSerializerOptions options)
        {
            _options = options;

            _properties = typeof(SagaModel)
                .GetProperties()
                .ToDictionary(property => property.Name, property => JsonExtensions.PropertyName(property, options.PropertyNamingPolicy));
        }

        public override InfoModel Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var result = JsonSerializer.Deserialize<InfoModel>(ref reader, _options);

            ThrowIfValueIsInvalid(result);

            return result;
        }

        public override void Write(Utf8JsonWriter writer, InfoModel value, JsonSerializerOptions options)
        {
            ThrowIfValueIsInvalid(value);

            JsonSerializer.Serialize(writer, value, _options);
        }

        #region Private Methods

        [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
        private void ThrowIfValueIsInvalid(InfoModel info)
        {
            if (info == default)
            {
                throw new JsonException($"`{nameof(info)}` не может быть равным null.");
            }

            if (string.IsNullOrWhiteSpace(info.HostName))
            {
                throw new JsonException($"`{nameof(info)}.{_properties[nameof(info.HostName)]}` не может быть пустым или null.");
            }

            if (string.IsNullOrWhiteSpace(info.ClientId))
            {
                throw new JsonException($"`{nameof(info)}.{_properties[nameof(info.ClientId)]}` не может быть пустым или null.");
            }

            if (info.CreationDateTime == default)
            {
                throw new JsonException($"`{nameof(info)}.{_properties[nameof(info.CreationDateTime)]}` не может быть `{default(DateTimeOffset)}` или null.");
            }
        }

        #endregion
    }
}
