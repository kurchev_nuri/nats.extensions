﻿using Nats.Extensions.Broker.Common;
using Nats.Extensions.Broker.Serializers.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace Nats.Extensions.Broker.Serializers.Converters.Newtonsoft
{
    internal sealed class RequestErrorTypeNewtonsoftConverter : JsonConverter<string>
    {
        private readonly SnakeCaseNamingStrategy _strategy = new();

        public override string ReadJson(JsonReader reader, Type objectType, string existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            existingValue = serializer.Deserialize<string>(reader);

            if (existingValue == default)
            {
                return CommonConsts.DefaultRequestErrorType;
            }

            return JsonHelper.FromSnakeCaseToPascalCase(existingValue);
        }

        public override void WriteJson(JsonWriter writer, string value, JsonSerializer serializer)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                serializer.Serialize(writer, _strategy.GetPropertyName(value, false));
            }
            else
            {
                serializer.Serialize(writer, _strategy.GetPropertyName(CommonConsts.DefaultRequestErrorType, false));
            }
        }
    }
}
