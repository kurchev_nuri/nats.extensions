﻿using Nats.Extensions.Broker.Serializers.Writers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;

namespace Nats.Extensions.Broker.Serializers.Converters.Newtonsoft
{
    internal sealed class DictionaryJsonConverter : JsonConverter<IDictionary>
    {
        public override bool CanWrite => false;

        public override IDictionary ReadJson(JsonReader reader, Type objectType, IDictionary existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var value = JToken.Load(reader);
            using (var writer = new FromSnakeCaseToPascalCaseJTokenWriter())
            {
                value.WriteTo(writer);

                return writer.CurrentToken.ToObject<IDictionary>();
            }
        }

        public override void WriteJson(JsonWriter writer, IDictionary value, JsonSerializer serializer) => throw new NotImplementedException();
    }
}
