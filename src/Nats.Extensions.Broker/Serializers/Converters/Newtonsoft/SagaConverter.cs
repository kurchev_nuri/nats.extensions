﻿using Nats.Extensions.Broker.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Nats.Extensions.Broker.Serializers.Converters.Newtonsoft
{
    internal class SagaConverter : JsonConverter<SagaModel>
    {
        private readonly IDictionary<string, string> _properties;

        public SagaConverter()
        {
            _properties = typeof(SagaModel).GetProperties()
                .Where(u => u.GetCustomAttribute<JsonPropertyAttribute>(false) != default)
                .ToDictionary(u => u.Name, u => u.GetCustomAttribute<JsonPropertyAttribute>(false).PropertyName);
        }

        public override SagaModel ReadJson(JsonReader reader, Type objectType, SagaModel existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            existingValue = serializer.Deserialize<SagaModel>(reader);

            ThrowIfValueIsInvalid(existingValue, serializer);

            return existingValue;
        }


        public override void WriteJson(JsonWriter writer, SagaModel value, JsonSerializer serializer)
        {
            ThrowIfValueIsInvalid(value, serializer);

            serializer.Serialize(writer, value);
        }

        private void ThrowIfValueIsInvalid(SagaModel value, JsonSerializer serializer)
        {
            if (value == default)
            {
                throw new InvalidOperationException("`saga` cannot be null.");
            }

            if (value.SagaId == Guid.Empty)
            {
                ThrowException(nameof(value.SagaId), serializer);
            }

            if (value.EventId == Guid.Empty)
            {
                ThrowException(nameof(value.EventId), serializer);
            }
        }

        private void ThrowException(string propertyName, JsonSerializer serializer)
        {
            if (_properties.TryGetValue(propertyName, out var resolvedName))
            {
                throw new InvalidOperationException($"`saga.{resolvedName}` cannot be equal: {Guid.Empty}");
            }
            else
            {
                resolvedName = (serializer.ContractResolver as DefaultContractResolver)
                    .GetResolvedPropertyName(propertyName);

                _properties.Add(propertyName, resolvedName);

                throw new InvalidOperationException($"`saga.{resolvedName}` cannot be equal: {Guid.Empty}");
            }
        }
    }
}
