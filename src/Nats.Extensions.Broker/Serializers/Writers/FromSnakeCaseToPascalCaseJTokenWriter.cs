﻿using Nats.Extensions.Broker.Serializers.Helpers;
using Newtonsoft.Json.Linq;

namespace Nats.Extensions.Broker.Serializers.Writers
{
    public sealed class FromSnakeCaseToPascalCaseJTokenWriter : JTokenWriter
    {
        public override void WritePropertyName(string name)
        {
            base.WritePropertyName(JsonHelper.FromSnakeCaseToPascalCase(name));
        }
    }
}
