﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.DependencyModel;
using Nats.Extensions.Broker.Activators.Requests;
using Nats.Extensions.Broker.Activators.Subscriptions;
using Nats.Extensions.Broker.Builders;
using Nats.Extensions.Broker.Builders.Requests;
using Nats.Extensions.Broker.Builders.Serializers;
using Nats.Extensions.Broker.Builders.Subscriptions;
using Nats.Extensions.Broker.Common;
using Nats.Extensions.Broker.Factories;
using Nats.Extensions.Broker.Handlers;
using Nats.Extensions.Broker.Implementations;
using Nats.Extensions.Broker.Options;
using Nats.Extensions.Broker.Validators;
using Nats.Extensions.Common.Utilities;
using Nats.Extensions.Helper;
using Nats.Extensions.Helper.Extensions;
using Nats.Extensions.Helper.Options;
using Scrutor;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nats.Extensions.Broker.Extensions
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Регистрация настроек подключений к stan или nats
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configure">Конфигурация подключения</param>
        /// <returns></returns>
        public static IServiceCollection AddNatsExtensionsBroker(this IServiceCollection services, Action<BrokerBuilder> configure)
        {
            ThrowIfConfigureIsEmpty(configure);

            var builder = new BrokerBuilder(services);

            services.AddNatsExtensionsHelper(_ => configure(builder));
            services.AddContentFactory(options =>
            {
                options.HostName = SystemUtilities.HostName;
                options.ClientId = builder.ConnectionBuilder.ClientId;
                options.ThrowExceptionIfDeserializationError = builder.ThrowExceptionIfDeserializationError;
            });

            return services;
        }

        /// <summary>
        /// Регистрация обработчиков со `ScopedLifetime`.
        /// </summary>
        /// <param name="configure">Конфигурация сериализатора. По умолчанию используется Newtonsoft.</param>
        /// <returns></returns>
        public static IServiceCollection AddHandlersWithScopedLifetime(this IServiceCollection services, Action<SerializersBuilder> configure = default)
        {
            var builder = new SerializersBuilder(services);
            if (configure != default)
            {
                configure(builder);
            }
            else
            {
                builder.AddNewtonsoftSerializer();
            }

            if (services.Any(descriptor => descriptor.ServiceType == typeof(IMessageQueueHelper<NatsExtensionsOptions>)))
            {
                services.Replace(ServiceDescriptor.Singleton<IRequestService, DefaultRequestService>());

                services.Replace(ServiceDescriptor.Singleton<IPublisher<NatsExtensionsOptions>, DefaultPublisher<NatsExtensionsOptions>>());
                services.Replace(ServiceDescriptor.Singleton<ISubscriber<NatsExtensionsOptions>, DefaultSubscriber<NatsExtensionsOptions>>());

                services.Replace(ServiceDescriptor.Singleton<IRequestSubscriptionBuilder, RequestSubscriptionBuilder>());
                services.Replace(ServiceDescriptor.Singleton<IRequestSubscriptionActivator, ScopeRequestSubscriptionActivator>());

                services.Replace(ServiceDescriptor.Singleton<ISubscriptionBuilder<NatsExtensionsOptions>, SubscriptionBuilder<NatsExtensionsOptions>>());
                services.Replace(ServiceDescriptor.Singleton<ISubscriptionActivator<NatsExtensionsOptions>, ScopeSubscriptionActivator<NatsExtensionsOptions>>());

                services.AddRequestHandlers(ServiceLifetime.Scoped);
            }

            if (services.Any(descriptor => descriptor.ServiceType == typeof(IMessageQueueHelper<StanExtensionsOptions>)))
            {
                services.Replace(ServiceDescriptor.Singleton<IPublisher<StanExtensionsOptions>, DefaultPublisher<StanExtensionsOptions>>());
                services.Replace(ServiceDescriptor.Singleton<ISubscriber<StanExtensionsOptions>, DefaultSubscriber<StanExtensionsOptions>>());

                services.Replace(ServiceDescriptor.Singleton<ISubscriptionBuilder<StanExtensionsOptions>, SubscriptionBuilder<StanExtensionsOptions>>());
                services.Replace(ServiceDescriptor.Singleton<ISubscriptionActivator<StanExtensionsOptions>, ScopeSubscriptionActivator<StanExtensionsOptions>>());
            }

            return services.AddSubjectHandlers(ServiceLifetime.Scoped);
        }

        /// <summary>
        /// Регистрация обработчиков со `ScopedLifetime`.
        /// </summary>
        /// <param name="configure">Конфигурация сериализатора. По умолчанию используется Newtonsoft.</param>
        /// <returns></returns>
        public static IServiceCollection AddHandlersWithSingletonLifetime(this IServiceCollection services, Action<SerializersBuilder> configure = default)
        {
            var builder = new SerializersBuilder(services);
            if (configure != default)
            {
                configure(builder);
            }
            else
            {
                builder.AddNewtonsoftSerializer();
            }

            if (services.Any(descriptor => descriptor.ServiceType == typeof(IMessageQueueHelper<NatsExtensionsOptions>)))
            {
                services.Replace(ServiceDescriptor.Singleton<IRequestService, DefaultRequestService>());

                services.Replace(ServiceDescriptor.Singleton<IPublisher<NatsExtensionsOptions>, DefaultPublisher<NatsExtensionsOptions>>());
                services.Replace(ServiceDescriptor.Singleton<ISubscriber<NatsExtensionsOptions>, DefaultSubscriber<NatsExtensionsOptions>>());

                services.Replace(ServiceDescriptor.Singleton<IRequestSubscriptionBuilder, RequestSubscriptionBuilder>());
                services.Replace(ServiceDescriptor.Singleton<IRequestSubscriptionActivator, SingletonRequestSubscriptionActivator>());

                services.Replace(ServiceDescriptor.Singleton<ISubscriptionBuilder<NatsExtensionsOptions>, SubscriptionBuilder<NatsExtensionsOptions>>());
                services.Replace(ServiceDescriptor.Singleton<ISubscriptionActivator<NatsExtensionsOptions>, SingletonSubscriptionActivator<NatsExtensionsOptions>>());

                services.AddRequestHandlers(ServiceLifetime.Singleton);
            }

            if (services.Any(descriptor => descriptor.ServiceType == typeof(IMessageQueueHelper<StanExtensionsOptions>)))
            {
                services.Replace(ServiceDescriptor.Singleton<IPublisher<StanExtensionsOptions>, DefaultPublisher<StanExtensionsOptions>>());
                services.Replace(ServiceDescriptor.Singleton<ISubscriber<StanExtensionsOptions>, DefaultSubscriber<StanExtensionsOptions>>());

                services.Replace(ServiceDescriptor.Singleton<ISubscriptionBuilder<StanExtensionsOptions>, SubscriptionBuilder<StanExtensionsOptions>>());
                services.Replace(ServiceDescriptor.Singleton<ISubscriptionActivator<StanExtensionsOptions>, SingletonSubscriptionActivator<StanExtensionsOptions>>());
            }

            return services.AddSubjectHandlers(ServiceLifetime.Singleton);
        }

        internal static IServiceCollection AddContentFactory(this IServiceCollection services, Action<ContentOptions> configure)
        {
            ThrowIfConfigureIsEmpty(configure);

            services.AddOptions<ContentOptions>()
                .Configure(configure)
                .Validate<IValidator<ContentOptions>>(Validator);

            services.TryAddSingleton<IContentFactory, ContentFactory>();
            services.TryAddSingleton<IValidator<ContentOptions>, ContentOptionsValidator>();

            return services;
        }

        #region Private Methods

        private static readonly List<RuntimeLibrary> _allowedDependencies = DependencyContext.Default
            .RuntimeLibraries
            .Where(library => library.Type == CommonConsts.ProjectType)
            .ToList();

        private static void ThrowIfConfigureIsEmpty<T>(Action<T> configure)
        {
            if (configure == default)
            {
                throw new ArgumentNullException(nameof(configure), $"Входной параметр {nameof(configure)} не может быть равным null");
            }
        }

        private static bool Validator<TOption>(TOption options, IValidator<TOption> validator)
        {
            validator.ValidateAndThrow(options);
            return true;
        }

        private static IServiceCollection AddSubjectHandlers(this IServiceCollection services, ServiceLifetime lifetime)
        {
            return services.Scan(u => u.FromApplicationDependencies(assembly => _allowedDependencies.Any(library => assembly.FullName.Contains(library.Name)))
               .AddClasses(type => type.AssignableTo(typeof(ISubjectHandler<>)))
                   .UsingRegistrationStrategy(RegistrationStrategy.Replace())
                   .As(type => type.GetInterfaces().Where(type => type.IsGenericType).ToArray())
                   .AsSelf()
               .WithLifetime(lifetime)
           );
        }

        private static IServiceCollection AddRequestHandlers(this IServiceCollection services, ServiceLifetime lifetime)
        {
            return services.Scan(u => u.FromApplicationDependencies(assembly => _allowedDependencies.Any(library => assembly.FullName.Contains(library.Name)))
               .AddClasses(type => type.AssignableTo(typeof(IRequestHandler<,>)))
                   .UsingRegistrationStrategy(RegistrationStrategy.Replace())
                   .As(types => types.GetInterfaces()
                       .Where(type => type.IsGenericType && type.GetGenericArguments().Length == 2)
                       .ToArray()
                    )
                   .AsSelf()
               .WithLifetime(lifetime)
           );
        }

        #endregion
    }
}
