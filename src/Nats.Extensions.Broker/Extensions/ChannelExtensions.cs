﻿using System;
using System.Reactive.Linq;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Extensions
{
    internal static class ChannelExtensions
    {
        public static IObservable<TResult> ToObservable<TResult>(this ChannelReader<TResult> reader)
        {
            return Observable.Create<TResult>((observer, cancellationToken) =>
            {
                return Task.Run(async () =>
                {
                    while (await reader.WaitToReadAsync(cancellationToken))
                    {
                        while (reader.TryRead(out TResult result))
                        {
                            observer.OnNext(result);
                        }
                    }
                });
            });
        }
    }
}
