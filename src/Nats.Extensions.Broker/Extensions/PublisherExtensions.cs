﻿using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Helper.Options;
using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker.Extensions
{
    public static class PublisherExtensions
    {
        /// <summary>
        /// Устанавливает sagaId для публикуемого сообщения.
        /// </summary>
        /// <typeparam name="TOptions">В зависимости от настроек сообщение будет отправлено в nats или stan.</typeparam>
        /// <param name="sagaId">Saga-Id</param>
        /// <returns></returns>
        public static (IPublisher<TOptions>, BaseMessage) BasedOn<TOptions>(this IPublisher<TOptions> publisher, Guid sagaId)
            where TOptions : class
        {
            return (publisher, new BaseMessage { Saga = new SagaModel { SagaId = sagaId } });
        }

        /// <summary>
        /// Устанавливает sagaId и event_id для публикуемого сообщения.
        /// </summary>
        /// <typeparam name="TOptions">В зависимости от настроек сообщение будет отправлено в nats или stan.</typeparam>
        /// <param name="sagaId">Saga-Id</param>
        /// <param name="eventId">Event-Id</param>
        /// <returns></returns>
        public static (IPublisher<TOptions>, BaseMessage) BasedOn<TOptions>(this IPublisher<TOptions> publisher, Guid sagaId, Guid eventId)
            where TOptions : class
        {
            return (publisher, new BaseMessage { Saga = new SagaModel { SagaId = sagaId, EventId = eventId } });
        }

        /// <summary>
        /// Связывает публикуемое сообщение с сообщением который указан в параметрах.
        /// Выставляет sagaId и event_prev в соответствии с сообщением что в параметрах 
        /// </summary>
        /// <typeparam name="TOptions">В зависимости от настроек сообщение будет отправлено в nats или stan.</typeparam>
        /// <param name="message">Сообщение чьи параметры будут привязаны к новому сообщению.</param>
        /// <returns></returns>
        public static (IPublisher<TOptions>, BaseMessage) BasedOn<TOptions>(this IPublisher<TOptions> publisher, BaseMessage message)
            where TOptions : class
        {
            return (publisher, message);
        }

        /// <summary>
        /// Создаёт новое сообщение на основе Recipient
        /// </summary>
        /// <typeparam name="TOptions">В зависимости от настроек сообщение будет отправлено в nats или stan.</typeparam>
        /// <param name="recipient"> Модель данных получателя </param>
        /// <returns>Сообщение чьи параметры будут привязаны к новому сообщению</returns>
        public static (IPublisher<TOptions>, BaseMessage) BasedOn<TOptions>(this IPublisher<TOptions> publisher, RecipientModel recipient)
            where TOptions : class
        {
            if (recipient == default)
            {
                throw new ArgumentNullException(nameof(recipient), "Переданный аргумент не может быть равен null.");
            }

            return (publisher, new BaseMessage
            {
                Recipient = recipient,
                Saga = new SagaModel { SagaId = Guid.NewGuid() }
            });
        }

        /// <summary>
        /// Добавляет поле Recipient в уже существующее сообщение
        /// </summary>
        /// <typeparam name="TOptions">В зависимости от настроек сообщение будет отправлено в nats или stan.</typeparam>
        /// <param name="recipient"> Модель данных получателя </param>
        /// <returns>Сообщение чьи параметры будут привязаны к переданному сообщению</returns>
        public static (IPublisher<TOptions>, BaseMessage) AddRecipient<TOptions>(this (IPublisher<TOptions>, BaseMessage) pair, RecipientModel recipient)
            where TOptions : class
        {
            var (publisher, message) = pair;

            if (message == default)
            {
                throw new ArgumentNullException(nameof(message), "Переданный аргумент не может быть равен null.");
            }

            message.Recipient = recipient ?? throw new ArgumentNullException(nameof(recipient), "Переданный аргумент не может быть равен null.");

            return (publisher, message);
        }

        /// <summary>
        /// Публикация (отправление) сообщений в очередь (NSS)
        /// </summary>
        /// <typeparam name="TOptions">В зависимости от настроек сообщение будет отправлено в nats или stan</typeparam>
        /// <typeparam name="TContent">Тип сообщения, должен совпадать с типом который обрабатывает обработчик</typeparam>
        /// <param name="subject">Канал в который будет отправлено сообщение</param>
        /// <param name="content">Сообщение</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns></returns>
        public static Task PublishAsync<TContent, TOptions>(this (IPublisher<TOptions>, BaseMessage) pair, string subject, TContent content, CancellationToken cancellationToken = default)
            where TOptions : class
        {
            var (publisher, message) = pair;
            if (message == default)
            {
                throw new ArgumentNullException(nameof(message), "Переданный аргумент не может быть равен null.");
            }

            var messageType = TypeDescriptor.GetAttributes(content).OfType<MessageTypeAttribute>().SingleOrDefault();
            if (messageType == default)
            {
                throw new ArgumentException($"Для типа контента `{typeof(TContent).Name}` должен быть определен атрибут `{nameof(MessageTypeAttribute)}` с заданным типом сообщения.");
            }

            return publisher.PublishAsync(subject, new BaseMessage<TContent>
            {
                Content = content,
                Recipient = message.Recipient,
                MessageType = messageType.Value,
                Saga = new SagaModel
                {
                    EventId = Guid.NewGuid(),
                    SagaId = message.Saga.SagaId,
                    PreviousEventId = message.Saga.EventId != Guid.Empty ? message.Saga.EventId : (Guid?)null
                }
            }, cancellationToken);
        }

        /// <summary>
        /// Устанавливает sagaId для публикуемого сообщения.
        /// </summary>
        /// <param name="sagaId">Saga-Id</param>
        /// <returns>Сообщение чьи параметры будут привязаны к новому сообщению</returns>
        public static (IRequestService, BaseMessage) BasedOn(this IRequestService service, Guid sagaId)
        {
            return (service, new BaseMessage { Saga = new SagaModel { SagaId = sagaId } });
        }

        /// <summary>
        /// Устанавливает sagaId и event_id для публикуемого сообщения.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="sagaId">Saga-Id</param>
        /// <param name="eventId">Event-Id</param>
        /// <returns>Сообщение чьи параметры будут привязаны к новому сообщению</returns>
        public static (IRequestService, BaseMessage) BasedOn(this IRequestService service, Guid sagaId, Guid eventId)
        {
            return (service, new BaseMessage { Saga = new SagaModel { SagaId = sagaId, EventId = eventId } });
        }

        /// <summary>
        /// Связывает публикуемое сообщение с сообщением который указан в параметрах.
        /// Выставляет sagaId и event_prev в соответствии с сообщением что в параметрах 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static (IRequestService, BaseMessage) BasedOn(this IRequestService service, BaseMessage message)
        {
            return (service, message);
        }

        /// <summary>
        /// Создает новое сообщение на основе модели Recipient
        /// </summary>
        /// <param name="recipient"> Модель данных получателя </param>
        /// <returns>Сообщение чьи параметры будут привязаны к новому сообщению</returns>
        public static (IRequestService, BaseMessage) BasedOn(this IRequestService publisher, RecipientModel recipient)
        {
            if (recipient == default)
            {
                throw new ArgumentNullException(nameof(recipient), "Переданный аргумент не может быть равен null.");
            }

            return (publisher, new BaseMessage
            {
                Recipient = recipient,
                Saga = new SagaModel { SagaId = Guid.NewGuid() }
            });
        }

        /// <summary>
        /// Добавляет поле Recipient в уже существующее сообщение
        /// </summary>
        /// <param name="recipient"> Модель данных получателя </param>
        /// <returns></returns>
        public static (IRequestService, BaseMessage) AddRecipient(this (IRequestService, BaseMessage) pair, RecipientModel recipient)
        {
            var (publisher, message) = pair;

            if (message == default)
            {
                throw new ArgumentNullException(nameof(message), "Переданный аргумент не может быть равен null.");
            }

            message.Recipient = recipient ?? throw new ArgumentNullException(nameof(recipient), "Переданный аргумент не может быть равен null.");

            return (publisher, message);
        }

        /// <summary>
        /// Публикация сообщения в шину (NSS) с ответом от обработчика.
        /// </summary>
        /// <typeparam name="TResponse">Тип ожидаемого от обработчика сообщения.</typeparam>
        /// <typeparam name="TRequest">Тип содержимого сообщения.</typeparam>
        /// <param name="subject">Канал в который будет опубликовано сообщение.</param>
        /// <param name="content">Содержимое сообщения.</param>
        /// <param name="options">Настройки, который могут быть переданны во время публикации сообщения.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns>Возвращает сообщение, которое было получено от обработчика.</returns>
        public static Task<BaseResponse<TResponse>> RequestAsync<TRequest, TResponse>(this (IRequestService, BaseMessage) pair, string subject, TRequest content, NatsRequestOptions options = default, CancellationToken cancellationToken = default)
            where TResponse : class
        {
            var (publisher, message) = pair;
            if (message == default)
            {
                throw new ArgumentNullException(nameof(message), "Переданный аргумент не может быть равен null.");
            }

            var messageType = TypeDescriptor.GetAttributes(content).OfType<MessageTypeAttribute>().SingleOrDefault();
            if (messageType == default)
            {
                throw new ArgumentException($"Для типа контента `{typeof(TRequest).Name}` должен быть определен атрибут `{nameof(MessageTypeAttribute)}` с заданным типом сообщения.");
            }

            return publisher.RequestAsync<TRequest, TResponse>(subject, new BaseMessage<TRequest>
            {
                Content = content,
                Recipient = message.Recipient,
                MessageType = messageType.Value,
                Saga = new SagaModel
                {
                    EventId = Guid.NewGuid(),
                    SagaId = message.Saga.SagaId,
                    PreviousEventId = message.Saga.EventId != Guid.Empty ? message.Saga.EventId : (Guid?)null
                }
            }, options, cancellationToken);
        }
    }
}
