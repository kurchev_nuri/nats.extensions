﻿using Nats.Extensions.Broker.Exceptions;
using Nats.Extensions.Broker.Exceptions.Base;
using Nats.Extensions.Broker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Nats.Extensions.Broker.Extensions
{
    public static class RequestExtensions
    {
        private static readonly Lazy<IDictionary<string, Type>> _exceptionTypes;

        static RequestExtensions()
        {
            _exceptionTypes = new Lazy<IDictionary<string, Type>>(ExceptionTypes, LazyThreadSafetyMode.ExecutionAndPublication);
        }

        /// <summary>
        /// Проверяет ответ на наличие ошибки, если она есть то генерируется исключение в соответствии с пришедшей ошибкой.
        /// </summary>
        /// <typeparam name="TResponse">Тип ожидаемого ответа.</typeparam>
        public static void EnsureSuccessResponse<TResponse>(this BaseResponse<TResponse> response)
            where TResponse : class
        {
            if (response.Error == default)
            {
                return;
            }

            if (_exceptionTypes.Value.TryGetValue(response.Error.Type, out var type))
            {
                var exception = response.Error.Data == default
                    ? Activator.CreateInstance(type, response.Error.Message)
                    : Activator.CreateInstance(type, response.Error.Message, response.Error.Data);

                throw exception as BaseRequestException;
            }
            else
            {
                throw new UnknownErrorException($"Произошла неизвестная ошибка. Тип ошибки : `{response.Error.Type}Exception`");
            }
        }

        private static IDictionary<string, Type> ExceptionTypes()
        {
            var types = AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(GetTypes);

            return types.Where(type => type.IsSubclassOf(typeof(BaseRequestException)))
                .ToDictionary(type => type.Name.Remove(type.Name.IndexOf(nameof(Exception))));

            static IEnumerable<Type> GetTypes(Assembly assembly)
            {
                try
                {

                    return assembly.GetTypes();
                }
                catch
                {
                    return Array.Empty<Type>();
                }
            }
        }
    }
}
