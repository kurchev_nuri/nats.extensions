﻿using System;

namespace Nats.Extensions.Broker.Attributes
{
    /// <summary>
    /// Атрибут указывающий тип сообщения. Является необходимой частью модели.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class MessageTypeAttribute : Attribute
    {
        public MessageTypeAttribute(int messageType) => Value = messageType;

        public int Value { get; }
    }
}
