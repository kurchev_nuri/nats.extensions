﻿using Nats.Extensions.Broker.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker
{
    /// <summary>
    /// Публикация (отправление) сообщений в очередь (NSS) 
    /// </summary>
    /// <typeparam name="TOptions">В зависимости от настроек сообщение будет отправлено в nats или stan</typeparam>
    public interface IPublisher<out TOptions>
         where TOptions : class
    {
        /// <summary>
        /// Публикация (отправление) сообщений в очередь (NSS)
        /// </summary>
        /// <param name="subject">Канал в который будет отправлено сообщение</param>
        /// <param name="content">Сообщение</param>
        /// <param name="cancellationToken"></param>
        /// <typeparam name="TContent">Тип сообщения, должен совпадать с типом который обрабатывает обработчик</typeparam>
        /// <returns></returns>
        Task PublishAsync<TContent>(string subject, TContent content, CancellationToken cancellationToken = default);

        /// <summary>
        /// Публикация (отправление) сообщений в очередь (NSS)
        /// </summary>
        /// <typeparam name="TContent">Тип сообщения</typeparam>
        /// <param name="subject">Канал в который будет отправлено сообщение</param>
        /// <param name="message">Сообщение</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task PublishAsync<TContent>(string subject, BaseMessage<TContent> message, CancellationToken cancellationToken = default);
    }
}
