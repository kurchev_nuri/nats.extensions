﻿using System;
using System.Collections.Generic;

namespace Nats.Extensions.Broker.Activators.Requests
{
    internal interface IRequestSubscriptionActivator
    {
        IDisposable CreateSubscription(string subject, List<(int, Type)> handlers, string queue = default);

        IDisposable CreateSubscription<TRequest, TResponse>(string subject, string queue = default)
            where TRequest : class
            where TResponse : class;
    }
}
