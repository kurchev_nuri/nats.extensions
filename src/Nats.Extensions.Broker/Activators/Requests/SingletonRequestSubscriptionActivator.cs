﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Utilities;
using Nats.Extensions.Helper;
using Russian.Post.MessageBroker.Requests;
using System;
using System.Collections.Generic;

namespace Nats.Extensions.Broker.Activators.Requests
{
    internal sealed class SingletonRequestSubscriptionActivator : IRequestSubscriptionActivator
    {
        private readonly IServiceProvider _provider;
        private readonly INatsRequestHelper _helper;

        public SingletonRequestSubscriptionActivator(INatsRequestHelper helper, IServiceProvider provider)
        {
            _helper = helper;
            _provider = provider;
        }

        public IDisposable CreateSubscription(string subject, List<(int, Type)> handlers, string queue = null)
        {
            var subscription = ActivatorUtilities.CreateInstance<SingletonRequestSubscriptionWithMultipleHandlers>(_provider, handlers);
            if (string.IsNullOrWhiteSpace(queue))
            {
                return _helper.Subscribe(subject, subscription.HandleAsync, exception =>
                {
                    return ActivatorUtilities.CreateInstance<DefaultResponseHelper<object>>(_provider).CreateInvalidRequestResponse(exception);
                });
            }
            else
            {
                return _helper.Subscribe(subject, queue, subscription.HandleAsync, exception =>
                {
                    return ActivatorUtilities.CreateInstance<DefaultResponseHelper<object>>(_provider).CreateInvalidRequestResponse(exception);
                });
            }
        }

        public IDisposable CreateSubscription<TRequest, TResponse>(string subject, string queue = default)
            where TRequest : class
            where TResponse : class
        {
            var subscription = ActivatorUtilities.CreateInstance<SingletonRequestSubscription<TRequest, TResponse>>(_provider);
            if (string.IsNullOrWhiteSpace(queue))
            {
                return _helper.Subscribe(subject, subscription.HandleAsync, exception =>
                {
                    return ActivatorUtilities.CreateInstance<DefaultResponseHelper<TResponse>>(_provider).CreateInvalidRequestResponse(exception);
                });
            }
            else
            {
                return _helper.Subscribe(subject, queue, subscription.HandleAsync, exception =>
                {
                    return ActivatorUtilities.CreateInstance<DefaultResponseHelper<TResponse>>(_provider).CreateInvalidRequestResponse(exception);
                });
            }
        }
    }
}
