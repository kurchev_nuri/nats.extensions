﻿using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Broker.Subscriptions;
using System;
using System.Collections.Generic;

namespace Nats.Extensions.Broker.Activators.Subscriptions
{
    internal sealed class SingletonSubscriptionActivator<TOptions> : ISubscriptionActivator<TOptions>
          where TOptions : class
    {
        private readonly IServiceProvider _provider;
        private readonly ISubscriber<TOptions> _subscriber;

        public SingletonSubscriptionActivator(IServiceProvider provider, ISubscriber<TOptions> subscriber)
        {
            _provider = provider;
            _subscriber = subscriber;
        }

        public IDisposable CreateSubscription(string subject, List<(int, Type)> handlers, string queue = default, TOptions options = default)
        {
            var subscription = ActivatorUtilities.CreateInstance<SingletonSubscriptionWithMultipleHandlers>(_provider, handlers);
            if (string.IsNullOrWhiteSpace(queue))
            {
                return _subscriber.Subscribe(subject, subscription, options);
            }
            else
            {
                return _subscriber.Subscribe(subject, queue, subscription, options);
            }
        }

        public IDisposable CreateSubscription<TContent>(string subject, string queue = default, TOptions options = default)
            where TContent : class
        {
            var subscription = ActivatorUtilities.CreateInstance<SingletonSubscription<TContent>>(_provider);
            if (string.IsNullOrWhiteSpace(queue))
            {
                return _subscriber.Subscribe(subject, subscription, options);
            }
            else
            {
                return _subscriber.Subscribe(subject, queue, subscription, options);
            }
        }
    }
}
