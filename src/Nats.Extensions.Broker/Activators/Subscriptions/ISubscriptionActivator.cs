﻿using System;
using System.Collections.Generic;

namespace Nats.Extensions.Broker.Activators.Subscriptions
{
    internal interface ISubscriptionActivator<TOptions>
        where TOptions : class
    {
        IDisposable CreateSubscription(string subject, List<(int, Type)> handlers, string queue = default, TOptions options = default);

        IDisposable CreateSubscription<TContent>(string subject, string queue = default, TOptions options = default)
            where TContent : class;
    }
}
