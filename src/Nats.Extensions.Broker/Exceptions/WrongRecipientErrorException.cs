﻿using Nats.Extensions.Broker.Exceptions.Base;
using System;
using System.Runtime.Serialization;

namespace Nats.Extensions.Broker.Exceptions
{
    public class WrongRecipientErrorException : BaseRequestException
    {
        public WrongRecipientErrorException()
        {
        }

        public WrongRecipientErrorException(object data) : base(data)
        {
        }

        public WrongRecipientErrorException(string message) : base(message)
        {
        }

        public WrongRecipientErrorException(string message, object data) : base(message, data)
        {
        }

        public WrongRecipientErrorException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public WrongRecipientErrorException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
