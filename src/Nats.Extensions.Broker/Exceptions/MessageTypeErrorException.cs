﻿using Nats.Extensions.Broker.Exceptions.Base;
using System;
using System.Runtime.Serialization;

namespace Nats.Extensions.Broker.Exceptions
{
    public sealed class MessageTypeErrorException : BaseRequestException
    {
        public MessageTypeErrorException()
        {
        }

        public MessageTypeErrorException(string message)
            : base(message)
        {
        }

        public MessageTypeErrorException(object data)
            : base(data)
        {
        }

        public MessageTypeErrorException(string message, object data)
            : base(message, data)
        {
        }

        public MessageTypeErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public MessageTypeErrorException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
