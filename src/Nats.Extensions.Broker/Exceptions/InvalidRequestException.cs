﻿using Nats.Extensions.Broker.Exceptions.Base;
using System;
using System.Runtime.Serialization;

namespace Nats.Extensions.Broker.Exceptions
{
    public class InvalidRequestException : BaseRequestException
    {
        public InvalidRequestException()
        {
        }

        public InvalidRequestException(object data) : base(data)
        {
        }

        public InvalidRequestException(string message) : base(message)
        {
        }

        public InvalidRequestException(string message, object data) : base(message, data)
        {
        }

        public InvalidRequestException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public InvalidRequestException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
