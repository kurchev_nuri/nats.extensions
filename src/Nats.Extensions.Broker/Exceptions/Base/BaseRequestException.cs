﻿using Nats.Extensions.Broker.Serializers.Converters.Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Runtime.Serialization;

namespace Nats.Extensions.Broker.Exceptions.Base
{
    public abstract class BaseRequestException : Exception
    {
        private readonly IDictionary _data;
        private static readonly JsonSerializer _serializer;

        static BaseRequestException()
        {
            _serializer = JsonSerializer.Create(new JsonSerializerSettings
            {
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore,
                DateParseHandling = DateParseHandling.DateTimeOffset,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Converters = new[] { new DictionaryJsonConverter() },
                ContractResolver = new DefaultContractResolver { NamingStrategy = new SnakeCaseNamingStrategy() }
            });
        }

        public override IDictionary Data => _data;

        protected BaseRequestException()
        {
        }

        public BaseRequestException(object data)
        {
            _data = CreateData(data);
        }

        public BaseRequestException(string message, object data)
            : base(message)
        {
            _data = CreateData(data);
        }

        protected BaseRequestException(string message)
            : base(message)
        {
        }

        protected BaseRequestException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        protected BaseRequestException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        #region Private Methods 

        private static IDictionary CreateData(object data)
        {
            if (data is JToken token)
            {
                return token.ToObject<IDictionary>(_serializer);
            }
            else
            {
                return JObject.FromObject(data, _serializer).ToObject<IDictionary>();
            }
        }

        #endregion
    }
}
