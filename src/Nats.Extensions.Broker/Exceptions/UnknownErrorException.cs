﻿using Nats.Extensions.Broker.Exceptions.Base;
using System;
using System.Runtime.Serialization;

namespace Nats.Extensions.Broker.Exceptions
{
    internal class UnknownErrorException : BaseRequestException
    {
        public UnknownErrorException()
        {
        }

        public UnknownErrorException(object data) : base(data)
        {
        }

        public UnknownErrorException(string message) : base(message)
        {
        }

        public UnknownErrorException(string message, object data) : base(message, data)
        {
        }

        public UnknownErrorException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public UnknownErrorException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}