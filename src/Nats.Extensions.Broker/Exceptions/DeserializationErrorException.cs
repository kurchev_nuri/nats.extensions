﻿using Nats.Extensions.Broker.Exceptions.Base;
using System;
using System.Runtime.Serialization;

namespace Nats.Extensions.Broker.Exceptions
{
    public class DeserializationErrorException : BaseRequestException
    {
        public DeserializationErrorException()
        {
        }

        public DeserializationErrorException(object data) : base(data)
        {
        }

        public DeserializationErrorException(string message) : base(message)
        {
        }

        public DeserializationErrorException(string message, object data) : base(message, data)
        {
        }

        public DeserializationErrorException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public DeserializationErrorException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
