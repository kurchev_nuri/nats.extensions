﻿namespace Nats.Extensions.Broker.Utilities
{
    internal static class SubscriptionUtilities
    {
        public static string MakeKey<THandler>(string subject)
            where THandler : class
        {
            return $"{subject}:{typeof(THandler).AssemblyQualifiedName}";
        }
    }
}
