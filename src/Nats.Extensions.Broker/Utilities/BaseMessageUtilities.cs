﻿using Nats.Extensions.Broker.Exceptions;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Broker.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nats.Extensions.Broker.Utilities
{
    internal static class BaseMessageUtilities
    {
        public static MessageTypeErrorException MessageTypeError(int messageType, IEnumerable<int> types)
        {
            var errorMessage = $"Ожидаемые типы сообщений [`{string.Join(',', types.Select(type => type.ToString()))}`] " +
                               $"принятый тип `{messageType}`. " +
                               $"Сообщение не может быть обработано.";

            return new MessageTypeErrorException(errorMessage);
        }

        public static WrongRecipientErrorException WrongRecipientError(ContentOptions options)
        {
            var recipient = new RecipientModel
            {
                HostName = options.HostName
            };

            return new WrongRecipientErrorException("Получен неверный получатель. Ожидаемые настройки получателя указаны в поле `Data`.", recipient);
        }

        public static ErrorModel HandleException(Exception exception)
        {
            var errorType = exception.GetType().Name;

            return new ErrorModel
            {
                Data = exception?.Data?.Count != 0 ? exception.Data : default,
                Type = errorType.Remove(errorType.IndexOf(nameof(Exception))),
                Message = exception?.Message ?? exception?.InnerException?.Message
            };
        }

        public static bool RecipientValidation(ContentOptions options, RecipientModel recipient)
        {
            var result = true;

            if (recipient == default)
            {
                return result;
            }

            if (recipient.HostName == string.Empty)
            {
                throw new WrongRecipientErrorException($"Значение поле `{nameof(recipient.HostName)}` не может быть пустым.");
            }

            if (result && recipient.HostName != default)
            {
                result = string.Equals(recipient.HostName, options.HostName, StringComparison.OrdinalIgnoreCase);
            }

            return result;
        }
    }
}
