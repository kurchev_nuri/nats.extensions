﻿using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Broker.Common;
using Nats.Extensions.Broker.Exceptions;
using Nats.Extensions.Broker.Factories;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Common.Serializers;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Nats.Extensions.Broker.Utilities
{
    internal sealed class DefaultResponseHelper<TResponse>
        where TResponse : class
    {
        private readonly ISerializer _serializer;
        private readonly IContentFactory _factory;

        public DefaultResponseHelper(ISerializer serializer, IContentFactory factory)
        {
            _factory = factory;
            _serializer = serializer;
        }

        public byte[] CreateInvalidRequestResponse(Exception exception)
        {
            var response = _factory.SetAuxiliaryInfo(new BaseResponse<TResponse>(new InvalidRequestException(exception.Message))
            {
                MessageType = typeof(TResponse).GetCustomAttribute<MessageTypeAttribute>()?.Value ?? CommonMessageTypes.UnknownType,
                Saga = new SagaModel
                {
                    SagaId = Guid.NewGuid(),
                    EventId = Guid.NewGuid()
                }
            });

            return _serializer.SerializeToUtf8Bytes(response);
        }

        public byte[] CreateMessageTypeErrorResponse(int messageType, IEnumerable<int> allowedMessageTypes)
        {
            var response = _factory.SetAuxiliaryInfo(new BaseResponse<TResponse>(BaseMessageUtilities.MessageTypeError(messageType, allowedMessageTypes))
            {
                MessageType = typeof(TResponse).GetCustomAttribute<MessageTypeAttribute>()?.Value ?? CommonMessageTypes.UnknownType,
                Saga = new SagaModel
                {
                    SagaId = Guid.NewGuid(),
                    EventId = Guid.NewGuid()
                }
            });

            return _serializer.SerializeToUtf8Bytes(response);
        }
    }
}
