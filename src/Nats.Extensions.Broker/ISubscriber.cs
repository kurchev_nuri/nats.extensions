﻿using Nats.Extensions.Broker.Subscriptions;
using System;

namespace Nats.Extensions.Broker
{
    /// <summary>
    /// Подписка на канал в шине (NSS).
    /// </summary>
    /// <typeparam name="TOptions">В зависимости от настроек будет подписка в nats или stan.</typeparam>
    public interface ISubscriber<in TOptions>
        where TOptions : class
    {
        /// <summary>
        /// Подписка на канал в шине (NSS).
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="subscription">Обработчик, который будет обрабатывать сообщение из канала.</param>
        /// <param name="options">Настройки, который могут быть переданны во время подписки.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.</returns>
        IDisposable Subscribe(string subject, ISubscription subscription, TOptions options = default);

        /// <summary>
        /// Подписка на канал в шине (NSS) c очередью.
        /// </summary>
        /// <param name="subject">Наименование канала.</param>
        /// <param name="queue">Наименование очереди.</param>
        /// <param name="subscription">Обработчик, который будет обрабатывать сообщение из канала.</param>
        /// <param name="options">Настройки, который могут быть переданны во время подписки.</param>
        /// <returns>Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.</returns>
        IDisposable Subscribe(string subject, string queue, ISubscription subscription, TOptions options = default);
    }
}
