﻿using System;

namespace Nats.Extensions.Broker.Contexts
{
    public class ExceptionContext<TContent>
        where TContent : class
    {
        public ExceptionContext(Exception exception, HandlerContext<TContent> handlerContext)
        {
            Exception = exception;
            HandlerContext = handlerContext;
        }

        public Exception Exception { get; }

        public HandlerContext<TContent> HandlerContext { get; }
    }
}
