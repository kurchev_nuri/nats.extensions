﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Broker.Common;
using Nats.Extensions.Broker.Exceptions;
using Nats.Extensions.Broker.Factories;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Broker.Options;
using Nats.Extensions.Common.Serializers;
using Nats.Extensions.Helper.Models;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Json;

namespace Nats.Extensions.Broker.Contexts
{
    public class HandlerContext
    {
        private const string type = nameof(type);

        public HandlerContext(Message message, ISerializer serializer, IServiceProvider provider, IOptions<ContentOptions> options)
        {
            Body = message.Data;
            Provider = provider;
            Serializer = serializer;
            Options = options.Value;
            Attributes = message.Attributes;
            MessageType = GetMessageType(message.Data);
            ThrowExceptionIfDeserializationError = options.Value.ThrowExceptionIfDeserializationError;
        }

        public HandlerContext(HandlerContext context)
        {
            Body = context.Body;
            Errors = context.Errors;
            Options = context.Options;
            Provider = context.Provider;
            Serializer = context.Serializer;
            Attributes = context.Attributes;
            MessageType = context.MessageType;
            ThrowExceptionIfDeserializationError = context.ThrowExceptionIfDeserializationError;
        }

        public int MessageType { get; }

        public ContentOptions Options { get; }

        public ReadOnlySequence<byte> Body { get; }

        public Dictionary<string, string> Attributes { get; }

        public List<Exception> Errors { get; protected set; }

        public bool IsValid => Errors == default || !Errors.Any();

        public ISerializer Serializer { get; }

        internal IServiceProvider Provider { get; }

        internal bool ThrowExceptionIfDeserializationError { get; }

        #region Static

        public static readonly JsonReaderState ReaderState = new(new JsonReaderOptions
        {
            AllowTrailingCommas = true,
            CommentHandling = JsonCommentHandling.Skip
        });

        #endregion

        #region Private Methods

        private int GetMessageType(in ReadOnlySequence<byte> message)
        {
            try
            {
                var reader = new Utf8JsonReader(message, isFinalBlock: true, ReaderState);
                if (reader.Read())
                {
                    while (reader.Read())
                    {
                        if (reader.TokenType == JsonTokenType.PropertyName && reader.ValueTextEquals(type))
                        {
                            reader.Read();
                            return reader.GetInt32();
                        }
                        else
                        {
                            reader.Skip();
                        }
                    }
                }

                Errors = new List<Exception> { new Exception("Тип сообщения не определен.") };

                return CommonMessageTypes.UnknownType;
            }
            catch (Exception exception)
            {
                Errors = new List<Exception> { exception };

                return CommonMessageTypes.UnknownType;
            }
        }

        #endregion
    }

    public class HandlerContext<TContent> : HandlerContext
        where TContent : class
    {
        public HandlerContext(Message message, ISerializer serializer, IServiceProvider provider, IOptions<ContentOptions> options)
            : base(message, serializer, provider, options)
        {
            if (IsValid)
            {
                try
                {
                    Message = Serializer.Deserialize<BaseMessage<TContent>>(Body.FirstSpan);
                }
                catch (Exception exception)
                {
                    Errors = new List<Exception> { exception };
                }
            }
        }

        public HandlerContext(HandlerContext context)
           : base(context)
        {
            if (context.IsValid)
            {
                try
                {
                    Message = Serializer.Deserialize<BaseMessage<TContent>>(context.Body.FirstSpan);
                }
                catch (Exception exception)
                {
                    Errors = new List<Exception> { exception };
                }
            }
        }

        public BaseMessage<TContent> Message { get; protected set; }
    }

    public class HandlerContext<TRequest, TResponse> : HandlerContext<TRequest>
        where TRequest : class
        where TResponse : class
    {
        private const string _errorType = nameof(DeserializationErrorException);

        internal HandlerContext(Message message, ISerializer serializer, IServiceProvider provider, IOptions<ContentOptions> options)
            : base(message, serializer, provider, options)
        {
            Response = CreateResopnse(Provider.GetRequiredService<IContentFactory>(), Message, Errors);
        }

        internal HandlerContext(HandlerContext context)
            : base(context)
        {
            Response = CreateResopnse(context.Provider.GetRequiredService<IContentFactory>(), Message, Errors);
        }

        public BaseResponse<TResponse> Response { get; }

        #region Private Methods 

        private BaseResponse<TResponse> CreateResopnse(IContentFactory factory, BaseMessage<TRequest> request, IList<Exception> errors)
        {
            return factory.SetAuxiliaryInfo(new BaseResponse<TResponse>
            {
                MessageType = typeof(TResponse).GetCustomAttribute<MessageTypeAttribute>()?.Value ?? request?.MessageType ?? CommonMessageTypes.UnknownType,
                Saga = new SagaModel
                {
                    EventId = Guid.NewGuid(),
                    PreviousEventId = request?.Saga?.EventId,
                    SagaId = request?.Saga?.SagaId ?? Guid.NewGuid()
                },
                Recipient = request?.Info == default ? default : new RecipientModel
                {
                    HostName = request.Info.HostName
                },
                Error = errors == default || !ThrowExceptionIfDeserializationError ? default : new ErrorModel
                {
                    Type = _errorType.Remove(_errorType.IndexOf(nameof(Exception))),
                    Message = string.Join(Environment.NewLine, errors.Select(exception => exception.Message ?? exception.InnerException?.Message))
                }
            });
        }

        #endregion
    }
}
