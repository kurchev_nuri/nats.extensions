﻿using Microsoft.Extensions.Options;
using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Broker.Models;
using Nats.Extensions.Broker.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Nats.Extensions.Broker.Factories
{
    internal sealed class ContentFactory : IContentFactory
    {
        private readonly IOptionsMonitor<ContentOptions> _options;

        public ContentFactory(IOptionsMonitor<ContentOptions> options) => _options = options;

        public BaseResponse<TResponse> SetAuxiliaryInfo<TResponse>(BaseResponse<TResponse> response)
        {
            if (response == default)
            {
                throw new ArgumentNullException(nameof(response), $"Аргумент {nameof(response)} не должен быть пустым.");
            }

            response.Info = new InfoModel
            {
                CreationDateTime = DateTimeOffset.Now,
                ClientId = _options.CurrentValue.ClientId,
                HostName = _options.CurrentValue.HostName
            };

            return response;
        }

        public BaseMessage<TContent> SetAuxiliaryInfo<TContent>(BaseMessage<TContent> message)
        {
            if (message == default)
            {
                throw new ArgumentNullException(nameof(message), $"Аргумент {nameof(message)} не должен быть пустым.");
            }

            message.Info = new InfoModel
            {
                CreationDateTime = DateTimeOffset.Now,
                ClientId = _options.CurrentValue.ClientId,
                HostName = _options.CurrentValue.HostName
            };

            return message;
        }

        public BaseMessage<TContent> CreateMessage<TContent>(TContent content)
        {
            if (EqualityComparer<TContent>.Default.Equals(content, default))
            {
                throw new ArgumentNullException(nameof(content), $"Аргумент {nameof(content)} не должен быть пустым.");
            }

            var messageType = TypeDescriptor.GetAttributes(content).OfType<MessageTypeAttribute>().SingleOrDefault();
            if (messageType == default)
            {
                throw new ArgumentException($"Для типа контента `{typeof(TContent).Name}` должен быть определен атрибут `{nameof(MessageTypeAttribute)}` с заданным типом сообщения.");
            }

            return new BaseMessage<TContent>
            {
                Content = content,
                MessageType = messageType.Value,
                Saga = new SagaModel
                {
                    SagaId = Guid.NewGuid(),
                    EventId = Guid.NewGuid()
                },
                Info = new InfoModel
                {
                    CreationDateTime = DateTimeOffset.Now,
                    ClientId = _options.CurrentValue.ClientId,
                    HostName = _options.CurrentValue.HostName
                }
            };
        }
    }
}
