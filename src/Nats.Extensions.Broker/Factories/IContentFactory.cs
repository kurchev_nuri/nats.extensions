﻿using Nats.Extensions.Broker.Models;

namespace Nats.Extensions.Broker.Factories
{
    internal interface IContentFactory
    {
        BaseMessage<TContent> CreateMessage<TContent>(TContent content);

        BaseMessage<TContent> SetAuxiliaryInfo<TContent>(BaseMessage<TContent> message);

        BaseResponse<TResponse> SetAuxiliaryInfo<TResponse>(BaseResponse<TResponse> response);
    }
}
