﻿using Nats.Extensions.Broker.Models;
using Nats.Extensions.Helper.Options;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Broker
{
    public interface IRequestService
    {
        /// <summary>
        /// Публикация сообщения в шину (NSS) с ответом от обработчика.
        /// </summary>
        /// <typeparam name="TResponse">Тип ожидаемого от обработчика сообщения.</typeparam>
        /// <typeparam name="TRequest">Тип содержимого сообщения.</typeparam>
        /// <param name="subject">Канал в который будет опубликовано сообщение.</param>
        /// <param name="content">Содержимое сообщения.</param>
        /// <param name="options">Настройки, который могут быть переданны во время публикации сообщения.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns>Возвращает сообщение, которое было получено от обработчика.</returns>
        Task<BaseResponse<TResponse>> RequestAsync<TRequest, TResponse>(string subject, TRequest content, NatsRequestOptions options = default, CancellationToken cancellationToken = default)
            where TResponse : class;

        /// <summary>
        /// Публикация сообщения в шину (NSS) с ответом от обработчика.
        /// </summary>
        /// <typeparam name="TResponse">Тип ожидаемого от обработчика сообщения.</typeparam>
        /// <typeparam name="TRequest">Тип содержимого сообщения.</typeparam>
        /// <param name="subject">Канал в который будет опубликовано сообщение.</param>
        /// <param name="message">Сообщение, которое будет опубликовано в шину (NSS).</param>
        /// <param name="options">Настройки, который могут быть переданны во время публикации сообщения.</param>
        /// <param name="cancellationToken">Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене.</param>
        /// <returns>Возвращает сообщение, которое было получено от обработчика.</returns>
        Task<BaseResponse<TResponse>> RequestAsync<TRequest, TResponse>(string subject, BaseMessage<TRequest> message, NatsRequestOptions options = default, CancellationToken cancellationToken = default)
            where TResponse : class;
    }
}
