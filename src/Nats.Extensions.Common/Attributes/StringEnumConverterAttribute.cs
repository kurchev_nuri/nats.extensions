﻿using System;
using System.Linq;

namespace Nats.Extensions.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    internal sealed class StringEnumConverterAttribute : Attribute
    {
        private readonly Type _enumType;

        public StringEnumConverterAttribute(Type enumType)
        {
            _enumType = enumType ?? throw new ArgumentNullException(nameof(enumType));
        }

        public object Convert(string value) => Enum.Parse(_enumType, DeleteWhiteSpaces(value));

        #region Private Methods

        private static string DeleteWhiteSpaces(string value)
        {
            var count = value.Count(elem => char.IsWhiteSpace(elem));
            if (count == 0)
            {
                return value;
            }

            return string.Create(value.Length - count, value, (elem, value) =>
            {
                for (int i = 0, j = 0; i < value.Length; i++)
                {
                    if (!char.IsWhiteSpace(value[i]))
                    {
                        elem[j++] = value[i];
                    }
                }
            });
        }

        #endregion
    }
}
