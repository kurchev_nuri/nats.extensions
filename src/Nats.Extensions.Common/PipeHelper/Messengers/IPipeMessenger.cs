﻿using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Common.PipeHelper.Messengers
{
    public interface IPipeMessenger<TResult>
        where TResult : class
    {
        Task<TResult> RetrieveMessageAsync(CancellationToken cancellationToken = default);
    }
}
