﻿using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Common.PipeHelper.Messengers
{
    internal sealed class PipeMessengerStub<TResult> : IPipeMessenger<TResult>
        where TResult : class
    {
        public Task<TResult> RetrieveMessageAsync(CancellationToken cancellationToken = default) => Task.FromResult(default(TResult));
    }
}
