﻿using Nats.Extensions.Common.Results;
using System;

namespace Nats.Extensions.Common.PipeHelper
{
    public interface IPipeManager
    {
        NatsExtensionResult<bool> VerifyClientCertificate(IntPtr pipePtr);

        NatsExtensionResult<bool> VerifyServerCertificate(IntPtr pipePtr);

        NatsExtensionResult<int> GetNamedPipeClientProcessId(IntPtr pipePtr);

        NatsExtensionResult<int> GetNamedPipeServerProcessId(IntPtr pipePtr);
    }
}
