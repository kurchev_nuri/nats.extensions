﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nats.Extensions.Common.Options;
using Nats.Extensions.Common.PipeHelper.Communications;
using System.IO.Pipes;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Common.PipeHelper.HostedServices
{
    internal sealed class NamedPipeServerBackgroundService<TModel> : BackgroundService
        where TModel : class
    {
        private readonly NamedPipeServerStream _pipeServer;
        private readonly IPipeCommunication<TModel> _communication;
        private readonly ILogger<NamedPipeServerBackgroundService<TModel>> _logger;

        public NamedPipeServerBackgroundService(IOptions<PipeManagerOptions> options,
                                                IPipeCommunication<TModel> communication,
                                                ILogger<NamedPipeServerBackgroundService<TModel>> logger)
        {
            _logger = logger;
            _communication = communication;

            var security = new PipeSecurity();

            security.AddAccessRule(new PipeAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, default), PipeAccessRights.ReadWrite, AccessControlType.Allow));

            //Solution comes from https://github.com/dotnet/runtime/issues/26869
            _pipeServer = NamedPipeServerStreamConstructors
                .New(options.Value.PipeName, PipeDirection.InOut, NamedPipeServerStream.MaxAllowedServerInstances, PipeTransmissionMode.Byte, PipeOptions.Asynchronous, 0, 0, security);
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Фоновый процесс раздачи паролей к `NSS` запущен.");

            while (!cancellationToken.IsCancellationRequested)
            {
                await _pipeServer.WaitForConnectionAsync(cancellationToken);
                await _communication.ExchangeAsync(_pipeServer, cancellationToken);

                _pipeServer.Disconnect();
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_pipeServer != default)
            {
                await _pipeServer.DisposeAsync();
            }

            _logger.LogInformation("Фоновый процесс раздачи паролей к `NSS` завершен.");
        }
    }
}
