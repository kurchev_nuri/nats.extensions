﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.Common.ProcessHelper;
using Nats.Extensions.Common.ProcessHelper.Base;
using Nats.Extensions.Common.Results;
using System;
using System.Runtime.InteropServices;

namespace Nats.Extensions.Common.PipeHelper
{
    internal sealed class PipeManager : IPipeManager
    {
        private readonly IProcessManager _manager;
        private readonly ILogger<PipeManager> _logger;

        public PipeManager(IProcessManager manager, ILogger<PipeManager> logger)
        {
            _logger = logger;
            _manager = manager;
        }

        public NatsExtensionResult<bool> VerifyClientCertificate(IntPtr pipePtr)
        {
            var processId = GetNamedPipeClientProcessId(pipePtr);
            if (!processId.IsSuccess)
            {
                return NatsExtensionResult.Failure<bool>(processId.Error);
            }

            return _manager.VerifyCertificateByProcessId(processId.Value);
        }

        public NatsExtensionResult<bool> VerifyServerCertificate(IntPtr pipePtr)
        {
            var processId = GetNamedPipeServerProcessId(pipePtr);
            if (!processId.IsSuccess)
            {
                return NatsExtensionResult.Failure<bool>(processId.Error);
            }

            return _manager.VerifyCertificateByProcessId(processId.Value);
        }

        public NatsExtensionResult<int> GetNamedPipeClientProcessId(IntPtr pipePtr)
        {
            if (pipePtr == IntPtr.Zero)
            {
                return NatsExtensionResult.Failure<int>($"Значение переменной `{nameof(pipePtr)}` не может быть равен `{IntPtr.Zero}`.");
            }

            if (!Win32.GetNamedPipeClientProcessId(pipePtr, out var processId))
            {
                var message = $"Во время получения `{nameof(processId)}` для `NamedPipeClient` произошла ошибка.";

                _logger.LogError(Marshal.GetExceptionForHR(Marshal.GetHRForLastWin32Error()), message);

                return NatsExtensionResult.Failure<int>(message);
            }

            return NatsExtensionResult.Success((int)processId);
        }

        public NatsExtensionResult<int> GetNamedPipeServerProcessId(IntPtr pipePtr)
        {
            if (pipePtr == IntPtr.Zero)
            {
                return NatsExtensionResult.Failure<int>($"Значение переменной `{nameof(pipePtr)}` не может быть равен `{IntPtr.Zero}`.");
            }

            if (!Win32.GetNamedPipeServerProcessId(pipePtr, out var processId))
            {
                var message = $"Во время получения `{nameof(processId)}` для `NamedPipeServer` произошла ошибка.";

                _logger.LogError(Marshal.GetExceptionForHR(Marshal.GetHRForLastWin32Error()), message);

                return NatsExtensionResult.Failure<int>(message);
            }

            return NatsExtensionResult.Success((int)processId);
        }
    }
}
