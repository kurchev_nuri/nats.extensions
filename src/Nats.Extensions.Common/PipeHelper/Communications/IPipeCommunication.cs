﻿using System.IO.Pipes;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Common.PipeHelper.Communications
{
    public interface IPipeCommunication<TResult>
         where TResult : class
    {
        Task ExchangeAsync(NamedPipeServerStream serverStream, CancellationToken cancellationToken = default);

        Task<TResult> ExchangeAsync(NamedPipeClientStream client, CancellationToken cancellationToken = default);
    }
}
