﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.Common.Consts;
using Nats.Extensions.Common.PipeHelper.Messengers;
using Nats.Extensions.Common.Serializers;
using System;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Common.PipeHelper.Communications
{
    internal sealed class PipeCommunication<TResult> : IPipeCommunication<TResult>
        where TResult : class
    {
        private readonly IPipeManager _manager;
        private readonly ISerializer _serializer;
        private readonly IPipeMessenger<TResult> _messenger;
        private readonly ILogger<PipeCommunication<TResult>> _logger;

        public PipeCommunication(IPipeManager manager, IPipeMessenger<TResult> messenger, ISerializer serializer, ILogger<PipeCommunication<TResult>> logger)
        {
            _logger = logger;
            _manager = manager;
            _messenger = messenger;
            _serializer = serializer;
        }

        public async Task ExchangeAsync(NamedPipeServerStream server, CancellationToken cancellationToken = default)
        {
            try
            {
                var verification = _manager.VerifyClientCertificate(server.SafePipeHandle.DangerousGetHandle());
                if (!verification.IsSuccess || !verification.Value)
                {
                    _logger.LogError("Клиент не может быть идентифицирован. Запрос не может быть обработан.");

                    return;
                }

                var writer = new StreamWriter(server) { AutoFlush = true };
                using var cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);

                cancellationTokenSource.CancelAfter(CommonConsts.PipeCommunicationTimeout);

                var message = await _messenger.RetrieveMessageAsync(cancellationTokenSource.Token);

                await writer.WriteAsync(_serializer.Serialize(message));
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Во время отправки учетных данных произошла ошибка.");
            }
        }

        public async Task<TResult> ExchangeAsync(NamedPipeClientStream client, CancellationToken cancellationToken = default)
        {
            try
            {
                var verification = _manager.VerifyServerCertificate(client.SafePipeHandle.DangerousGetHandle());
                if (!verification.IsSuccess || !verification.Value)
                {
                    _logger.LogError("Сервер не может быть идентифицирован. Ответ от сервера не может быть обработан.");

                    return Activator.CreateInstance<TResult>();
                }

                var credentials = string.Empty;
                var reader = new StreamReader(client);

                while (!cancellationToken.IsCancellationRequested)
                {
                    if (reader.Peek() >= 0)
                    {
                        credentials = await reader.ReadToEndAsync();
                        break;
                    }

                    await Task.Delay(CommonConsts.PipeReadTimeout, cancellationToken);
                }

                if (string.IsNullOrEmpty(credentials) && cancellationToken.IsCancellationRequested)
                {
                    cancellationToken.ThrowIfCancellationRequested();
                }

                return _serializer.Deserialize<TResult>(credentials);
            }
            catch (OperationCanceledException)
            {
                _logger.LogError("Отведенное время на обмен сообщениями истекло либо запрос был отменен.");
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "Во время обработки сообщения от сервера произошла ошибка.");
            }

            return Activator.CreateInstance<TResult>();
        }
    }
}
