﻿using Nats.Extensions.Common.Models;
using Nats.Extensions.Common.Results;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Common.PipeHelper.Providers
{
    public interface ICredentialsProvider
    {
        Task<NatsExtensionResult<PipeCredentialsModel>> RequestAsync(CancellationToken cancellationToken = default);
    }
}
