﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nats.Extensions.Common.Consts;
using Nats.Extensions.Common.Models;
using Nats.Extensions.Common.Options;
using Nats.Extensions.Common.PipeHelper.Communications;
using Nats.Extensions.Common.Results;
using System;
using System.IO.Pipes;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Common.PipeHelper.Providers
{
    internal sealed class CredentialsProvider : ICredentialsProvider
    {
        private readonly ILogger<CredentialsProvider> _logger;
        private readonly IOptions<PipeManagerOptions> _options;
        private readonly IPipeCommunication<PipeCredentialsModel> _communication;

        public CredentialsProvider(ILogger<CredentialsProvider> logger,
                                   IOptions<PipeManagerOptions> options,
                                   IPipeCommunication<PipeCredentialsModel> communication)
        {
            _logger = logger;
            _options = options;
            _communication = communication;
        }

        public async Task<NatsExtensionResult<PipeCredentialsModel>> RequestAsync(CancellationToken cancellationToken = default)
        {
            using var scoped = _logger.BeginScope(("requestId", Guid.NewGuid()));
            using var cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);

            cancellationTokenSource.CancelAfter(CommonConsts.PipeCommunicationTimeout);

            _logger.LogInformation("Попытка получения учетных данных клиента.");

            try
            {
                await using var client = new NamedPipeClientStream(_options.Value.ServerName, _options.Value.PipeName, PipeDirection.InOut, PipeOptions.Asynchronous);

                await client.ConnectAsync(cancellationTokenSource.Token);

                var credentials = await _communication.ExchangeAsync(client, cancellationTokenSource.Token);
                if (!credentials.IsSuccess)
                {
                    var message = "Запрос на получения учетных данных клиента завершился неудачно.";

                    _logger.LogError(message);

                    return NatsExtensionResult.Failure<PipeCredentialsModel>(message);
                }

                _logger.LogInformation("Учетные данные клиента получены.");

                return NatsExtensionResult.Success(credentials);
            }
            catch (OperationCanceledException)
            {
                var message = "Отведенное время на получения учетных данных клиента истекло либо запрос был отменен.";

                _logger.LogError(message);

                return NatsExtensionResult.Failure<PipeCredentialsModel>(message);
            }
            catch (Exception exception)
            {
                var message = "Запрос на получения учетных данных клиента завершился неудачно.";

                _logger.LogError(exception, message);

                return NatsExtensionResult.Failure<PipeCredentialsModel>(message);
            }
        }
    }
}
