﻿namespace Nats.Extensions.Common.Models
{
    public class PipeCredentialsModel
    {
        public string User { get; set; }

        public string Password { get; set; }

        internal bool IsSuccess => !string.IsNullOrWhiteSpace(User) && !string.IsNullOrWhiteSpace(Password);
    }
}
