﻿using Nats.Extensions.Common.Attributes;
using System.ServiceProcess;

namespace Nats.Extensions.Common.Models
{
    public class ServiceInfoModel
    {
        private string _pathName;

        public uint ProcessId { get; set; }

        public bool Started { get; set; }

        public string Status { get; set; }

        public string DisplayName { get; set; }

        [StringEnumConverter(typeof(ServiceControllerStatus))]
        public ServiceControllerStatus State { get; set; }

        public string PathName { get => _pathName; set => _pathName = value?.Trim('\\', '"'); }
    }
}
