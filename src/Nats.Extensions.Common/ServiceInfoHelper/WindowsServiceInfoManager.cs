﻿using Nats.Extensions.Common.Extensions;
using Nats.Extensions.Common.Models;
using Nats.Extensions.Common.Results;
using System;
using System.Linq;
using System.Management;

namespace Nats.Extensions.Common.ServiceInfoHelper
{
    internal sealed class WindowsServiceInfoManager : IServiceInfoManager
    {
        public NatsExtensionResult<ServiceInfoModel> GetServiceInfoByName(string serviceName)
        {
            if (string.IsNullOrWhiteSpace(serviceName))
            {
                return NatsExtensionResult.Failure<ServiceInfoModel>($"Входное значение `{nameof(serviceName)}` не может быть пустым null.");
            }

            using var searcher = new ManagementObjectSearcher(AllPropertiesQuery(serviceName));

            try
            {
                var result = searcher.Get()
                    .OfType<ManagementBaseObject>()
                    .SelectMany(management => management.Properties.OfType<PropertyData>())
                    .ToDictionary(property => property.Name, property => property.Value);

                if (!result.Any())
                {
                    return NatsExtensionResult.Failure<ServiceInfoModel>($"По заданному имени сервиса `{serviceName}` ничего не найдено.");
                }

                return NatsExtensionResult.Success(result.ToServiceInfo());
            }
            catch (Exception exception)
            {
                return NatsExtensionResult.Failure<ServiceInfoModel>(
                    $"Запрос `{nameof(ManagementObjectSearcher)}` завершился с ошибкой.{Environment.NewLine}{HandleException(exception)}"
                );
            }
        }

        #region Private Methods

        private static string AllPropertiesQuery(string serviceName) => $"SELECT * FROM Win32_Service WHERE Name='{serviceName}'";

        private static string HandleException(Exception exception)
        {
            var result = exception.Message;
            if (exception.InnerException != default)
            {
                result += $"{Environment.NewLine}{exception.InnerException.Message}";
            }

            return result;
        }

        #endregion
    }
}
