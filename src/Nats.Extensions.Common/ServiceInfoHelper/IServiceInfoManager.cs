﻿using Nats.Extensions.Common.Models;
using Nats.Extensions.Common.Results;

namespace Nats.Extensions.Common.ServiceInfoHelper
{
    public interface IServiceInfoManager
    {
        NatsExtensionResult<ServiceInfoModel> GetServiceInfoByName(string serviceName);
    }
}
