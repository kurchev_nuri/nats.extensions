﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Threading;

namespace Nats.Extensions.Common.Serializers.Helper
{
    public static class JsonSerializerHelper
    {
        #region SystemText

        private static readonly Lazy<JsonSerializerOptions> _lazyDefaultSystemTextSettings = new(InitDefaultSystemTextSettings, LazyThreadSafetyMode.PublicationOnly);

        public static JsonSerializerOptions DefaultSystemTextSerializerSettings => _lazyDefaultSystemTextSettings.Value;

        private static JsonSerializerOptions InitDefaultSystemTextSettings()
        {
            return new JsonSerializerOptions { IgnoreNullValues = true, Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping };
        }

        #endregion

        #region Newtonsoft

        private static readonly Lazy<JsonSerializerSettings> _lazyDefaultNewtonsoft = new(InitDefaultNewtonsoftSettings, LazyThreadSafetyMode.PublicationOnly);
        private static readonly Lazy<JsonSerializerSettings> _lazyCamelCaseNewtonsoft = new(InitCamelCaseNewtonsoftSettings, LazyThreadSafetyMode.PublicationOnly);
        private static readonly Lazy<JsonSerializerSettings> _lazySnakeCaseNewtonsoft = new(InitSnakeCaseNewtonsoftSettings, LazyThreadSafetyMode.PublicationOnly);

        public static JsonSerializerSettings DefaultNewtonsoftSerializerSettings => _lazyDefaultNewtonsoft.Value;

        public static JsonSerializerSettings CamelCaseNewtonsoftSerializerSettings => _lazyCamelCaseNewtonsoft.Value;

        public static JsonSerializerSettings SnakeCaseNewtonsoftSerializerSettings => _lazySnakeCaseNewtonsoft.Value;

        private static JsonSerializerSettings InitCamelCaseNewtonsoftSettings()
        {
            return new JsonSerializerSettings
            {
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore,
                DateParseHandling = DateParseHandling.DateTimeOffset,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                ContractResolver = new DefaultContractResolver { NamingStrategy = new CamelCaseNamingStrategy() }
            };
        }

        private static JsonSerializerSettings InitSnakeCaseNewtonsoftSettings()
        {
            return new JsonSerializerSettings
            {
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore,
                DateParseHandling = DateParseHandling.DateTimeOffset,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                ContractResolver = new DefaultContractResolver { NamingStrategy = new SnakeCaseNamingStrategy() }
            };
        }

        private static JsonSerializerSettings InitDefaultNewtonsoftSettings()
        {
            return new JsonSerializerSettings
            {
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore,
                DateParseHandling = DateParseHandling.DateTimeOffset,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatHandling = DateFormatHandling.IsoDateFormat
            };
        }

        #endregion
    }
}
