﻿using System;

namespace Nats.Extensions.Common.Serializers
{
    public interface ISerializer
    {
        string Serialize<TValue>(TValue value) where TValue : class;

        byte[] SerializeToUtf8Bytes<TValue>(TValue value) where TValue : class;

        TValue Deserialize<TValue>(string value) where TValue : class;

        TValue Deserialize<TValue>(ReadOnlySpan<byte> value) where TValue : class;
    }
}
