﻿using Nats.Extensions.Common.Serializers.Helper;
using Newtonsoft.Json;
using System;
using System.Text;

namespace Nats.Extensions.Common.Serializers
{
    internal sealed class NewtonsoftSerializer : ISerializer
    {
        private readonly JsonSerializerSettings _settings;

        public NewtonsoftSerializer(JsonSerializerSettings settings = default)
        {
            _settings = settings ?? JsonSerializerHelper.CamelCaseNewtonsoftSerializerSettings;
        }

        public string Serialize<TValue>(TValue value)
            where TValue : class
        {
            if (typeof(TValue) == typeof(string))
            {
                return value as string;
            }

            return JsonConvert.SerializeObject(value, _settings);
        }

        public byte[] SerializeToUtf8Bytes<TValue>(TValue value)
            where TValue : class
        {
            return Encoding.UTF8.GetBytes(Serialize(value));
        }

        public TValue Deserialize<TValue>(string value)
            where TValue : class
        {
            if (typeof(TValue) == typeof(string))
            {
                return value as TValue;
            }

            return JsonConvert.DeserializeObject<TValue>(value, _settings);
        }

        public TValue Deserialize<TValue>(ReadOnlySpan<byte> value)
            where TValue : class
        {
            return Deserialize<TValue>(Encoding.UTF8.GetString(value));
        }
    }
}
