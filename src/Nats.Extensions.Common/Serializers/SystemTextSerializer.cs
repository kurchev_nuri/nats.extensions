﻿using Nats.Extensions.Common.Serializers.Helper;
using System;
using System.Text;
using System.Text.Json;

namespace Nats.Extensions.Common.Serializers
{
    internal sealed class SystemTextSerializer : ISerializer
    {
        private readonly JsonSerializerOptions _options;

        public SystemTextSerializer(JsonSerializerOptions options = default)
        {
            _options = options ?? JsonSerializerHelper.DefaultSystemTextSerializerSettings;
        }

        public string Serialize<TValue>(TValue value)
            where TValue : class
        {
            if (typeof(TValue) == typeof(string))
            {
                return value as string;
            }

            return JsonSerializer.Serialize(value, _options);
        }

        public byte[] SerializeToUtf8Bytes<TValue>(TValue value)
            where TValue : class
        {
            if (typeof(TValue) == typeof(string))
            {
                return Encoding.UTF8.GetBytes(value as string);
            }

            return JsonSerializer.SerializeToUtf8Bytes(value, _options);
        }

        public TValue Deserialize<TValue>(string value)
            where TValue : class
        {
            if (typeof(TValue) == typeof(string))
            {
                return value as TValue;
            }

            return JsonSerializer.Deserialize<TValue>(value, _options);
        }

        public TValue Deserialize<TValue>(ReadOnlySpan<byte> value)
            where TValue : class
        {
            if (typeof(TValue) == typeof(string))
            {
                return Encoding.UTF8.GetString(value) as TValue;
            }

            return JsonSerializer.Deserialize<TValue>(value, _options);
        }
    }
}
