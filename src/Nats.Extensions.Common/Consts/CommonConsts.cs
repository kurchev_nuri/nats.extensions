﻿using System;

namespace Nats.Extensions.Common.Consts
{
    internal static class CommonConsts
    {
        public static readonly TimeSpan PipeReadTimeout = TimeSpan.FromMilliseconds(500);
        public static readonly TimeSpan PipeCommunicationTimeout = TimeSpan.FromSeconds(5);
    }
}
