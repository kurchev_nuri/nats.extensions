﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Nats.Extensions.Common.Results;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Common.ValidatorHelper
{
    internal sealed class CommonValidator : ICommonValidator
    {
        private readonly IServiceProvider _provider;

        public CommonValidator(IServiceProvider provider) => _provider = provider;

        public TValue ValidateAndThrow<TValue>(TValue value)
            where TValue : class
        {
            _provider.GetRequiredService<IValidator<TValue>>()
                .ValidateAndThrow(value);

            return value;
        }

        public async Task<TValue> ValidateAndThrowAsync<TValue>(TValue value, CancellationToken cancellationToken = default)
            where TValue : class
        {
            await _provider.GetRequiredService<IValidator<TValue>>()
                .ValidateAndThrowAsync(value, cancellationToken: cancellationToken);

            return value;
        }

        public NatsExtensionResult<TValue> Validate<TValue>(TValue value)
            where TValue : class
        {
            var result = _provider.GetRequiredService<IValidator<TValue>>().Validate(value);
            if (result.IsValid)
            {
                return NatsExtensionResult.Success(value);
            }

            return NatsExtensionResult.Failure<TValue>(string.Join(Environment.NewLine, result.Errors.Select(u => u.ErrorMessage)));
        }

        public async Task<NatsExtensionResult<TValue>> ValidateAsync<TValue>(TValue value, CancellationToken cancellationToken = default)
            where TValue : class
        {
            var result = await _provider.GetRequiredService<IValidator<TValue>>()
                .ValidateAsync(value, cancellationToken);

            if (result.IsValid)
            {
                return NatsExtensionResult.Success(value);
            }

            return NatsExtensionResult.Failure<TValue>(string.Join(Environment.NewLine, result.Errors.Select(u => u.ErrorMessage)));
        }
    }
}
