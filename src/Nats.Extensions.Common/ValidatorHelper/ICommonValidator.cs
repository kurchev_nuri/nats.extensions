﻿using Nats.Extensions.Common.Results;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Common.ValidatorHelper
{
    public interface ICommonValidator
    {
        TValue ValidateAndThrow<TValue>(TValue value) where TValue : class;

        NatsExtensionResult<TValue> Validate<TValue>(TValue value) where TValue : class;

        Task<TValue> ValidateAndThrowAsync<TValue>(TValue value, CancellationToken cancellationToken = default) where TValue : class;

        Task<NatsExtensionResult<TValue>> ValidateAsync<TValue>(TValue value, CancellationToken cancellationToken = default) where TValue : class;
    }
}
