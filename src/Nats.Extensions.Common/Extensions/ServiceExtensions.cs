﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Nats.Extensions.Common.Options;
using Nats.Extensions.Common.PipeHelper;
using Nats.Extensions.Common.PipeHelper.Communications;
using Nats.Extensions.Common.PipeHelper.HostedServices;
using Nats.Extensions.Common.PipeHelper.Messengers;
using Nats.Extensions.Common.PipeHelper.Providers;
using Nats.Extensions.Common.ProcessHelper;
using Nats.Extensions.Common.Serializers;
using Nats.Extensions.Common.ServiceInfoHelper;
using Nats.Extensions.Common.ValidatorHelper;
using Newtonsoft.Json;
using Scrutor;
using System;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.Json;

namespace Nats.Extensions.Common.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddSystemTextSerializer(this IServiceCollection services, Action<JsonSerializerOptions> configure = default)
        {
            if (configure != default)
            {
                var settings = new JsonSerializerOptions();

                configure(settings);

                return services.Replace(ServiceDescriptor.Singleton<ISerializer>(provider => new SystemTextSerializer(settings)));
            }
            else
            {
                return services.Replace(ServiceDescriptor.Singleton<ISerializer>(provider => new SystemTextSerializer()));
            }
        }

        public static IServiceCollection AddNewtonsoftSerializer(this IServiceCollection services, Action<JsonSerializerSettings> configure = default)
        {
            if (configure != default)
            {
                var settings = new JsonSerializerSettings();

                configure(settings);

                return services.Replace(ServiceDescriptor.Singleton<ISerializer>(provider => new NewtonsoftSerializer(settings)));
            }
            else
            {
                return services.Replace(ServiceDescriptor.Singleton<ISerializer>(provider => new NewtonsoftSerializer()));
            }
        }

        public static IServiceCollection AddProcessManager(this IServiceCollection services)
        {
            services.TryAddSingleton<IProcessManager>(provider =>
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    return ActivatorUtilities.CreateInstance<LinuxProcessManager>(provider);
                }

                return ActivatorUtilities.CreateInstance<WindowsProcessManager>(provider);
            });

            services.TryAddSingleton<IServiceInfoManager>(provider =>
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    return ActivatorUtilities.CreateInstance<LinuxServiceInfoManager>(provider);
                }

                return ActivatorUtilities.CreateInstance<WindowsServiceInfoManager>(provider);
            });

            return services;
        }

        public static IServiceCollection AddClientPipeManager<TModel>(this IServiceCollection services, Action<PipeManagerOptions> configure)
            where TModel : class
        {
            services.AddOptions<PipeManagerOptions>()
                .Configure(configure)
                .Validate<IValidator<PipeManagerOptions>>(Validate);

            services.AddProcessManager();
            services.AddNewtonsoftSerializer();
            services.AddCommonValidators(typeof(ServiceExtensions).Assembly);

            services.TryAddSingleton<IPipeManager, PipeManager>();
            services.TryAddSingleton<ICredentialsProvider, CredentialsProvider>();
            services.TryAddSingleton<IPipeMessenger<TModel>, PipeMessengerStub<TModel>>();
            services.TryAddSingleton<IPipeCommunication<TModel>, PipeCommunication<TModel>>();

            return services;
        }

        public static IServiceCollection AddServerPipeManager<TModel, TMessenger>(this IServiceCollection services, Action<PipeManagerOptions> configure)
            where TModel : class
            where TMessenger : class, IPipeMessenger<TModel>
        {
            services.AddClientPipeManager<TModel>(configure);

            services.Replace(ServiceDescriptor.Singleton<IPipeMessenger<TModel>, TMessenger>());

            services.AddHostedService<NamedPipeServerBackgroundService<TModel>>();

            return services;
        }

        public static IServiceCollection AddCommonValidators(this IServiceCollection services, params Assembly[] assemblies)
        {
            services.TryAddSingleton<ICommonValidator, CommonValidator>();

            return services.Scan(u => u.FromAssemblies(assemblies)
                .AddClasses(v => v.AssignableTo(typeof(IValidator<>)))
                    .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                    .As(type => type.GetInterfaces().Where(type => type.IsGenericType && type.Name.Contains(nameof(IValidator))))
                .WithSingletonLifetime()
            );
        }

        #region Private Methods 

        private static bool Validate<TOptions>(TOptions options, IValidator<TOptions> validator)
        {
            validator.ValidateAndThrow(options);
            return true;
        }

        #endregion
    }
}
