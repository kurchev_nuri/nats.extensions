﻿using Nats.Extensions.Common.Models;
using Nats.Extensions.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Nats.Extensions.Common.Extensions
{
    internal static class CommonExtensions
    {
        public static ServiceInfoModel ToServiceInfo(this IDictionary<string, object> properties)
        {
            var result = Activator.CreateInstance<ServiceInfoModel>();

            foreach (var property in typeof(ServiceInfoModel).GetProperties())
            {
                if (properties.TryGetValue(property.Name, out var value))
                {
                    var converter = property.GetCustomAttribute<StringEnumConverterAttribute>();
                    if (converter != default)
                    {
                        property.SetValue(result, converter.Convert(value.ToString()));
                    }
                    else
                    {
                        property.SetValue(result, value);
                    }
                }
            }

            return result;
        }
    }
}
