﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.Common.ProcessHelper.Base;
using Nats.Extensions.Common.Resources;
using Nats.Extensions.Common.Results;
using Nats.Extensions.Common.ServiceInfoHelper;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Nats.Extensions.Common.ProcessHelper
{
    internal sealed class WindowsProcessManager : IProcessManager
    {
        private readonly IServiceInfoManager _manager;
        private readonly ILogger<WindowsProcessManager> _logger;

        public WindowsProcessManager(IServiceInfoManager manager, ILogger<WindowsProcessManager> logger)
        {
            _logger = logger;
            _manager = manager;
        }

        public NatsExtensionResult<string> GetProcessPathByWinApi(Process process)
        {
            if (process == default)
            {
                return NatsExtensionResult.Failure<string>(string.Format(ProcessManagerResource.IncorrectArgument, nameof(process)));
            }

            int capacity = 2000;
            var builder = new StringBuilder(capacity);

            var intPtr = Win32.OpenProcess(ProcessAccessFlags.QueryLimitedInformation, false, process.Id);
            if (intPtr == IntPtr.Zero)
            {
                return HandleError(ProcessManagerResource.OpenProcessFailure);
            }

            if (!Win32.QueryFullProcessImageName(intPtr, 0, builder, ref capacity))
            {
                return HandleError(ProcessManagerResource.QueryProcessNameFailure);
            }

            return NatsExtensionResult.Success(builder.ToString());

            NatsExtensionResult<string> HandleError(string message)
            {
                _logger.LogError(Marshal.GetExceptionForHR(Marshal.GetHRForLastWin32Error()), message);

                return NatsExtensionResult.Failure<string>(message);
            }
        }

        public NatsExtensionResult KillServiceByName(string serviceName, bool entireProcessTree = false)
        {
            var serviceInfo = _manager.GetServiceInfoByName(serviceName);
            if (!serviceInfo.IsSuccess)
            {
                return NatsExtensionResult.Failure(serviceInfo.Error);
            }

            try
            {
                Process.GetProcessById((int)serviceInfo.Value.ProcessId)?.Kill(entireProcessTree);

                return NatsExtensionResult.Success();
            }
            catch (Exception exception)
            {
                var message = string.Format(ProcessManagerResource.KillProcessFailure, serviceInfo.Value.ProcessId);

                _logger.LogError(exception, message);

                return NatsExtensionResult.Failure(message);
            }
        }

        public NatsExtensionResult<bool> VerifyCertificateByProcessId(int processId)
        {
            var ownProcessPath = GetProcessPathByWinApi(Process.GetCurrentProcess());
            if (!ownProcessPath.IsSuccess)
            {
                return NatsExtensionResult.Failure<bool>(ownProcessPath.Error);
            }

            var clientProcessPath = GetProcessPathByWinApi(Process.GetProcessById(processId));
            if (!clientProcessPath.IsSuccess)
            {
                return NatsExtensionResult.Failure<bool>(clientProcessPath.Error);
            }

            try
            {
                _logger.LogInformation(string.Format(ProcessManagerResource.CertificateVerification, clientProcessPath.Value));

                var ownCertificate = X509Certificate.CreateFromSignedFile(ownProcessPath.Value);
                var clientCertificate = X509Certificate.CreateFromSignedFile(clientProcessPath.Value);

                return NatsExtensionResult.Success(ownCertificate.Equals(clientCertificate));
            }
            catch (Exception exception)
            {
                var message = string.Format(ProcessManagerResource.CertificateVerificationFailure, clientProcessPath.Value);

                _logger.LogError(exception, message);

                return NatsExtensionResult.Failure<bool>(message);
            }
        }
    }
}
