﻿using System;

namespace Nats.Extensions.Common.ProcessHelper.Base
{
    [Flags]
    internal enum ProcessAccessFlags : uint
    {
        QueryLimitedInformation = 0x00001000
    }
}
