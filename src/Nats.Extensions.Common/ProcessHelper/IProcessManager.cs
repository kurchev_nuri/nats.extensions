﻿using Nats.Extensions.Common.Results;
using System.Diagnostics;

namespace Nats.Extensions.Common.ProcessHelper
{
    public interface IProcessManager
    {
        NatsExtensionResult<string> GetProcessPathByWinApi(Process process);

        NatsExtensionResult<bool> VerifyCertificateByProcessId(int processId);

        NatsExtensionResult KillServiceByName(string serviceName, bool entireProcessTree = false);
    }
}
