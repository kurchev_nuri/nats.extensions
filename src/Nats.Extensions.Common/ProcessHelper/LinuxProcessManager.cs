﻿using Nats.Extensions.Common.Results;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nats.Extensions.Common.ProcessHelper
{
    internal sealed class LinuxProcessManager : IProcessManager
    {
        public NatsExtensionResult<string> GetProcessPathByWinApi(Process process)
        {
            throw new NotImplementedException();
        }

        public NatsExtensionResult KillServiceByName(string serviceName, bool entireProcessTree = false)
        {
            throw new NotImplementedException();
        }

        public NatsExtensionResult<bool> VerifyCertificateByProcessId(int processId)
        {
            throw new NotImplementedException();
        }
    }
}
