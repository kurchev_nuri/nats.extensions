﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;

namespace Nats.Extensions.Common.Utilities
{
    public static class SystemUtilities
    {
        private static readonly char[] _invalidChars = { '\r', '\n', '\t', ' ' };

        private static readonly Lazy<IPAddress> _lazyIp = new(InitializerIP, LazyThreadSafetyMode.PublicationOnly);
        private static readonly Lazy<PhysicalAddress> _lazyMac = new(InitializerMac, LazyThreadSafetyMode.PublicationOnly);
        private static readonly Lazy<string> _lazyHost = new(() => Dns.GetHostName().ToLower(), LazyThreadSafetyMode.PublicationOnly);

        public static string HostName => _lazyHost.Value;

        public static IPAddress IPAddress => _lazyIp.Value;

        public static PhysicalAddress MacAddress => _lazyMac.Value;

        public static string NormalizedHostName => NormalizeHostName(HostName);

        public static string NormalizedMacAddress => Normalize(MacAddress);

        public static string NormalizedIPAddress => IPAddress.ToString().Replace('.', '-');

        public static string ApplicationNameWithHostName => Concat(Assembly.GetEntryAssembly()?.FullName, NormalizedHostName);

        public static string ConcatServiceNameWithHostName(string serviceName) => Concat(serviceName, NormalizedHostName);

        public static string ConcatServiceNameWithHostName(string serviceName, string version) => Concat(serviceName, version, NormalizedHostName);

        public static string ConcatServiceNameWithMacAddress(string serviceName, string version) => Concat(serviceName, version, NormalizedMacAddress);

        public static string GetHostName(string hostNameOrAddress)
        {
            return Dns.GetHostEntry(hostNameOrAddress).HostName.ToLowerInvariant();
        }

        public static bool IsMyHost(string hostNameOrAddress)
        {
            try
            {
                return Dns.GetHostAddresses(hostNameOrAddress)
                    .Any(u => IPAddress.IsLoopback(u) || Dns.GetHostAddresses(HostName).Contains(u));
            }
            catch
            {
                return false;
            }
        }

        private static PhysicalAddress InitializerMac()
        {
            return NetworkInterface.GetAllNetworkInterfaces()
                .Where(u => u.NetworkInterfaceType == NetworkInterfaceType.Ethernet && u.OperationalStatus == OperationalStatus.Up)
                .Select(u => u.GetPhysicalAddress())
                .First();
        }

        public static IPAddress InitializerIP()
        {
            return Dns.GetHostEntry(HostName)
                .AddressList
                .Where(u => u.AddressFamily == AddressFamily.InterNetwork)
                .First();
        }

        public static string Normalize(this PhysicalAddress address)
        {
            var strAddress = address.ToString();
            var builder = new StringBuilder(strAddress.Length + (strAddress.Length / 2) - 1);

            for (int index = 0; index < strAddress.Length; index += 2)
            {
                builder.Append(strAddress.Substring(index, 2).ToLower());
                if ((strAddress.Length - index) != 2)
                {
                    builder.Append('-');
                }
            }

            return builder.ToString();
        }

        public static string NormalizeHostName(string hostName)
        {
            var result = new StringBuilder(hostName.Length);
            for (var i = 0; i < hostName.Length; i++)
            {
                var c = hostName[i];

                if (!_invalidChars.Contains(c))
                {
                    if (c == '.' || c == '_')
                    {
                        result.Append('-');
                    }
                    else
                    {
                        result.Append(char.ToLower(c));
                    }
                }
            }

            return result.ToString();
        }

        public static string Concat(string serviceName, string hostName)
        {
            if (string.IsNullOrEmpty(serviceName))
            {
                throw new ArgumentNullException(nameof(serviceName), $"{nameof(serviceName)} не может быть пустым или равным null.");
            }

            if (string.IsNullOrEmpty(hostName))
            {
                throw new ArgumentNullException(nameof(hostName), $"{nameof(hostName)} не может быть пустым или равным null.");
            }

            return $"{serviceName}_{hostName}";
        }

        public static string Concat(string serviceName, string version, string hostName)
        {
            if (string.IsNullOrEmpty(serviceName))
            {
                throw new ArgumentNullException(nameof(serviceName), $"{nameof(serviceName)} не может быть пустым или равным null.");
            }

            if (string.IsNullOrEmpty(version))
            {
                throw new ArgumentNullException(nameof(version), $"{nameof(version)} не может быть пустым или равным null.");
            }

            if (string.IsNullOrEmpty(hostName))
            {
                throw new ArgumentNullException(nameof(hostName), $"{nameof(hostName)} не может быть пустым или равным null.");
            }

            return $"{serviceName}_{version.Replace(".", "-")}_{hostName}";
        }
    }
}
