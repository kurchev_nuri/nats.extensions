﻿using FluentValidation;
using Nats.Extensions.Common.Options;

namespace Nats.Extensions.Common.Validators.Options
{
    internal sealed class PipeManagerOptionsValidator : AbstractValidator<PipeManagerOptions>
    {
        public PipeManagerOptionsValidator()
        {
            RuleFor(u => u.PipeName)
               .NotEmpty()
               .WithMessage($"Поле `{nameof(PipeManagerOptions)}.{nameof(PipeManagerOptions.PipeName)}` не должно быть пустым или равным null.");
        }
    }
}
