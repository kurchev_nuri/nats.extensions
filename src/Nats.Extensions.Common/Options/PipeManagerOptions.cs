﻿namespace Nats.Extensions.Common.Options
{
    public sealed class PipeManagerOptions
    {
        public string PipeName { get; set; }

        public string ServerName { get; set; } = ".";
    }
}
