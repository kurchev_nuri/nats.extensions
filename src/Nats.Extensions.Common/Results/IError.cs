﻿namespace Nats.Extensions.Common.Results
{
    public interface IError
    {
        string Message { get; set; }
    }
}
