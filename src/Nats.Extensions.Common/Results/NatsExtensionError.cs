﻿namespace Nats.Extensions.Common.Results
{
    public struct NatsExtensionError : IError
    {
        public string Message { get; set; }
    }
}
