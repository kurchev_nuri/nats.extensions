﻿namespace Nats.Extensions.Common.Results
{
    public interface IResult
    {
        bool IsSuccess { get; }
    }

    public interface IValue<out T>
    {
        T Value { get; }
    }
}
