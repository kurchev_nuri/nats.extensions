﻿using System.Collections.Generic;
using System.Linq;

namespace Nats.Extensions.Common.Results
{
    internal readonly struct ResultCommonLogic<TError>
        where TError : IError
    {
        public TError Error { get; }

        public ResultCommonLogic(TError error) => Error = error;

        public bool IsSuccess => string.IsNullOrWhiteSpace(Error?.Message);
    }

    internal readonly struct WebResultCommonLogic<TError>
    {
        public IEnumerable<TError> Errors { get; }

        public WebResultCommonLogic(IEnumerable<TError> errors) => Errors = errors;

        public bool IsSuccess => Errors == default || !Errors.Any();
    }
}
