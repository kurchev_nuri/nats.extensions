﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using NewtonJsonConstructor = Newtonsoft.Json.JsonConstructorAttribute;
using NewtonJsonIgnore = Newtonsoft.Json.JsonIgnoreAttribute;
using SystemTextConstructor = System.Text.Json.Serialization.JsonConstructorAttribute;
using SystemTextIgnore = System.Text.Json.Serialization.JsonIgnoreAttribute;

namespace Nats.Extensions.Common.Results
{
    public readonly struct NatsExtensionResult : IResult
    {
        private readonly ResultCommonLogic<NatsExtensionError> _logic;

        [NewtonJsonConstructor, SystemTextConstructor]
        internal NatsExtensionResult(string errorMessage)
        {
            _logic = new ResultCommonLogic<NatsExtensionError>(new NatsExtensionError { Message = errorMessage });
        }

        [NewtonJsonIgnore, SystemTextIgnore]
        public bool IsSuccess => _logic.IsSuccess;

        public string Error => _logic.Error.Message;

        public static NatsExtensionResult Success() => new(default);

        public static NatsExtensionResult Failure(string errorMessage) => new(errorMessage);

        public static NatsExtensionResult<T> Success<T>(T value) => new(value, default);

        public static NatsExtensionResult<T> Failure<T>(string errorMessage) => new(default, errorMessage);

        public static NatsExtensionResult<T> Failure<T>(T value, string errorMessage) => new(value, errorMessage);

        public static NatsExtensionResult<T, E> Success<T, E>(T value) where E : IError => new(value, default);

        public static NatsExtensionResult<T, E> Failure<T, E>(E error) where E : IError => new(default, error);

        public static NatsExtensionResult<T, E> Failure<T, E>(T value, E error) where E : IError => new(value, error);
    }

    public readonly struct NatsExtensionResult<T> : IResult, IValue<T>
    {
        private readonly ResultCommonLogic<NatsExtensionError> _logic;

        [NewtonJsonConstructor, SystemTextConstructor]
        internal NatsExtensionResult(T value, string errorMessage)
        {
            Value = value;

            _logic = new ResultCommonLogic<NatsExtensionError>(new NatsExtensionError { Message = errorMessage });
        }

        public T Value { get; }

        [NewtonJsonIgnore, SystemTextIgnore]
        public bool IsSuccess => _logic.IsSuccess;

        public string Error => _logic.Error.Message;

        public static implicit operator NatsExtensionResult(NatsExtensionResult<T> result)
        {
            return result.IsSuccess ? NatsExtensionResult.Success() : NatsExtensionResult.Failure(result.Error);
        }
    }

    public readonly struct NatsExtensionResult<T, E> : IResult, IValue<T>
        where E : IError
    {
        private readonly ResultCommonLogic<E> _logic;

        [NewtonJsonConstructor, SystemTextConstructor]
        internal NatsExtensionResult(T value, E error)
        {
            Value = value;
            Error = error;

            _logic = new ResultCommonLogic<E>(error);
        }

        public T Value { get; }

        public E Error { get; }

        [NewtonJsonIgnore, SystemTextIgnore]
        public bool IsSuccess => _logic.IsSuccess;

        public static implicit operator NatsExtensionResult(NatsExtensionResult<T, E> result)
        {
            return result.IsSuccess ? NatsExtensionResult.Success() : NatsExtensionResult.Failure(result.Error.Message);
        }

        public static implicit operator NatsExtensionResult<T>(NatsExtensionResult<T, E> result)
        {
            return result.IsSuccess ? NatsExtensionResult.Success(result.Value) : NatsExtensionResult.Failure<T>(result.Error.Message);
        }
    }

    public readonly struct NatsExtensionWebResult<T> : IResult, IValue<T>
    {
        private readonly WebResultCommonLogic<string> _logic;

        [NewtonJsonConstructor, SystemTextConstructor]
        internal NatsExtensionWebResult(T result, IEnumerable<string> errors)
        {
            Value = result;
            Errors = errors;

            _logic = new WebResultCommonLogic<string>(errors);
        }

        [JsonProperty("Result")]
        [JsonPropertyName("Result")]
        public T Value { get; }

        public IEnumerable<string> Errors { get; }

        [NewtonJsonIgnore, SystemTextIgnore]
        public bool IsSuccess => _logic.IsSuccess;

        public static NatsExtensionWebResult<T> Success(T result) => new(result, default);

        public static NatsExtensionWebResult<T> Failure(string error) => new(default, new[] { error });

        public static NatsExtensionWebResult<T> Failure(IEnumerable<string> errors) => new(default, errors);

        public static NatsExtensionWebResult<T> Failure(T result, string error) => new(result, new[] { error });

        public static NatsExtensionWebResult<T> Failure(T result, IEnumerable<string> errors) => new(result, errors);
    }
}
