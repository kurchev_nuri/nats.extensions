﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nats.Extensions.AspNet.Logger.Common;
using Nats.Extensions.AspNet.Logger.Options;
using Nats.Extensions.AspNet.Logger.Providers.Base;
using Nats.Extensions.Helper.Options;
using System;

namespace Nats.Extensions.AspNet.Logger.Providers
{
    [ProviderAlias(InternalConsts.StanLoggerName)]
    internal sealed class StanLoggerProvider : NatsExtensionsLoggerProvider<StanExtensionsOptions, StanLoggerOptions>
    {
        private readonly IServiceProvider _provider;
        private readonly IExternalScopeProvider _scopeProvider;

        public StanLoggerProvider(IServiceProvider provider, IExternalScopeProvider scopeProvider, IOptionsMonitor<StanLoggerOptions> options)
            : base(options)
        {
            _provider = provider;
            _scopeProvider = scopeProvider;
        }

        protected override NatsExtensionsLogger<StanExtensionsOptions, StanLoggerOptions> CreateNatsExtensionsLogger(string categoryName)
        {
            return new NatsExtensionsLogger<StanExtensionsOptions, StanLoggerOptions>(categoryName, _provider, _scopeProvider)
            {
                Options = Options
            };
        }
    }
}
