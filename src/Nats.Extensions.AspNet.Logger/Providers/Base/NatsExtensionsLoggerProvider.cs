﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Nats.Extensions.AspNet.Logger.Options;
using System;
using System.Collections.Concurrent;
using System.Linq;

namespace Nats.Extensions.AspNet.Logger.Providers.Base
{
    internal abstract class NatsExtensionsLoggerProvider<TOptions, TLoggerOptions> : ILoggerProvider
         where TOptions : class
         where TLoggerOptions : NatsExtensionsLoggerOptions
    {
        private readonly IOptionsMonitor<TLoggerOptions> _options;

        private readonly IDisposable _optionsReloadToken;
        private readonly ConcurrentDictionary<string, NatsExtensionsLogger<TOptions, TLoggerOptions>> _loggers;

        protected NatsExtensionsLoggerProvider(IOptionsMonitor<TLoggerOptions> options)
        {
            _options = options;
            _loggers = new ConcurrentDictionary<string, NatsExtensionsLogger<TOptions, TLoggerOptions>>();

            ReloadLoggerOptions(options.CurrentValue);
            _optionsReloadToken = _options.OnChange(ReloadLoggerOptions);
        }

        protected TLoggerOptions Options => _options.CurrentValue;

        public virtual ILogger CreateLogger(string categoryName)
        {
            if (string.IsNullOrWhiteSpace(categoryName))
            {
                return NullLogger.Instance;
            }

            return _loggers.GetOrAdd(categoryName, key => CreateNatsExtensionsLogger(key));
        }

        public virtual void Dispose() => _optionsReloadToken.Dispose();

        protected abstract NatsExtensionsLogger<TOptions, TLoggerOptions> CreateNatsExtensionsLogger(string categoryName);

        private void ReloadLoggerOptions(TLoggerOptions options)
        {
            _loggers.Values.ToList().ForEach(logger =>
            {
                logger.Options = options;
                logger.ClearPublishers();
            });
        }
    }
}
