﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nats.Extensions.AspNet.Logger.Common;
using Nats.Extensions.AspNet.Logger.Options;
using Nats.Extensions.AspNet.Logger.Providers.Base;
using Nats.Extensions.Helper.Options;
using System;

namespace Nats.Extensions.AspNet.Logger.Providers
{
    [ProviderAlias(InternalConsts.NatsLoggerName)]
    internal sealed class NatsLoggerProvider : NatsExtensionsLoggerProvider<NatsExtensionsOptions, NatsLoggerOptions>
    {
        private readonly IServiceProvider _provider;
        private readonly IExternalScopeProvider _scopeProvider;

        public NatsLoggerProvider(IServiceProvider provider, IExternalScopeProvider scopeProvider, IOptionsMonitor<NatsLoggerOptions> options)
            : base(options)
        {
            _provider = provider;
            _scopeProvider = scopeProvider;
        }

        protected override NatsExtensionsLogger<NatsExtensionsOptions, NatsLoggerOptions> CreateNatsExtensionsLogger(string categoryName)
        {
            return new NatsExtensionsLogger<NatsExtensionsOptions, NatsLoggerOptions>(categoryName, _provider, _scopeProvider)
            {
                Options = Options
            };
        }
    }
}
