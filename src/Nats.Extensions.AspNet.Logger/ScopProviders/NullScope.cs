﻿using System;
using System.Threading;

namespace Nats.Extensions.AspNet.Logger.ScopProviders
{
    internal sealed class NullScope : IDisposable
    {
        public static NullScope Instance => _provider.Value;

        private static readonly Lazy<NullScope> _provider = new(() => new NullScope(), LazyThreadSafetyMode.PublicationOnly);

        private NullScope() { }

        public void Dispose() { }
    }
}
