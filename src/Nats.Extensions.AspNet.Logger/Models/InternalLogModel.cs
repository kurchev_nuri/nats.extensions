﻿using Nats.Extensions.Broker.Models;

namespace Nats.Extensions.AspNet.Logger.Models
{
    internal sealed class InternalLogModel
    {
        public string Message { get; set; }

        public BaseMessage BaseMessage { get; set; }
    }
}
