﻿using Nats.Extensions.Broker.Attributes;
using Nats.Extensions.Broker.Common;
using System.Text.Json.Serialization;

namespace Nats.Extensions.AspNet.Logger.Models
{
    [MessageType(CommonMessageTypes.Logs)]
    public sealed class LogModel
    {
        [JsonPropertyName("data")]
        public LogData Data { get; set; }

        [JsonPropertyName("log_level")]
        public string LogLevel { get; set; }

        [JsonPropertyName("service_name")]
        public string ServiceName { get; set; }

        [JsonPropertyName("environment")]
        public string Environment { get; set; }
    }
}
