﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Nats.Extensions.AspNet.Logger.Models
{
    public class LogData
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("category_name")]
        public string CategoryName { get; set; }

        [JsonPropertyName("variables")]
        public IDictionary<string, object> Variables { get; set; }

        [JsonProperty("trace")]
        [JsonPropertyName("trace")]
        public Exception Exception { get; set; }
    }
}
