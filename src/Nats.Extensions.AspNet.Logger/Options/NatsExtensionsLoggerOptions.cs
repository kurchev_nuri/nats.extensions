﻿namespace Nats.Extensions.AspNet.Logger.Options
{
    public class NatsExtensionsLoggerOptions
    {
        public string Subject { get; set; }

        public string ServiceName { get; set; }

        public string Environment { get; set; }

        public bool IncludeScopes { get; set; }
    }
}
