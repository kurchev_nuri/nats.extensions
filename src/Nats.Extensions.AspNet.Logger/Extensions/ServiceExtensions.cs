﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using Nats.Extensions.AspNet.Logger.Common;
using Nats.Extensions.AspNet.Logger.Options;
using Nats.Extensions.AspNet.Logger.Providers;
using Scrutor;
using System;
using System.Linq;

namespace Nats.Extensions.AspNet.Logger.Extensions
{
    public static class ServiceExtensions
    {
        public static ILoggingBuilder AddNatsExtensionsLogger(this ILoggingBuilder builder, Action<NatsExtensionsLoggerOptions> configure)
        {
            builder.Services.AddOptions<NatsLoggerOptions>()
                .Configure(configure)
                .Validate<IValidator<NatsLoggerOptions>>(Validate);

            builder.Services.AddOptions<StanLoggerOptions>()
                .Configure(configure)
                .Validate<IValidator<StanLoggerOptions>>(Validate);

            builder.AddConfiguration();

            LoggerProviderOptions.RegisterProviderOptions<StanLoggerOptions, StanLoggerProvider>(builder.Services);
            LoggerProviderOptions.RegisterProviderOptions<NatsLoggerOptions, NatsLoggerProvider>(builder.Services);

            builder.Services.AddSingleton<IExternalScopeProvider, LoggerExternalScopeProvider>();

            builder.Services.Scan(u => u.FromAssemblies(typeof(ServiceExtensions).Assembly)
                .AddClasses(v => v.AssignableTo<ILoggerProvider>())
                    .AsImplementedInterfaces()
                    .WithSingletonLifetime()
                .AddClasses(v => v.AssignableTo(typeof(IValidator<>)))
                    .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                    .As(type => new[] { type.GetInterfaces().Single(u => u.IsGenericType && u.Name.Contains(nameof(IValidator))) })
                    .WithSingletonLifetime()
            );

            return builder;
        }

        /// <summary>
        /// Логирование в NSS
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="serviceName">Должен совпадать с именем сервиса</param>
        /// <returns></returns>
        public static IHostBuilder UseNatsExtensionsLogger(this IHostBuilder builder, string serviceName = default)
        {
            return builder.ConfigureLogging((context, loggingBuilder) =>
            {
                var (configuration, environment) = (context.Configuration, context.HostingEnvironment);

                loggingBuilder.AddNatsExtensionsLogger(options =>
                {
                    options.Environment = environment.EnvironmentName;
                    options.Subject = configuration.GetSection(InternalConsts.LogsSubjectName).Value;
                    options.ServiceName = !string.IsNullOrWhiteSpace(serviceName) ? serviceName : configuration.GetSection(InternalConsts.ServiceName).Value;
                });
            });
        }

        #region Private Methods

        private static bool Validate<TOptions>(TOptions options, IValidator<TOptions> validator)
        {
            validator.ValidateAndThrow(options);
            return true;
        }

        #endregion
    }
}