﻿using Microsoft.Extensions.Logging;
using Nats.Extensions.AspNet.Logger.Models;
using Nats.Extensions.Broker.Models;
using System;
using System.Text;

namespace Nats.Extensions.AspNet.Logger.Extensions
{
    public static class LoggerExtensions
    {
        public static string GetScopeInformation(this IExternalScopeProvider provider)
        {
            var stringBuilder = new StringBuilder();

            provider.ForEachScope((scope, builder) =>
            {
                if (builder.Length == 0)
                {
                    builder.Append("=> ");
                }
                else
                {
                    builder.Append(" => ");
                }

                builder.Append(scope);

            }, stringBuilder);

            return (stringBuilder.Length > 0) ? stringBuilder.AppendLine().ToString() : string.Empty;
        }

        //------------------------------------------DEBUG------------------------------------------//

        public static void LogDebug(this ILogger logger, EventId eventId, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Debug, eventId, exception, message, baseMessage);
        }

        public static void LogDebug(this ILogger logger, EventId eventId, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Debug, eventId, default, message, baseMessage);
        }

        public static void LogDebug(this ILogger logger, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Debug, default, exception, message, baseMessage);
        }

        public static void LogDebug(this ILogger logger, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Debug, default, default, message, baseMessage);
        }

        //------------------------------------------TRACE------------------------------------------//

        public static void LogTrace(this ILogger logger, EventId eventId, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Trace, eventId, exception, message, baseMessage);
        }

        public static void LogTrace(this ILogger logger, EventId eventId, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Trace, eventId, default, message, baseMessage);
        }

        public static void LogTrace(this ILogger logger, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Trace, default, exception, message, baseMessage);
        }

        public static void LogTrace(this ILogger logger, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Trace, default, default, message, baseMessage);
        }

        //------------------------------------------INFORMATION------------------------------------------//

        public static void LogInformation(this ILogger logger, EventId eventId, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Information, eventId, exception, message, baseMessage);
        }

        public static void LogInformation(this ILogger logger, EventId eventId, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Information, eventId, default, message, baseMessage);
        }

        public static void LogInformation(this ILogger logger, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Information, default, exception, message, baseMessage);
        }

        public static void LogInformation(this ILogger logger, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Information, default, default, message, baseMessage);
        }

        //------------------------------------------WARNING------------------------------------------//

        public static void LogWarning(this ILogger logger, EventId eventId, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Warning, eventId, exception, message, baseMessage);
        }

        public static void LogWarning(this ILogger logger, EventId eventId, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Warning, eventId, default, message, baseMessage);
        }

        public static void LogWarning(this ILogger logger, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Warning, default, exception, message, baseMessage);
        }

        public static void LogWarning(this ILogger logger, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Warning, default, default, message, baseMessage);
        }

        //------------------------------------------ERROR------------------------------------------//

        public static void LogError(this ILogger logger, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Error, default, exception, message, baseMessage);
        }

        public static void LogError(this ILogger logger, EventId eventId, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Error, eventId, exception, message, baseMessage);
        }

        public static void LogError(this ILogger logger, EventId eventId, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Error, eventId, default, message, baseMessage);
        }

        public static void LogError(this ILogger logger, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Error, default, default, message, baseMessage);
        }

        //------------------------------------------CRITICAL------------------------------------------//

        public static void LogCritical(this ILogger logger, EventId eventId, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Critical, eventId, exception, message, baseMessage);
        }

        public static void LogCritical(this ILogger logger, EventId eventId, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Critical, eventId, default, message, baseMessage);
        }

        public static void LogCritical(this ILogger logger, Exception exception, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Critical, default, exception, message, baseMessage);
        }

        public static void LogCritical(this ILogger logger, string message, BaseMessage baseMessage)
        {
            Log(logger, LogLevel.Critical, default, default, message, baseMessage);
        }

        //------------------------------------------COMMON------------------------------------------//

        public static void Log(this ILogger logger, LogLevel logLevel, EventId eventId, Exception exception, string message, BaseMessage baseMessage)
        {
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            logger.Log(logLevel, eventId, new InternalLogModel { Message = message, BaseMessage = baseMessage }, exception, (model, exception) => model.Message);
        }
    }
}
