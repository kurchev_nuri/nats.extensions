﻿using FluentValidation;
using Nats.Extensions.AspNet.Logger.Options;

namespace Nats.Extensions.AspNet.Logger.Validators.Options
{
    internal sealed class StanLoggerOptionsValidator : AbstractValidator<StanLoggerOptions>
    {
        public StanLoggerOptionsValidator()
        {
            Include(new NatsExtensionsLoggerOptionsValidator());
        }
    }
}
