﻿using FluentValidation;
using Nats.Extensions.AspNet.Logger.Options;

namespace Nats.Extensions.AspNet.Logger.Validators.Options
{
    internal sealed class NatsLoggerOptionsValidator : AbstractValidator<NatsLoggerOptions>
    {
        public NatsLoggerOptionsValidator()
        {
            Include(new NatsExtensionsLoggerOptionsValidator());
        }
    }
}
