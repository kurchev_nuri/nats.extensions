﻿using FluentValidation;
using Nats.Extensions.AspNet.Logger.Options;

namespace Nats.Extensions.AspNet.Logger.Validators.Options
{
    internal sealed class NatsExtensionsLoggerOptionsValidator : AbstractValidator<NatsExtensionsLoggerOptions>
    {
        public NatsExtensionsLoggerOptionsValidator()
        {
            RuleFor(u => u.Subject)
                .NotEmpty()
                .WithMessage($"Входное значение `{nameof(NatsExtensionsLoggerOptions.Subject)}` не может быть пустым или null.");

            RuleFor(u => u.ServiceName)
                .NotEmpty()
                .WithMessage($"Входное значение `{nameof(NatsExtensionsLoggerOptions.ServiceName)}` не может быть пустым или null.");

            RuleFor(u => u.Environment)
                .NotEmpty()
                .WithMessage($"Входное значение `{nameof(NatsExtensionsLoggerOptions.Environment)}` не может быть пустым или null.");
        }
    }
}
