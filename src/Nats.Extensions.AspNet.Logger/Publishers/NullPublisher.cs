﻿using Nats.Extensions.Broker;
using Nats.Extensions.Broker.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.AspNet.Logger.Publishers
{
    internal sealed class NullPublisher<TOptions> : IPublisher<TOptions>
         where TOptions : class
    {
        public static NullPublisher<TOptions> Instance => _provider.Value;

        private static readonly Lazy<NullPublisher<TOptions>> _provider = new(() => new NullPublisher<TOptions>(), LazyThreadSafetyMode.PublicationOnly);

        public Task PublishAsync<TContent>(string subject, TContent content, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }

        public Task PublishAsync<TContent>(string subject, BaseMessage<TContent> message, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }
    }
}
