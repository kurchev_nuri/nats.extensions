﻿namespace Nats.Extensions.AspNet.Logger.Common
{
    internal static class InternalConsts
    {
        public const string NatsLoggerName = "Nats";
        public const string StanLoggerName = "Stan";

        public const string ServiceName = "Settings:ServiceName";
        public const string LogsSubjectName = "Settings:LogsSubject";
    }
}
