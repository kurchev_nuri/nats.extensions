﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nats.Extensions.AspNet.Logger.Extensions;
using Nats.Extensions.AspNet.Logger.Models;
using Nats.Extensions.AspNet.Logger.Options;
using Nats.Extensions.AspNet.Logger.Publishers;
using Nats.Extensions.AspNet.Logger.ScopProviders;
using Nats.Extensions.Broker;
using Nats.Extensions.Broker.Extensions;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.AspNet.Logger
{
    class NatsExtensionsLogger<TOptions, TLoggerOptions> : ILogger
        where TOptions : class
        where TLoggerOptions : NatsExtensionsLoggerOptions
    {
        private readonly string _categoryName;
        private readonly IServiceProvider _provider;
        private readonly IExternalScopeProvider _scopeProvider;

        private readonly ConcurrentDictionary<string, Lazy<IPublisher<TOptions>>> _publishers;

        public NatsExtensionsLogger(string categoryName, IServiceProvider provider, IExternalScopeProvider scopeProvider)
        {
            _provider = provider;
            _categoryName = categoryName;
            _scopeProvider = scopeProvider;

            _publishers = new ConcurrentDictionary<string, Lazy<IPublisher<TOptions>>>();
        }

        internal TLoggerOptions Options { set; get; }

        internal void ClearPublishers() => _publishers.Clear();

        public IDisposable BeginScope<TState>(TState state) => _scopeProvider?.Push(state) ?? NullScope.Instance;

        public bool IsEnabled(LogLevel logLevel) => logLevel != LogLevel.None;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }

            var message = formatter?.Invoke(state, exception);
            var scopeInfo = Options.IncludeScopes ? _scopeProvider.GetScopeInformation() : string.Empty;
            var content = CreateLog(logLevel, exception, string.Concat(scopeInfo, " ", message).Trim());

            Task.Run(() =>
            {
                if (state is InternalLogModel model)
                {
                    GetPublisher(_categoryName).BasedOn(model.BaseMessage).PublishAsync(Options.Subject, content);
                }
                else
                {
                    GetPublisher(_categoryName).PublishAsync(Options.Subject, content);
                }
            });
        }

        #region Private Methods

        private LogModel CreateLog(LogLevel logLevel, Exception exception = default, string message = default)
        {
            return new LogModel
            {
                LogLevel = logLevel.ToString(),
                Environment = Options.Environment,
                ServiceName = Options.ServiceName,
                Data = new LogData
                {
                    Message = message,
                    Exception = exception,
                    CategoryName = _categoryName
                }
            };
        }

        private IPublisher<TOptions> GetPublisher(string categoryName)
        {
            var publisher = _publishers.GetOrAdd(categoryName, key =>
            {
                return new Lazy<IPublisher<TOptions>>(CreatePublisher, LazyThreadSafetyMode.ExecutionAndPublication);
            });

            return publisher.Value;
        }

        private IPublisher<TOptions> CreatePublisher()
        {
            try
            {
                return _provider.GetRequiredService<IPublisher<TOptions>>();
            }
            catch
            {
                return NullPublisher<TOptions>.Instance;
            }
        }

        #endregion
    }
}
