﻿using Bogus;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Nats.Extensions.Helper.Clients.Nats;
using Nats.Extensions.Helper.Factories;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Tests.Common;
using Nats.Extensions.Helper.Tests.Helpers;
using Nats.Extensions.Helper.Tests.Resources.NssClient;
using NATS.Client;
using System;
using System.Threading;
using Xunit;
using Xunit.Abstractions;

namespace Nats.Extensions.Helper.Tests
{
    public class NatsClientTests : BaseClientTest<NatsConnectionOptions>
    {
        private readonly ITestOutputHelper _output;
        private readonly Mock<ILogger<NatsClient>> _logger;
        private readonly Mock<IConnectionFactory<NatsConnectionOptions, IConnection>> _factory;

        public NatsClientTests(ITestOutputHelper output)
        {
            _output = output;
            _logger = new Mock<ILogger<NatsClient>>();
            _factory = new Mock<IConnectionFactory<NatsConnectionOptions, IConnection>>();
        }

        protected override void SetupOptions(Mock<IOptions<NatsConnectionOptions>> options)
        {
            var fakeOptions = new Faker<NatsConnectionOptions>()
                .RuleFor(u => u.Name, u => u.Random.String2(16))
                .RuleFor(u => u.User, u => u.Random.String2(16))
                .RuleFor(u => u.Password, u => u.Random.String2(16))
                .RuleFor(u => u.Uri, u => new Uri(u.PickRandom(InternalConsts.FakeUrls)))
                .RuleFor(u => u.ReconnectTimeout, u => InternalConsts.ReconnectTimeout);

            options.Setup(u => u.Value).Returns(() => fakeOptions.Generate());
        }

        [Theory(DisplayName = "Подключение/переподключение к `NATS` когда соединение разорвано.")]
        [NatsClientData]
        public void Connect_ReturnsConnection(int repetitionAmount)
        {
            using var countdown = new CountdownEvent(repetitionAmount);

            _factory.Setup(u => u.CreateConnection(It.IsNotNull<NatsConnectionOptions>(), It.IsAny<Action<(ConnState?, Exception)>>()))
                .Returns(ConnectionHelper.MockNatsConnection.Object)
                .Callback<NatsConnectionOptions, Action<(ConnState?, Exception)>>((options, onClosed) =>
                {
                    if (countdown.CurrentCount != 0)
                    {
                        countdown.Signal();
                        onClosed((ConnState.CLOSED, new Exception("Connection is closed.")));
                    }
                });

            using var client = new NatsClient(_logger.Object, Options.Object, ConnectionState.Object, _factory.Object);
            using var subscription = client.Connection.Subscribe(connection => _output.WriteLine(connection.State.ToString()));

            countdown.Wait();

            CommonHelpers.AwaitRxDisposing();

            _factory.Verify(u => u.DestroyConnection(It.IsNotNull<NatsConnectionOptions>()), Times.Exactly(repetitionAmount));
            _factory.Verify(u => u.CreateConnection(It.IsNotNull<NatsConnectionOptions>(), It.IsAny<Action<(ConnState?, Exception)>>()), Times.Exactly(repetitionAmount));
        }

        [Theory(DisplayName = "Подключение/переподключение к `NATS` когда сервер недоступен.")]
        [StanClientData]
        public void Connect_WhenServerIsUnavailable_ReturnConnection(int repetitionAmount)
        {
            using var countdown = new CountdownEvent(repetitionAmount);

            _factory.Setup(u => u.CreateConnection(It.IsNotNull<NatsConnectionOptions>(), It.IsAny<Action<(ConnState?, Exception)>>()))
                .Returns(() =>
                {
                    if (countdown.CurrentCount != 0)
                    {
                        countdown.Signal();
                        throw new Exception("Unable to connect to a server.");
                    }

                    return ConnectionHelper.MockNatsConnection.Object;
                });

            using var client = new NatsClient(_logger.Object, Options.Object, ConnectionState.Object, _factory.Object);
            using var subscription = client.Connection.Subscribe(connection => _output.WriteLine(connection.State.ToString()));

            countdown.Wait();

            CommonHelpers.AwaitRxDisposing();

            _factory.Verify(u => u.DestroyConnection(It.IsNotNull<NatsConnectionOptions>()), Times.Exactly(repetitionAmount));
            _factory.Verify(u => u.CreateConnection(It.IsNotNull<NatsConnectionOptions>(), It.IsAny<Action<(ConnState?, Exception)>>()), Times.Exactly(repetitionAmount));
        }

        [Fact(DisplayName = "Вызов события о том что клиент потерял соединения с `NATS`.")]
        public void Connect_InvokeConnectionIsLostHandler()
        {
            using var countdown = new CountdownEvent(1);

            _factory.Setup(u => u.CreateConnection(It.IsNotNull<NatsConnectionOptions>(), It.IsAny<Action<(ConnState?, Exception)>>()))
                .Returns(ConnectionHelper.MockNatsConnection.Object)
                .Callback<NatsConnectionOptions, Action<(ConnState?, Exception)>>((options, onClosed) =>
                {
                    if (countdown.CurrentCount != 0)
                    {
                        countdown.Signal();
                        onClosed((ConnState.CLOSED, new Exception("Connection is closed.")));
                    }
                });

            using var client = new NatsClient(_logger.Object, Options.Object, ConnectionState.Object, _factory.Object);
            using var subscription = client.Connection.Subscribe(connection => _output.WriteLine(connection.State.ToString()));

            countdown.Wait();

            ConnectionState.Verify(u => u.SetConnectionIsInvalid(), Times.Once());
            ConnectionState.Verify(u => u.SetConnectionIsValidAsync(It.IsAny<CancellationToken>()), Times.Once());
        }
    }
}
