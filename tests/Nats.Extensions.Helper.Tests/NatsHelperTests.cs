﻿using Bogus;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Nats.Extensions.Helper.Implementations.Nats;
using Nats.Extensions.Helper.Models;
using Nats.Extensions.Helper.Options;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Tests.Helpers;
using Nats.Extensions.Helper.Tests.Resources.NssConnection;
using Nats.Extensions.Helper.Tests.Resources.NssHelper.Publications;
using Nats.Extensions.Helper.Tests.Resources.NssHelper.Subcriptions;
using NATS.Client;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Nats.Extensions.Helper.Tests
{
    public sealed class NatsHelperTests : BaseHelperTest<NatsConnectionOptions, IConnection>
    {
        private readonly NatsHelper _helper;

        public NatsHelperTests()
        {
            var logger = new Mock<ILogger<NatsHelper>>();

            _helper = new NatsHelper(logger.Object, Client.Object);
        }

        protected override void SetupOptions(Mock<IOptions<NatsConnectionOptions>> options)
        {
            options.Setup(u => u.Value)
                .Returns(() => new Faker<NatsConnectionOptions>().Generate());
        }

        #region Publish Tests

        [Theory(DisplayName = "Публикация валидных данных в канал `NATS`.")]
        [NssCorrectDataForPublish]
        public async Task NATS_PublishAsync_WithCorrectParams_NotThrowException(string subject, byte[] data, CancellationToken cancellationToken)
        {
            ConnectionSubject.OnNext(GetValidNatsConnection().Object);

            Func<Task> publish = () => _helper.PublishAsync(subject, data, cancellationToken);

            await publish.Should().NotThrowAsync();
        }
        
        [Theory(DisplayName = "Публикация невалидных данных в канал `NATS`. Генерируется исключение `TaskCanceledException`.")]
        [NssIncorrectDataForPublish]
        public async Task NATS_PublishAsync_WithIncorrectParams_ThrowTaskCanceledException(string subject, byte[] data, CancellationToken cancellationToken)
        {
            ConnectionSubject.OnNext(GetValidNatsConnection().Object);

            Func<Task> publish = () => _helper.PublishAsync(subject, data, cancellationToken);

            await publish.Should().ThrowExactlyAsync<TaskCanceledException>();
        }

        [Theory(DisplayName = "Публикация сообщений в канал `NATS` с неверными параметрами. Генерируется исключение `ArgumentNullException`.")]
        [NssHelperIncorrectPublishData]
        public async Task PublishToSubject_WithIncorrectParams_ThrowArgumentNullException(string subject, byte[] message, CancellationToken cancellationToken)
        {
            Func<Task> publish = () => _helper.PublishAsync(subject, message, cancellationToken);

            await publish.Should().ThrowExactlyAsync<ArgumentException>();
        }

        [Theory(DisplayName = "Публикация сообщений в канал `NATS` c невалидным соединением к NSS. Генерируется исключение `InvalidOperationException`.")]
        [NssHelperCorrectPublishData]
        public async Task PublishToSubject_WithIncorrectNssConnection_ThrowInvalidOperationException(string subject, byte[] message, CancellationToken cancellationToken)
        {
            ConnectionSubject.OnNext(GetClosedNatsConnection().Object);

            Func<Task> publish = () => _helper.PublishAsync(subject, message, cancellationToken);

            await publish.Should().ThrowExactlyAsync<InvalidOperationException>();
        }

        [Theory(DisplayName = "Публикация сообщений в канал `NATS` c валидным соединением к NSS.")]
        [NssHelperCorrectPublishData]
        public async Task PublishToSubject_WithCorrectNssConnection_NoThrowExceptions(string subject, byte[] message, CancellationToken cancellationToken)
        {
            ConnectionSubject.OnNext(GetValidNatsConnection().Object);

            Func<Task> publish = () => _helper.PublishAsync(subject, message, cancellationToken);

            await publish.Should().NotThrowAsync();
        }

        #endregion

        #region General Subscription Tests

        [Theory(DisplayName = "Подписка на канал `NATS` с неверными параметрами. Генерируется исключение `ArgumentNullException`.")]
        [NatsHelperSubscriptionIncorrectData]
        public void SubscribeToSubject_WithIncorrectParams_ThrowArgumentNullException(string subject, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            Func<IDisposable> subscribe = () => _helper.Subscribe(subject, handler, options);

            subscribe.Should().ThrowExactly<ArgumentException>();
        }

        [Theory(DisplayName = "Подписка на канал `NATS` с верными параметрами. Возвращается `IDisposable`.")]
        [NatsHelperSubscriptionCorrectData]
        public void SubscribeToSubject_WithCorrectParams_ReturnIDisposable(string subject, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            Func<IDisposable> subscribe = () => _helper.Subscribe(subject, handler, options);

            subscribe.Should().NotThrow()
                .Which.Should().NotBeNull()
                .And.BeAssignableTo<IDisposable>();
        }

        [Theory(DisplayName = "Подписка на канал `NATS` c валидным соединением к NSS.")]
        [NatsHelperSubscriptionCorrectData]
        public void SubscribeToSubject_WithCorrectNssConnection_NoThrowExceptions(string subject, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            using var subscription = _helper.Subscribe(subject, handler, options);

            var nssConnection = GetValidNatsConnection();

            ConnectionSubject.OnNext(nssConnection.Object);

            nssConnection.Verify(u => u.SubscribeAsync(It.IsAny<string>(), It.IsAny<EventHandler<MsgHandlerEventArgs>>()), Times.Once());
        }

        [Theory(DisplayName = "Подписка на канал `NATS` c невалидным соединением к NSS.")]
        [NatsHelperSubscriptionCorrectData]
        public void SubscribeToSubject_WithIncorrectNssConnection_NoThrowExceptions(string subject, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            using var subscription = _helper.Subscribe(subject, handler, options);

            var nssConnection = GetClosedNatsConnection();

            ConnectionSubject.OnNext(default);
            ConnectionSubject.OnNext(nssConnection.Object);

            nssConnection.Verify(u => u.SubscribeAsync(It.IsAny<string>(), It.IsAny<EventHandler<MsgHandlerEventArgs>>()), Times.Never());
        }

        [Theory(DisplayName = "Отписка от канала `NATS`.")]
        [NatsHelperSubscriptionCorrectData]
        public void UnsubscribeFromSubject_WithCorrectNssConnection_NoThrowExceptions(string subject, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            var (nssConnection, nssSubscription) = GetValidNatsConnectionAndSubscription();

            using (_helper.Subscribe(subject, handler, options))
            {
                ConnectionSubject.OnNext(nssConnection.Object);
            }

            nssSubscription.Verify(u => u.Dispose(), Times.Once());
        }

        [Theory(DisplayName = "Переподписка на канал `NATS`.")]
        [NatsHelperSubscriptionCorrectData]
        public void ResubscribeToSubject_WithCorrectNssConnection_NoThrowExceptions(string subject, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            var (nssConnection, nssSubscription) = GetValidNatsConnectionAndSubscription();

            using var subscription = _helper.Subscribe(subject, handler, options);

            ConnectionSubject.OnNext(nssConnection.Object);
            ConnectionSubject.OnNext(nssConnection.Object);

            nssSubscription.Verify(u => u.Dispose(), Times.Once());
            nssConnection.Verify(u => u.SubscribeAsync(It.IsAny<string>(), It.IsAny<EventHandler<MsgHandlerEventArgs>>()), Times.Exactly(2));
        }

        #endregion

        #region General Subscription With Queue Tests

        [Theory(DisplayName = "Подписка на канал `NATS` с очередью с неверными параметрами. Генерируется исключение `ArgumentNullException`.")]
        [NatsHelperSubscriptionWithQueueIncorrectData]
        public void SubscribeToSubjectWithQueue_WithIncorrectParams_ThrowArgumentNullException(string subject, string queue, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            Func<IDisposable> subscribe = () => _helper.Subscribe(subject, queue, handler, options);

            subscribe.Should().ThrowExactly<ArgumentException>();
        }

        [Theory(DisplayName = "Подписка на канал `NATS` с очередью с верными параметрами. Возвращается `IDisposable`.")]
        [NatsHelperSubscriptionWithQueueCorrectData]
        public void SubscribeToSubjectWithQueue_WithCorrectParams_ReturnIDisposable(string subject, string queue, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            Func<IDisposable> subscribe = () => _helper.Subscribe(subject, queue, handler, options);

            subscribe.Should().NotThrow()
                .Which.Should().NotBeNull()
                .And.BeAssignableTo<IDisposable>();
        }

        [Theory(DisplayName = "Подписка на канал `NATS` с очередью c валидным соединением к NSS.")]
        [NatsHelperSubscriptionWithQueueCorrectData]
        public void SubscribeToSubjectWithQueue_WithCorrectNssConnection_NoThrowExceptions(string subject, string queue, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            using var subscription = _helper.Subscribe(subject, queue, handler, options);

            var nssConnection = GetValidNatsConnection();

            ConnectionSubject.OnNext(nssConnection.Object);

            nssConnection.Verify(u => u.SubscribeAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<EventHandler<MsgHandlerEventArgs>>()), Times.Once());
        }

        [Theory(DisplayName = "Подписка на канал `NATS` с очередью c невалидным соединением к NSS.")]
        [NatsHelperSubscriptionWithQueueCorrectData]
        public void SubscribeToSubjectWithQueue_WithIncorrectNssConnection_NoThrowExceptions(string subject, string queue, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            using var subscription = _helper.Subscribe(subject, queue, handler, options);

            var nssConnection = GetClosedNatsConnection();

            ConnectionSubject.OnNext(default);
            ConnectionSubject.OnNext(nssConnection.Object);

            nssConnection.Verify(u => u.SubscribeAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<EventHandler<MsgHandlerEventArgs>>()), Times.Never());
        }

        [Theory(DisplayName = "Отписка от канала `NATS` с очередью.")]
        [NatsHelperSubscriptionWithQueueCorrectData]
        public void UnsubscribeFromSubjectWithQueue_WithCorrectNssConnection_NoThrowExceptions(string subject, string queue, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            var (nssConnection, nssSubscription) = GetValidNatsConnectionAndSubscriptionWithQueue();

            using (_helper.Subscribe(subject, queue, handler, options))
            {
                ConnectionSubject.OnNext(nssConnection.Object);
            }

            nssSubscription.Verify(u => u.Dispose(), Times.Once());
        }

        [Theory(DisplayName = "Переподписка на канал `NATS` с очередью.")]
        [NatsHelperSubscriptionWithQueueCorrectData]
        public void ResubscribeToSubjectWithQueue_WithCorrectNssConnection_NoThrowExceptions(string subject, string queue, Func<Message, CancellationToken, Task> handler, NatsExtensionsOptions options)
        {
            var (nssConnection, nssSubscription) = GetValidNatsConnectionAndSubscriptionWithQueue();

            using var subscription = _helper.Subscribe(subject, queue, handler, options);

            ConnectionSubject.OnNext(nssConnection.Object);
            ConnectionSubject.OnNext(nssConnection.Object);

            nssSubscription.Verify(u => u.Dispose(), Times.Once());
            nssConnection.Verify(u => u.SubscribeAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<EventHandler<MsgHandlerEventArgs>>()), Times.Exactly(2));
        }

        #endregion
    }
}
