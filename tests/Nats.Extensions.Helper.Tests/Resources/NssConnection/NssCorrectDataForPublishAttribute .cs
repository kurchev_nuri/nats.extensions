﻿using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;
using Xunit.Sdk;

namespace Nats.Extensions.Helper.Tests.Resources.NssConnection
{
    internal sealed class NssCorrectDataForPublishAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            var cancellationToken = new CancellationToken();
            yield return new object[] { "test_subject", Encoding.UTF8.GetBytes("Test-Data"), cancellationToken };
        }
    }
}
