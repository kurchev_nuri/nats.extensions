﻿using System.Collections.Generic;
using System.Reflection;
using Xunit.Sdk;

namespace Nats.Extensions.Helper.Tests.Resources.NssClient
{
    internal sealed class NatsClientDataAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            yield return new object[] { 3 };
        }
    }
}
