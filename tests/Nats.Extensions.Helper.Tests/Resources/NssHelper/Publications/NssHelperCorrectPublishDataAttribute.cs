﻿using Bogus;
using Nats.Extensions.Helper.Tests.Common;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using Xunit.Sdk;

namespace Nats.Extensions.Helper.Tests.Resources.NssHelper.Publications
{
    internal sealed class NssHelperCorrectPublishDataAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            yield return new object[] { "subject", new Randomizer().Bytes(InternalConsts.MessageLength), new CancellationToken() };
        }
    }
}
