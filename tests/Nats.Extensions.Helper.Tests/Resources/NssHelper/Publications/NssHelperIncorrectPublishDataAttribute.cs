﻿using Bogus;
using Nats.Extensions.Helper.Tests.Common;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using Xunit.Sdk;

namespace Nats.Extensions.Helper.Tests.Resources.NssHelper.Publications
{
    internal sealed class NssHelperIncorrectPublishDataAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            var message = new Randomizer().Bytes(InternalConsts.MessageLength);

            yield return new object[] { "subject", null, new CancellationToken() };
            yield return new object[] { null, message, new CancellationToken() };
            yield return new object[] { string.Empty, message, new CancellationToken() };
        }
    }
}
