﻿using Bogus;
using Nats.Extensions.Helper.Models;
using Nats.Extensions.Helper.Options;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace Nats.Extensions.Helper.Tests.Resources.NssHelper.Subcriptions
{
    internal sealed class NatsHelperSubscriptionWithQueueIncorrectDataAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            Func<Message, CancellationToken, Task> handler = (message, cancellationToken) => Task.CompletedTask;

            yield return new object[] { null, null, handler, new Faker<NatsExtensionsOptions>().Generate() };
            yield return new object[] { string.Empty, string.Empty, handler, new Faker<NatsExtensionsOptions>().Generate() };
            yield return new object[] { "subject", null, default, new Faker<NatsExtensionsOptions>().Generate() };
            yield return new object[] { "subject", string.Empty, default, new Faker<NatsExtensionsOptions>().Generate() };
            yield return new object[] { null, "queue", default, new Faker<NatsExtensionsOptions>().Generate() };
            yield return new object[] { string.Empty, "queue", default, new Faker<NatsExtensionsOptions>().Generate() };
            yield return new object[] { "subject", "queue", default, new Faker<NatsExtensionsOptions>().Generate() };
            yield return new object[] { string.Empty, "queue", handler, new Faker<NatsExtensionsOptions>().Generate() };
        }
    }
}
