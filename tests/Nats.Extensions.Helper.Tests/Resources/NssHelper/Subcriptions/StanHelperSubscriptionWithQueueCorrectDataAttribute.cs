﻿using Bogus;
using Nats.Extensions.Helper.Models;
using Nats.Extensions.Helper.Options;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace Nats.Extensions.Helper.Tests.Resources.NssHelper.Subcriptions
{
    internal sealed class StanHelperSubscriptionWithQueueCorrectDataAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            Func<Message, CancellationToken, Task> handler = (message, cancellationToken) => Task.CompletedTask;

            yield return new object[] { "subject", "queue", handler, new Faker<StanExtensionsOptions>().Generate() };
        }
    }
}
