﻿using Bogus;
using Nats.Extensions.Helper.Models;
using Nats.Extensions.Helper.Options;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace Nats.Extensions.Helper.Tests.Resources.NssHelper.Subcriptions
{
    internal sealed class StanHelperSubscriptionIncorrectDataAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            Func<Message, CancellationToken, Task> handler = (message, cancellationToken) => Task.CompletedTask;

            yield return new object[] { null, handler, new Faker<StanExtensionsOptions>().Generate() };
            yield return new object[] { string.Empty, handler, new Faker<StanExtensionsOptions>().Generate() };
            yield return new object[] { "subject", default, new Faker<StanExtensionsOptions>().Generate() };
        }
    }
}
