﻿using System;

namespace Nats.Extensions.Helper.Tests.Common
{
    internal static class InternalConsts
    {
        public const int MaxReplayCount = 1;
        public const int StartConnectingImmediately = -1;

        public static readonly TimeSpan ReconnectTimeout = TimeSpan.FromSeconds(2);
        public static readonly string[] FakeUrls = new[] { "nats://fake:4222", "nats://fake:4223", "nats://fake:4224" };

        public const int MessageLength = 10;
    }
}
