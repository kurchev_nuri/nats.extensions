﻿using Bogus;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Nats.Extensions.Helper.Clients;
using Nats.Extensions.Helper.Clients.Stan;
using Nats.Extensions.Helper.Factories;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Tests.Common;
using Nats.Extensions.Helper.Tests.Helpers;
using Nats.Extensions.Helper.Tests.Resources.NssClient;
using NATS.Client;
using STAN.Client;
using System;
using System.Reactive.Subjects;
using System.Threading;
using Xunit;

namespace Nats.Extensions.Helper.Tests
{
    public sealed class StanClientTests : BaseClientTest<StanConnectionOptions>, IDisposable
    {
        private readonly StanClient _client;
        private readonly Subject<IConnection> _connectionSubject;
        private readonly Mock<IConnectionFactory<StanConnectionOptions, IStanConnection>> _factory;

        public StanClientTests()
        {
            var logger = new Mock<ILogger<StanClient>>();
            var client = new Mock<IClient<IConnection>>();

            _connectionSubject = new Subject<IConnection>();
            _factory = new Mock<IConnectionFactory<StanConnectionOptions, IStanConnection>>();

            client.Setup(client => client.Connection).Returns(ConnectionHelper.CreateObservable(_connectionSubject));

            _client = new StanClient(logger.Object, client.Object, Options.Object, ConnectionState.Object, _factory.Object);
        }

        public void Dispose() => _client.Dispose();

        protected override void SetupOptions(Mock<IOptions<StanConnectionOptions>> options)
        {
            var fakeOptions = new Faker<StanConnectionOptions>()
                .RuleFor(u => u.ClientId, u => u.Random.String2(16))
                .RuleFor(u => u.ClusterId, u => u.Random.String2(8))
                .RuleFor(u => u.Uri, u => new Uri(u.PickRandom(InternalConsts.FakeUrls)))
                .RuleFor(u => u.ReconnectTimeout, u => u.Date.Timespan(InternalConsts.ReconnectTimeout));

            options.Setup(u => u.Value).Returns(() => fakeOptions.Generate());
        }

        [Theory(DisplayName = "Подключение/переподключение к `STAN` когда соединение разорвано.")]
        [StanClientData]
        public void Connect_WhenConnectionIsClosed_ReturnConnection(int repetitionAmount)
        {
            using var countdown = new CountdownEvent(repetitionAmount);

            _factory.Setup(u => u.CreateConnection(It.IsAny<StanConnectionOptions>(), It.IsAny<Action<(ConnState?, Exception)>>()))
                .Returns(ConnectionHelper.MockStanConnection.Object)
                .Callback<StanConnectionOptions, Action<(ConnState?, Exception)>>((connection, onError) =>
                {
                    if (countdown.CurrentCount != 0)
                    {
                        countdown.Signal();
                        onError((ConnState.CLOSED, ObjectCreatorHelper.Create<StanMaxPingsException>("Connection lost due to PING failure.")));
                    }
                });

            _connectionSubject.OnNext(ConnectionHelper.MockNatsConnection.Object);

            countdown.Wait();

            _factory.Verify(u => u.DestroyConnection(It.IsNotNull<StanConnectionOptions>()), Times.Exactly(repetitionAmount));
            _factory.Verify(u => u.CreateConnection(It.IsNotNull<StanConnectionOptions>(), It.IsAny<Action<(ConnState?, Exception)>>()), Times.Exactly(repetitionAmount));
        }

        [Theory(DisplayName = "Подключение/переподключение к `STAN` когда сервер недоступен.")]
        [StanClientData]
        public void Connect_WhenServerIsUnavailable_ReturnConnection(int repetitionAmount)
        {
            using var countdown = new CountdownEvent(repetitionAmount);

            _factory.Setup(u => u.CreateConnection(It.IsAny<StanConnectionOptions>(), It.IsAny<Action<(ConnState?, Exception)>>()))
                .Returns(() =>
                {
                    if (countdown.CurrentCount != 0)
                    {
                        countdown.Signal();
                        throw ObjectCreatorHelper.Create<StanMaxPingsException>("Connection lost due to PING failure.");
                    }

                    return ConnectionHelper.MockStanConnection.Object;
                });

            _connectionSubject.OnNext(ConnectionHelper.MockNatsConnection.Object);

            countdown.Wait();

            _factory.Verify(u => u.DestroyConnection(It.IsNotNull<StanConnectionOptions>()), Times.Exactly(repetitionAmount));
            _factory.Verify(u => u.CreateConnection(It.IsNotNull<StanConnectionOptions>(), It.IsAny<Action<(ConnState?, Exception)>>()), Times.Exactly(repetitionAmount));
        }

        [Fact(DisplayName = "Вызов события о том что клиент потерял соединения со `STAN`.")]
        public void Connect_InvokeConnectionIsLostHandler()
        {
            using var countdown = new CountdownEvent(1);

            _factory.Setup(u => u.CreateConnection(It.IsAny<StanConnectionOptions>(), It.IsAny<Action<(ConnState?, Exception)>>()))
                .Returns(ConnectionHelper.MockStanConnection.Object)
                .Callback<StanConnectionOptions, Action<(ConnState?, Exception)>>((connection, onError) =>
                {
                    if (countdown.CurrentCount != 0)
                    {
                        countdown.Signal();
                        onError((ConnState.CLOSED, ObjectCreatorHelper.Create<StanMaxPingsException>("Connection lost due to PING failure.")));
                    }
                });

            _connectionSubject.OnNext(ConnectionHelper.MockNatsConnection.Object);

            countdown.Wait();

            ConnectionState.Verify(u => u.SetConnectionIsValid(), Times.Once());
            ConnectionState.Verify(u => u.SetConnectionIsInvalid(), Times.Once());
        }
    }
}
