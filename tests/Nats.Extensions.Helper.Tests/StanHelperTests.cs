﻿using Bogus;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Nats.Extensions.Helper.Implementations.Stan;
using Nats.Extensions.Helper.Models;
using Nats.Extensions.Helper.Options;
using Nats.Extensions.Helper.Options.Connection;
using Nats.Extensions.Helper.Options.Facades;
using Nats.Extensions.Helper.Tests.Helpers;
using Nats.Extensions.Helper.Tests.Resources.NssConnection;
using Nats.Extensions.Helper.Tests.Resources.NssHelper.Publications;
using Nats.Extensions.Helper.Tests.Resources.NssHelper.Subcriptions;
using STAN.Client;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Nats.Extensions.Helper.Tests
{
    public sealed class StanHelperTests : BaseHelperTest<StanConnectionOptions, IStanConnection>
    {
        private readonly StanHelper _helper;

        public StanHelperTests()
        {
            var logger = new Mock<ILogger<StanHelper>>();
            var rules = new Mock<IOptionRules<StanExtensionsOptions, StanSubscriptionOptions>>();

            rules.Setup(u => u.ApplyRules(It.IsAny<StanExtensionsOptions>()))
               .Returns(() => StanSubscriptionOptions.GetDefaultOptions());

            _helper = new StanHelper(logger.Object, Client.Object, rules.Object);
        }

        protected override void SetupOptions(Mock<IOptions<StanConnectionOptions>> options)
        {
            options.Setup(u => u.Value)
                .Returns(() => new Faker<StanConnectionOptions>().Generate());
        }

        #region Publish Tests

        [Theory(DisplayName = "Публикация невалидных данных в канал `STAN`.")]
        [NssIncorrectDataForPublish]
        public async Task STAN_PublishAsync_WithIncorrectParams_ThrowTaskCanceledException(string subject, byte[] data, CancellationToken cancellationToken)
        {
            ConnectionSubject.OnNext(GetValidStanConnection().Object);

            Func<Task> publish = () => _helper.PublishAsync(subject, data, cancellationToken);

            await publish.Should().ThrowExactlyAsync<TaskCanceledException>();
        }

        [Theory(DisplayName = "Публикация сообщений в канал `STAN` с неверными параметрами.  Генерируется исключение `ArgumentNullException`.")]
        [NssHelperIncorrectPublishData]
        public async Task PublishToSubject_WithIncorrectParams_ThrowArgumentNullException(string subject, byte[] message, CancellationToken cancellationToken)
        {
            Func<Task> publish = () => _helper.PublishAsync(subject, message, cancellationToken);

            await publish.Should().ThrowExactlyAsync<ArgumentException>();
        }

        [Theory(DisplayName = "Публикация сообщений в канал `STAN` c невалидным соединением к NSS. Генерируется исключение `InvalidOperationException`.")]
        [NssHelperCorrectPublishData]
        public async Task PublishToSubject_WithIncorrectNssConnection_ThrowInvalidOperationException(string subject, byte[] message, CancellationToken cancellationToken)
        {
            ConnectionSubject.OnNext(GetClosedStanConnection().Object);

            Func<Task> publish = () => _helper.PublishAsync(subject, message, cancellationToken);

            await publish.Should().ThrowExactlyAsync<InvalidOperationException>();
        }

        [Theory(DisplayName = "Публикация сообщений в канал `STAN` c валидным соединением к NSS.")]
        [NssHelperCorrectPublishData]
        public async Task PublishToSubject_WithCorrectNssConnection_NoThrowExceptions(string subject, byte[] message, CancellationToken cancellationToken)
        {
            ConnectionSubject.OnNext(GetValidStanConnection().Object);

            Func<Task> publish = () => _helper.PublishAsync(subject, message, cancellationToken);

            await publish.Should().NotThrowAsync();
        }

        #endregion

        #region General Subscription Tests

        [Theory(DisplayName = "Подписка на канал `STAN` с неверными параметрами. Генерируется исключение `ArgumentNullException`.")]
        [StanHelperSubscriptionIncorrectData]
        public void SubscribeToSubject_WithIncorrectParams_ThrowArgumentNullException(string subject, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            Func<IDisposable> subscribe = () => _helper.Subscribe(subject, handler, options);

            subscribe.Should().ThrowExactly<ArgumentException>();
        }

        [Theory(DisplayName = "Подписка на канал `STAN` с верными параметрами. Возвращается `IDisposable`.")]
        [StanHelperSubscriptionCorrectData]
        public void SubscribeToSubject_WithCorrectParams_ReturnIDisposable(string subject, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            Func<IDisposable> subscribe = () => _helper.Subscribe(subject, handler, options);

            subscribe.Should().NotThrow()
                .Which.Should().NotBeNull()
                .And.BeAssignableTo<IDisposable>();
        }

        [Theory(DisplayName = "Подписка на канал `STAN` c валидным соединением к NSS.")]
        [StanHelperSubscriptionCorrectData]
        public void SubscribeToSubject_WithCorrectNssConnection_NoThrowExceptions(string subject, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            using var subscription = _helper.Subscribe(subject, handler, options);

            var nssConnection = GetValidStanConnection();

            ConnectionSubject.OnNext(nssConnection.Object);

            nssConnection.Verify(u => u.Subscribe(It.IsAny<string>(), It.IsAny<StanSubscriptionOptions>(), It.IsAny<EventHandler<StanMsgHandlerArgs>>()), Times.Once());
        }

        [Theory(DisplayName = "Подписка на канал `STAN` c невалидным соединением к NSS.")]
        [StanHelperSubscriptionCorrectData]
        public void SubscribeToSubject_WithIncorrectNssConnection_NoThrowExceptions(string subject, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            using var subscription = _helper.Subscribe(subject, handler, options);

            var nssConnection = GetClosedStanConnection();

            ConnectionSubject.OnNext(default);
            ConnectionSubject.OnNext(nssConnection.Object);

            nssConnection.Verify(u => u.Subscribe(It.IsAny<string>(), It.IsAny<StanSubscriptionOptions>(), It.IsAny<EventHandler<StanMsgHandlerArgs>>()), Times.Never());
        }

        [Theory(DisplayName = "Отписка от канала `STAN`.")]
        [StanHelperSubscriptionCorrectData]
        public void UnsubscribeFromSubject_WithCorrectNssConnection_NoThrowExceptions(string subject, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            var (nssConnection, nssSubscription) = GetValidStanConnectionAndSubscription();

            using (_helper.Subscribe(subject, handler, options))
            {
                ConnectionSubject.OnNext(nssConnection.Object);
            }

            nssSubscription.Verify(u => u.Dispose(), Times.Once());
        }

        [Theory(DisplayName = "Переподписка на канал `STAN`.")]
        [StanHelperSubscriptionCorrectData]
        public void ResubscribeToSubject_WithCorrectNssConnection_NoThrowExceptions(string subject, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            var (nssConnection, nssSubscription) = GetValidStanConnectionAndSubscription();

            using var subscription = _helper.Subscribe(subject, handler, options);

            ConnectionSubject.OnNext(nssConnection.Object);
            ConnectionSubject.OnNext(nssConnection.Object);

            nssSubscription.Verify(u => u.Dispose(), Times.Once());
            nssConnection.Verify(u => u.Subscribe(It.IsAny<string>(), It.IsAny<StanSubscriptionOptions>(), It.IsAny<EventHandler<StanMsgHandlerArgs>>()), Times.Exactly(2));
        }

        #endregion

        #region General Subscription With Queue Tests

        [Theory(DisplayName = "Подписка на канал `STAN` с очередью с неверными параметрами. Генерируется исключение `ArgumentNullException`.")]
        [StanHelperSubscriptionWithQueueIncorrectData]
        public void SubscribeToSubjectWithQueue_WithIncorrectParams_ThrowArgumentNullException(string subject, string queue, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            Func<IDisposable> subscribe = () => _helper.Subscribe(subject, queue, handler, options);

            subscribe.Should().ThrowExactly<ArgumentException>();
        }

        [Theory(DisplayName = "Подписка на канал `STAN` с очередью с верными параметрами. Возвращается `IDisposable`.")]
        [StanHelperSubscriptionWithQueueCorrectData]
        public void SubscribeToSubjectWithQueue_WithCorrectParams_ReturnIDisposable(string subject, string queue, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            Func<IDisposable> subscribe = () => _helper.Subscribe(subject, queue, handler, options);

            subscribe.Should().NotThrow()
                .Which.Should().NotBeNull()
                .And.BeAssignableTo<IDisposable>();
        }

        [Theory(DisplayName = "Подписка на канал `STAN` с очередью c валидным соединением к NSS.")]
        [StanHelperSubscriptionWithQueueCorrectData]
        public void SubscribeToSubjectWithQueue_WithCorrectNssConnection_NoThrowExceptions(string subject, string queue, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            using var subscription = _helper.Subscribe(subject, queue, handler, options);

            var nssConnection = GetValidStanConnection();

            ConnectionSubject.OnNext(nssConnection.Object);

            nssConnection.Verify(u => u.Subscribe(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<StanSubscriptionOptions>(), It.IsAny<EventHandler<StanMsgHandlerArgs>>()), Times.Once());
        }

        [Theory(DisplayName = "Подписка на канал `STAN` с очередью c невалидным соединением к NSS.")]
        [StanHelperSubscriptionWithQueueCorrectData]
        public void SubscribeToSubjectWithQueue_WithIncorrectNssConnection_NoThrowExceptions(string subject, string queue, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            using var subscription = _helper.Subscribe(subject, queue, handler, options);

            var nssConnection = GetClosedStanConnection();

            ConnectionSubject.OnNext(default);
            ConnectionSubject.OnNext(nssConnection.Object);

            nssConnection.Verify(u => u.Subscribe(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<StanSubscriptionOptions>(), It.IsAny<EventHandler<StanMsgHandlerArgs>>()), Times.Never());
        }

        [Theory(DisplayName = "Отписка от канала `STAN` с очередью.")]
        [StanHelperSubscriptionWithQueueCorrectData]
        public void UnsubscribeFromSubjectWithQueue_WithCorrectNssConnection_NoThrowExceptions(string subject, string queue, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            var (nssConnection, nssSubscription) = GetValidStanConnectionAndSubscriptionWithQueue();

            using (_helper.Subscribe(subject, queue, handler, options))
            {
                ConnectionSubject.OnNext(nssConnection.Object);
            }

            nssSubscription.Verify(u => u.Dispose(), Times.Once());
        }

        [Theory(DisplayName = "Переподписка на канал `STAN` с очередью.")]
        [StanHelperSubscriptionWithQueueCorrectData]
        public void ResubscribeToSubjectWithQueue_WithCorrectNssConnection_NoThrowExceptions(string subject, string queue, Func<Message, CancellationToken, Task> handler, StanExtensionsOptions options)
        {
            var (nssConnection, nssSubscription) = GetValidStanConnectionAndSubscriptionWithQueue();

            using var subscription = _helper.Subscribe(subject, queue, handler, options);

            ConnectionSubject.OnNext(nssConnection.Object);
            ConnectionSubject.OnNext(nssConnection.Object);

            nssSubscription.Verify(u => u.Dispose(), Times.Once());
            nssConnection.Verify(u => u.Subscribe(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<StanSubscriptionOptions>(), It.IsAny<EventHandler<StanMsgHandlerArgs>>()), Times.Exactly(2));
        }

        #endregion
    }
}
