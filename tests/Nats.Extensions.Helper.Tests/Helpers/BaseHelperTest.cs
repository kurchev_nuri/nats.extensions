﻿using Bogus;
using Microsoft.Extensions.Options;
using Moq;
using Nats.Extensions.Helper.Clients;
using NATS.Client;
using STAN.Client;
using System;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper.Tests.Helpers
{
    public abstract class BaseHelperTest<TOptions, TConnection>
          where TConnection : class
          where TOptions : class, new()
    {
        protected BaseHelperTest()
        {
            Options = new Mock<IOptions<TOptions>>();
            Client = new Mock<IClient<TConnection>>();
            ConnectionSubject = new Subject<TConnection>();
            ConnectionIsEstablishedHandler = new Mock<EventHandler<string>>();

            SetupOptions(Options);
            SetupNssClient(Client);
        }

        protected Mock<IOptions<TOptions>> Options { get; }

        internal Mock<IClient<TConnection>> Client { get; }

        protected Subject<TConnection> ConnectionSubject { get; }

        protected Mock<EventHandler<string>> ConnectionIsEstablishedHandler { get; }

        protected abstract void SetupOptions(Mock<IOptions<TOptions>> options);

        protected static Mock<IConnection> GetValidNatsConnection()
        {
            var response = new Randomizer();
            var connection = new Mock<IConnection>();

            connection.Setup(u => u.IsClosed()).Returns(false);
            connection.Setup(u => u.RequestAsync(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<CancellationToken>()))
                .Returns<string, byte[], CancellationToken>((subject, data, cancellationToken) =>
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        return Task.FromCanceled<Msg>(cancellationToken);
                    }

                    return Task.FromResult(new Msg(subject, response.Bytes(10)));
                });

            return connection;
        }

        protected static Mock<IStanConnection> GetValidStanConnection()
        {
            var response = new Randomizer();
            var connection = new Mock<IConnection>();
            var stanConnection = new Mock<IStanConnection>();

            connection.Setup(u => u.IsClosed()).Returns(false);
            stanConnection.Setup(u => u.NATSConnection).Returns(connection.Object);

            return stanConnection;
        }

        protected static Mock<IConnection> GetClosedNatsConnection()
        {
            var connection = new Mock<IConnection>();

            connection.Setup(u => u.IsClosed()).Returns(true);

            return connection;
        }

        protected static Mock<IStanConnection> GetClosedStanConnection()
        {
            var connection = new Mock<IConnection>();
            var stanConnection = new Mock<IStanConnection>();

            connection.Setup(u => u.IsClosed()).Returns(true);
            stanConnection.Setup(u => u.NATSConnection).Returns(connection.Object);

            return stanConnection;
        }

        protected static (Mock<IConnection>, Mock<IAsyncSubscription>) GetValidNatsConnectionAndSubscription()
        {
            var connection = new Mock<IConnection>();
            var subscription = new Mock<IAsyncSubscription>();

            connection.Setup(u => u.IsClosed()).Returns(() => false);
            connection.Setup(u => u.SubscribeAsync(It.IsAny<string>(), It.IsAny<EventHandler<MsgHandlerEventArgs>>()))
                .Returns(subscription.Object);

            return (connection, subscription);
        }

        protected static (Mock<IStanConnection>, Mock<IStanSubscription>) GetValidStanConnectionAndSubscription()
        {
            var connection = new Mock<IConnection>();
            var subscription = new Mock<IStanSubscription>();
            var stanConnection = new Mock<IStanConnection>();

            connection.Setup(u => u.IsClosed()).Returns(() => false);
            stanConnection.Setup(u => u.NATSConnection).Returns(connection.Object);
            stanConnection.Setup(u => u.Subscribe(It.IsAny<string>(), It.IsAny<StanSubscriptionOptions>(), It.IsAny<EventHandler<StanMsgHandlerArgs>>()))
                .Returns(subscription.Object);

            return (stanConnection, subscription);
        }

        protected static (Mock<IConnection>, Mock<IAsyncSubscription>) GetValidNatsConnectionAndSubscriptionWithQueue()
        {
            var connection = new Mock<IConnection>();
            var subscription = new Mock<IAsyncSubscription>();

            connection.Setup(u => u.IsClosed()).Returns(() => false);
            connection.Setup(u => u.SubscribeAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<EventHandler<MsgHandlerEventArgs>>()))
                .Returns(subscription.Object);

            return (connection, subscription);
        }

        protected static (Mock<IStanConnection>, Mock<IStanSubscription>) GetValidStanConnectionAndSubscriptionWithQueue()
        {
            var connection = new Mock<IConnection>();
            var subscription = new Mock<IStanSubscription>();
            var stanConnection = new Mock<IStanConnection>();

            connection.Setup(u => u.IsClosed()).Returns(() => false);
            stanConnection.Setup(u => u.NATSConnection).Returns(connection.Object);
            stanConnection.Setup(u => u.Subscribe(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<StanSubscriptionOptions>(), It.IsAny<EventHandler<StanMsgHandlerArgs>>()))
                .Returns(subscription.Object);

            return (stanConnection, subscription);
        }

        #region Private Methods

        private void SetupNssClient(Mock<IClient<TConnection>> client)
        {
            client.Setup(client => client.Connection).Returns(ConnectionHelper.CreateObservable(ConnectionSubject));
        }

        #endregion
    }
}
