﻿using Bogus;
using Moq;
using Nats.Extensions.Helper.Tests.Common;
using NATS.Client;
using STAN.Client;
using System;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Nats.Extensions.Helper.Tests.Helpers
{
    internal static class ConnectionHelper
    {
        public static IObservable<TConnection> CreateObservable<TConnection>(IObservable<TConnection> observable)
            where TConnection : class
        {
            return observable.Replay(InternalConsts.MaxReplayCount).AutoConnect(InternalConsts.StartConnectingImmediately);
        }

        internal static Mock<IConnection> MockNatsConnection => GetMockNatsConnection();

        internal static Mock<IStanConnection> MockStanConnection => GetMockStanConnection();

        private static Mock<IStanConnection> GetMockStanConnection()
        {
            var response = new Randomizer();
            var connection = new Mock<IStanConnection>();

            connection.Setup(u => u.NATSConnection.IsClosed()).Returns(false);

            connection.Setup(u => u.Subscribe(It.IsAny<string>(), It.IsAny<StanSubscriptionOptions>(), It.IsAny<EventHandler<StanMsgHandlerArgs>>()))
                .Returns(new Mock<IStanSubscription>().Object);

            connection.Setup(u => u.Subscribe(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<StanSubscriptionOptions>(), It.IsAny<EventHandler<StanMsgHandlerArgs>>()))
                .Returns(new Mock<IStanSubscription>().Object);

            connection.Setup(u => u.PublishAsync(It.IsAny<string>(), It.IsAny<byte[]>()))
                .Returns(() => Task.FromResult(response.String(22)));

            return connection;
        }

        internal static Mock<IConnection> GetMockNatsConnection()
        {
            var response = new Randomizer();
            var connection = new Mock<IConnection>();

            connection.Setup(u => u.IsClosed()).Returns(false);

            connection.Setup(u => u.SubscribeAsync(It.IsAny<string>(), It.IsAny<EventHandler<MsgHandlerEventArgs>>()))
                .Returns(new Mock<IAsyncSubscription>().Object);

            connection.Setup(u => u.SubscribeAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<EventHandler<MsgHandlerEventArgs>>()))
               .Returns(new Mock<IAsyncSubscription>().Object);

            connection.Setup(u => u.RequestAsync(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<CancellationToken>()))
                .Returns<string, byte[], CancellationToken>(
                    (subject, data, cancellationToken) =>
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            return Task.FromCanceled<Msg>(cancellationToken);
                        }

                        return Task.FromResult(new Msg(subject, response.Bytes(22)));
                    }
                );

            return connection;
        }
    }
}
