﻿using System.Threading;

namespace Nats.Extensions.Helper.Tests.Helpers
{
    internal static class CommonHelpers
    {
        /// <summary>
        /// It should be here due to Rx-Dispose invoking.
        /// When connection.Dispose() is invoked it need a small chunk of time to clearly unsubscribe from the subject.
        /// </summary>
        public static void AwaitRxDisposing() => Thread.Sleep(1000);
    }
}
