﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Nats.Extensions.Helper.Tests.Helpers
{
    internal static class ObjectCreatorHelper
    {
        /// <summary>
        /// Создает объект чей конструктор явялется internal. 
        /// </summary>
        /// <typeparam name="T">Тип создаваемого объекта.</typeparam>
        /// <param name="arguments">Параметры, которые необходимо инициализовать. Важен порядок инциализации.</param>
        /// <returns>Возвращает объект типа T</returns>
        public static T Create<T>(params object[] arguments)
            where T : class
        {
            var result = FormatterServices.GetSafeUninitializedObject(typeof(T));
            if (result == default)
            {
                throw new InvalidOperationException($"Невозможно создать объекта типа `{typeof(T)}`.");
            }

            if (arguments != default)
            {
                foreach (var argument in typeof(T).GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic))
                {
                    var value = arguments.FirstOrDefault(u => u.GetType() == argument.FieldType);
                    if (value != default)
                    {
                        argument.SetValue(result, value);
                    }
                }
            }

            return result as T;
        }
    }
}
