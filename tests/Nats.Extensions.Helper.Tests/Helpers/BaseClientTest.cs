﻿using Microsoft.Extensions.Options;
using Moq;
using Nats.Extensions.Helper.Connection.States;

namespace Nats.Extensions.Helper.Tests.Helpers
{
    public abstract class BaseClientTest<TOptions>
        where TOptions : class, new()
    {
        protected BaseClientTest()
        {
            ConnectionState = new Mock<IConnectionState<TOptions>>();

            SetupOptions(Options = new Mock<IOptions<TOptions>>());
        }

        protected Mock<IOptions<TOptions>> Options { get; }

        internal Mock<IConnectionState<TOptions>> ConnectionState { get; }

        protected abstract void SetupOptions(Mock<IOptions<TOptions>> options);
    }
}
