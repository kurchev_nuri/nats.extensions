<a name='assembly'></a>
# Nats.Extensions

## Contents

- [HostBuilderExtensions](#T-Nats-Extensions-HostBuilderExtensions 'Nats.Extensions.HostBuilderExtensions')
  - [ConfigureNatsExtensions(configure)](#M-Nats-Extensions-HostBuilderExtensions-ConfigureNatsExtensions-Microsoft-Extensions-Hosting-IHostBuilder,System-Action{Nats-Extensions-Builders-InternalServicesBuilder}- 'Nats.Extensions.HostBuilderExtensions.ConfigureNatsExtensions(Microsoft.Extensions.Hosting.IHostBuilder,System.Action{Nats.Extensions.Builders.InternalServicesBuilder})')
- [InternalServicesBuilder](#T-Nats-Extensions-Builders-InternalServicesBuilder 'Nats.Extensions.Builders.InternalServicesBuilder')
  - [ShouldUsePipeAuthorization](#P-Nats-Extensions-Builders-InternalServicesBuilder-ShouldUsePipeAuthorization 'Nats.Extensions.Builders.InternalServicesBuilder.ShouldUsePipeAuthorization')
  - [ThrowExceptionIfDeserializationError](#P-Nats-Extensions-Builders-InternalServicesBuilder-ThrowExceptionIfDeserializationError 'Nats.Extensions.Builders.InternalServicesBuilder.ThrowExceptionIfDeserializationError')
  - [ConfigureNatsWatcher(configure)](#M-Nats-Extensions-Builders-InternalServicesBuilder-ConfigureNatsWatcher-System-Action{Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher{Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions}}- 'Nats.Extensions.Builders.InternalServicesBuilder.ConfigureNatsWatcher(System.Action{Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher{Nats.Extensions.Helper.Options.Connection.NatsConnectionOptions}})')
  - [ConfigureStanWatcher(configure)](#M-Nats-Extensions-Builders-InternalServicesBuilder-ConfigureStanWatcher-System-Action{Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher{Nats-Extensions-Helper-Options-Connection-StanConnectionOptions}}- 'Nats.Extensions.Builders.InternalServicesBuilder.ConfigureStanWatcher(System.Action{Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher{Nats.Extensions.Helper.Options.Connection.StanConnectionOptions}})')
  - [UseStartup\`\`1()](#M-Nats-Extensions-Builders-InternalServicesBuilder-UseStartup``1 'Nats.Extensions.Builders.InternalServicesBuilder.UseStartup``1')

<a name='T-Nats-Extensions-HostBuilderExtensions'></a>
## HostBuilderExtensions `type`

##### Namespace

Nats.Extensions

<a name='M-Nats-Extensions-HostBuilderExtensions-ConfigureNatsExtensions-Microsoft-Extensions-Hosting-IHostBuilder,System-Action{Nats-Extensions-Builders-InternalServicesBuilder}-'></a>
### ConfigureNatsExtensions(configure) `method`

##### Summary

Конфигурация общих настроек для сервиса встраиваемого в Core.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [Microsoft.Extensions.Hosting.IHostBuilder](#T-Microsoft-Extensions-Hosting-IHostBuilder 'Microsoft.Extensions.Hosting.IHostBuilder') | Метод конфигурации дополнительных зависимостей сервиса. |

<a name='T-Nats-Extensions-Builders-InternalServicesBuilder'></a>
## InternalServicesBuilder `type`

##### Namespace

Nats.Extensions.Builders

<a name='P-Nats-Extensions-Builders-InternalServicesBuilder-ShouldUsePipeAuthorization'></a>
### ShouldUsePipeAuthorization `property`

##### Summary

Если поле установлено в \`True\` то будет использоваться авторизация через сторонний сервис по средствам \`System.IO.Pipes\`, иначе необходимо явно указать учетные данные в \`embeddedsettings.json\`.
Значение по умолчанию \`True\`.

<a name='P-Nats-Extensions-Builders-InternalServicesBuilder-ThrowExceptionIfDeserializationError'></a>
### ThrowExceptionIfDeserializationError `property`

##### Summary

Если поле установлено в \`True\` то будет пробрасываться \`Exception\` при возникновении ошибок десериализации, иначе сообщение будет принято и передано обработчику \`HandleAsync\`.
Значение по умолчанию \`True\`.

<a name='M-Nats-Extensions-Builders-InternalServicesBuilder-ConfigureNatsWatcher-System-Action{Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher{Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions}}-'></a>
### ConfigureNatsWatcher(configure) `method`

##### Summary

Метод добавления обработчиков на события (потеря/появление) соединения с NATS

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [System.Action{Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher{Nats.Extensions.Helper.Options.Connection.NatsConnectionOptions}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Action 'System.Action{Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher{Nats.Extensions.Helper.Options.Connection.NatsConnectionOptions}}') | Интерфейс, который позволяет добавить обработчики на события (потеря/появление) соединения с NATS |

<a name='M-Nats-Extensions-Builders-InternalServicesBuilder-ConfigureStanWatcher-System-Action{Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher{Nats-Extensions-Helper-Options-Connection-StanConnectionOptions}}-'></a>
### ConfigureStanWatcher(configure) `method`

##### Summary

Метод добавления обработчиков на события (потеря/появление) соединения со STAN

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [System.Action{Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher{Nats.Extensions.Helper.Options.Connection.StanConnectionOptions}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Action 'System.Action{Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher{Nats.Extensions.Helper.Options.Connection.StanConnectionOptions}}') | Интерфейс, который позволяет добавить обработчики на события (потеря/появление) соединения со STAN |

<a name='M-Nats-Extensions-Builders-InternalServicesBuilder-UseStartup``1'></a>
### UseStartup\`\`1() `method`

##### Summary

Метод настройки внутренних зависимостей сервиса.

##### Parameters

This method has no parameters.

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TStartup | Тип класса с зависимостями. |
