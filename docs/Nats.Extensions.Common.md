<a name='assembly'></a>
# Nats.Extensions.Common

## Contents

- [ProcessManagerResource](#T-Nats-Extensions-Common-Resources-ProcessManagerResource 'Nats.Extensions.Common.Resources.ProcessManagerResource')
  - [CertificateVerification](#P-Nats-Extensions-Common-Resources-ProcessManagerResource-CertificateVerification 'Nats.Extensions.Common.Resources.ProcessManagerResource.CertificateVerification')
  - [CertificateVerificationFailure](#P-Nats-Extensions-Common-Resources-ProcessManagerResource-CertificateVerificationFailure 'Nats.Extensions.Common.Resources.ProcessManagerResource.CertificateVerificationFailure')
  - [Culture](#P-Nats-Extensions-Common-Resources-ProcessManagerResource-Culture 'Nats.Extensions.Common.Resources.ProcessManagerResource.Culture')
  - [IncorrectArgument](#P-Nats-Extensions-Common-Resources-ProcessManagerResource-IncorrectArgument 'Nats.Extensions.Common.Resources.ProcessManagerResource.IncorrectArgument')
  - [KillProcessFailure](#P-Nats-Extensions-Common-Resources-ProcessManagerResource-KillProcessFailure 'Nats.Extensions.Common.Resources.ProcessManagerResource.KillProcessFailure')
  - [OpenProcessFailure](#P-Nats-Extensions-Common-Resources-ProcessManagerResource-OpenProcessFailure 'Nats.Extensions.Common.Resources.ProcessManagerResource.OpenProcessFailure')
  - [QueryProcessNameFailure](#P-Nats-Extensions-Common-Resources-ProcessManagerResource-QueryProcessNameFailure 'Nats.Extensions.Common.Resources.ProcessManagerResource.QueryProcessNameFailure')
  - [ResourceManager](#P-Nats-Extensions-Common-Resources-ProcessManagerResource-ResourceManager 'Nats.Extensions.Common.Resources.ProcessManagerResource.ResourceManager')

<a name='T-Nats-Extensions-Common-Resources-ProcessManagerResource'></a>
## ProcessManagerResource `type`

##### Namespace

Nats.Extensions.Common.Resources

##### Summary

A strongly-typed resource class, for looking up localized strings, etc.

<a name='P-Nats-Extensions-Common-Resources-ProcessManagerResource-CertificateVerification'></a>
### CertificateVerification `property`

##### Summary

Looks up a localized string similar to Валидация сертификата для \`{0}\`..

<a name='P-Nats-Extensions-Common-Resources-ProcessManagerResource-CertificateVerificationFailure'></a>
### CertificateVerificationFailure `property`

##### Summary

Looks up a localized string similar to Во время валидации сертификата для \`{0}\` произошла ошибка..

<a name='P-Nats-Extensions-Common-Resources-ProcessManagerResource-Culture'></a>
### Culture `property`

##### Summary

Overrides the current thread's CurrentUICulture property for all
  resource lookups using this strongly typed resource class.

<a name='P-Nats-Extensions-Common-Resources-ProcessManagerResource-IncorrectArgument'></a>
### IncorrectArgument `property`

##### Summary

Looks up a localized string similar to Значение переменной \`{0}\` не может быть null..

<a name='P-Nats-Extensions-Common-Resources-ProcessManagerResource-KillProcessFailure'></a>
### KillProcessFailure `property`

##### Summary

Looks up a localized string similar to Во время \`Kill\` процесса с \`Id : {0}\` произошла ошибка..

<a name='P-Nats-Extensions-Common-Resources-ProcessManagerResource-OpenProcessFailure'></a>
### OpenProcessFailure `property`

##### Summary

Looks up a localized string similar to Во время получения информации о процессе произошла ошибка..

<a name='P-Nats-Extensions-Common-Resources-ProcessManagerResource-QueryProcessNameFailure'></a>
### QueryProcessNameFailure `property`

##### Summary

Looks up a localized string similar to Невозможно получить путь до исполняемого файла процесса..

<a name='P-Nats-Extensions-Common-Resources-ProcessManagerResource-ResourceManager'></a>
### ResourceManager `property`

##### Summary

Returns the cached ResourceManager instance used by this class.
