<a name='assembly'></a>
# Nats.Extensions.AspNet.Logger

## Contents

- [ServiceExtensions](#T-Nats-Extensions-AspNet-Logger-Extensions-ServiceExtensions 'Nats.Extensions.AspNet.Logger.Extensions.ServiceExtensions')
  - [UseNatsExtensionsLogger(builder,serviceName)](#M-Nats-Extensions-AspNet-Logger-Extensions-ServiceExtensions-UseNatsExtensionsLogger-Microsoft-Extensions-Hosting-IHostBuilder,System-String- 'Nats.Extensions.AspNet.Logger.Extensions.ServiceExtensions.UseNatsExtensionsLogger(Microsoft.Extensions.Hosting.IHostBuilder,System.String)')

<a name='T-Nats-Extensions-AspNet-Logger-Extensions-ServiceExtensions'></a>
## ServiceExtensions `type`

##### Namespace

Nats.Extensions.AspNet.Logger.Extensions

<a name='M-Nats-Extensions-AspNet-Logger-Extensions-ServiceExtensions-UseNatsExtensionsLogger-Microsoft-Extensions-Hosting-IHostBuilder,System-String-'></a>
### UseNatsExtensionsLogger(builder,serviceName) `method`

##### Summary

Логирование в NSS

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| builder | [Microsoft.Extensions.Hosting.IHostBuilder](#T-Microsoft-Extensions-Hosting-IHostBuilder 'Microsoft.Extensions.Hosting.IHostBuilder') |  |
| serviceName | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Должен совпадать с именем сервиса |
