<a name='assembly'></a>
# Nats.Extensions.Helper

## Contents

- [BufferResource](#T-Nats-Extensions-Helper-Resources-BufferResource 'Nats.Extensions.Helper.Resources.BufferResource')
  - [CannotReadFromBufferError](#P-Nats-Extensions-Helper-Resources-BufferResource-CannotReadFromBufferError 'Nats.Extensions.Helper.Resources.BufferResource.CannotReadFromBufferError')
  - [CannotWriteToBufferError](#P-Nats-Extensions-Helper-Resources-BufferResource-CannotWriteToBufferError 'Nats.Extensions.Helper.Resources.BufferResource.CannotWriteToBufferError')
  - [Culture](#P-Nats-Extensions-Helper-Resources-BufferResource-Culture 'Nats.Extensions.Helper.Resources.BufferResource.Culture')
  - [HandlingError](#P-Nats-Extensions-Helper-Resources-BufferResource-HandlingError 'Nats.Extensions.Helper.Resources.BufferResource.HandlingError')
  - [ObjectDisposedError](#P-Nats-Extensions-Helper-Resources-BufferResource-ObjectDisposedError 'Nats.Extensions.Helper.Resources.BufferResource.ObjectDisposedError')
  - [OperationCanceledError](#P-Nats-Extensions-Helper-Resources-BufferResource-OperationCanceledError 'Nats.Extensions.Helper.Resources.BufferResource.OperationCanceledError')
  - [ReadFromBufferError](#P-Nats-Extensions-Helper-Resources-BufferResource-ReadFromBufferError 'Nats.Extensions.Helper.Resources.BufferResource.ReadFromBufferError')
  - [ResourceManager](#P-Nats-Extensions-Helper-Resources-BufferResource-ResourceManager 'Nats.Extensions.Helper.Resources.BufferResource.ResourceManager')
  - [WriteToBufferError](#P-Nats-Extensions-Helper-Resources-BufferResource-WriteToBufferError 'Nats.Extensions.Helper.Resources.BufferResource.WriteToBufferError')
- [BuilderResource](#T-Nats-Extensions-Helper-Resources-BuilderResource 'Nats.Extensions.Helper.Resources.BuilderResource')
  - [Culture](#P-Nats-Extensions-Helper-Resources-BuilderResource-Culture 'Nats.Extensions.Helper.Resources.BuilderResource.Culture')
  - [ResourceManager](#P-Nats-Extensions-Helper-Resources-BuilderResource-ResourceManager 'Nats.Extensions.Helper.Resources.BuilderResource.ResourceManager')
  - [ShouldUse](#P-Nats-Extensions-Helper-Resources-BuilderResource-ShouldUse 'Nats.Extensions.Helper.Resources.BuilderResource.ShouldUse')
- [ClientResource](#T-Nats-Extensions-Helper-Resources-ClientResource 'Nats.Extensions.Helper.Resources.ClientResource')
  - [AutoRefreshNatsConnection](#P-Nats-Extensions-Helper-Resources-ClientResource-AutoRefreshNatsConnection 'Nats.Extensions.Helper.Resources.ClientResource.AutoRefreshNatsConnection')
  - [AutoRefreshStanConnection](#P-Nats-Extensions-Helper-Resources-ClientResource-AutoRefreshStanConnection 'Nats.Extensions.Helper.Resources.ClientResource.AutoRefreshStanConnection')
  - [ConnectionState](#P-Nats-Extensions-Helper-Resources-ClientResource-ConnectionState 'Nats.Extensions.Helper.Resources.ClientResource.ConnectionState')
  - [Culture](#P-Nats-Extensions-Helper-Resources-ClientResource-Culture 'Nats.Extensions.Helper.Resources.ClientResource.Culture')
  - [EmptyCredentials](#P-Nats-Extensions-Helper-Resources-ClientResource-EmptyCredentials 'Nats.Extensions.Helper.Resources.ClientResource.EmptyCredentials')
  - [NatsConnectionLost](#P-Nats-Extensions-Helper-Resources-ClientResource-NatsConnectionLost 'Nats.Extensions.Helper.Resources.ClientResource.NatsConnectionLost')
  - [NatsReconnect](#P-Nats-Extensions-Helper-Resources-ClientResource-NatsReconnect 'Nats.Extensions.Helper.Resources.ClientResource.NatsReconnect')
  - [ResourceManager](#P-Nats-Extensions-Helper-Resources-ClientResource-ResourceManager 'Nats.Extensions.Helper.Resources.ClientResource.ResourceManager')
  - [StanConnectionLost](#P-Nats-Extensions-Helper-Resources-ClientResource-StanConnectionLost 'Nats.Extensions.Helper.Resources.ClientResource.StanConnectionLost')
  - [StanReconnect](#P-Nats-Extensions-Helper-Resources-ClientResource-StanReconnect 'Nats.Extensions.Helper.Resources.ClientResource.StanReconnect')
- [CommonResource](#T-Nats-Extensions-Helper-Resources-CommonResource 'Nats.Extensions.Helper.Resources.CommonResource')
  - [ArgumentNull](#P-Nats-Extensions-Helper-Resources-CommonResource-ArgumentNull 'Nats.Extensions.Helper.Resources.CommonResource.ArgumentNull')
  - [Culture](#P-Nats-Extensions-Helper-Resources-CommonResource-Culture 'Nats.Extensions.Helper.Resources.CommonResource.Culture')
  - [InvalidQueueGroup](#P-Nats-Extensions-Helper-Resources-CommonResource-InvalidQueueGroup 'Nats.Extensions.Helper.Resources.CommonResource.InvalidQueueGroup')
  - [InvalidSubject](#P-Nats-Extensions-Helper-Resources-CommonResource-InvalidSubject 'Nats.Extensions.Helper.Resources.CommonResource.InvalidSubject')
  - [ResourceManager](#P-Nats-Extensions-Helper-Resources-CommonResource-ResourceManager 'Nats.Extensions.Helper.Resources.CommonResource.ResourceManager')
- [ConnectionBuilder](#T-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder')
  - [ClientId](#P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-ClientId 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder.ClientId')
  - [Host](#P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-Host 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder.Host')
  - [Password](#P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-Password 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder.Password')
  - [Port](#P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-Port 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder.Port')
  - [ReconnectTimeout](#P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-ReconnectTimeout 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder.ReconnectTimeout')
  - [RefreshConnectionTimeout](#P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-RefreshConnectionTimeout 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder.RefreshConnectionTimeout')
  - [Servers](#P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-Servers 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder.Servers')
  - [StanRefreshConnectionTimeout](#P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-StanRefreshConnectionTimeout 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder.StanRefreshConnectionTimeout')
  - [User](#P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-User 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder.User')
- [ConnectionBuilderExtensions](#T-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilderExtensions 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilderExtensions')
  - [AddPipeAuthorization(configure)](#M-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilderExtensions-AddPipeAuthorization-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder,System-Action{Nats-Extensions-Common-Options-PipeManagerOptions}- 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilderExtensions.AddPipeAuthorization(Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder,System.Action{Nats.Extensions.Common.Options.PipeManagerOptions})')
  - [AddServers(servers)](#M-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilderExtensions-AddServers-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder,System-String[]- 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilderExtensions.AddServers(Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder,System.String[])')
- [ConnectionOptions](#T-Nats-Extensions-Helper-Options-Connection-ConnectionOptions 'Nats.Extensions.Helper.Options.Connection.ConnectionOptions')
  - [Password](#P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-Password 'Nats.Extensions.Helper.Options.Connection.ConnectionOptions.Password')
  - [ReconnectTimeout](#P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-ReconnectTimeout 'Nats.Extensions.Helper.Options.Connection.ConnectionOptions.ReconnectTimeout')
  - [RefreshConnectionTimeout](#P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-RefreshConnectionTimeout 'Nats.Extensions.Helper.Options.Connection.ConnectionOptions.RefreshConnectionTimeout')
  - [Servers](#P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-Servers 'Nats.Extensions.Helper.Options.Connection.ConnectionOptions.Servers')
  - [Uri](#P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-Uri 'Nats.Extensions.Helper.Options.Connection.ConnectionOptions.Uri')
  - [User](#P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-User 'Nats.Extensions.Helper.Options.Connection.ConnectionOptions.User')
- [HelperBuilderExtensions](#T-Nats-Extensions-Helper-Builders-HelperBuilderExtensions 'Nats.Extensions.Helper.Builders.HelperBuilderExtensions')
  - [ConfigureConnection(configure)](#M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-ConfigureConnection-Nats-Extensions-Helper-Builders-HelperBuilder,System-Action{Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder}- 'Nats.Extensions.Helper.Builders.HelperBuilderExtensions.ConfigureConnection(Nats.Extensions.Helper.Builders.HelperBuilder,System.Action{Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder})')
  - [UseNats()](#M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-UseNats-Nats-Extensions-Helper-Builders-HelperBuilder- 'Nats.Extensions.Helper.Builders.HelperBuilderExtensions.UseNats(Nats.Extensions.Helper.Builders.HelperBuilder)')
  - [UseNats(configure)](#M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-UseNats-Nats-Extensions-Helper-Builders-HelperBuilder,System-Action{Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions}- 'Nats.Extensions.Helper.Builders.HelperBuilderExtensions.UseNats(Nats.Extensions.Helper.Builders.HelperBuilder,System.Action{Nats.Extensions.Helper.Options.Connection.NatsConnectionOptions})')
  - [UseNats(configure)](#M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-UseNats-Nats-Extensions-Helper-Builders-HelperBuilder,System-Action{Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions,Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher{Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions}}- 'Nats.Extensions.Helper.Builders.HelperBuilderExtensions.UseNats(Nats.Extensions.Helper.Builders.HelperBuilder,System.Action{Nats.Extensions.Helper.Options.Connection.NatsConnectionOptions,Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher{Nats.Extensions.Helper.Options.Connection.NatsConnectionOptions}})')
  - [UseStan(configure)](#M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-UseStan-Nats-Extensions-Helper-Builders-HelperBuilder,System-Action{Nats-Extensions-Helper-Options-Connection-StanConnectionOptions}- 'Nats.Extensions.Helper.Builders.HelperBuilderExtensions.UseStan(Nats.Extensions.Helper.Builders.HelperBuilder,System.Action{Nats.Extensions.Helper.Options.Connection.StanConnectionOptions})')
  - [UseStan(configure)](#M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-UseStan-Nats-Extensions-Helper-Builders-HelperBuilder,System-Action{Nats-Extensions-Helper-Options-Connection-StanConnectionOptions,Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher{Nats-Extensions-Helper-Options-Connection-StanConnectionOptions}}- 'Nats.Extensions.Helper.Builders.HelperBuilderExtensions.UseStan(Nats.Extensions.Helper.Builders.HelperBuilder,System.Action{Nats.Extensions.Helper.Options.Connection.StanConnectionOptions,Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher{Nats.Extensions.Helper.Options.Connection.StanConnectionOptions}})')
- [HelperResource](#T-Nats-Extensions-Helper-Resources-HelperResource 'Nats.Extensions.Helper.Resources.HelperResource')
  - [Culture](#P-Nats-Extensions-Helper-Resources-HelperResource-Culture 'Nats.Extensions.Helper.Resources.HelperResource.Culture')
  - [HandlerError](#P-Nats-Extensions-Helper-Resources-HelperResource-HandlerError 'Nats.Extensions.Helper.Resources.HelperResource.HandlerError')
  - [NatsConnectionEstablished](#P-Nats-Extensions-Helper-Resources-HelperResource-NatsConnectionEstablished 'Nats.Extensions.Helper.Resources.HelperResource.NatsConnectionEstablished')
  - [NatsConnectionLost](#P-Nats-Extensions-Helper-Resources-HelperResource-NatsConnectionLost 'Nats.Extensions.Helper.Resources.HelperResource.NatsConnectionLost')
  - [ResourceManager](#P-Nats-Extensions-Helper-Resources-HelperResource-ResourceManager 'Nats.Extensions.Helper.Resources.HelperResource.ResourceManager')
  - [StanConnectionEstablished](#P-Nats-Extensions-Helper-Resources-HelperResource-StanConnectionEstablished 'Nats.Extensions.Helper.Resources.HelperResource.StanConnectionEstablished')
  - [StanConnectionLost](#P-Nats-Extensions-Helper-Resources-HelperResource-StanConnectionLost 'Nats.Extensions.Helper.Resources.HelperResource.StanConnectionLost')
  - [SubscriptionError](#P-Nats-Extensions-Helper-Resources-HelperResource-SubscriptionError 'Nats.Extensions.Helper.Resources.HelperResource.SubscriptionError')
- [IConnectionWatcher\`1](#T-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1')
  - [ExecuteWhenConnectionIsEstablished(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsEstablished-System-Linq-Expressions-Expression{System-Action}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenConnectionIsEstablished(System.Linq.Expressions.Expression{System.Action})')
  - [ExecuteWhenConnectionIsEstablished(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsEstablished-System-Linq-Expressions-Expression{System-Func{System-Threading-CancellationToken,System-Threading-Tasks-Task}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenConnectionIsEstablished(System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}})')
  - [ExecuteWhenConnectionIsEstablished\`\`1(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsEstablished``1-System-Linq-Expressions-Expression{System-Action{``0}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenConnectionIsEstablished``1(System.Linq.Expressions.Expression{System.Action{``0}})')
  - [ExecuteWhenConnectionIsEstablished\`\`1(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsEstablished``1-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenConnectionIsEstablished``1(System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task}})')
  - [ExecuteWhenConnectionIsEstablished\`\`2(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsEstablished``2-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task{``1}}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenConnectionIsEstablished``2(System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task{``1}}})')
  - [ExecuteWhenConnectionIsLost(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsLost-System-Linq-Expressions-Expression{System-Action}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenConnectionIsLost(System.Linq.Expressions.Expression{System.Action})')
  - [ExecuteWhenConnectionIsLost(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsLost-System-Linq-Expressions-Expression{System-Func{System-Threading-CancellationToken,System-Threading-Tasks-Task}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenConnectionIsLost(System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}})')
  - [ExecuteWhenConnectionIsLost\`\`1(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsLost``1-System-Linq-Expressions-Expression{System-Action{``0}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenConnectionIsLost``1(System.Linq.Expressions.Expression{System.Action{``0}})')
  - [ExecuteWhenConnectionIsLost\`\`1(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsLost``1-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenConnectionIsLost``1(System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task}})')
  - [ExecuteWhenConnectionIsLost\`\`2(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsLost``2-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task{``1}}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenConnectionIsLost``2(System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task{``1}}})')
  - [ExecuteWhenFirstConnectionIsEstablished(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsEstablished-System-Linq-Expressions-Expression{System-Action}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenFirstConnectionIsEstablished(System.Linq.Expressions.Expression{System.Action})')
  - [ExecuteWhenFirstConnectionIsEstablished(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsEstablished-System-Linq-Expressions-Expression{System-Func{System-Threading-CancellationToken,System-Threading-Tasks-Task}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenFirstConnectionIsEstablished(System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}})')
  - [ExecuteWhenFirstConnectionIsEstablished\`\`1(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsEstablished``1-System-Linq-Expressions-Expression{System-Action{``0}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenFirstConnectionIsEstablished``1(System.Linq.Expressions.Expression{System.Action{``0}})')
  - [ExecuteWhenFirstConnectionIsEstablished\`\`1(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsEstablished``1-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenFirstConnectionIsEstablished``1(System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task}})')
  - [ExecuteWhenFirstConnectionIsEstablished\`\`2(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsEstablished``2-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task{``1}}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenFirstConnectionIsEstablished``2(System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task{``1}}})')
  - [ExecuteWhenFirstConnectionIsLost(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsLost-System-Linq-Expressions-Expression{System-Action}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenFirstConnectionIsLost(System.Linq.Expressions.Expression{System.Action})')
  - [ExecuteWhenFirstConnectionIsLost(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsLost-System-Linq-Expressions-Expression{System-Func{System-Threading-CancellationToken,System-Threading-Tasks-Task}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenFirstConnectionIsLost(System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}})')
  - [ExecuteWhenFirstConnectionIsLost\`\`1(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsLost``1-System-Linq-Expressions-Expression{System-Action{``0}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenFirstConnectionIsLost``1(System.Linq.Expressions.Expression{System.Action{``0}})')
  - [ExecuteWhenFirstConnectionIsLost\`\`1(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsLost``1-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenFirstConnectionIsLost``1(System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task}})')
  - [ExecuteWhenFirstConnectionIsLost\`\`2(methodCall)](#M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsLost``2-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task{``1}}}- 'Nats.Extensions.Helper.Connection.Watchers.IConnectionWatcher`1.ExecuteWhenFirstConnectionIsLost``2(System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task{``1}}})')
- [IMessageQueueHelper\`1](#T-Nats-Extensions-Helper-IMessageQueueHelper`1 'Nats.Extensions.Helper.IMessageQueueHelper`1')
  - [IsConnected](#P-Nats-Extensions-Helper-IMessageQueueHelper`1-IsConnected 'Nats.Extensions.Helper.IMessageQueueHelper`1.IsConnected')
  - [PublishAsync(subject,message,cancellationToken)](#M-Nats-Extensions-Helper-IMessageQueueHelper`1-PublishAsync-System-String,System-Byte[],System-Threading-CancellationToken- 'Nats.Extensions.Helper.IMessageQueueHelper`1.PublishAsync(System.String,System.Byte[],System.Threading.CancellationToken)')
  - [Subscribe(subject,handler,options)](#M-Nats-Extensions-Helper-IMessageQueueHelper`1-Subscribe-System-String,System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task},`0- 'Nats.Extensions.Helper.IMessageQueueHelper`1.Subscribe(System.String,System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task},`0)')
  - [Subscribe(subject,queue,handler,options)](#M-Nats-Extensions-Helper-IMessageQueueHelper`1-Subscribe-System-String,System-String,System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task},`0- 'Nats.Extensions.Helper.IMessageQueueHelper`1.Subscribe(System.String,System.String,System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task},`0)')
- [INatsRequestHelper](#T-Nats-Extensions-Helper-INatsRequestHelper 'Nats.Extensions.Helper.INatsRequestHelper')
  - [RequestAsync(subject,message,options,cancellationToken)](#M-Nats-Extensions-Helper-INatsRequestHelper-RequestAsync-System-String,System-Byte[],Nats-Extensions-Helper-Options-NatsRequestOptions,System-Threading-CancellationToken- 'Nats.Extensions.Helper.INatsRequestHelper.RequestAsync(System.String,System.Byte[],Nats.Extensions.Helper.Options.NatsRequestOptions,System.Threading.CancellationToken)')
  - [Subscribe(subject,handler,failure)](#M-Nats-Extensions-Helper-INatsRequestHelper-Subscribe-System-String,System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task{System-Byte[]}},System-Func{System-Exception,System-Byte[]}- 'Nats.Extensions.Helper.INatsRequestHelper.Subscribe(System.String,System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task{System.Byte[]}},System.Func{System.Exception,System.Byte[]})')
  - [Subscribe(subject,handler,failure,queue)](#M-Nats-Extensions-Helper-INatsRequestHelper-Subscribe-System-String,System-String,System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task{System-Byte[]}},System-Func{System-Exception,System-Byte[]}- 'Nats.Extensions.Helper.INatsRequestHelper.Subscribe(System.String,System.String,System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task{System.Byte[]}},System.Func{System.Exception,System.Byte[]})')
- [NatsConnectionOptions](#T-Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions 'Nats.Extensions.Helper.Options.Connection.NatsConnectionOptions')
  - [Name](#P-Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions-Name 'Nats.Extensions.Helper.Options.Connection.NatsConnectionOptions.Name')
- [NatsRequestOptions](#T-Nats-Extensions-Helper-Options-NatsRequestOptions 'Nats.Extensions.Helper.Options.NatsRequestOptions')
  - [RequestTimeout](#P-Nats-Extensions-Helper-Options-NatsRequestOptions-RequestTimeout 'Nats.Extensions.Helper.Options.NatsRequestOptions.RequestTimeout')
- [ServiceExtensions](#T-Nats-Extensions-Helper-Extensions-ServiceExtensions 'Nats.Extensions.Helper.Extensions.ServiceExtensions')
  - [AddNatsExtensionsHelper(configure)](#M-Nats-Extensions-Helper-Extensions-ServiceExtensions-AddNatsExtensionsHelper-Microsoft-Extensions-DependencyInjection-IServiceCollection,System-Action{Nats-Extensions-Helper-Builders-HelperBuilder}- 'Nats.Extensions.Helper.Extensions.ServiceExtensions.AddNatsExtensionsHelper(Microsoft.Extensions.DependencyInjection.IServiceCollection,System.Action{Nats.Extensions.Helper.Builders.HelperBuilder})')
- [StanConnectionOptions](#T-Nats-Extensions-Helper-Options-Connection-StanConnectionOptions 'Nats.Extensions.Helper.Options.Connection.StanConnectionOptions')
  - [ClientId](#P-Nats-Extensions-Helper-Options-Connection-StanConnectionOptions-ClientId 'Nats.Extensions.Helper.Options.Connection.StanConnectionOptions.ClientId')
  - [ClusterId](#P-Nats-Extensions-Helper-Options-Connection-StanConnectionOptions-ClusterId 'Nats.Extensions.Helper.Options.Connection.StanConnectionOptions.ClusterId')
  - [NatsConnection](#P-Nats-Extensions-Helper-Options-Connection-StanConnectionOptions-NatsConnection 'Nats.Extensions.Helper.Options.Connection.StanConnectionOptions.NatsConnection')
  - [StanRefreshConnectionTimeout](#P-Nats-Extensions-Helper-Options-Connection-StanConnectionOptions-StanRefreshConnectionTimeout 'Nats.Extensions.Helper.Options.Connection.StanConnectionOptions.StanRefreshConnectionTimeout')
- [StanExtensionsOptions](#T-Nats-Extensions-Helper-Options-StanExtensionsOptions 'Nats.Extensions.Helper.Options.StanExtensionsOptions')
  - [AckWait](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-AckWait 'Nats.Extensions.Helper.Options.StanExtensionsOptions.AckWait')
  - [DurableName](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-DurableName 'Nats.Extensions.Helper.Options.StanExtensionsOptions.DurableName')
  - [LeaveOpen](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-LeaveOpen 'Nats.Extensions.Helper.Options.StanExtensionsOptions.LeaveOpen')
  - [ManualAcks](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-ManualAcks 'Nats.Extensions.Helper.Options.StanExtensionsOptions.ManualAcks')
  - [MaxInflight](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-MaxInflight 'Nats.Extensions.Helper.Options.StanExtensionsOptions.MaxInflight')
  - [SerialAcks](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-SerialAcks 'Nats.Extensions.Helper.Options.StanExtensionsOptions.SerialAcks')
  - [ShouldDeliverAllAvailable](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-ShouldDeliverAllAvailable 'Nats.Extensions.Helper.Options.StanExtensionsOptions.ShouldDeliverAllAvailable')
  - [ShouldStartWithLastReceived](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-ShouldStartWithLastReceived 'Nats.Extensions.Helper.Options.StanExtensionsOptions.ShouldStartWithLastReceived')
  - [StartAtDuration](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-StartAtDuration 'Nats.Extensions.Helper.Options.StanExtensionsOptions.StartAtDuration')
  - [StartAtSequence](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-StartAtSequence 'Nats.Extensions.Helper.Options.StanExtensionsOptions.StartAtSequence')
  - [StartAtTime](#P-Nats-Extensions-Helper-Options-StanExtensionsOptions-StartAtTime 'Nats.Extensions.Helper.Options.StanExtensionsOptions.StartAtTime')
- [StanHelper](#T-Nats-Extensions-Helper-Implementations-Stan-StanHelper 'Nats.Extensions.Helper.Implementations.Stan.StanHelper')
  - [CreateConsequentHandler(action,cancellationToken)](#M-Nats-Extensions-Helper-Implementations-Stan-StanHelper-CreateConsequentHandler-System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task},System-Threading-CancellationToken- 'Nats.Extensions.Helper.Implementations.Stan.StanHelper.CreateConsequentHandler(System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task},System.Threading.CancellationToken)')
  - [CreateInconsequentHandler(subject,action,cancellationToken)](#M-Nats-Extensions-Helper-Implementations-Stan-StanHelper-CreateInconsequentHandler-System-String,System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task},System-Threading-CancellationToken- 'Nats.Extensions.Helper.Implementations.Stan.StanHelper.CreateInconsequentHandler(System.String,System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task},System.Threading.CancellationToken)')
  - [CreateSerialHandler(action,cancellationToken)](#M-Nats-Extensions-Helper-Implementations-Stan-StanHelper-CreateSerialHandler-System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task},System-Threading-CancellationToken- 'Nats.Extensions.Helper.Implementations.Stan.StanHelper.CreateSerialHandler(System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task},System.Threading.CancellationToken)')
- [ValidatorResource](#T-Nats-Extensions-Helper-Resources-ValidatorResource 'Nats.Extensions.Helper.Resources.ValidatorResource')
  - [AnyServer](#P-Nats-Extensions-Helper-Resources-ValidatorResource-AnyServer 'Nats.Extensions.Helper.Resources.ValidatorResource.AnyServer')
  - [Culture](#P-Nats-Extensions-Helper-Resources-ValidatorResource-Culture 'Nats.Extensions.Helper.Resources.ValidatorResource.Culture')
  - [Empty](#P-Nats-Extensions-Helper-Resources-ValidatorResource-Empty 'Nats.Extensions.Helper.Resources.ValidatorResource.Empty')
  - [EmptyNatsUri](#P-Nats-Extensions-Helper-Resources-ValidatorResource-EmptyNatsUri 'Nats.Extensions.Helper.Resources.ValidatorResource.EmptyNatsUri')
  - [GreaterThan](#P-Nats-Extensions-Helper-Resources-ValidatorResource-GreaterThan 'Nats.Extensions.Helper.Resources.ValidatorResource.GreaterThan')
  - [NotEmpty](#P-Nats-Extensions-Helper-Resources-ValidatorResource-NotEmpty 'Nats.Extensions.Helper.Resources.ValidatorResource.NotEmpty')
  - [NotNull](#P-Nats-Extensions-Helper-Resources-ValidatorResource-NotNull 'Nats.Extensions.Helper.Resources.ValidatorResource.NotNull')
  - [ResourceManager](#P-Nats-Extensions-Helper-Resources-ValidatorResource-ResourceManager 'Nats.Extensions.Helper.Resources.ValidatorResource.ResourceManager')
- [WatcherResource](#T-Nats-Extensions-Helper-Resources-WatcherResource 'Nats.Extensions.Helper.Resources.WatcherResource')
  - [ConnectionEstablishedError](#P-Nats-Extensions-Helper-Resources-WatcherResource-ConnectionEstablishedError 'Nats.Extensions.Helper.Resources.WatcherResource.ConnectionEstablishedError')
  - [ConnectionLostError](#P-Nats-Extensions-Helper-Resources-WatcherResource-ConnectionLostError 'Nats.Extensions.Helper.Resources.WatcherResource.ConnectionLostError')
  - [Culture](#P-Nats-Extensions-Helper-Resources-WatcherResource-Culture 'Nats.Extensions.Helper.Resources.WatcherResource.Culture')
  - [ResourceManager](#P-Nats-Extensions-Helper-Resources-WatcherResource-ResourceManager 'Nats.Extensions.Helper.Resources.WatcherResource.ResourceManager')

<a name='T-Nats-Extensions-Helper-Resources-BufferResource'></a>
## BufferResource `type`

##### Namespace

Nats.Extensions.Helper.Resources

##### Summary

A strongly-typed resource class, for looking up localized strings, etc.

<a name='P-Nats-Extensions-Helper-Resources-BufferResource-CannotReadFromBufferError'></a>
### CannotReadFromBufferError `property`

##### Summary

Looks up a localized string similar to Нет возможности читать из буфера для канала  \`{0}\`..

<a name='P-Nats-Extensions-Helper-Resources-BufferResource-CannotWriteToBufferError'></a>
### CannotWriteToBufferError `property`

##### Summary

Looks up a localized string similar to Нет возможности писать для канала \`{0}\`..

<a name='P-Nats-Extensions-Helper-Resources-BufferResource-Culture'></a>
### Culture `property`

##### Summary

Overrides the current thread's CurrentUICulture property for all
  resource lookups using this strongly typed resource class.

<a name='P-Nats-Extensions-Helper-Resources-BufferResource-HandlingError'></a>
### HandlingError `property`

##### Summary

Looks up a localized string similar to Во время обработки сообщения из канала: \`{0}\` произошла ошибка. {1}Сообщение: \`{2}\`..

<a name='P-Nats-Extensions-Helper-Resources-BufferResource-ObjectDisposedError'></a>
### ObjectDisposedError `property`

##### Summary

Looks up a localized string similar to Cообщение из канала: \`{0}\` было закэшировано но не было обработано, обработчик завершил работу. {1}Сообщение: \`{2}\`..

<a name='P-Nats-Extensions-Helper-Resources-BufferResource-OperationCanceledError'></a>
### OperationCanceledError `property`

##### Summary

Looks up a localized string similar to Cообщение из канала: \`{0}\`  не было обработано, обработчик завершил работу. {1}Сообщение: \`{2}\`..

<a name='P-Nats-Extensions-Helper-Resources-BufferResource-ReadFromBufferError'></a>
### ReadFromBufferError `property`

##### Summary

Looks up a localized string similar to Чтение из буфера для канала \`{0}\` завершено..

<a name='P-Nats-Extensions-Helper-Resources-BufferResource-ResourceManager'></a>
### ResourceManager `property`

##### Summary

Returns the cached ResourceManager instance used by this class.

<a name='P-Nats-Extensions-Helper-Resources-BufferResource-WriteToBufferError'></a>
### WriteToBufferError `property`

##### Summary

Looks up a localized string similar to Запись в буфер для канала \`{0}\` завершена..

<a name='T-Nats-Extensions-Helper-Resources-BuilderResource'></a>
## BuilderResource `type`

##### Namespace

Nats.Extensions.Helper.Resources

##### Summary

A strongly-typed resource class, for looking up localized strings, etc.

<a name='P-Nats-Extensions-Helper-Resources-BuilderResource-Culture'></a>
### Culture `property`

##### Summary

Overrides the current thread's CurrentUICulture property for all
  resource lookups using this strongly typed resource class.

<a name='P-Nats-Extensions-Helper-Resources-BuilderResource-ResourceManager'></a>
### ResourceManager `property`

##### Summary

Returns the cached ResourceManager instance used by this class.

<a name='P-Nats-Extensions-Helper-Resources-BuilderResource-ShouldUse'></a>
### ShouldUse `property`

##### Summary

Looks up a localized string similar to Перед тем как вызвать метод: \`{0} или {1}\` необходимо настроить параметры подключения через \`{2}\`.

<a name='T-Nats-Extensions-Helper-Resources-ClientResource'></a>
## ClientResource `type`

##### Namespace

Nats.Extensions.Helper.Resources

##### Summary

A strongly-typed resource class, for looking up localized strings, etc.

<a name='P-Nats-Extensions-Helper-Resources-ClientResource-AutoRefreshNatsConnection'></a>
### AutoRefreshNatsConnection `property`

##### Summary

Looks up a localized string similar to Автоматическое обновление подключение к \`NATS\`..

<a name='P-Nats-Extensions-Helper-Resources-ClientResource-AutoRefreshStanConnection'></a>
### AutoRefreshStanConnection `property`

##### Summary

Looks up a localized string similar to Автоматическое обновление подключение к \`STAN\`..

<a name='P-Nats-Extensions-Helper-Resources-ClientResource-ConnectionState'></a>
### ConnectionState `property`

##### Summary

Looks up a localized string similar to Статус подключения к \`NATS\` -> \`{0}\`..

<a name='P-Nats-Extensions-Helper-Resources-ClientResource-Culture'></a>
### Culture `property`

##### Summary

Overrides the current thread's CurrentUICulture property for all
  resource lookups using this strongly typed resource class.

<a name='P-Nats-Extensions-Helper-Resources-ClientResource-EmptyCredentials'></a>
### EmptyCredentials `property`

##### Summary

Looks up a localized string similar to Учетные данные клиента не определены..

<a name='P-Nats-Extensions-Helper-Resources-ClientResource-NatsConnectionLost'></a>
### NatsConnectionLost `property`

##### Summary

Looks up a localized string similar to Пропало подключение к \`NATS\`..

<a name='P-Nats-Extensions-Helper-Resources-ClientResource-NatsReconnect'></a>
### NatsReconnect `property`

##### Summary

Looks up a localized string similar to Попытка подключения к \`NATS\`..

<a name='P-Nats-Extensions-Helper-Resources-ClientResource-ResourceManager'></a>
### ResourceManager `property`

##### Summary

Returns the cached ResourceManager instance used by this class.

<a name='P-Nats-Extensions-Helper-Resources-ClientResource-StanConnectionLost'></a>
### StanConnectionLost `property`

##### Summary

Looks up a localized string similar to Пропало подключение к \`STAN\`..

<a name='P-Nats-Extensions-Helper-Resources-ClientResource-StanReconnect'></a>
### StanReconnect `property`

##### Summary

Looks up a localized string similar to Попытка подключения к \`STAN\`..

<a name='T-Nats-Extensions-Helper-Resources-CommonResource'></a>
## CommonResource `type`

##### Namespace

Nats.Extensions.Helper.Resources

##### Summary

A strongly-typed resource class, for looking up localized strings, etc.

<a name='P-Nats-Extensions-Helper-Resources-CommonResource-ArgumentNull'></a>
### ArgumentNull `property`

##### Summary

Looks up a localized string similar to Входной параметр не может быть пустым или null.

<a name='P-Nats-Extensions-Helper-Resources-CommonResource-Culture'></a>
### Culture `property`

##### Summary

Overrides the current thread's CurrentUICulture property for all
  resource lookups using this strongly typed resource class.

<a name='P-Nats-Extensions-Helper-Resources-CommonResource-InvalidQueueGroup'></a>
### InvalidQueueGroup `property`

##### Summary

Looks up a localized string similar to Входной параметр не может быть пустым или null, содержать \`[{0}, ..]\`.

<a name='P-Nats-Extensions-Helper-Resources-CommonResource-InvalidSubject'></a>
### InvalidSubject `property`

##### Summary

Looks up a localized string similar to Входной параметр не может быть пустым или null, содержать \`[{0}, ..]\`, начинаться и заканчиваться на '.'.

<a name='P-Nats-Extensions-Helper-Resources-CommonResource-ResourceManager'></a>
### ResourceManager `property`

##### Summary

Returns the cached ResourceManager instance used by this class.

<a name='T-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder'></a>
## ConnectionBuilder `type`

##### Namespace

Nats.Extensions.Helper.Builders.Connection

<a name='P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-ClientId'></a>
### ClientId `property`

##### Summary

Уникальный идентификатор подключения к NSS.

<a name='P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-Host'></a>
### Host `property`

##### Summary

Host на котором запущена служба NSS.

<a name='P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-Password'></a>
### Password `property`

##### Summary

Пароль пользователя \`NATS\`

<a name='P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-Port'></a>
### Port `property`

##### Summary

Порт на котором запущена служба NSS.

<a name='P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-ReconnectTimeout'></a>
### ReconnectTimeout `property`

##### Summary

Период переподключения при потери соединения с NSS.

<a name='P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-RefreshConnectionTimeout'></a>
### RefreshConnectionTimeout `property`

##### Summary

Период обновления соединений с NSS.
Если значение задано, то обновления будут происходит по периоду.

<a name='P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-Servers'></a>
### Servers `property`

##### Summary

Добавляет сервера кластера к которому клиент может подключится.

<a name='P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-StanRefreshConnectionTimeout'></a>
### StanRefreshConnectionTimeout `property`

##### Summary

Через заданный инетрвал будет происходить обновление подключения к \`STAN\`.
 Если значение задано, то обновления будут происходит по периоду.

<a name='P-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder-User'></a>
### User `property`

##### Summary

Учетная запись пользователя \`NATS\`

<a name='T-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilderExtensions'></a>
## ConnectionBuilderExtensions `type`

##### Namespace

Nats.Extensions.Helper.Builders.Connection

<a name='M-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilderExtensions-AddPipeAuthorization-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder,System-Action{Nats-Extensions-Common-Options-PipeManagerOptions}-'></a>
### AddPipeAuthorization(configure) `method`

##### Summary

Добавляет сервис авторизации по

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder](#T-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder') | Метод инициализации. |

<a name='M-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilderExtensions-AddServers-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder,System-String[]-'></a>
### AddServers(servers) `method`

##### Summary

Добавляет сервера кластера к которому клиент может подключится.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| servers | [Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder](#T-Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder 'Nats.Extensions.Helper.Builders.Connection.ConnectionBuilder') | Полной адрес с указанием порта. Пример: nats://localhost:4222 |

<a name='T-Nats-Extensions-Helper-Options-Connection-ConnectionOptions'></a>
## ConnectionOptions `type`

##### Namespace

Nats.Extensions.Helper.Options.Connection

<a name='P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-Password'></a>
### Password `property`

##### Summary

Пароль пользователя \`NATS\`

<a name='P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-ReconnectTimeout'></a>
### ReconnectTimeout `property`

##### Summary

Через заданный инетрвал будет происходить переподключение

<a name='P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-RefreshConnectionTimeout'></a>
### RefreshConnectionTimeout `property`

##### Summary

Через заданный инетрвал будет происходить обновления подключений

<a name='P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-Servers'></a>
### Servers `property`

##### Summary

Список серверов кластера

<a name='P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-Uri'></a>
### Uri `property`

##### Summary

URI для подключения

<a name='P-Nats-Extensions-Helper-Options-Connection-ConnectionOptions-User'></a>
### User `property`

##### Summary

Учетная запись пользователя \`NATS\`

<a name='T-Nats-Extensions-Helper-Builders-HelperBuilderExtensions'></a>
## HelperBuilderExtensions `type`

##### Namespace

Nats.Extensions.Helper.Builders

<a name='M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-ConfigureConnection-Nats-Extensions-Helper-Builders-HelperBuilder,System-Action{Nats-Extensions-Helper-Builders-Connection-ConnectionBuilder}-'></a>
### ConfigureConnection(configure) `method`

##### Summary

Конфигурация основных настроек подключения.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [Nats.Extensions.Helper.Builders.HelperBuilder](#T-Nats-Extensions-Helper-Builders-HelperBuilder 'Nats.Extensions.Helper.Builders.HelperBuilder') | Метод конфигурации. |

<a name='M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-UseNats-Nats-Extensions-Helper-Builders-HelperBuilder-'></a>
### UseNats() `method`

##### Summary

При вызове метода будет использоваться NATS соединение.

##### Returns



##### Parameters

This method has no parameters.

<a name='M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-UseNats-Nats-Extensions-Helper-Builders-HelperBuilder,System-Action{Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions}-'></a>
### UseNats(configure) `method`

##### Summary

При вызове метода будет использоваться NATS соединение.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [Nats.Extensions.Helper.Builders.HelperBuilder](#T-Nats-Extensions-Helper-Builders-HelperBuilder 'Nats.Extensions.Helper.Builders.HelperBuilder') | Метод конфигурации. |

<a name='M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-UseNats-Nats-Extensions-Helper-Builders-HelperBuilder,System-Action{Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions,Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher{Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions}}-'></a>
### UseNats(configure) `method`

##### Summary

При вызове метода будет использоваться NATS соединение.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [Nats.Extensions.Helper.Builders.HelperBuilder](#T-Nats-Extensions-Helper-Builders-HelperBuilder 'Nats.Extensions.Helper.Builders.HelperBuilder') | Метод конфигурации. |

<a name='M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-UseStan-Nats-Extensions-Helper-Builders-HelperBuilder,System-Action{Nats-Extensions-Helper-Options-Connection-StanConnectionOptions}-'></a>
### UseStan(configure) `method`

##### Summary

При вызове метода будет использоваться STAN поверх существующего NATS соединения.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [Nats.Extensions.Helper.Builders.HelperBuilder](#T-Nats-Extensions-Helper-Builders-HelperBuilder 'Nats.Extensions.Helper.Builders.HelperBuilder') | Метод конфигурации. |

<a name='M-Nats-Extensions-Helper-Builders-HelperBuilderExtensions-UseStan-Nats-Extensions-Helper-Builders-HelperBuilder,System-Action{Nats-Extensions-Helper-Options-Connection-StanConnectionOptions,Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher{Nats-Extensions-Helper-Options-Connection-StanConnectionOptions}}-'></a>
### UseStan(configure) `method`

##### Summary

При вызове метода будет использоваться STAN поверх существующего NATS соединения.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [Nats.Extensions.Helper.Builders.HelperBuilder](#T-Nats-Extensions-Helper-Builders-HelperBuilder 'Nats.Extensions.Helper.Builders.HelperBuilder') | Метод конфигурации. |

<a name='T-Nats-Extensions-Helper-Resources-HelperResource'></a>
## HelperResource `type`

##### Namespace

Nats.Extensions.Helper.Resources

##### Summary

A strongly-typed resource class, for looking up localized strings, etc.

<a name='P-Nats-Extensions-Helper-Resources-HelperResource-Culture'></a>
### Culture `property`

##### Summary

Overrides the current thread's CurrentUICulture property for all
  resource lookups using this strongly typed resource class.

<a name='P-Nats-Extensions-Helper-Resources-HelperResource-HandlerError'></a>
### HandlerError `property`

##### Summary

Looks up a localized string similar to Во время обработки сообщения из канала: \`{0}\` произошла ошибка. Сообщение: \`{1}\`.

<a name='P-Nats-Extensions-Helper-Resources-HelperResource-NatsConnectionEstablished'></a>
### NatsConnectionEstablished `property`

##### Summary

Looks up a localized string similar to Соединение с \`NATS\` установлено по адресу \`{0}\`..

<a name='P-Nats-Extensions-Helper-Resources-HelperResource-NatsConnectionLost'></a>
### NatsConnectionLost `property`

##### Summary

Looks up a localized string similar to Нет подключения к \`NATS\`..

<a name='P-Nats-Extensions-Helper-Resources-HelperResource-ResourceManager'></a>
### ResourceManager `property`

##### Summary

Returns the cached ResourceManager instance used by this class.

<a name='P-Nats-Extensions-Helper-Resources-HelperResource-StanConnectionEstablished'></a>
### StanConnectionEstablished `property`

##### Summary

Looks up a localized string similar to Соединение с \`STAN\` установлено по адресу \`{0}\`..

<a name='P-Nats-Extensions-Helper-Resources-HelperResource-StanConnectionLost'></a>
### StanConnectionLost `property`

##### Summary

Looks up a localized string similar to Нет подключения к \`STAN\`..

<a name='P-Nats-Extensions-Helper-Resources-HelperResource-SubscriptionError'></a>
### SubscriptionError `property`

##### Summary

Looks up a localized string similar to Во время подписки на топик \`{0}\` произошла ошибка..

<a name='T-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1'></a>
## IConnectionWatcher\`1 `type`

##### Namespace

Nats.Extensions.Helper.Connection.Watchers

##### Summary

Данный интерфейс позволяет следить за соединением с NSS.

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TOptions | Тип соединения (NATS/STAN) |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsEstablished-System-Linq-Expressions-Expression{System-Action}-'></a>
### ExecuteWhenConnectionIsEstablished(methodCall) `method`

##### Summary

Подписка на событие об успешном соединении с (NATS/STAN)

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Action}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Action}') | Метод, который будет вызван. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsEstablished-System-Linq-Expressions-Expression{System-Func{System-Threading-CancellationToken,System-Threading-Tasks-Task}}-'></a>
### ExecuteWhenConnectionIsEstablished(methodCall) `method`

##### Summary

Подписка на событие об успешном соединении с (NATS/STAN)

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}}') | Метод, который будет вызван. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsEstablished``1-System-Linq-Expressions-Expression{System-Action{``0}}-'></a>
### ExecuteWhenConnectionIsEstablished\`\`1(methodCall) `method`

##### Summary

Подписка на событие об успешном соединении с (NATS/STAN)

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Action{\`\`0}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Action{``0}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsEstablished``1-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task}}-'></a>
### ExecuteWhenConnectionIsEstablished\`\`1(methodCall) `method`

##### Summary

Подписка на событие об успешном соединении с (NATS/STAN)

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{\`\`0,System.Threading.CancellationToken,System.Threading.Tasks.Task}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsEstablished``2-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task{``1}}}-'></a>
### ExecuteWhenConnectionIsEstablished\`\`2(methodCall) `method`

##### Summary

Подписка на событие об успешном соединении с (NATS/STAN)

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{\`\`0,System.Threading.CancellationToken,System.Threading.Tasks.Task{\`\`1}}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task{``1}}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |
| TResult | Тип, который возвращает метод. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsLost-System-Linq-Expressions-Expression{System-Action}-'></a>
### ExecuteWhenConnectionIsLost(methodCall) `method`

##### Summary

Подписка на событие о потери соединения с (NATS/STAN)

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Action}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Action}') | Метод, который будет вызван. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsLost-System-Linq-Expressions-Expression{System-Func{System-Threading-CancellationToken,System-Threading-Tasks-Task}}-'></a>
### ExecuteWhenConnectionIsLost(methodCall) `method`

##### Summary

Подписка на событие о потери соединения с (NATS/STAN)

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}}') | Метод, который будет вызван. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsLost``1-System-Linq-Expressions-Expression{System-Action{``0}}-'></a>
### ExecuteWhenConnectionIsLost\`\`1(methodCall) `method`

##### Summary

Подписка на событие о потери соединения с (NATS/STAN)

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Action{\`\`0}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Action{``0}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsLost``1-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task}}-'></a>
### ExecuteWhenConnectionIsLost\`\`1(methodCall) `method`

##### Summary

Подписка на событие о потери соединения с (NATS/STAN)

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{\`\`0,System.Threading.CancellationToken,System.Threading.Tasks.Task}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenConnectionIsLost``2-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task{``1}}}-'></a>
### ExecuteWhenConnectionIsLost\`\`2(methodCall) `method`

##### Summary

Подписка на событие о потери соединения с (NATS/STAN)

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{\`\`0,System.Threading.CancellationToken,System.Threading.Tasks.Task{\`\`1}}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task{``1}}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |
| TResult | Тип, который возвращает метод. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsEstablished-System-Linq-Expressions-Expression{System-Action}-'></a>
### ExecuteWhenFirstConnectionIsEstablished(methodCall) `method`

##### Summary

Подписка на первое событие об успешном соединении с (NATS/STAN).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Action}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Action}') | Метод, который будет вызван. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsEstablished-System-Linq-Expressions-Expression{System-Func{System-Threading-CancellationToken,System-Threading-Tasks-Task}}-'></a>
### ExecuteWhenFirstConnectionIsEstablished(methodCall) `method`

##### Summary

Подписка на первое событие об успешном соединении с (NATS/STAN).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}}') | Метод, который будет вызван. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsEstablished``1-System-Linq-Expressions-Expression{System-Action{``0}}-'></a>
### ExecuteWhenFirstConnectionIsEstablished\`\`1(methodCall) `method`

##### Summary

Подписка на первое событие об успешном соединении с (NATS/STAN).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Action{\`\`0}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Action{``0}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsEstablished``1-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task}}-'></a>
### ExecuteWhenFirstConnectionIsEstablished\`\`1(methodCall) `method`

##### Summary

Подписка на первое событие об успешном соединении с (NATS/STAN).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{\`\`0,System.Threading.CancellationToken,System.Threading.Tasks.Task}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsEstablished``2-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task{``1}}}-'></a>
### ExecuteWhenFirstConnectionIsEstablished\`\`2(methodCall) `method`

##### Summary

Подписка на первое событие об успешном соединении с (NATS/STAN).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{\`\`0,System.Threading.CancellationToken,System.Threading.Tasks.Task{\`\`1}}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task{``1}}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |
| TResult | Тип, который возвращает метод. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsLost-System-Linq-Expressions-Expression{System-Action}-'></a>
### ExecuteWhenFirstConnectionIsLost(methodCall) `method`

##### Summary

Подписка на первое событие о потери соединения с (NATS/STAN).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Action}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Action}') | Метод, который будет вызван. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsLost-System-Linq-Expressions-Expression{System-Func{System-Threading-CancellationToken,System-Threading-Tasks-Task}}-'></a>
### ExecuteWhenFirstConnectionIsLost(methodCall) `method`

##### Summary

Подписка на первое событие о потери соединения с (NATS/STAN).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{System.Threading.CancellationToken,System.Threading.Tasks.Task}}') | Метод, который будет вызван. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsLost``1-System-Linq-Expressions-Expression{System-Action{``0}}-'></a>
### ExecuteWhenFirstConnectionIsLost\`\`1(methodCall) `method`

##### Summary

Подписка на первое событие о потери соединения с (NATS/STAN).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Action{\`\`0}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Action{``0}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsLost``1-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task}}-'></a>
### ExecuteWhenFirstConnectionIsLost\`\`1(methodCall) `method`

##### Summary

Подписка на первое событие о потери соединения с (NATS/STAN).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{\`\`0,System.Threading.CancellationToken,System.Threading.Tasks.Task}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |

<a name='M-Nats-Extensions-Helper-Connection-Watchers-IConnectionWatcher`1-ExecuteWhenFirstConnectionIsLost``2-System-Linq-Expressions-Expression{System-Func{``0,System-Threading-CancellationToken,System-Threading-Tasks-Task{``1}}}-'></a>
### ExecuteWhenFirstConnectionIsLost\`\`2(methodCall) `method`

##### Summary

Подписка на первое событие о потери соединения с (NATS/STAN).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на событие если оно еще не произошло.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| methodCall | [System.Linq.Expressions.Expression{System.Func{\`\`0,System.Threading.CancellationToken,System.Threading.Tasks.Task{\`\`1}}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Linq.Expressions.Expression 'System.Linq.Expressions.Expression{System.Func{``0,System.Threading.CancellationToken,System.Threading.Tasks.Task{``1}}}') | Метод, который будет вызван. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TService | Тип объекта чей метод будет вызван при возникновении события. |
| TResult | Тип, который возвращает метод. |

<a name='T-Nats-Extensions-Helper-IMessageQueueHelper`1'></a>
## IMessageQueueHelper\`1 `type`

##### Namespace

Nats.Extensions.Helper

##### Summary

Вспомогательная обертка над шиной (NSS).

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TOptions |  |

<a name='P-Nats-Extensions-Helper-IMessageQueueHelper`1-IsConnected'></a>
### IsConnected `property`

##### Summary

Статус подключения

<a name='M-Nats-Extensions-Helper-IMessageQueueHelper`1-PublishAsync-System-String,System-Byte[],System-Threading-CancellationToken-'></a>
### PublishAsync(subject,message,cancellationToken) `method`

##### Summary

Публикация сообщения в шину (NSS).

##### Returns

Возвращает Task

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| message | [System.Byte[]](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Byte[] 'System.Byte[]') | Сообщение, которое будет опубликовано в шину (NSS). |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

<a name='M-Nats-Extensions-Helper-IMessageQueueHelper`1-Subscribe-System-String,System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task},`0-'></a>
### Subscribe(subject,handler,options) `method`

##### Summary

Подписка на канал в шине (NSS).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| handler | [System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Func 'System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task}') | Обработчик, который будет обрабатывать сообщение из канала. |
| options | [\`0](#T-`0 '`0') | Настройки, который могут быть переданны во время подписки. |

<a name='M-Nats-Extensions-Helper-IMessageQueueHelper`1-Subscribe-System-String,System-String,System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task},`0-'></a>
### Subscribe(subject,queue,handler,options) `method`

##### Summary

Подписка на канал в шине (NSS) c очередью.

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| queue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование очереди. |
| handler | [System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Func 'System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task}') | Обработчик, который будет обрабатывать сообщение из канала. |
| options | [\`0](#T-`0 '`0') | Настройки, который могут быть переданны во время подписки. |

<a name='T-Nats-Extensions-Helper-INatsRequestHelper'></a>
## INatsRequestHelper `type`

##### Namespace

Nats.Extensions.Helper

<a name='M-Nats-Extensions-Helper-INatsRequestHelper-RequestAsync-System-String,System-Byte[],Nats-Extensions-Helper-Options-NatsRequestOptions,System-Threading-CancellationToken-'></a>
### RequestAsync(subject,message,options,cancellationToken) `method`

##### Summary

Публикация сообщения в шину (NSS) с ответом от обработчика.

##### Returns

Возвращает сообщение, которое было получено от обработчика

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| message | [System.Byte[]](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Byte[] 'System.Byte[]') | Сообщение, которое будет опубликовано в шину (NSS). |
| options | [Nats.Extensions.Helper.Options.NatsRequestOptions](#T-Nats-Extensions-Helper-Options-NatsRequestOptions 'Nats.Extensions.Helper.Options.NatsRequestOptions') | Настройки, который могут быть переданны во время публикации сообщения. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

<a name='M-Nats-Extensions-Helper-INatsRequestHelper-Subscribe-System-String,System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task{System-Byte[]}},System-Func{System-Exception,System-Byte[]}-'></a>
### Subscribe(subject,handler,failure) `method`

##### Summary

Подписка на канал в шине (NSS) с возможностью ответа на запрос.

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| handler | [System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task{System.Byte[]}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Func 'System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task{System.Byte[]}}') | Обработчик, который будет обрабатывать сообщение из канала. |
| failure | [System.Func{System.Exception,System.Byte[]}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Func 'System.Func{System.Exception,System.Byte[]}') | Метод обратного вызова для обработки ошибок. |

<a name='M-Nats-Extensions-Helper-INatsRequestHelper-Subscribe-System-String,System-String,System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task{System-Byte[]}},System-Func{System-Exception,System-Byte[]}-'></a>
### Subscribe(subject,handler,failure,queue) `method`

##### Summary

Подписка на канал в шине (NSS) с возможностью ответа на запрос.

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| handler | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Обработчик, который будет обрабатывать сообщение из канала. |
| failure | [System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task{System.Byte[]}}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Func 'System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task{System.Byte[]}}') | Метод обратного вызова для обработки ошибок. |
| queue | [System.Func{System.Exception,System.Byte[]}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Func 'System.Func{System.Exception,System.Byte[]}') | Наименование очереди. Опционально. |

<a name='T-Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions'></a>
## NatsConnectionOptions `type`

##### Namespace

Nats.Extensions.Helper.Options.Connection

##### Summary

Настройки подключения Nats

<a name='P-Nats-Extensions-Helper-Options-Connection-NatsConnectionOptions-Name'></a>
### Name `property`

##### Summary

Уникальный Client-Id сервиса

<a name='T-Nats-Extensions-Helper-Options-NatsRequestOptions'></a>
## NatsRequestOptions `type`

##### Namespace

Nats.Extensions.Helper.Options

<a name='P-Nats-Extensions-Helper-Options-NatsRequestOptions-RequestTimeout'></a>
### RequestTimeout `property`

##### Summary

Интервал, который отведен на запрос. По умолчанию равен 5 секундам.

<a name='T-Nats-Extensions-Helper-Extensions-ServiceExtensions'></a>
## ServiceExtensions `type`

##### Namespace

Nats.Extensions.Helper.Extensions

<a name='M-Nats-Extensions-Helper-Extensions-ServiceExtensions-AddNatsExtensionsHelper-Microsoft-Extensions-DependencyInjection-IServiceCollection,System-Action{Nats-Extensions-Helper-Builders-HelperBuilder}-'></a>
### AddNatsExtensionsHelper(configure) `method`

##### Summary

Метод подключения и конфигурации зависимостей для NSS

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [Microsoft.Extensions.DependencyInjection.IServiceCollection](#T-Microsoft-Extensions-DependencyInjection-IServiceCollection 'Microsoft.Extensions.DependencyInjection.IServiceCollection') | Метод конфигурации. |

<a name='T-Nats-Extensions-Helper-Options-Connection-StanConnectionOptions'></a>
## StanConnectionOptions `type`

##### Namespace

Nats.Extensions.Helper.Options.Connection

<a name='P-Nats-Extensions-Helper-Options-Connection-StanConnectionOptions-ClientId'></a>
### ClientId `property`

##### Summary

Уникальный Id сервиса

<a name='P-Nats-Extensions-Helper-Options-Connection-StanConnectionOptions-ClusterId'></a>
### ClusterId `property`

##### Summary

Название кластера, это константа

<a name='P-Nats-Extensions-Helper-Options-Connection-StanConnectionOptions-NatsConnection'></a>
### NatsConnection `property`

##### Summary



<a name='P-Nats-Extensions-Helper-Options-Connection-StanConnectionOptions-StanRefreshConnectionTimeout'></a>
### StanRefreshConnectionTimeout `property`

##### Summary

Через заданный инетрвал будет происходить обновление подключения

<a name='T-Nats-Extensions-Helper-Options-StanExtensionsOptions'></a>
## StanExtensionsOptions `type`

##### Namespace

Nats.Extensions.Helper.Options

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-AckWait'></a>
### AckWait `property`

##### Summary

Кол-во времени, которое кластер будет ждать ACK от клиент, для данного сообщения.

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-DurableName'></a>
### DurableName `property`

##### Summary

Именованная подписка.

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-LeaveOpen'></a>
### LeaveOpen `property`

##### Summary

Если значение равно True, то вызывает \`Close()\` во время утилизации подписки или \`Unsubscribe()\`.
Использовать когда есть необходимость продолжить обрабатывать сообщения по \`DurableName\`.
Значение по умолчанию равно True.

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-ManualAcks'></a>
### ManualAcks `property`

##### Summary

Если значение равно True, то ACK произойдет после того как \`callback\` на подписку отработает.

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-MaxInflight'></a>
### MaxInflight `property`

##### Summary

Управляет количеством сообщений, которые кластер будет получать без ACK.

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-SerialAcks'></a>
### SerialAcks `property`

##### Summary

Если значение равно True, то сообщения будут браться из очереди последовательно, то есть пока не произойдет ACK на предыдущее сообщение следующее не возьмется.

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-ShouldDeliverAllAvailable'></a>
### ShouldDeliverAllAvailable `property`

##### Summary

Получить все доступные сообщения.

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-ShouldStartWithLastReceived'></a>
### ShouldStartWithLastReceived `property`

##### Summary

Если значение равно True, то подписка получит последнее сообщение в канале.

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-StartAtDuration'></a>
### StartAtDuration `property`

##### Summary

Если значение задано, то подписка получит все сообщения с заданной дельтой.

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-StartAtSequence'></a>
### StartAtSequence `property`

##### Summary

Если значение задано, то подписка получит все сообщения с заданого sequence-id.

<a name='P-Nats-Extensions-Helper-Options-StanExtensionsOptions-StartAtTime'></a>
### StartAtTime `property`

##### Summary

Если значение задано, то подписка получит все сообщения с заданного времени.

<a name='T-Nats-Extensions-Helper-Implementations-Stan-StanHelper'></a>
## StanHelper `type`

##### Namespace

Nats.Extensions.Helper.Implementations.Stan

<a name='M-Nats-Extensions-Helper-Implementations-Stan-StanHelper-CreateConsequentHandler-System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task},System-Threading-CancellationToken-'></a>
### CreateConsequentHandler(action,cancellationToken) `method`

##### Summary

Сообщения будут браться из очереди по приходу из канала, ACK произойдет после того как \`callback\` на подписку отработает.

##### Returns

Возвращается NSS-обработчик

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| action | [System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Func 'System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task}') | Обработчик сообщения. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

<a name='M-Nats-Extensions-Helper-Implementations-Stan-StanHelper-CreateInconsequentHandler-System-String,System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task},System-Threading-CancellationToken-'></a>
### CreateInconsequentHandler(subject,action,cancellationToken) `method`

##### Summary

Сообщения будут браться из очереди по приходу из канала и клясться в буфер, ACK произойдет как только сообщение попадет в буфер.

##### Returns

Возвращается NSS-обработчик

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') |  |
| action | [System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Func 'System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task}') | Обработчик сообщения. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

<a name='M-Nats-Extensions-Helper-Implementations-Stan-StanHelper-CreateSerialHandler-System-Func{Nats-Extensions-Helper-Models-Message,System-Threading-CancellationToken,System-Threading-Tasks-Task},System-Threading-CancellationToken-'></a>
### CreateSerialHandler(action,cancellationToken) `method`

##### Summary

Сообщения будут браться из очереди последовательно, то есть пока не произойдет ACK на предыдущее сообщение следующее не возьмется.

##### Returns

Возвращается NSS-обработчик

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| action | [System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Func 'System.Func{Nats.Extensions.Helper.Models.Message,System.Threading.CancellationToken,System.Threading.Tasks.Task}') | Обработчик сообщения. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

<a name='T-Nats-Extensions-Helper-Resources-ValidatorResource'></a>
## ValidatorResource `type`

##### Namespace

Nats.Extensions.Helper.Resources

##### Summary

A strongly-typed resource class, for looking up localized strings, etc.

<a name='P-Nats-Extensions-Helper-Resources-ValidatorResource-AnyServer'></a>
### AnyServer `property`

##### Summary

Looks up a localized string similar to Входной параметр \`{0}.{1}\` должен содержать хотя бы один адрес сервера..

<a name='P-Nats-Extensions-Helper-Resources-ValidatorResource-Culture'></a>
### Culture `property`

##### Summary

Overrides the current thread's CurrentUICulture property for all
  resource lookups using this strongly typed resource class.

<a name='P-Nats-Extensions-Helper-Resources-ValidatorResource-Empty'></a>
### Empty `property`

##### Summary

Looks up a localized string similar to Входной параметр \`{0}.{1}\` не может быть опеределн совместно с \`{2}\`..

<a name='P-Nats-Extensions-Helper-Resources-ValidatorResource-EmptyNatsUri'></a>
### EmptyNatsUri `property`

##### Summary

Looks up a localized string similar to Входной параметр строки подключения к NATS \`{0}.{1}\` не может быть опеределн совместно с \`{2}\`..

<a name='P-Nats-Extensions-Helper-Resources-ValidatorResource-GreaterThan'></a>
### GreaterThan `property`

##### Summary

Looks up a localized string similar to Входной параметр \`{0}.{1}\` должен бысть строго больше \`{2}\`..

<a name='P-Nats-Extensions-Helper-Resources-ValidatorResource-NotEmpty'></a>
### NotEmpty `property`

##### Summary

Looks up a localized string similar to Входной параметр \`{0}.{1}\` не может быть пустым или null..

<a name='P-Nats-Extensions-Helper-Resources-ValidatorResource-NotNull'></a>
### NotNull `property`

##### Summary

Looks up a localized string similar to Входной параметр \`{0}.{1}\` не может быть равным null..

<a name='P-Nats-Extensions-Helper-Resources-ValidatorResource-ResourceManager'></a>
### ResourceManager `property`

##### Summary

Returns the cached ResourceManager instance used by this class.

<a name='T-Nats-Extensions-Helper-Resources-WatcherResource'></a>
## WatcherResource `type`

##### Namespace

Nats.Extensions.Helper.Resources

##### Summary

A strongly-typed resource class, for looking up localized strings, etc.

<a name='P-Nats-Extensions-Helper-Resources-WatcherResource-ConnectionEstablishedError'></a>
### ConnectionEstablishedError `property`

##### Summary

Looks up a localized string similar to Фоновая задача \`{0}\` после установки соединения с NSS завершился с ошибкой..

<a name='P-Nats-Extensions-Helper-Resources-WatcherResource-ConnectionLostError'></a>
### ConnectionLostError `property`

##### Summary

Looks up a localized string similar to Фоновая задача \`{0}\` после разрыва соединения с NSS завершился с ошибкой..

<a name='P-Nats-Extensions-Helper-Resources-WatcherResource-Culture'></a>
### Culture `property`

##### Summary

Overrides the current thread's CurrentUICulture property for all
  resource lookups using this strongly typed resource class.

<a name='P-Nats-Extensions-Helper-Resources-WatcherResource-ResourceManager'></a>
### ResourceManager `property`

##### Summary

Returns the cached ResourceManager instance used by this class.
