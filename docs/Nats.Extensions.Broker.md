<a name='assembly'></a>
# Nats.Extensions.Broker

## Contents

- [AbstractRequestHandler\`2](#T-Nats-Extensions-Broker-Handlers-Base-AbstractRequestHandler`2 'Nats.Extensions.Broker.Handlers.Base.AbstractRequestHandler`2')
  - [HandleAsync(context,cancellationToken)](#M-Nats-Extensions-Broker-Handlers-Base-AbstractRequestHandler`2-HandleAsync-Nats-Extensions-Broker-Contexts-HandlerContext{`0},System-Threading-CancellationToken- 'Nats.Extensions.Broker.Handlers.Base.AbstractRequestHandler`2.HandleAsync(Nats.Extensions.Broker.Contexts.HandlerContext{`0},System.Threading.CancellationToken)')
  - [RecipientValidation(recipient,options)](#M-Nats-Extensions-Broker-Handlers-Base-AbstractRequestHandler`2-RecipientValidation-Nats-Extensions-Broker-Options-ContentOptions,Nats-Extensions-Broker-Models-RecipientModel- 'Nats.Extensions.Broker.Handlers.Base.AbstractRequestHandler`2.RecipientValidation(Nats.Extensions.Broker.Options.ContentOptions,Nats.Extensions.Broker.Models.RecipientModel)')
  - [ShouldValidateRecipient()](#M-Nats-Extensions-Broker-Handlers-Base-AbstractRequestHandler`2-ShouldValidateRecipient 'Nats.Extensions.Broker.Handlers.Base.AbstractRequestHandler`2.ShouldValidateRecipient')
- [AbstractSubjectHandler\`1](#T-Nats-Extensions-Broker-Handlers-Base-AbstractSubjectHandler`1 'Nats.Extensions.Broker.Handlers.Base.AbstractSubjectHandler`1')
  - [HandleAsync(context,cancellationToken)](#M-Nats-Extensions-Broker-Handlers-Base-AbstractSubjectHandler`1-HandleAsync-Nats-Extensions-Broker-Contexts-HandlerContext{`0},System-Threading-CancellationToken- 'Nats.Extensions.Broker.Handlers.Base.AbstractSubjectHandler`1.HandleAsync(Nats.Extensions.Broker.Contexts.HandlerContext{`0},System.Threading.CancellationToken)')
  - [RecipientValidation(recipient,options)](#M-Nats-Extensions-Broker-Handlers-Base-AbstractSubjectHandler`1-RecipientValidation-Nats-Extensions-Broker-Options-ContentOptions,Nats-Extensions-Broker-Models-RecipientModel- 'Nats.Extensions.Broker.Handlers.Base.AbstractSubjectHandler`1.RecipientValidation(Nats.Extensions.Broker.Options.ContentOptions,Nats.Extensions.Broker.Models.RecipientModel)')
  - [ShouldInvoke(context)](#M-Nats-Extensions-Broker-Handlers-Base-AbstractSubjectHandler`1-ShouldInvoke-Nats-Extensions-Broker-Contexts-HandlerContext{`0}- 'Nats.Extensions.Broker.Handlers.Base.AbstractSubjectHandler`1.ShouldInvoke(Nats.Extensions.Broker.Contexts.HandlerContext{`0})')
  - [ShouldValidateRecipient()](#M-Nats-Extensions-Broker-Handlers-Base-AbstractSubjectHandler`1-ShouldValidateRecipient 'Nats.Extensions.Broker.Handlers.Base.AbstractSubjectHandler`1.ShouldValidateRecipient')
- [CommonMessageTypes](#T-Nats-Extensions-Broker-Common-CommonMessageTypes 'Nats.Extensions.Broker.Common.CommonMessageTypes')
- [IHandlerFilter\`1](#T-Nats-Extensions-Broker-Filters-IHandlerFilter`1 'Nats.Extensions.Broker.Filters.IHandlerFilter`1')
  - [OnException(context,cancellationToken)](#M-Nats-Extensions-Broker-Filters-IHandlerFilter`1-OnException-Nats-Extensions-Broker-Contexts-ExceptionContext{`0},System-Threading-CancellationToken- 'Nats.Extensions.Broker.Filters.IHandlerFilter`1.OnException(Nats.Extensions.Broker.Contexts.ExceptionContext{`0},System.Threading.CancellationToken)')
  - [OnExecuted(context,cancellationToken)](#M-Nats-Extensions-Broker-Filters-IHandlerFilter`1-OnExecuted-Nats-Extensions-Broker-Contexts-HandlerContext{`0},System-Threading-CancellationToken- 'Nats.Extensions.Broker.Filters.IHandlerFilter`1.OnExecuted(Nats.Extensions.Broker.Contexts.HandlerContext{`0},System.Threading.CancellationToken)')
  - [OnExecuting(context,cancellationToken)](#M-Nats-Extensions-Broker-Filters-IHandlerFilter`1-OnExecuting-Nats-Extensions-Broker-Contexts-HandlerContext{`0},System-Threading-CancellationToken- 'Nats.Extensions.Broker.Filters.IHandlerFilter`1.OnExecuting(Nats.Extensions.Broker.Contexts.HandlerContext{`0},System.Threading.CancellationToken)')
- [IPublisher\`1](#T-Nats-Extensions-Broker-IPublisher`1 'Nats.Extensions.Broker.IPublisher`1')
  - [PublishAsync\`\`1(subject,content,cancellationToken)](#M-Nats-Extensions-Broker-IPublisher`1-PublishAsync``1-System-String,``0,System-Threading-CancellationToken- 'Nats.Extensions.Broker.IPublisher`1.PublishAsync``1(System.String,``0,System.Threading.CancellationToken)')
  - [PublishAsync\`\`1(subject,message,cancellationToken)](#M-Nats-Extensions-Broker-IPublisher`1-PublishAsync``1-System-String,Nats-Extensions-Broker-Models-BaseMessage{``0},System-Threading-CancellationToken- 'Nats.Extensions.Broker.IPublisher`1.PublishAsync``1(System.String,Nats.Extensions.Broker.Models.BaseMessage{``0},System.Threading.CancellationToken)')
- [IRequestService](#T-Nats-Extensions-Broker-IRequestService 'Nats.Extensions.Broker.IRequestService')
  - [RequestAsync\`\`2(subject,content,options,cancellationToken)](#M-Nats-Extensions-Broker-IRequestService-RequestAsync``2-System-String,``0,Nats-Extensions-Helper-Options-NatsRequestOptions,System-Threading-CancellationToken- 'Nats.Extensions.Broker.IRequestService.RequestAsync``2(System.String,``0,Nats.Extensions.Helper.Options.NatsRequestOptions,System.Threading.CancellationToken)')
  - [RequestAsync\`\`2(subject,message,options,cancellationToken)](#M-Nats-Extensions-Broker-IRequestService-RequestAsync``2-System-String,Nats-Extensions-Broker-Models-BaseMessage{``0},Nats-Extensions-Helper-Options-NatsRequestOptions,System-Threading-CancellationToken- 'Nats.Extensions.Broker.IRequestService.RequestAsync``2(System.String,Nats.Extensions.Broker.Models.BaseMessage{``0},Nats.Extensions.Helper.Options.NatsRequestOptions,System.Threading.CancellationToken)')
- [IRequestSubscriptionBuilder](#T-Nats-Extensions-Broker-Builders-Requests-IRequestSubscriptionBuilder 'Nats.Extensions.Broker.Builders.Requests.IRequestSubscriptionBuilder')
  - [SubscribeToSubject(subject,queue)](#M-Nats-Extensions-Broker-Builders-Requests-IRequestSubscriptionBuilder-SubscribeToSubject-System-String,System-String- 'Nats.Extensions.Broker.Builders.Requests.IRequestSubscriptionBuilder.SubscribeToSubject(System.String,System.String)')
  - [SubscribeToSubject\`\`3(subject,queue)](#M-Nats-Extensions-Broker-Builders-Requests-IRequestSubscriptionBuilder-SubscribeToSubject``3-System-String,System-String- 'Nats.Extensions.Broker.Builders.Requests.IRequestSubscriptionBuilder.SubscribeToSubject``3(System.String,System.String)')
  - [UnsubscribeFromSubject(subject,queue)](#M-Nats-Extensions-Broker-Builders-Requests-IRequestSubscriptionBuilder-UnsubscribeFromSubject-System-String,System-String- 'Nats.Extensions.Broker.Builders.Requests.IRequestSubscriptionBuilder.UnsubscribeFromSubject(System.String,System.String)')
  - [UnsubscribeFromSubject\`\`3(subject)](#M-Nats-Extensions-Broker-Builders-Requests-IRequestSubscriptionBuilder-UnsubscribeFromSubject``3-System-String- 'Nats.Extensions.Broker.Builders.Requests.IRequestSubscriptionBuilder.UnsubscribeFromSubject``3(System.String)')
- [ISubscriber\`1](#T-Nats-Extensions-Broker-ISubscriber`1 'Nats.Extensions.Broker.ISubscriber`1')
  - [Subscribe(subject,subscription,options)](#M-Nats-Extensions-Broker-ISubscriber`1-Subscribe-System-String,Nats-Extensions-Broker-Subscriptions-ISubscription,`0- 'Nats.Extensions.Broker.ISubscriber`1.Subscribe(System.String,Nats.Extensions.Broker.Subscriptions.ISubscription,`0)')
  - [Subscribe(subject,queue,subscription,options)](#M-Nats-Extensions-Broker-ISubscriber`1-Subscribe-System-String,System-String,Nats-Extensions-Broker-Subscriptions-ISubscription,`0- 'Nats.Extensions.Broker.ISubscriber`1.Subscribe(System.String,System.String,Nats.Extensions.Broker.Subscriptions.ISubscription,`0)')
- [ISubscriptionBuilder\`1](#T-Nats-Extensions-Broker-Builders-Subscriptions-ISubscriptionBuilder`1 'Nats.Extensions.Broker.Builders.Subscriptions.ISubscriptionBuilder`1')
  - [SubscribeToSubject(subject,queue,options)](#M-Nats-Extensions-Broker-Builders-Subscriptions-ISubscriptionBuilder`1-SubscribeToSubject-System-String,System-String,`0- 'Nats.Extensions.Broker.Builders.Subscriptions.ISubscriptionBuilder`1.SubscribeToSubject(System.String,System.String,`0)')
  - [SubscribeToSubject\`\`2(subject,queue,options)](#M-Nats-Extensions-Broker-Builders-Subscriptions-ISubscriptionBuilder`1-SubscribeToSubject``2-System-String,System-String,`0- 'Nats.Extensions.Broker.Builders.Subscriptions.ISubscriptionBuilder`1.SubscribeToSubject``2(System.String,System.String,`0)')
  - [UnsubscribeFromSubject(subject,queue,options)](#M-Nats-Extensions-Broker-Builders-Subscriptions-ISubscriptionBuilder`1-UnsubscribeFromSubject-System-String,System-String,`0- 'Nats.Extensions.Broker.Builders.Subscriptions.ISubscriptionBuilder`1.UnsubscribeFromSubject(System.String,System.String,`0)')
  - [UnsubscribeFromSubject\`\`2(subject)](#M-Nats-Extensions-Broker-Builders-Subscriptions-ISubscriptionBuilder`1-UnsubscribeFromSubject``2-System-String- 'Nats.Extensions.Broker.Builders.Subscriptions.ISubscriptionBuilder`1.UnsubscribeFromSubject``2(System.String)')
- [MessageTypeAttribute](#T-Nats-Extensions-Broker-Attributes-MessageTypeAttribute 'Nats.Extensions.Broker.Attributes.MessageTypeAttribute')
- [PublisherExtensions](#T-Nats-Extensions-Broker-Extensions-PublisherExtensions 'Nats.Extensions.Broker.Extensions.PublisherExtensions')
  - [AddRecipient(recipient)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-AddRecipient-System-ValueTuple{Nats-Extensions-Broker-IRequestService,Nats-Extensions-Broker-Models-BaseMessage},Nats-Extensions-Broker-Models-RecipientModel- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.AddRecipient(System.ValueTuple{Nats.Extensions.Broker.IRequestService,Nats.Extensions.Broker.Models.BaseMessage},Nats.Extensions.Broker.Models.RecipientModel)')
  - [AddRecipient\`\`1(recipient)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-AddRecipient``1-System-ValueTuple{Nats-Extensions-Broker-IPublisher{``0},Nats-Extensions-Broker-Models-BaseMessage},Nats-Extensions-Broker-Models-RecipientModel- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.AddRecipient``1(System.ValueTuple{Nats.Extensions.Broker.IPublisher{``0},Nats.Extensions.Broker.Models.BaseMessage},Nats.Extensions.Broker.Models.RecipientModel)')
  - [BasedOn(sagaId)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn-Nats-Extensions-Broker-IRequestService,System-Guid- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.BasedOn(Nats.Extensions.Broker.IRequestService,System.Guid)')
  - [BasedOn(service,sagaId,eventId)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn-Nats-Extensions-Broker-IRequestService,System-Guid,System-Guid- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.BasedOn(Nats.Extensions.Broker.IRequestService,System.Guid,System.Guid)')
  - [BasedOn(message)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn-Nats-Extensions-Broker-IRequestService,Nats-Extensions-Broker-Models-BaseMessage- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.BasedOn(Nats.Extensions.Broker.IRequestService,Nats.Extensions.Broker.Models.BaseMessage)')
  - [BasedOn(recipient)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn-Nats-Extensions-Broker-IRequestService,Nats-Extensions-Broker-Models-RecipientModel- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.BasedOn(Nats.Extensions.Broker.IRequestService,Nats.Extensions.Broker.Models.RecipientModel)')
  - [BasedOn\`\`1(sagaId)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn``1-Nats-Extensions-Broker-IPublisher{``0},System-Guid- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.BasedOn``1(Nats.Extensions.Broker.IPublisher{``0},System.Guid)')
  - [BasedOn\`\`1(sagaId,eventId)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn``1-Nats-Extensions-Broker-IPublisher{``0},System-Guid,System-Guid- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.BasedOn``1(Nats.Extensions.Broker.IPublisher{``0},System.Guid,System.Guid)')
  - [BasedOn\`\`1(message)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn``1-Nats-Extensions-Broker-IPublisher{``0},Nats-Extensions-Broker-Models-BaseMessage- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.BasedOn``1(Nats.Extensions.Broker.IPublisher{``0},Nats.Extensions.Broker.Models.BaseMessage)')
  - [BasedOn\`\`1(recipient)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn``1-Nats-Extensions-Broker-IPublisher{``0},Nats-Extensions-Broker-Models-RecipientModel- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.BasedOn``1(Nats.Extensions.Broker.IPublisher{``0},Nats.Extensions.Broker.Models.RecipientModel)')
  - [PublishAsync\`\`2(subject,content,cancellationToken)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-PublishAsync``2-System-ValueTuple{Nats-Extensions-Broker-IPublisher{``1},Nats-Extensions-Broker-Models-BaseMessage},System-String,``0,System-Threading-CancellationToken- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.PublishAsync``2(System.ValueTuple{Nats.Extensions.Broker.IPublisher{``1},Nats.Extensions.Broker.Models.BaseMessage},System.String,``0,System.Threading.CancellationToken)')
  - [RequestAsync\`\`2(subject,content,options,cancellationToken)](#M-Nats-Extensions-Broker-Extensions-PublisherExtensions-RequestAsync``2-System-ValueTuple{Nats-Extensions-Broker-IRequestService,Nats-Extensions-Broker-Models-BaseMessage},System-String,``0,Nats-Extensions-Helper-Options-NatsRequestOptions,System-Threading-CancellationToken- 'Nats.Extensions.Broker.Extensions.PublisherExtensions.RequestAsync``2(System.ValueTuple{Nats.Extensions.Broker.IRequestService,Nats.Extensions.Broker.Models.BaseMessage},System.String,``0,Nats.Extensions.Helper.Options.NatsRequestOptions,System.Threading.CancellationToken)')
- [RequestExtensions](#T-Nats-Extensions-Broker-Extensions-RequestExtensions 'Nats.Extensions.Broker.Extensions.RequestExtensions')
  - [EnsureSuccessResponse\`\`1()](#M-Nats-Extensions-Broker-Extensions-RequestExtensions-EnsureSuccessResponse``1-Nats-Extensions-Broker-Models-BaseResponse{``0}- 'Nats.Extensions.Broker.Extensions.RequestExtensions.EnsureSuccessResponse``1(Nats.Extensions.Broker.Models.BaseResponse{``0})')
- [ServiceExtensions](#T-Nats-Extensions-Broker-Extensions-ServiceExtensions 'Nats.Extensions.Broker.Extensions.ServiceExtensions')
  - [AddHandlersWithScopedLifetime(configure)](#M-Nats-Extensions-Broker-Extensions-ServiceExtensions-AddHandlersWithScopedLifetime-Microsoft-Extensions-DependencyInjection-IServiceCollection,System-Action{Nats-Extensions-Broker-Builders-Serializers-SerializersBuilder}- 'Nats.Extensions.Broker.Extensions.ServiceExtensions.AddHandlersWithScopedLifetime(Microsoft.Extensions.DependencyInjection.IServiceCollection,System.Action{Nats.Extensions.Broker.Builders.Serializers.SerializersBuilder})')
  - [AddHandlersWithSingletonLifetime(configure)](#M-Nats-Extensions-Broker-Extensions-ServiceExtensions-AddHandlersWithSingletonLifetime-Microsoft-Extensions-DependencyInjection-IServiceCollection,System-Action{Nats-Extensions-Broker-Builders-Serializers-SerializersBuilder}- 'Nats.Extensions.Broker.Extensions.ServiceExtensions.AddHandlersWithSingletonLifetime(Microsoft.Extensions.DependencyInjection.IServiceCollection,System.Action{Nats.Extensions.Broker.Builders.Serializers.SerializersBuilder})')
  - [AddNatsExtensionsBroker(services,configure)](#M-Nats-Extensions-Broker-Extensions-ServiceExtensions-AddNatsExtensionsBroker-Microsoft-Extensions-DependencyInjection-IServiceCollection,System-Action{Nats-Extensions-Broker-Builders-BrokerBuilder}- 'Nats.Extensions.Broker.Extensions.ServiceExtensions.AddNatsExtensionsBroker(Microsoft.Extensions.DependencyInjection.IServiceCollection,System.Action{Nats.Extensions.Broker.Builders.BrokerBuilder})')

<a name='T-Nats-Extensions-Broker-Handlers-Base-AbstractRequestHandler`2'></a>
## AbstractRequestHandler\`2 `type`

##### Namespace

Nats.Extensions.Broker.Handlers.Base

##### Summary

Базовый класс для обработчиков (Handler) сообщений с возможностью ответа на запрос.

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TRequest | Тип обрабатываемого сообщения. |
| TResponse | Тип отправляемого в ответ сообщения. |

<a name='M-Nats-Extensions-Broker-Handlers-Base-AbstractRequestHandler`2-HandleAsync-Nats-Extensions-Broker-Contexts-HandlerContext{`0},System-Threading-CancellationToken-'></a>
### HandleAsync(context,cancellationToken) `method`

##### Summary

Обрабатывает входящее сообщение.

##### Returns

TResponse отправляется в качестве ответа на Replay для NATS.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| context | [Nats.Extensions.Broker.Contexts.HandlerContext{\`0}](#T-Nats-Extensions-Broker-Contexts-HandlerContext{`0} 'Nats.Extensions.Broker.Contexts.HandlerContext{`0}') | Сообщение. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

<a name='M-Nats-Extensions-Broker-Handlers-Base-AbstractRequestHandler`2-RecipientValidation-Nats-Extensions-Broker-Options-ContentOptions,Nats-Extensions-Broker-Models-RecipientModel-'></a>
### RecipientValidation(recipient,options) `method`

##### Summary

Производит консолидацию принятого получателя с настройками машины по умолчанию согласно

##### Returns

Возвращает значение по которому будет принято решение об обработке принятого сообщения.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| recipient | [Nats.Extensions.Broker.Options.ContentOptions](#T-Nats-Extensions-Broker-Options-ContentOptions 'Nats.Extensions.Broker.Options.ContentOptions') | Получатель сообщения. |
| options | [Nats.Extensions.Broker.Models.RecipientModel](#T-Nats-Extensions-Broker-Models-RecipientModel 'Nats.Extensions.Broker.Models.RecipientModel') | Настройками машины по умолчанию. |

<a name='M-Nats-Extensions-Broker-Handlers-Base-AbstractRequestHandler`2-ShouldValidateRecipient'></a>
### ShouldValidateRecipient() `method`

##### Summary

При переопределении позволяет задать дополнительные условия для запуска метода \`HandleAsync\`.
По умолчанию производится проверка поле \`Recipient\`.

##### Returns

Возвращает значение по которому будет принято решение о запуске метода \`HandleAsync\`.

##### Parameters

This method has no parameters.

<a name='T-Nats-Extensions-Broker-Handlers-Base-AbstractSubjectHandler`1'></a>
## AbstractSubjectHandler\`1 `type`

##### Namespace

Nats.Extensions.Broker.Handlers.Base

##### Summary

Базовый класс для обработчиков (Handler) сообщений.

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TContent | Тип обрабатываемого сообщения. |

<a name='M-Nats-Extensions-Broker-Handlers-Base-AbstractSubjectHandler`1-HandleAsync-Nats-Extensions-Broker-Contexts-HandlerContext{`0},System-Threading-CancellationToken-'></a>
### HandleAsync(context,cancellationToken) `method`

##### Summary

Обрабатывает входящее сообщение.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| context | [Nats.Extensions.Broker.Contexts.HandlerContext{\`0}](#T-Nats-Extensions-Broker-Contexts-HandlerContext{`0} 'Nats.Extensions.Broker.Contexts.HandlerContext{`0}') | Контекст принятого сообщения. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

<a name='M-Nats-Extensions-Broker-Handlers-Base-AbstractSubjectHandler`1-RecipientValidation-Nats-Extensions-Broker-Options-ContentOptions,Nats-Extensions-Broker-Models-RecipientModel-'></a>
### RecipientValidation(recipient,options) `method`

##### Summary

Производит проверку принятого получателя с настройками машины по умолчанию согласно

##### Returns

Возвращает значение по которому будет принято решение об обработке принятого сообщения.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| recipient | [Nats.Extensions.Broker.Options.ContentOptions](#T-Nats-Extensions-Broker-Options-ContentOptions 'Nats.Extensions.Broker.Options.ContentOptions') | Получатель сообщения. |
| options | [Nats.Extensions.Broker.Models.RecipientModel](#T-Nats-Extensions-Broker-Models-RecipientModel 'Nats.Extensions.Broker.Models.RecipientModel') | Настройками машины по умолчанию. |

<a name='M-Nats-Extensions-Broker-Handlers-Base-AbstractSubjectHandler`1-ShouldInvoke-Nats-Extensions-Broker-Contexts-HandlerContext{`0}-'></a>
### ShouldInvoke(context) `method`

##### Summary

При переопределении позволяет задать дополнительные условия для запуска метода \`HandleAsync\`.
По умолчанию запуск происходит по типу сообщения \`BaseMessage.MessageType\`.

##### Returns

Возвращает значение по которому будет принято решение о запуске метода \`HandleAsync\`.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| context | [Nats.Extensions.Broker.Contexts.HandlerContext{\`0}](#T-Nats-Extensions-Broker-Contexts-HandlerContext{`0} 'Nats.Extensions.Broker.Contexts.HandlerContext{`0}') | Контекст принятого сообщения. |

<a name='M-Nats-Extensions-Broker-Handlers-Base-AbstractSubjectHandler`1-ShouldValidateRecipient'></a>
### ShouldValidateRecipient() `method`

##### Summary

При переопределении позволяет задать дополнительные условия для запуска метода \`HandleAsync\`.
По умолчанию производится проверка поле \`Recipient\`.

##### Returns

Возвращает значение по которому будет принято решение о запуске метода \`HandleAsync\`.

##### Parameters

This method has no parameters.

<a name='T-Nats-Extensions-Broker-Common-CommonMessageTypes'></a>
## CommonMessageTypes `type`

##### Namespace

Nats.Extensions.Broker.Common

##### Summary

Основные типы сообщений используемые платформой

<a name='T-Nats-Extensions-Broker-Filters-IHandlerFilter`1'></a>
## IHandlerFilter\`1 `type`

##### Namespace

Nats.Extensions.Broker.Filters

##### Summary

Фильтр, который может быть применен для метода \`HandleAsync\`.

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TContent | Тип обрабатываемого сообщения. |

<a name='M-Nats-Extensions-Broker-Filters-IHandlerFilter`1-OnException-Nats-Extensions-Broker-Contexts-ExceptionContext{`0},System-Threading-CancellationToken-'></a>
### OnException(context,cancellationToken) `method`

##### Summary

Метод, который будет вызван в случае возникновения \`Exception\` в методе \`HandleAsync\`.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| context | [Nats.Extensions.Broker.Contexts.ExceptionContext{\`0}](#T-Nats-Extensions-Broker-Contexts-ExceptionContext{`0} 'Nats.Extensions.Broker.Contexts.ExceptionContext{`0}') | Контекст обработки. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

<a name='M-Nats-Extensions-Broker-Filters-IHandlerFilter`1-OnExecuted-Nats-Extensions-Broker-Contexts-HandlerContext{`0},System-Threading-CancellationToken-'></a>
### OnExecuted(context,cancellationToken) `method`

##### Summary

Метод, который будет вызван при успешном завершении метода \`HandleAsync\`.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| context | [Nats.Extensions.Broker.Contexts.HandlerContext{\`0}](#T-Nats-Extensions-Broker-Contexts-HandlerContext{`0} 'Nats.Extensions.Broker.Contexts.HandlerContext{`0}') | Контекст обработки. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

<a name='M-Nats-Extensions-Broker-Filters-IHandlerFilter`1-OnExecuting-Nats-Extensions-Broker-Contexts-HandlerContext{`0},System-Threading-CancellationToken-'></a>
### OnExecuting(context,cancellationToken) `method`

##### Summary

Метод, который будет вызван перед вызовом метода \`HandleAsync\`.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| context | [Nats.Extensions.Broker.Contexts.HandlerContext{\`0}](#T-Nats-Extensions-Broker-Contexts-HandlerContext{`0} 'Nats.Extensions.Broker.Contexts.HandlerContext{`0}') | Контекст обработки. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

<a name='T-Nats-Extensions-Broker-IPublisher`1'></a>
## IPublisher\`1 `type`

##### Namespace

Nats.Extensions.Broker

##### Summary

Публикация (отправление) сообщений в очередь (NSS)

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TOptions | В зависимости от настроек сообщение будет отправлено в nats или stan |

<a name='M-Nats-Extensions-Broker-IPublisher`1-PublishAsync``1-System-String,``0,System-Threading-CancellationToken-'></a>
### PublishAsync\`\`1(subject,content,cancellationToken) `method`

##### Summary

Публикация (отправление) сообщений в очередь (NSS)

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Канал в который будет отправлено сообщение |
| content | [\`\`0](#T-``0 '``0') | Сообщение |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') |  |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TContent | Тип сообщения, должен совпадать с типом который обрабатывает обработчик |

<a name='M-Nats-Extensions-Broker-IPublisher`1-PublishAsync``1-System-String,Nats-Extensions-Broker-Models-BaseMessage{``0},System-Threading-CancellationToken-'></a>
### PublishAsync\`\`1(subject,message,cancellationToken) `method`

##### Summary

Публикация (отправление) сообщений в очередь (NSS)

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Канал в который будет отправлено сообщение |
| message | [Nats.Extensions.Broker.Models.BaseMessage{\`\`0}](#T-Nats-Extensions-Broker-Models-BaseMessage{``0} 'Nats.Extensions.Broker.Models.BaseMessage{``0}') | Сообщение |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') |  |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TContent | Тип сообщения |

<a name='T-Nats-Extensions-Broker-IRequestService'></a>
## IRequestService `type`

##### Namespace

Nats.Extensions.Broker

<a name='M-Nats-Extensions-Broker-IRequestService-RequestAsync``2-System-String,``0,Nats-Extensions-Helper-Options-NatsRequestOptions,System-Threading-CancellationToken-'></a>
### RequestAsync\`\`2(subject,content,options,cancellationToken) `method`

##### Summary

Публикация сообщения в шину (NSS) с ответом от обработчика.

##### Returns

Возвращает сообщение, которое было получено от обработчика.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Канал в который будет опубликовано сообщение. |
| content | [\`\`0](#T-``0 '``0') | Содержимое сообщения. |
| options | [Nats.Extensions.Helper.Options.NatsRequestOptions](#T-Nats-Extensions-Helper-Options-NatsRequestOptions 'Nats.Extensions.Helper.Options.NatsRequestOptions') | Настройки, который могут быть переданны во время публикации сообщения. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TResponse | Тип ожидаемого от обработчика сообщения. |
| TRequest | Тип содержимого сообщения. |

<a name='M-Nats-Extensions-Broker-IRequestService-RequestAsync``2-System-String,Nats-Extensions-Broker-Models-BaseMessage{``0},Nats-Extensions-Helper-Options-NatsRequestOptions,System-Threading-CancellationToken-'></a>
### RequestAsync\`\`2(subject,message,options,cancellationToken) `method`

##### Summary

Публикация сообщения в шину (NSS) с ответом от обработчика.

##### Returns

Возвращает сообщение, которое было получено от обработчика.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Канал в который будет опубликовано сообщение. |
| message | [Nats.Extensions.Broker.Models.BaseMessage{\`\`0}](#T-Nats-Extensions-Broker-Models-BaseMessage{``0} 'Nats.Extensions.Broker.Models.BaseMessage{``0}') | Сообщение, которое будет опубликовано в шину (NSS). |
| options | [Nats.Extensions.Helper.Options.NatsRequestOptions](#T-Nats-Extensions-Helper-Options-NatsRequestOptions 'Nats.Extensions.Helper.Options.NatsRequestOptions') | Настройки, который могут быть переданны во время публикации сообщения. |
| cancellationToken | [System.Threading.CancellationToken](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Threading.CancellationToken 'System.Threading.CancellationToken') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TResponse | Тип ожидаемого от обработчика сообщения. |
| TRequest | Тип содержимого сообщения. |

<a name='T-Nats-Extensions-Broker-Builders-Requests-IRequestSubscriptionBuilder'></a>
## IRequestSubscriptionBuilder `type`

##### Namespace

Nats.Extensions.Broker.Builders.Requests

<a name='M-Nats-Extensions-Broker-Builders-Requests-IRequestSubscriptionBuilder-SubscribeToSubject-System-String,System-String-'></a>
### SubscribeToSubject(subject,queue) `method`

##### Summary

Подписка на канал с несколькими обработчиками отличные по типу сообщения.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| queue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование очереди. Опционально. |

<a name='M-Nats-Extensions-Broker-Builders-Requests-IRequestSubscriptionBuilder-SubscribeToSubject``3-System-String,System-String-'></a>
### SubscribeToSubject\`\`3(subject,queue) `method`

##### Summary

Подписка на канал в шине (NSS) с возможностью ответа на запрос.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| queue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование очереди. Опционально. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TRequest | Тип принимаего сообщения. |
| TResponse | Тип отправляемого сообщения. |
| THandler | Тип обработчика, который будет обрабатывать сообщение. |

<a name='M-Nats-Extensions-Broker-Builders-Requests-IRequestSubscriptionBuilder-UnsubscribeFromSubject-System-String,System-String-'></a>
### UnsubscribeFromSubject(subject,queue) `method`

##### Summary

Удаляет указанные обработчики из канала. Если все обработчики были удалены, то происходит отписка от канала.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| queue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование очереди. Опционально. |

<a name='M-Nats-Extensions-Broker-Builders-Requests-IRequestSubscriptionBuilder-UnsubscribeFromSubject``3-System-String-'></a>
### UnsubscribeFromSubject\`\`3(subject) `method`

##### Summary

Отписка от канала с одним строго типизированным обработчиком с возможностью  ответа на запрос.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TRequest | Тип принимаего сообщения. |
| TResponse | Тип отправляемого сообщения. |
| THandler | Тип обработчика, который будет обрабатывать сообщение. |

<a name='T-Nats-Extensions-Broker-ISubscriber`1'></a>
## ISubscriber\`1 `type`

##### Namespace

Nats.Extensions.Broker

##### Summary

Подписка на канал в шине (NSS).

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TOptions | В зависимости от настроек будет подписка в nats или stan. |

<a name='M-Nats-Extensions-Broker-ISubscriber`1-Subscribe-System-String,Nats-Extensions-Broker-Subscriptions-ISubscription,`0-'></a>
### Subscribe(subject,subscription,options) `method`

##### Summary

Подписка на канал в шине (NSS).

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| subscription | [Nats.Extensions.Broker.Subscriptions.ISubscription](#T-Nats-Extensions-Broker-Subscriptions-ISubscription 'Nats.Extensions.Broker.Subscriptions.ISubscription') | Обработчик, который будет обрабатывать сообщение из канала. |
| options | [\`0](#T-`0 '`0') | Настройки, который могут быть переданны во время подписки. |

<a name='M-Nats-Extensions-Broker-ISubscriber`1-Subscribe-System-String,System-String,Nats-Extensions-Broker-Subscriptions-ISubscription,`0-'></a>
### Subscribe(subject,queue,subscription,options) `method`

##### Summary

Подписка на канал в шине (NSS) c очередью.

##### Returns

Возвращает IDisposable. Вызова Dispose() отменит подписку на канал.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| queue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование очереди. |
| subscription | [Nats.Extensions.Broker.Subscriptions.ISubscription](#T-Nats-Extensions-Broker-Subscriptions-ISubscription 'Nats.Extensions.Broker.Subscriptions.ISubscription') | Обработчик, который будет обрабатывать сообщение из канала. |
| options | [\`0](#T-`0 '`0') | Настройки, который могут быть переданны во время подписки. |

<a name='T-Nats-Extensions-Broker-Builders-Subscriptions-ISubscriptionBuilder`1'></a>
## ISubscriptionBuilder\`1 `type`

##### Namespace

Nats.Extensions.Broker.Builders.Subscriptions

<a name='M-Nats-Extensions-Broker-Builders-Subscriptions-ISubscriptionBuilder`1-SubscribeToSubject-System-String,System-String,`0-'></a>
### SubscribeToSubject(subject,queue,options) `method`

##### Summary

Подписка на канал с несколькими обработчиками отличные по типу сообщения.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| queue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование очереди. Опционально. |
| options | [\`0](#T-`0 '`0') | Настройки, который могут быть переданны во время подписки. |

<a name='M-Nats-Extensions-Broker-Builders-Subscriptions-ISubscriptionBuilder`1-SubscribeToSubject``2-System-String,System-String,`0-'></a>
### SubscribeToSubject\`\`2(subject,queue,options) `method`

##### Summary

Подписка на канал с одним строго типизированным обработчиком.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| queue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование очереди. Опционально. |
| options | [\`0](#T-`0 '`0') | Настройки, который могут быть переданны во время подписки. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TContent | Тип сообщения, который ожидается в канале. |
| THandler | Тип обработчкиа, который будет обрабатывать сообщение. |

<a name='M-Nats-Extensions-Broker-Builders-Subscriptions-ISubscriptionBuilder`1-UnsubscribeFromSubject-System-String,System-String,`0-'></a>
### UnsubscribeFromSubject(subject,queue,options) `method`

##### Summary

Удаляет указанные обработчики из канала. Если все обработчики были удалены, то происходит отписка от канала.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |
| queue | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование очереди. Опционально. |
| options | [\`0](#T-`0 '`0') | Настройки, который могут быть переданны во время подписки. |

<a name='M-Nats-Extensions-Broker-Builders-Subscriptions-ISubscriptionBuilder`1-UnsubscribeFromSubject``2-System-String-'></a>
### UnsubscribeFromSubject\`\`2(subject) `method`

##### Summary

Отписка от канала с одним строго типизированным обработчиком.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Наименование канала. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TContent | Тип сообщения, который ожидается в канале. |
| THandler | Тип обработчкиа, который будет обрабатывать сообщение. |

<a name='T-Nats-Extensions-Broker-Attributes-MessageTypeAttribute'></a>
## MessageTypeAttribute `type`

##### Namespace

Nats.Extensions.Broker.Attributes

##### Summary

Атрибут указывающий тип сообщения. Является необходимой частью модели.

<a name='T-Nats-Extensions-Broker-Extensions-PublisherExtensions'></a>
## PublisherExtensions `type`

##### Namespace

Nats.Extensions.Broker.Extensions

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-AddRecipient-System-ValueTuple{Nats-Extensions-Broker-IRequestService,Nats-Extensions-Broker-Models-BaseMessage},Nats-Extensions-Broker-Models-RecipientModel-'></a>
### AddRecipient(recipient) `method`

##### Summary

Добавляет поле Recipient в уже существующее сообщение

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| recipient | [System.ValueTuple{Nats.Extensions.Broker.IRequestService,Nats.Extensions.Broker.Models.BaseMessage}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.ValueTuple 'System.ValueTuple{Nats.Extensions.Broker.IRequestService,Nats.Extensions.Broker.Models.BaseMessage}') | Модель данных получателя |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-AddRecipient``1-System-ValueTuple{Nats-Extensions-Broker-IPublisher{``0},Nats-Extensions-Broker-Models-BaseMessage},Nats-Extensions-Broker-Models-RecipientModel-'></a>
### AddRecipient\`\`1(recipient) `method`

##### Summary

Добавляет поле Recipient в уже существующее сообщение

##### Returns

Сообщение чьи параметры будут привязаны к переданному сообщению

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| recipient | [System.ValueTuple{Nats.Extensions.Broker.IPublisher{\`\`0},Nats.Extensions.Broker.Models.BaseMessage}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.ValueTuple 'System.ValueTuple{Nats.Extensions.Broker.IPublisher{``0},Nats.Extensions.Broker.Models.BaseMessage}') | Модель данных получателя |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TOptions | В зависимости от настроек сообщение будет отправлено в nats или stan. |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn-Nats-Extensions-Broker-IRequestService,System-Guid-'></a>
### BasedOn(sagaId) `method`

##### Summary

Устанавливает sagaId для публикуемого сообщения.

##### Returns

Сообщение чьи параметры будут привязаны к новому сообщению

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| sagaId | [Nats.Extensions.Broker.IRequestService](#T-Nats-Extensions-Broker-IRequestService 'Nats.Extensions.Broker.IRequestService') | Saga-Id |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn-Nats-Extensions-Broker-IRequestService,System-Guid,System-Guid-'></a>
### BasedOn(service,sagaId,eventId) `method`

##### Summary

Устанавливает sagaId и event_id для публикуемого сообщения.

##### Returns

Сообщение чьи параметры будут привязаны к новому сообщению

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| service | [Nats.Extensions.Broker.IRequestService](#T-Nats-Extensions-Broker-IRequestService 'Nats.Extensions.Broker.IRequestService') |  |
| sagaId | [System.Guid](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Guid 'System.Guid') | Saga-Id |
| eventId | [System.Guid](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Guid 'System.Guid') | Event-Id |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn-Nats-Extensions-Broker-IRequestService,Nats-Extensions-Broker-Models-BaseMessage-'></a>
### BasedOn(message) `method`

##### Summary

Связывает публикуемое сообщение с сообщением который указан в параметрах.
Выставляет sagaId и event_prev в соответствии с сообщением что в параметрах

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| message | [Nats.Extensions.Broker.IRequestService](#T-Nats-Extensions-Broker-IRequestService 'Nats.Extensions.Broker.IRequestService') |  |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn-Nats-Extensions-Broker-IRequestService,Nats-Extensions-Broker-Models-RecipientModel-'></a>
### BasedOn(recipient) `method`

##### Summary

Создает новое сообщение на основе модели Recipient

##### Returns

Сообщение чьи параметры будут привязаны к новому сообщению

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| recipient | [Nats.Extensions.Broker.IRequestService](#T-Nats-Extensions-Broker-IRequestService 'Nats.Extensions.Broker.IRequestService') | Модель данных получателя |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn``1-Nats-Extensions-Broker-IPublisher{``0},System-Guid-'></a>
### BasedOn\`\`1(sagaId) `method`

##### Summary

Устанавливает sagaId для публикуемого сообщения.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| sagaId | [Nats.Extensions.Broker.IPublisher{\`\`0}](#T-Nats-Extensions-Broker-IPublisher{``0} 'Nats.Extensions.Broker.IPublisher{``0}') | Saga-Id |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TOptions | В зависимости от настроек сообщение будет отправлено в nats или stan. |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn``1-Nats-Extensions-Broker-IPublisher{``0},System-Guid,System-Guid-'></a>
### BasedOn\`\`1(sagaId,eventId) `method`

##### Summary

Устанавливает sagaId и event_id для публикуемого сообщения.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| sagaId | [Nats.Extensions.Broker.IPublisher{\`\`0}](#T-Nats-Extensions-Broker-IPublisher{``0} 'Nats.Extensions.Broker.IPublisher{``0}') | Saga-Id |
| eventId | [System.Guid](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Guid 'System.Guid') | Event-Id |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TOptions | В зависимости от настроек сообщение будет отправлено в nats или stan. |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn``1-Nats-Extensions-Broker-IPublisher{``0},Nats-Extensions-Broker-Models-BaseMessage-'></a>
### BasedOn\`\`1(message) `method`

##### Summary

Связывает публикуемое сообщение с сообщением который указан в параметрах.
Выставляет sagaId и event_prev в соответствии с сообщением что в параметрах

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| message | [Nats.Extensions.Broker.IPublisher{\`\`0}](#T-Nats-Extensions-Broker-IPublisher{``0} 'Nats.Extensions.Broker.IPublisher{``0}') | Сообщение чьи параметры будут привязаны к новому сообщению. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TOptions | В зависимости от настроек сообщение будет отправлено в nats или stan. |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-BasedOn``1-Nats-Extensions-Broker-IPublisher{``0},Nats-Extensions-Broker-Models-RecipientModel-'></a>
### BasedOn\`\`1(recipient) `method`

##### Summary

Создаёт новое сообщение на основе Recipient

##### Returns

Сообщение чьи параметры будут привязаны к новому сообщению

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| recipient | [Nats.Extensions.Broker.IPublisher{\`\`0}](#T-Nats-Extensions-Broker-IPublisher{``0} 'Nats.Extensions.Broker.IPublisher{``0}') | Модель данных получателя |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TOptions | В зависимости от настроек сообщение будет отправлено в nats или stan. |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-PublishAsync``2-System-ValueTuple{Nats-Extensions-Broker-IPublisher{``1},Nats-Extensions-Broker-Models-BaseMessage},System-String,``0,System-Threading-CancellationToken-'></a>
### PublishAsync\`\`2(subject,content,cancellationToken) `method`

##### Summary

Публикация (отправление) сообщений в очередь (NSS)

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.ValueTuple{Nats.Extensions.Broker.IPublisher{\`\`1},Nats.Extensions.Broker.Models.BaseMessage}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.ValueTuple 'System.ValueTuple{Nats.Extensions.Broker.IPublisher{``1},Nats.Extensions.Broker.Models.BaseMessage}') | Канал в который будет отправлено сообщение |
| content | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Сообщение |
| cancellationToken | [\`\`0](#T-``0 '``0') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TOptions | В зависимости от настроек сообщение будет отправлено в nats или stan |
| TContent | Тип сообщения, должен совпадать с типом который обрабатывает обработчик |

<a name='M-Nats-Extensions-Broker-Extensions-PublisherExtensions-RequestAsync``2-System-ValueTuple{Nats-Extensions-Broker-IRequestService,Nats-Extensions-Broker-Models-BaseMessage},System-String,``0,Nats-Extensions-Helper-Options-NatsRequestOptions,System-Threading-CancellationToken-'></a>
### RequestAsync\`\`2(subject,content,options,cancellationToken) `method`

##### Summary

Публикация сообщения в шину (NSS) с ответом от обработчика.

##### Returns

Возвращает сообщение, которое было получено от обработчика.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| subject | [System.ValueTuple{Nats.Extensions.Broker.IRequestService,Nats.Extensions.Broker.Models.BaseMessage}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.ValueTuple 'System.ValueTuple{Nats.Extensions.Broker.IRequestService,Nats.Extensions.Broker.Models.BaseMessage}') | Канал в который будет опубликовано сообщение. |
| content | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | Содержимое сообщения. |
| options | [\`\`0](#T-``0 '``0') | Настройки, который могут быть переданны во время публикации сообщения. |
| cancellationToken | [Nats.Extensions.Helper.Options.NatsRequestOptions](#T-Nats-Extensions-Helper-Options-NatsRequestOptions 'Nats.Extensions.Helper.Options.NatsRequestOptions') | Токен отмены, который может быть использован другими потоками или объектами для получения уведомления об отмене. |

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TResponse | Тип ожидаемого от обработчика сообщения. |
| TRequest | Тип содержимого сообщения. |

<a name='T-Nats-Extensions-Broker-Extensions-RequestExtensions'></a>
## RequestExtensions `type`

##### Namespace

Nats.Extensions.Broker.Extensions

<a name='M-Nats-Extensions-Broker-Extensions-RequestExtensions-EnsureSuccessResponse``1-Nats-Extensions-Broker-Models-BaseResponse{``0}-'></a>
### EnsureSuccessResponse\`\`1() `method`

##### Summary

Проверяет ответ на наличие ошибки, если она есть то генерируется исключение в соответствии с пришедшей ошибкой.

##### Parameters

This method has no parameters.

##### Generic Types

| Name | Description |
| ---- | ----------- |
| TResponse | Тип ожидаемого ответа. |

<a name='T-Nats-Extensions-Broker-Extensions-ServiceExtensions'></a>
## ServiceExtensions `type`

##### Namespace

Nats.Extensions.Broker.Extensions

<a name='M-Nats-Extensions-Broker-Extensions-ServiceExtensions-AddHandlersWithScopedLifetime-Microsoft-Extensions-DependencyInjection-IServiceCollection,System-Action{Nats-Extensions-Broker-Builders-Serializers-SerializersBuilder}-'></a>
### AddHandlersWithScopedLifetime(configure) `method`

##### Summary

Регистрация обработчиков со \`ScopedLifetime\`.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [Microsoft.Extensions.DependencyInjection.IServiceCollection](#T-Microsoft-Extensions-DependencyInjection-IServiceCollection 'Microsoft.Extensions.DependencyInjection.IServiceCollection') | Конфигурация сериализатора. По умолчанию используется Newtonsoft. |

<a name='M-Nats-Extensions-Broker-Extensions-ServiceExtensions-AddHandlersWithSingletonLifetime-Microsoft-Extensions-DependencyInjection-IServiceCollection,System-Action{Nats-Extensions-Broker-Builders-Serializers-SerializersBuilder}-'></a>
### AddHandlersWithSingletonLifetime(configure) `method`

##### Summary

Регистрация обработчиков со \`ScopedLifetime\`.

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| configure | [Microsoft.Extensions.DependencyInjection.IServiceCollection](#T-Microsoft-Extensions-DependencyInjection-IServiceCollection 'Microsoft.Extensions.DependencyInjection.IServiceCollection') | Конфигурация сериализатора. По умолчанию используется Newtonsoft. |

<a name='M-Nats-Extensions-Broker-Extensions-ServiceExtensions-AddNatsExtensionsBroker-Microsoft-Extensions-DependencyInjection-IServiceCollection,System-Action{Nats-Extensions-Broker-Builders-BrokerBuilder}-'></a>
### AddNatsExtensionsBroker(services,configure) `method`

##### Summary

Регистрация настроек подключений к stan или nats

##### Returns



##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| services | [Microsoft.Extensions.DependencyInjection.IServiceCollection](#T-Microsoft-Extensions-DependencyInjection-IServiceCollection 'Microsoft.Extensions.DependencyInjection.IServiceCollection') |  |
| configure | [System.Action{Nats.Extensions.Broker.Builders.BrokerBuilder}](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Action 'System.Action{Nats.Extensions.Broker.Builders.BrokerBuilder}') | Конфигурация подключения |
